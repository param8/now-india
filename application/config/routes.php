<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Authantication';
$route['404_override'] = '';
// Web Detail

// Admin Details
$route['signin'] = 'Authantication';
$route['dashboard'] = 'dashboard';
$route['setting'] = 'setting';
$route['role'] = 'Setting/role';

$route['admin'] = 'admin';
$route['create-admin'] = 'admin/create';
$route['admin-site-setting/(:any)'] = 'admin/siteSetting/$1';
$route['edit-admin/(:any)'] = 'admin/edit/$1';
$route['view-admin/(:any)'] = 'admin/view/$1';

$route['users'] = 'User';
$route['create-user'] = 'User/create';
$route['edit-user/(:any)'] = 'User/edit/$1';
$route['view-user/(:any)'] = 'User/view/$1';
$route['profile'] = 'User/profile';
$route['change-password'] = 'User/change_password';

$route['customers'] = 'Customer';
$route['create-customer'] = 'Customer/create';
$route['edit-customer/(:any)'] = 'Customer/edit/$1';

$route['enquires'] = 'Enquiry';
$route['re-enquires'] = 'Enquiry/re_enquires';
$route['enquiry-generated'] = 'Enquiry/enquiry_generated';

$route['followup'] = 'Followup';
$route['create-followup/(:any)'] = 'Followup/create/$1';
$route['followup-report'] = 'Followup/followup_report';
$route['followup-status/(:any)'] = 'Followup/followup_status/$1';

$route['sales'] = 'Sale';
$route['create-sale/(:any)'] = 'Sale/create/$1';
$route['edit-sale/(:any)'] = 'Sale/edit/$1';
$route['payment-history'] = 'Sale/payment_history';

$route['services'] = 'Services';
$route['create-service/(:any)/(:any)'] = 'Services/create/$1/$2';
$route['edit-service/(:any)'] = 'Services/edit/$1';
$route['services-renewal'] = 'Services/service_renewal';
$route['renewal-services/(:any)'] = 'Services/create_renewal/$1';


$route['task'] = 'Task';
$route['create-task/(:any)'] = 'Task/create/$1';
$route['edit-task/(:any)/(:any)'] = 'Task/edit/$1/$2';
$route['view-task/(:any)'] = 'Task/view/$1';
$route['task-report/(:any)'] = 'Task/task_report/$1';

$route['general-setting'] = 'Setting';
$route['Store-general-form'] = 'Setting/store_siteInfo';

$route['email-setting'] = 'Setting/email_setting';