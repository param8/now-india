<?php 
class Ajax_controller extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
      $this->load->model('user_model');
      $this->load->model('customer_model');
      $this->load->model('setting_model');
      $this->load->model('auth_model');

	}

	public function get_city()
    {
      $userID = "";
      $customerID = "";
      $userID = $this->input->post('userID');
      $customerID = $this->input->post('customerID');
      $stateID =  $this->input->post('stateID');
      if(!empty($userID)){
       $user = $this->user_model->get_user(array('users.id' => $userID));
      }
      
      if(!empty($customerID)){
        $user = $this->customer_model->get_customer(array('customers.id' => $customerID));
       }
       
       $cities = $this->Common_model->get_city(array('state_id'=>$stateID));
       if(count($cities) > 0)
       {
        ?>
        <option value="">Select City</option>
        <?php foreach($cities as $city){ ?>
           <option value="<?=$city->id?>" <?=$user->city == $city->id ? 'selected' : '' ;?>><?=$city->name?></option>
        <?php
        }
       }else
       {
        ?>
        <option value="">No City found</option>
        <?php
       }
    }

    public function customer_detail(){
      $phone = $this->input->post('phone');
      if(!empty($phone)){
       $condition = array('customers.phone'=>$phone,'customers.adminID'=>$this->session->userdata('adminID'));
      $customers = $this->customer_model->get_customers($condition,'Like');
    
      if(count($customers)>0){
         foreach($customers as $customer ){
            ?>
         <p><a href="javascript:void(0)" onclick="get_customer_data('<?=$customer->phone?>')"><?=$customer->phone.'('.$customer->name.')'?></a></p>
       
         <?php
         }
      }else{
         echo false;
      }
   }

   }

   public function get_customer_field(){
    $contact = $this->input->post('contact');
    $customer = $this->customer_model->get_customer(array('customers.phone'=>$contact));
    echo json_encode($customer);

 }

   public function getroleAdminSession(){
      $id = $this->input->post('id');  
      $this->session->set_userdata('roleAdminID',$id);
     }

     public function getcustomerAdminSession(){
      $id = $this->input->post('id');  
      $this->session->set_userdata('customerAdminID',$id);
     }

     public function getEnquiryAdminSession(){
      $id = $this->input->post('id');  
      $this->session->set_userdata('enquiryAdminID',$id);
     }

     public function getFollowupAdminSession(){
      $id = $this->input->post('id');  
      $this->session->set_userdata('followupAdminID',$id);
     }

     public function getSaleAdminSession(){
      $id = $this->input->post('id');  
      $this->session->set_userdata('saleAdminID',$id);
     }

     public function getPaymentHistoryAdminSession(){
      $id = $this->input->post('id');  
      $this->session->set_userdata('paymentHistoryAdminID',$id);
     }

     public function getServiceAdminSession(){
      $id = $this->input->post('id');  
      $this->session->set_userdata('serviceAdminID',$id);
     }

    public function reset_discount(){
      $this->session->unset_userdata('course_id');
      $this->session->unset_userdata('addPrice');
      $this->session->unset_userdata('gst');
      $this->session->unset_userdata('total_price');
      $this->session->unset_userdata('discount');
      $this->session->unset_userdata('course_price');
      $this->session->unset_userdata('coupon_code');
    }

    public function get_users(){
      $roleID = $this->input->post('id');
      $assign_to = explode(',',$this->input->post('assign_to'));
      $roleIDS = array(7,8);
      $roleIDs = $roleID == 0 ? $roleIDS  : array($roleID);

      //$this->db->where_in('user_type',$roleIDs);
      $users = $this->db->where_in('user_type',$roleIDs)->get('users')->result();
      if(count($users) > 0){
      ?>
      <option value="">Select User</option>
     <?php
         foreach($users as $user){
      ?>
       <option value="<?=$user->id?>" <?=in_array($user->id,$assign_to) ? 'selected' : ''?>><?=$user->name?></option>
      <?php
         }
    }

    }

    

  

  }
      

   
