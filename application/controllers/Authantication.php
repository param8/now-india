<?php 
class Authantication extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Auth_model');
		$this->load->model('User_model');
        $this->load->model('setting_model');
	}


	public function index()
	{	
		$data['page_title'] = 'Login';
		$this->login_template('signin',$data);
		// $this->load->view('layout/login_head',$data);
	}



	public function login(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$recaptcha = $this->input->post('g-recaptcha-response');
		$secret = "6LcNxXYmAAAAAAQu6QkqSTN1DQC8YimXPp7M7krM";
		if(empty($email)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your email or contact']); 	
			exit();
		}
		if(empty($password)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your password']); 	
			exit();
		}
		// if($this->input->post('login_by')=='User'){
		// 	if(empty($recaptcha)){
		// 		echo json_encode(['status'=>403, 'message'=>'Please verify recaptcha']); 	
		// 		exit();
		// 	}
		// 	$responceKey = verify_reCaptcha($secret,$recaptcha);
		// 	if(!$responceKey['success']){
		// 		echo json_encode(['status'=>403, 'message'=>'Recaptcha Failed - Please verify recaptcha']); 	
		// 		exit();
		// 	}
	  // }
		$login = $this->Auth_model->login($email,$password);
		if($login==403){
			echo json_encode(['status'=>403, 'message'=>'email or password incorrect!']);
		}elseif($login==302){
			echo json_encode(['status'=>302, 'message'=>'Your are not active please contact the administrator']);   
		}else{
			echo json_encode(['status'=>200, 'message'=>'Login Successfully','user_type'=>$this->session->userdata('user_type'),'verifivation_status'=>$this->session->userdata('verification_status')]);
			// redirect(base_url('dashboard'), 'refresh');
    }
	}


	public function create_user(){
    $data['page_title'] = 'New User';
		$this->admin_template('users/create',$data);
	}

	public function register(){
		if($this->session->userdata('email')){
			redirect(base_url('home'));
		}
    $data['page_title'] = 'Registration';
		$data['states'] = $this->Common_model->get_states(array('country_id'=>101));
		$this->template('web/register',$data);
	}


	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url(), 'refresh');
	}

//   Admin Cradinciales


public function adminLogin(){
	//$this->logged_in();
	 $email = $this->input->post('username');
	 $password = $this->input->post('password');
	 $type = 'Admin';
	if(empty($email)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your email address']); 	
		exit();
	}
	if(empty($password)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your password']); 	
		exit();
	}
	$login = $this->Auth_model->login($email,$password);

	if($login){
		echo json_encode(['status'=>200, 'message'=>'Login successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'Invalid username or password!']);   
	}
}

public function adminLogout()
{
	$this->session->sess_destroy();
	redirect(base_url(), 'refresh');
}


	
}