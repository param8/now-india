<?php 
class Cronjob extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
      $this->load->model('user_model');
      $this->load->model('customer_model');
      $this->load->model('setting_model');
      $this->load->model('followup_model');
      $this->load->model('auth_model');
      $this->load->model('service_model');

	}




   function send_mail_users($userID){
    
      $adminID = 3;
      //print_r($userID);die;
      $this->db->where(array('adminID'=>$adminID ,'user_type'=>2));
      $admin =  $this->db->get('users')->row();

      $this->db->where(array('adminID'=>$adminID,'id'=>$userID));
      $user =  $this->db->get('users')->row();
      $user_type = $user->user_type;
  
      $roles =  $this->db->query("WITH RECURSIVE EmpCTE AS (
        Select id,name,parent_role,adminID
        From role 
        Where id = $user_type AND adminID = $adminID
        UNION ALL
        Select role.id ,role.name,role.parent_role,role.adminID
        From role
        JOIN EmpCTE
        ON role.id = EmpCTE.parent_role WHERE role.adminID = $adminID
        )
        Select * From EmpCTE")->result();
      $roleID = array();
      
      foreach($roles as $role){
        if($role->id != $user_type){
          $roleID[] =$role->id;
        }
        
      }
      $roleID = array_merge($roleID,[2]); 
      $this->db->where('adminID',$adminID);
      $this->db->where_in('user_type',$roleID);
      $users_data =  $this->db->get('users')->result();
      $users_id =array();
      foreach($users_data as $user_data){
        $users_id[] = $user_data->id;
      }
  
      $users_id =array_merge($users_id,[$user->id]);
  
      $this->db->where('adminID',3);
      $this->db->where_in('id',$users_id);
      return $users_data =  $this->db->get('users')->result();
  
     }


   public function hot_lead(){
      $users = array();
      $userIDS = array();
      $customer_details = array();
      $query = $this->followup_model->get_followups(array('followup.status'=>0,'followup.adminID'=>3,'followup.lead_status'=>2));

      foreach($query as $key=>$row){
         $userIDS[$row->userID]=array('email'=>$row->assigin_to_email,'name'=>$row->assigin_to_name);
         $customer_details[$row->userID.'@'.$key] = array('customer_name'=>$row->customerName,'company_name'=>$row->company_name,'customer_phone'=>$row->customerPhone);
      }

      //print_r(count($customer_details));die;
         $emal_data = array();
      foreach($userIDS as $key1=>$userID){
          $customer_data = array();
          for($i=0; $i<count($customer_details); $i++){
            $customer_data[] = $customer_details[$key1.'@'.$i];
          }
         $users =  $this->send_mail_users($key1);
         $email_data['siteinfo'] = $this->siteinfo();
         $subject = 'Followup Status';
         $site_data = array('site_name'=>$email_data['siteinfo']->site_name);
         $email_data['name'] = $userIDS[$key1]['name'];
         $email_data['customers'] =array_filter($customer_data);
         foreach($users as $user)
         {
            
           echo $html = $this->load->view('email-template/followup-status',$email_data, true);
            $email = $user->email;
            //sendEmail($email,$subject,$html,$site_data);
         }
      }
    
   }

   public function send_renewal_reminder(){
   
    $this->db->select('services.*,customers.name as customer_name,customers.email as customer_email,customers.phone as customer_phone,customers.company_name as company_name,users.name as users_name,users.email as users_email');
    $this->db->from('services');
    $this->db->join('enquiry','enquiry.id = services.enquiryID','left');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('users','users.id = services.userID','left');
    $this->db->where(array('services.status'=>1,'services.adminID'=>3));

    $query = $this->db->get()->result_array();

    $domain_previous_Month = array();
		$email_previous_Month = array();
		$hosting_previous_Month = array();
		$ssl_previous_Month = array();
		$seo_previous_Month = array();
		$smo_previous_Month = array();
		$smm_previous_Month = array();
		$ppc_previous_Month = array();
      $maintenence_previous_Month = array();
      $userIDS = array();
		foreach($query as $serve)
		{
			$service_detail = json_decode($serve['service_detail']);

			$domain_renew_date = !empty($service_detail->Domain->doamin_renew_date) ? date('Y-m-d',strtotime($service_detail->Domain->doamin_renew_date)) : '';
			$domain_month = strtotime($domain_renew_date.'-1 month');
         $domain_days_15[$serve['userID'].'_'.$serve['id'].'_Domain'] = !empty($service_detail->Domain->doamin_renew_date) ? date('Y-m-d',strtotime($domain_renew_date . " -15 days")) : '';
         $domain_days_7[$serve['userID'].'_'.$serve['id'].'_Domain'] = !empty($service_detail->Domain->doamin_renew_date) ?date('Y-m-d',strtotime($domain_renew_date . " -1 week")) : '';
         $domain_days_3[$serve['userID'].'_'.$serve['id'].'_Domain'] = !empty($service_detail->Domain->doamin_renew_date) ? date('Y-m-d',strtotime($domain_renew_date . " -3 days")) : '';
			$domain_previous_Month[$serve['userID'].'_'.$serve['id'].'_Domain'] = !empty($service_detail->Domain->doamin_renew_date) ? date('Y-m-d',$domain_month) : '';


			$email_renew_date = date('Y-m-d',strtotime($service_detail->Email->email_rDate));
			$email_month = strtotime($email_renew_date.'-1 month');
         $email_days_15[$serve['userID'].'_'.$serve['id'].'_Email'] = !empty($service_detail->Email->email_rDate) ? date('Y-m-d',strtotime($email_renew_date . "-15 days")) : '';
        
         $email_days_7[$serve['userID'].'_'.$serve['id'].'_Email'] = !empty($service_detail->Email->email_rDate) ?date('Y-m-d',strtotime($email_renew_date . " -1 week")) : '';
         $email_days_3[$serve['userID'].'_'.$serve['id'].'_Email'] = !empty($service_detail->Email->email_rDate) ? date('Y-m-d',strtotime($email_renew_date . " -3 days")) : '';
			$email_previous_Month[$serve['userID'].'_'.$serve['id'].'_Email'] = !empty($service_detail->Email->email_rDate) ? date('Y-m-d',$email_month) : '';
      

			$hosting_renew_date = date('Y-m-d',strtotime($service_detail->Hosting->hosting_renew_date));
			$hosting_month = strtotime($hosting_renew_date.'-1 month');
         $hosting_days_15[$serve['userID'].'_'.$serve['id'].'_Hosting'] = !empty($service_detail->Hosting->hosting_renew_date) ? date('Y-m-d',strtotime($hosting_renew_date . " -15 days")) : '';
         $hosting_days_7[$serve['userID'].'_'.$serve['id'].'_Hosting'] = !empty($service_detail->Hosting->hosting_renew_date) ?date('Y-m-d',strtotime($hosting_renew_date . " -1 week")) : '';
         $hosting_days_3[$serve['userID'].'_'.$serve['id'].'_Hosting'] = !empty($service_detail->Hosting->hosting_renew_date) ? date('Y-m-d',strtotime($hosting_renew_date . " -3 days")) : '';
			$hosting_previous_Month[$serve['userID'].'_'.$serve['id'].'_Hosting'] = !empty($service_detail->Hosting->hosting_renew_date) ? date('Y-m-d',$hosting_month) : '';

			$ssl_renew_date = date('Y-m-d',strtotime($service_detail->SSL->ssl_renew_date));
			$ssl_month = strtotime($ssl_renew_date.'-1 month');
         $ssl_days_15[$serve['userID'].'_'.$serve['id'].'_SSL'] = !empty($service_detail->SSL->ssl_renew_date) ? date('Y-m-d',strtotime($ssl_renew_date . " -15 days")) : '';
         $ssl_days_7[$serve['userID'].'_'.$serve['id'].'_SSL'] = !empty($service_detail->SSL->ssl_renew_date) ?date('Y-m-d',strtotime($ssl_renew_date . " -1 week")) : '';
         $ssl_days_3[$serve['userID'].'_'.$serve['id'].'_SSL'] = !empty($service_detail->SSL->ssl_renew_date) ? date('Y-m-d',strtotime($ssl_renew_date . " -3 days")) : '';
			$ssl_previous_Month[$serve['userID'].'_'.$serve['id'].'_SSL'] = !empty($service_detail->SSL->ssl_renew_date) ? date('Y-m-d',$ssl_month) : '';

			$seo_renew_date = date('Y-m-d',strtotime($service_detail->SEO->seo_rDate));
         $seo_days_15[$serve['userID'].'_'.$serve['id'].'_SEO'] = !empty($service_detail->SEO->seo_rDate) ? date('Y-m-d',strtotime($seo_renew_date . " -15 days")) : '';
         $seo_days_7[$serve['userID'].'_'.$serve['id'].'_SEO'] = !empty($service_detail->SEO->seo_rDate) ?date('Y-m-d',strtotime($seo_renew_date . " -1 week")) : '';
         $seo_days_3[$serve['userID'].'_'.$serve['id'].'_SEO'] = !empty($service_detail->SEO->seo_rDate) ? date('Y-m-d',strtotime($seo_renew_date . " -3 days")) : '';
			$seo_month = strtotime($seo_renew_date.'-1 month');
			$seo_previous_Month[$serve['userID'].'_'.$serve['id'].'_SEO'] = !empty($service_detail->SEO->seo_rDate) ? date('Y-m-d',$seo_month) : '';

			$smo_renew_date = date('Y-m-d',strtotime($service_detail->SMO->smo_rDate));
         $smo_days_15[$serve['userID'].'_'.$serve['id'].'_SMO'] = !empty($service_detail->SMO->smo_rDate) ? date('Y-m-d',strtotime($smo_renew_date . " -15 days")) : '';
         $smo_days_7[$serve['userID'].'_'.$serve['id'].'_SMO'] = !empty($service_detail->SMO->smo_rDate) ?date('Y-m-d',strtotime($smo_renew_date . " -1 week")) : '';
         $smo_days_3[$serve['userID'].'_'.$serve['id'].'_SMO'] = !empty($service_detail->SMO->smo_rDate) ? date('Y-m-d',strtotime($smo_renew_date . " -3 days")) : '';
			$smo_month = strtotime($smo_renew_date.'-1 month');
			$smo_previous_Month[$serve['userID'].'_'.$serve['id'].'_SMO'] = !empty($service_detail->SMO->smo_rDate) ? date('Y-m-d',$smo_month) : '';

			$smm_renew_date = date('Y-m-d',strtotime($service_detail->SMM->smm_rDate));
         $smm_days_15[$serve['userID'].'_'.$serve['id'].'_SMM'] = !empty($service_detail->SMM->smm_rDate) ? date('Y-m-d',strtotime($smm_renew_date . " -15 days")) : '';
         $smm_days_7[$serve['userID'].'_'.$serve['id'].'_SMM'] = !empty($service_detail->SMM->smm_rDate) ?date('Y-m-d',strtotime($smm_renew_date . " -1 week")) : '';
         $smm_days_3[$serve['userID'].'_'.$serve['id'].'_SMM'] = !empty($service_detail->SMM->smm_rDate) ? date('Y-m-d',strtotime($smm_renew_date . " -3 days")) : '';
			$smm_month = strtotime($smm_renew_date.'-1 month');
			$smm_previous_Month[$serve['userID'].'_'.$serve['id'].'_SMM'] = !empty($service_detail->SMM->smm_rDate) ? date('Y-m-d',$smm_month) : '';

			$ppc_renew_date = date('Y-m-d',strtotime($service_detail->PPC->ppc_rDate));
         $ppc_days_15[$serve['userID'].'_'.$serve['id'].'_PPC'] = !empty($service_detail->PPC->ppc_rDate) ? date('Y-m-d',strtotime($ppc_renew_date . " -15 days")) : '';
         $ppc_days_7[$serve['userID'].'_'.$serve['id'].'_PPC'] = !empty($service_detail->PPC->ppc_rDate) ?date('Y-m-d',strtotime($ppc_renew_date . " -1 week")) : '';
         $ppc_days_3[$serve['userID'].'_'.$serve['id'].'_PPC'] = !empty($service_detail->PPC->ppc_rDate) ? date('Y-m-d',strtotime($ppc_renew_date . " -3 days")) : '';
			$ppc_month = strtotime($ppc_renew_date.'-1 month');
			$ppc_previous_Month[$serve['userID'].'_'.$serve['id'].'_PPC'] = !empty($service_detail->PPC->ppc_rDate) ? date('Y-m-d',$ppc_month) : '';
      
      $maintenence_renew_date = date('Y-m-d',strtotime($service_detail->Maintenence->maintenence_rDate));
      $maintenence_days_15[$serve['userID'].'_'.$serve['id'].'_Maintenence'] = !empty($service_detail->Maintenence->maintenence_rDate) ? date('Y-m-d',strtotime($maintenence_renew_date . " -15 days")) : '';
       $maintenence_days_7[$serve['userID'].'_'.$serve['id'].'_Maintenence'] = !empty($service_detail->Maintenence->maintenence_rDate) ?date('Y-m-d',strtotime($maintenence_renew_date . " -1 week")) : '';
      $maintenence_days_3[$serve['userID'].'_'.$serve['id'].'_Maintenence'] = !empty($service_detail->Maintenence->maintenence_rDate) ? date('Y-m-d',strtotime($maintenence_renew_date . " -3 days")) : '';
		$maintenence_month = strtotime($maintenence_renew_date.'-1 month');
		$maintenence_previous_Month[$serve['userID'].'_'.$serve['id'].'_Maintenence'] = !empty($service_detail->Maintenence->maintenence_rDate) ? date('Y-m-d',$maintenence_month) : '';
     
		}

		$domainData       = array_filter($domain_previous_Month);
      $domain_days_15_data   = array_filter($domain_days_15);
      $domain_days_7_data    = array_filter($domain_days_7);
      $domain_days_3_data    = array_filter($domain_days_3);
		$emailData        = array_filter($email_previous_Month);
      $email_days_15_data   = array_filter($email_days_15);
      $email_days_7_data    = array_filter($email_days_7);
      $email_days_3_data    = array_filter($email_days_3);
		$hostingData      = array_filter($hosting_previous_Month);
      $hosting_days_15_data   = array_filter($hosting_days_15);
      $hosting_days_7_data    = array_filter($hosting_days_7);
      $hosting_days_3_data    = array_filter($hosting_days_3);
		$sslData          = array_filter($ssl_previous_Month);
      $ssl_days_15_data   = array_filter($ssl_days_15);
      $ssl_days_7_data    = array_filter($ssl_days_7);
      $ssl_days_3_data    = array_filter($ssl_days_3);
		$seoData          = array_filter($seo_previous_Month);
      $seo_days_15_data   = array_filter($seo_days_15);
      $seo_days_7_data    = array_filter($seo_days_7);
      $seo_days_3_data    = array_filter($seo_days_3);
		$smoData          = array_filter($smo_previous_Month);
      $smo_days_15_data   = array_filter($smo_days_15);
      $smo_days_7_data    = array_filter($smo_days_7);
      $smo_days_3_data    = array_filter($smo_days_3);
		$smmData          = array_filter($smm_previous_Month);
      $smm_days_15_data   = array_filter($smm_days_15);
      $smm_days_7_data    = array_filter($smm_days_7);
      $smm_days_3_data    = array_filter($smm_days_3);
		$ppcData          = array_filter($ppc_previous_Month);
      $ppc_days_15_data   = array_filter($ppc_days_15);
      $ppc_days_7_data    = array_filter($ppc_days_7);
      $ppc_days_3_data    = array_filter($ppc_days_3);
      $maintenenceData  = array_filter($maintenence_previous_Month);
      $maintenence_days_15_data   = array_filter($maintenence_days_15);
      $maintenence_days_7_data    = array_filter($maintenence_days_7);
      $maintenence_days_3_data    = array_filter($maintenence_days_3);

    foreach($query as $key => $service){
      $service_details = json_decode($service['service_detail']);

			
			$sno = 1;
         $counter = 0;
			foreach($service_details as $detail_key => $detail){


		$domainFilter  = 	!empty($domainData[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($domainData[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
      $domain_days_15_Filter  = 	!empty($service['userID'].'_'.$domain_days_15_data[$service['id'].'_'.$detail_key]) ? strtotime($domain_days_15_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
      $domain_days_7_Filter  = 	!empty($domain_days_7_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($domain_days_7_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
      $domain_days_3_Filter  = 	!empty($domain_days_3_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($domain_days_3_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
		$emailFilter   = 	!empty($emailData[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($emailData[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')). ' OR ' : '';
      $email_days_15_Filter  = 	!empty($email_days_15_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($email_days_15_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
      $email_days_7_Filter  = 	!empty($email_days_7_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($email_days_7_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
      $email_days_3_Filter  = 	!empty($email_days_3_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($email_days_3_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
		$hostingFilter = 	!empty($hostingData[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($hostingData[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')). ' OR ' : '';
      $hosting_days_15_Filter  = 	!empty($hosting_days_15_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($hosting_days_15_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
      $hosting_days_7_Filter  = 	!empty($hosting_days_7_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($hosting_days_7_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
      $hosting_days_3_Filter  = 	!empty($hosting_days_3_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($hosting_days_3_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
		$sslFilter     = 	!empty($sslData[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($sslData[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')). ' OR ' : '';
      $ssl_days_15_Filter  = 	!empty($ssl_days_15_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($ssl_days_15_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
      $ssl_days_7_Filter  = 	!empty($ssl_days_7_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($ssl_days_7_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
      $ssl_days_3_Filter  = 	!empty($ssl_days_3_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($ssl_days_3_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
		$seoFilter     = 	!empty($seoData[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($seoData[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')). ' OR ' : '';
      $seo_days_15_Filter  = 	!empty($seo_days_15_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($seo_days_15_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
      $seo_days_7_Filter  = 	!empty($seo_days_7_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($seo_days_7_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
      $seo_days_3_Filter  = 	!empty($seo_days_3_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($seo_days_3_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
		$smoFilter     = 	!empty($smoData[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($smoData[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')). ' OR ' : '';
      $smo_days_15_Filter  = 	!empty($smo_days_15_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($smo_days_15_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
      $smo_days_7_Filter  = 	!empty($smo_days_7_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($smo_days_7_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
      $smo_days_3_Filter  = 	!empty($smo_days_3_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($smo_days_3_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
		$smmFilter     = 	!empty($smmData[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($smmData[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')). ' OR ' : '';
      $smm_days_15_Filter  = 	!empty($smm_days_15_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($smm_days_15_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
      $smm_days_7_Filter  = 	!empty($smm_days_7_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ?strtotime($smm_days_7_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
      $smm_days_3_Filter  = 	!empty($smm_days_3_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($smm_days_3_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
		$ppcFilter     = 	!empty($ppcData[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($ppcData[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) : '';
      $ppc_days_15_Filter  = 	!empty($ppc_days_15_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($ppc_days_15_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
      $ppc_days_7_Filter  = 	!empty($service['userID'].'_'.$ppc_days_7_data[$service['id'].'_'.$detail_key]) ? strtotime($ppc_days_7_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
      $ppc_days_3_Filter  = 	!empty($ppc_days_3_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($ppc_days_3_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
    $maintenenceFilter  = 	!empty($maintenenceData[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($maintenenceData[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) : '';
    $maintenence_days_15_Filter  = 	!empty($maintenence_days_15_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($maintenence_days_15_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
    $maintenence_days_7_Filter  = 	!empty($maintenence_days_7_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($maintenence_days_7_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
    $maintenence_days_3_Filter  = 	!empty($maintenence_days_3_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) ? strtotime($maintenence_days_3_data[$service['userID'].'_'.$service['id'].'_'.$detail_key]) == strtotime(date('Y-m-d')) . ' OR ' : '';
    
			
		if($domainFilter.$domain_days_15_Filter.$domain_days_7_Filter.$domain_days_3_Filter.$emailFilter.$email_days_15_Filter.$email_days_7_Filter.$email_days_3_Filter.$hostingFilter.$hosting_days_15_Filter.$hosting_days_7_Filter.$hosting_days_3_Filter.$sslFilter.$ssl_days_15_Filter.$ssl_days_7_Filter.$ssl_days_3_Filter.$seoFilter.$seo_days_15_Filter.$seo_days_7_Filter.$seo_days_3_Filter.$smoFilter.$smo_days_15_Filter.$smo_days_7_Filter.$smo_days_3_Filter.$smmFilter.$smm_days_15_Filter.$smm_days_7_Filter.$smm_days_3_Filter.$ppcFilter.$ppc_days_15_Filter.$ppc_days_7_Filter.$ppc_days_3_Filter.$maintenenceFilter.$maintenence_days_15_Filter.$maintenence_days_7_Filter.$maintenence_days_3_Filter)
		{
			// echo "<pre>";
			// print_r($detail);

			$dmoain_name = $detail_key=='Domain' ? $detail->domain_name :($detail_key=='Email' ? $detail->email_domain_name :($detail_key=='Hosting' ? $detail->hosting_domain_name :($detail_key=='SSL' ? $detail->ssl_domain_name :($detail_key=='SEO' ? $detail->seo_url :($detail_key=='SMO' ? $detail->smo_url :($detail_key=='SMM' ? $detail->smm_url :($detail_key=='PPC' ? $detail->ppc_url :($detail_key=='Maintenence' ? $detail->websiteMaintenenceDetail :''))))))));

			$product_name = $detail_key=='Domain' ? 'Domain' :($detail_key=='Email' ? 'Email' :($detail_key=='Hosting' ? 'Hosting' :($detail_key=='SSL' ? 'SSL' :($detail_key=='SEO' ? 'SEO' :($detail_key=='SMO' ? 'SMO' :($detail_key=='SMM' ? 'SMM' :($detail_key=='PPC' ? 'PPC' :($detail_key=='Maintenence' ? 'Maintenence' :''))))))));

         $price =$detail_key=='Domain' ? $detail->domain_total_price :($detail_key=='Email' ? $detail->email_total_price :($detail_key=='Hosting' ? $detail->hosting_total_price :($detail_key=='SSL' ? $detail->ssl_total_price :($detail_key=='SEO' ? $detail->tseocost :($detail_key=='SMO' ? $detail->tsmocost :($detail_key=='SMM' ? $detail->tsmmcost :($detail_key=='PPC' ? $detail->tppccost :($detail_key=='Maintenence' ? $detail->tpricemaintenence :''))))))));

			$renew_date = $detail_key=='Domain' ? date('d-M-Y', strtotime($detail->doamin_renew_date))  :($detail_key=='Email' ? date('d-M-Y', strtotime($detail->email_rDate)) :($detail_key=='Hosting' ? date('d-M-Y', strtotime($detail->hosting_renew_date)) :($detail_key=='SSL' ? date('d-M-Y', strtotime($detail->ssl_renew_date)) :($detail_key=='SEO' ? date('d-M-Y', strtotime($detail->seo_rDate)) :($detail_key=='SMO' ? date('d-M-Y', strtotime($detail->smo_rDate)) :($detail_key=='SMM' ? date('d-M-Y', strtotime($detail->smm_rDate)) :($detail_key=='PPC' ? date('d-M-Y', strtotime($detail->ppc_rDate)) :($detail_key=='Maintenence' ? date('d-M-Y', strtotime($detail->maintenence_rDate)) :''))))))));
         
      if(($detail_key=='Domain' AND $detail->domainStatus=='New') OR ($detail_key=='Email' AND $detail->emailStatus=='New') OR ($detail_key=='Hosting' AND $detail->hostingStatus=='New') OR ($detail_key=='SSL' AND $detail->sslStatus=='New') OR ($detail_key=='SEO' AND $detail->seoStatus=='New') OR ($detail_key=='SMO' AND $detail->smoStatus=='New') OR ($detail_key=='SMM' AND $detail->smmStatus=='New') OR ($detail_key=='PPC' AND $detail->ppcStatus=='New') OR ($detail_key=='Maintenence' AND $detail->maintenenceStatus=='New') ){
   
         
          $userIDS[$service['userID']] = array('user_name'=>$service['users_name'],'user_email'=>$service['users_email']);
			$button = '';
			$sub_array = array();
			// if($permission[0]=='View')
			// {
			 $button .= '<a href="'.base_url('renewal-services/'.base64_encode($service['id'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Renew Now!" class="btn btn-outline-primary btn-sm"> Renew Now!</a>';
			//}

			$sub_array[] = $sno++;
         $sub_array[] = $service['customer_name'];
         $sub_array[] = $service['customer_phone'];
         $sub_array[] = $service['customer_email'];
         $sub_array[] = $service['company_name'];
			$sub_array[] = $dmoain_name;
			$sub_array[] = $product_name;
         $sub_array[] = $price;
			$sub_array[] = $renew_date;

		  $data[$service['userID'].'@'.$counter]      = $sub_array;

		 }
    }
    $counter = $counter+1;
    }
    
 }

    $emal_data = array();
 foreach($userIDS as $key1=>$userID){
       //print_r($userID);
     $service_data = array();
     for($i=0; $i<count($data); $i++){
      
       $service_data[] = $data[$key1.'@'.$i];
     }
    $users =  $this->send_mail_users($key1);
    
    $email_data['siteinfo'] = $this->siteinfo();
    $subject = 'Renewal Services';
    $site_data = array('site_name'=>$email_data['siteinfo']->site_name);
    $email_data['name'] = $userID['user_name'];
    $email_data['services'] =array_filter($service_data);
    foreach($users as $user)
    {
       
      echo $html = $this->load->view('email-template/service-renewal',$email_data, true);
       $email = $user->email;
       sendEmail($email,$subject,$html,$site_data);
    }
 }


  

  return $data;
   }
  

  }
      

   
