<?php 
date_default_timezone_set('Asia/Kolkata');
class Customer extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('customer_model');
		$this->load->model('user_model');
		$this->load->model('setting_model');
		$this->load->model('enquiry_model');
		$this->load->model('followup_model');
		$this->load->model('sale_model');
		$this->load->library('csvimport');
	}

	public function index()
	{
		$this->not_admin_logged_in();
		$data['uri'] = $this->uri->segment(1);
		$permission = permission($data['uri']);
		if($permission[0]=='View'){
			$data['permission'] = $permission;
			$data['page_title'] = 'Customer';
			$data['admins'] = $this->user_model->get_users(array('users.user_type'=>2));
			$data['users'] =  get_users_function(array('users.user_type<>'=>2,'users.user_type<>'=>1,'users.adminID'=>$this->session->userdata('adminID')),'Sale');
			$data['url'] = $data['uri'];
		  $validate_lead = $this->enquiry_model->get_enquiries(array('enquiry.adminID' => $this->session->userdata('adminID'),'enquiry.lead_month'=>date('m-Y')));
      $user = $this->user_model->get_user(array('users.adminID' => $this->session->userdata('id')));
		  $data['message'] = $user->lead_permission == 0  ? '<span class="text-success">Unlimited lead access</span>' : ( $user->lead_permission <= count($validate_lead) ? '<span class="text-danger">This month lead permission limit up please upgrade</span>' : '') ;
	    $this->admin_template('customer/customers',$data);
		}else{
			redirect(base_url('dashboard'));
		}
    
	}

	public function ajaxCustomer(){
		$this->not_admin_logged_in();
		  $data['uri'] = $this->uri->segment(3);
      $permission = permission($data['uri']);
			$role = role();

			// $condition = $this->session->userdata('user_type')==1 ? array('customers.adminID'=>$this->session->userdata('customerAdminID')) : 
			// ($this->session->userdata('user_type')==2 ? array('customers.adminID'=>$this->session->userdata('adminID')) : ($permission[7]=='Like-admin' ? array('customers.adminID'=>$this->session->userdata('adminID')):array('customers.adminID'=>$this->session->userdata('customerAdminID'),'customers.userID'=>$this->session->userdata('id'))));

			$condition = $this->session->userdata('user_type')==1 ? array('customers.adminID'=>$this->session->userdata('customerAdminID')) : 
			($this->session->userdata('user_type')==2 ? array('customers.adminID'=>$this->session->userdata('adminID')) : array('customers.adminID'=>$this->session->userdata('adminID')));
		$customers = $this->customer_model->make_datatables($condition); // this will call modal function for fetching data
		$data = array();
		//print_r($admins);die;
		foreach($customers as $key=>$customer) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
			if($permission[0]=='View'){
        $button .= '<a href="javascript:void(0)" onclick="view_customer('.$customer['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View customer Detail" class="btn  btn-sm  text-warning"><i class="fa fa-eye"></i></a>';
      }
			if($permission[2]=='Edit'){
				$button .= '<a href="'.base_url('edit-customer/'.base64_encode($customer['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit customer Detail" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
      }
			if($permission[3]=='Delete'){
				$button .= '<a href="javascript:void(0)" onclick="delete_customer('.$customer['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete customer" class="btn  btn-sm  text-danger"><i class="fa fa-trash"></i> </a>';
      }
      		
			$sub_array[] = $key+1;
			$sub_array[] = $button;
			$sub_array[] = $customer['name'];
			$sub_array[] = $customer['phone'];
			$sub_array[] = $customer['email'];
			$sub_array[] = date('d-m-Y', strtotime($customer['created_at']));
			$sub_array[] = $customer['address'];
			$sub_array[] = $customer['cityName'];
			$sub_array[] = $customer['stateName'];
			$sub_array[] = !empty($customer['pincode'])?$customer['pincode']:'-';
		
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->customer_model->get_all_data($condition),
			"recordsFiltered"         =>     $this->customer_model->get_filtered_data($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	  }

		public function create(){
			$this->not_admin_logged_in();
			$data['uri'] = 'customers';
			$permission = permission($data['uri']);

			


		$users =	permission($data['uri']);
			if($permission[1]=='Add'){
				$data['page_title'] = 'Customer';
				// $condition = $this->session->userdata('user_type')==2 ? array('users.adminID'=>$this->session->userdata('adminID'),'users.status'=>1) : ($permission[7]=='Like-admin' ? array('users.adminID'=>$this->session->userdata('adminID'),'users.status'=>1) :array('users.adminID'=>$this->session->userdata('adminID')));
				$condition = array('users.adminID'=>$this->session->userdata('adminID'));
				$data['users'] =  get_users_function($condition,'Sale');//$this->user_model->get_users($condition);
				$data['lead_status'] = $this->setting_model->get_lead_status(array('id'=>1,'status'=>1));
				$this->admin_template('customer/create-customer',$data);
			}else{
				redirect(base_url('dashboard'));
			}
			
		}

		public function store(){
			
			$adminID = $this->session->userdata('adminID');
			$name = trim(ucwords($this->input->post('name')));
			$company_name = trim(ucwords($this->input->post('company_name')));
			$email = trim($this->input->post('email'));
			$phone = trim($this->input->post('phone'));
			$alternate_no = $this->input->post('alternate_no');
			//$state = $this->input->post('state');
			//$city = $this->input->post('city');
			$address = $this->input->post('address');
			//$pincode = $this->input->post('pincode');
			$lead_type = $this->input->post('lead_type');
			$lead_sourse = $this->input->post('lead_sourse');
			$lead_status = $this->input->post('lead_status');
			$userID = $this->input->post('userID');
			$requirement = $this->input->post('requirement');
			$is_sale = $this->input->post('is_sale');
			// permission start
	
		  $validate_phone = $this->customer_model->get_customer(array('customers.phone' => $phone));
			$validate_lead = $this->enquiry_model->get_enquiries(array('enquiry.adminID' => $adminID,'enquiry.lead_month'=>date('m-Y')));
      $user = $this->user_model->get_user(array('users.adminID' => $adminID,'user_detail.lead_permission <>'=>0));
			
			if($user->lead_permission <= count($validate_lead) && $user->lead_permission!=0){
				echo json_encode(['status'=>403, 'message'=>'This month Lead limit up']); 	
				exit();
			}

			if(empty($phone)){
				echo json_encode(['status'=>403, 'message'=>'Please enter your phone ']); 	
				exit();
			}
				
		 if(empty($name)){
				echo json_encode(['status'=>403, 'message'=>'Please enter name']); 	
				exit();
			}
			
			// $this->direct_sale($adminID,$name,$company_name,$email,$phone,$alternate_no,$address,$lead_type,$lead_sourse,$lead_status,$userID,$requirement,$is_sale,$validate_phone,$validate_lead,$user);

			// if(empty($address)){
			// 	echo json_encode(['status'=>403, 'message'=>'Please enter address ']); 	
			// 	exit();
			// }
				
				// if(empty($lead_type)){
				// 	echo json_encode(['status'=>403, 'message'=>'Please enter lead type ']); 	
				// 	exit();
				// }
				// if(empty($lead_sourse)){
				// 	echo json_encode(['status'=>403, 'message'=>'Please enter lead sourse ']); 	
				// 	exit();
				// }
				
		
				if(empty($lead_status)){
					echo json_encode(['status'=>403, 'message'=>'Please select lead status']);  	
					exit();
				}

				if(empty($userID)){
					echo json_encode(['status'=>403, 'message'=>'Please select assigined to']);  	
					exit();
				}

				// if(empty($requirement)){
				// 	echo json_encode(['status'=>403, 'message'=>'Please enter requirement']);  	
				// 	exit();
				// }
        if(empty($is_sale)){
				if($validate_phone){

					$enquiry_data = array(	
						'adminID'     => $adminID,
						'clientID'    => $validate_phone->id,
						'userID'      => $userID,
						'assigned_by' => $this->session->userdata('id'),
						'lead_type'   => $lead_type,
						'lead_sourse' => $lead_sourse,
						'lead_status' => $lead_status,
						'requirement' => $requirement,
						'assigin_date'=> date('Y-m-d'),
						'lead_month'  => date('m-Y'),
					);

					$store_enquiry = $this->enquiry_model->store_enquiry($enquiry_data);

					$enquiry_history = array(	
						'adminID'     => $adminID,
						'enquiryID'   => $store_enquiry,
						'clientID'    => $validate_phone->id,
						'userID'      => $userID,
						'assigned_by' => $this->session->userdata('id'),
						'lead_type'   => $lead_type,
						'lead_sourse' => $lead_sourse,
						'lead_status' => $lead_status,
						'requirement' => $requirement,
						'assigin_date'=> date('Y-m-d'),
						'lead_month'  => date('m-Y'),
					);

					$store_enquiry = $this->enquiry_model->store_enquiry_history($enquiry_history);

					if($store_enquiry){


						echo json_encode(['status'=>200, 'message'=>'Lead Added Successfully.Send mail please wait....','sale_page'=>0,'id'=>$userID]);   
					}else{
						echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
					}

				}else{

					$data = array(	
						'adminID'     => $adminID,
						'userID'      => $userID,
						'name'        => $name,
						'company_name'=> $company_name,
						'email'       => $email,
						'phone'       => $phone,
						'address'     => $address,
						//'city'        => $city,
						//'state'       => $state,
						//'pincode'     => $pincode,
						'alternate_no'=> $alternate_no,
					);
			
					$customerID = $this->customer_model->store_customer($data);
					if($customerID){

						$enquiry_data = array(	
							'adminID'     => $adminID,
							'clientID'    => $customerID,
							'userID'      => $userID,
							'assigned_by' => $this->session->userdata('id'),
							'lead_type'   => $lead_type,
							'lead_sourse' => $lead_sourse,
							'lead_status' => $lead_status,
							'requirement' => $requirement,
							'assigin_date'=> date('Y-m-d'),
							'lead_month'  => date('m-Y'),
						);

						$store_enquiry = $this->enquiry_model->store_enquiry($enquiry_data);

						$enquiry_history = array(	
							'adminID'     => $adminID,
							'enquiryID'   => $store_enquiry,
							'clientID'    => $customerID,
							'userID'      => $userID,
							'assigned_by' => $this->session->userdata('id'),
							'lead_type'   => $lead_type,
							'lead_sourse' => $lead_sourse,
							'lead_status' => $lead_status,
							'requirement' => $requirement,
							'assigin_date'=> date('Y-m-d'),
							'lead_month'  => date('m-Y'),
						);

						$store_enquiry = $this->enquiry_model->store_enquiry_history($enquiry_history);

					 echo json_encode(['status'=>200, 'message'=>'Lead Added Successfully. Send Mail Please wait....','sale_page'=>0,'id'=>$userID]);   
					}else{
						echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
					}

				}
			}else{
				$enquiryID = $this->direct_sale($adminID,$name,$company_name,$email,$phone,$alternate_no,$address,$lead_type,$lead_sourse,$lead_status,$userID,$requirement,$is_sale,$validate_phone,$validate_lead,$user);
       
				if(!empty($enquiryID)){
					echo json_encode(['status'=>200, 'message'=>'Lead Added Successfully','sale_page'=>base64_encode($enquiryID),'id'=>$userID]);   
				}else{
					echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
				}
				


			}
	
		}


		function direct_sale($adminID,$name,$company_name,$email,$phone,$alternate_no,$address,$lead_type,$lead_sourse,$lead_status,$userID,$requirement,$is_sale,$validate_phone,$validate_lead,$user){


			if($validate_phone){

				$enquiry_data = array(	
					'adminID'     => $adminID,
					'clientID'    => $validate_phone->id,
					'userID'      => $userID,
					'assigned_by' => $this->session->userdata('id'),
					'lead_type'   => $lead_type,
					'lead_sourse' => $lead_sourse,
					'lead_status' => 5,
					'requirement' => $requirement,
					'assigin_date'=> date('Y-m-d'),
					'lead_month'  => date('m-Y'),
				);

				$store_enquiry = $this->enquiry_model->store_enquiry($enquiry_data);

				$enquiry_history = array(	
					'adminID'     => $adminID,
					'enquiryID'   => $store_enquiry,
					'clientID'    => $validate_phone->id,
					'userID'      => $userID,
					'assigned_by' => $this->session->userdata('id'),
					'lead_type'   => $lead_type,
					'lead_sourse' => $lead_sourse,
					'lead_status' => 5,
					'requirement' => $requirement,
					'assigin_date'=> date('Y-m-d'),
					'lead_month'  => date('m-Y'),
				);

				$store_enquiry_history = $this->enquiry_model->store_enquiry_history($enquiry_history);


				if($store_enquiry_history){

					$followup_data = array(	
						'adminID'       => $adminID,
						'enquriyID'     => $store_enquiry,
						'userID'        => $this->session->userdata('id'),
						'remark'        => 'Sale Added',
						'lead_status'   => 5,
						'followup_date' => date('Y-m-d'),
						'followup_time' => date('H:i:s'),
						'today_followup_status' => date('Y-m-d')
					);

					$followupID = $this->followup_model->store_followup($followup_data);

					$data = array(	
						'adminID'       => $adminID,
						'enquriyID'     => $store_enquiry,
						'followupID'    => $followupID,
						'userID'        => $this->session->userdata('id'),
						'remark'        => 'Sale Added',
						'lead_status'   => 5,
						'followup_date' => date('Y-m-d'),
						'followup_time' => date('H:i:s'),
					);
			
					$followupHistory = $this->followup_model->store_followup_history($data);

					//$sale_page = $sale==1 ? $enquiryID : 0 ;

					$assigin_user = $this->user_model->get_user(array('users.id'=>$userID));
					$users = send_mail_users($userID);
					$email_data['siteinfo'] = $this->siteinfo();
					$subject = 'Assign Customer';
					$site_data = array('site_name'=>$email_data['siteinfo']->site_name);
					//$email_data['name'] = $assigin_user->name;
					$email_data['assigin_by'] = $this->session->userdata('name');
					$html = $this->load->view('email-template/assigin-lead',$email_data, true);
	
					$user_ids=array();
					foreach($users as $user)
					{
						$email = $user->email;
						$email_data['name'] = $user->name;
						sendEmail($email,$subject,$html,$site_data);
					}
					
        if($followupHistory)
				{
					 return $store_enquiry;   
					}else{
						return 0;
					}

				}
			}else{

				$data = array(	
					'adminID'     => $adminID,
					'userID'      => $userID,
					'name'        => $name,
					'company_name'=> $company_name,
					'email'       => $email,
					'phone'       => $phone,
					'address'     => $address,
					//'city'        => $city,
					//'state'       => $state,
					//'pincode'     => $pincode,
					'alternate_no'=> $alternate_no,
				);
		
				$customerID = $this->customer_model->store_customer($data);
				if($customerID){

					$enquiry_data = array(	
						'adminID'     => $adminID,
						'clientID'    => $customerID,
						'userID'      => $userID,
						'assigned_by' => $this->session->userdata('id'),
						'lead_type'   => $lead_type,
						'lead_sourse' => $lead_sourse,
						'lead_status' => 5,
						'requirement' => $requirement,
						'assigin_date'=> date('Y-m-d'),
						'lead_month'  => date('m-Y'),
					);

					$store_enquiry = $this->enquiry_model->store_enquiry($enquiry_data);

					$enquiry_history = array(	
						'adminID'     => $adminID,
						'enquiryID'   => $store_enquiry,
						'clientID'    => $customerID,
						'userID'      => $userID,
						'assigned_by' => $this->session->userdata('id'),
						'lead_type'   => $lead_type,
						'lead_sourse' => $lead_sourse,
						'lead_status' => 5,
						'requirement' => $requirement,
						'assigin_date'=> date('Y-m-d'),
						'lead_month'  => date('m-Y'),
					);


					
					$followup_data = array(	
						'adminID'       => $adminID,
						'enquriyID'     => $store_enquiry,
						'userID'        => $this->session->userdata('id'),
						'remark'        => 'Sale Added',
						'lead_status'   => 5,
						'followup_date' => date('Y-m-d'),
						'followup_time' => date('H:i:s'),
						'today_followup_status' => date('Y-m-d')
					);

					$followupID = $this->followup_model->store_followup($followup_data);

					$data = array(	
						'adminID'       => $adminID,
						'enquriyID'     => $store_enquiry,
						'followupID'    => $followupID,
						'userID'        => $this->session->userdata('id'),
						'remark'        => 'Sale Added',
						'lead_status'   => 5,
						'followup_date' => date('Y-m-d'),
						'followup_time' => date('H:i:s'),
					);
			
					$followupHistory = $this->followup_model->store_followup_history($data);

					//$sale_page = $sale==1 ? $enquiryID : 0 ;

					$assigin_user = $this->user_model->get_user(array('users.id'=>$userID));
					$users = send_mail_users($userID);
					$email_data['siteinfo'] = $this->siteinfo();
					$subject = 'Assign Customer';
					$site_data = array('site_name'=>$email_data['siteinfo']->site_name);
					//$email_data['name'] = $assigin_user->name;
					$email_data['assigin_by'] = $this->session->userdata('name');
					$html = $this->load->view('email-template/assigin-lead',$email_data, true);
	
					$user_ids=array();
					foreach($users as $user)
					{
						$email = $user->email;
						$email_data['name'] = $user->name;
						sendEmail($email,$subject,$html,$site_data);
					}
					
        if($followupHistory)
				{
					return $store_enquiry;   
					}else{
						return 0;
					}

				 
				}else{
					return 0;
				}

			}

		}

		public function customerViewForm(){
			$id = $this->input->post("id");
			$customer = $this->customer_model->get_customer(array('customers.id' => $id));
			?>
<div class="row">
  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group row">
      <label class="col-lg-3 col-form-label">Phone No.</label>
      <div class="col-lg-9">
        <input type="text" class="form-control" id="phone" name="phone" value="<?=$customer->phone?>"
          placeholder="Enter Role Name" readonly>
      </div>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group row">
      <label class="col-lg-3 col-form-label">Name</label>
      <div class="col-lg-9">
        <input type="text" class="form-control" id="name" name="name" value="<?=$customer->name?>"
          placeholder="Enter Role Name" readonly>
      </div>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group row">
      <label class="col-lg-3 col-form-label">Company Name</label>
      <div class="col-lg-9">
        <input type="text" class="form-control" id="company_name" name="company_name"
          value="<?=$customer->company_name?>" placeholder="Enter Role Name" readonly>
      </div>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group row">
      <label class="col-lg-3 col-form-label">Email</label>
      <div class="col-lg-9">
        <input type="text" class="form-control" id="email" name="email" value="<?=$customer->email?>"
          placeholder="Enter Role Name" readonly>
      </div>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group row">
      <label class="col-lg-3 col-form-label">Phone No.</label>
      <div class="col-lg-9">
        <input type="text" class="form-control" id="name" name="name" value="<?=$customer->phone?>"
          placeholder="Enter Role Name" readonly>
      </div>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group row">
      <label class="col-lg-3 col-form-label">Alternate No.</label>
      <div class="col-lg-9">
        <input type="text" class="form-control" id="alternate_no" name="alternate_no"
          value="<?=$customer->alternate_no?>" placeholder="Enter Role Name" readonly>
      </div>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group row">
      <label class="col-lg-3 col-form-label">Sate</label>
      <div class="col-lg-9">
        <input type="text" class="form-control" id="state" name="state" value="<?=$customer->stateName?>"
          placeholder="Enter Role Name" readonly>
      </div>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group row">
      <label class="col-lg-3 col-form-label">City</label>
      <div class="col-lg-9">
        <input type="text" class="form-control" id="city" name="city" value="<?=$customer->cityName?>"
          placeholder="Enter Role Name" readonly>
      </div>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group row">
      <label class="col-lg-3 col-form-label">Pincode</label>
      <div class="col-lg-9">
        <input type="text" class="form-control" id="pincode" name="pincode" value="<?=$customer->pincode?>"
          placeholder="Enter Role Name" readonly>
      </div>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group row">
      <label class="col-lg-3 col-form-label">Full Address</label>
      <div class="col-lg-9">
        <textarea class="form-control" name="address" id="address" readonly><?=$customer->address?></textarea>

      </div>
    </div>
  </div>

</div>
<?php
		 }

	public function delete(){
	    
		$id = $this->input->post("id");   

		$enquiry = $this->enquiry_model->get_enquiry(array('enquiry.clientID'=>$id));

		$delete_followup = $this->followup_model->delete_followup(array('enquriyID'=>$enquiry->id));

		$delete_followup_history = $this->followup_model->delete_followup_history(array('enquriyID'=>$enquiry->id));

		$delete_enquiry_history = $this->enquiry_model->delete_enquiry_history(array('clientID'=>$id));
		
		$delete_enquiry = $this->enquiry_model->delete_enquiry(array('clientID'=>$id));
		
		$delete_customer = $this->customer_model->delete_customer(array('id'=>$id));

		if($delete_customer){
			echo json_encode(['status'=>200, 'message'=>'Customer Deleted Successfully']);   
		}else{
			echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
		}
		
	}


		public function edit(){

			$this->not_admin_logged_in();
			$data['uri'] = 'customers';
			$permission = permission($data['uri']);
			if($permission[1]=='Add'){
				$data['page_title'] = 'Customer';
				$id = base64_decode($this->uri->segment(2));
				$data['customer'] = $this->customer_model->get_customer(array('customers.id'=>$id));
				$data['users'] = get_users_function(array('users.adminID'=>$this->session->userdata('adminID')),'Sale');//$this->user_model->get_users();
				$data['lead_status'] = $this->setting_model->get_lead_status(array('id'=>1,'status'=>1));
				$this->admin_template('customer/edit-customer',$data);
			}else{
				redirect(base_url('dashboard'));
			}
			
			 
		}


		public function update(){
			$id = $this->input->post('id');
			$name = $this->input->post('name');
			$company_name = $this->input->post('company_name');
			$email = $this->input->post('email');
			$phone = $this->input->post('phone');
			$alternate_no = $this->input->post('alternate_no');
			$state = $this->input->post('state');
			$city = $this->input->post('city');
			$address = $this->input->post('address');
			$pincode = $this->input->post('pincode');
			
		 if(empty($name)){
				echo json_encode(['status'=>403, 'message'=>'Please enter name']); 	
				exit();
			}
			
			// if(empty($address)){
			// 	echo json_encode(['status'=>403, 'message'=>'Please enter address ']); 	
			// 	exit();
			// }
						
				$data = array(	
					'name'        => $name,
					'company_name'=> $company_name,
					'email'       => $email,
					'address'     => $address,
					'city'        => $city,
					'state'       => $state,
					'pincode'     => $pincode,
					'alternate_no'=> $alternate_no,
				);
			
			$customerID = $this->customer_model->update_customer($data,array('id'=>$id));
			if($customerID){
				echo json_encode(['status'=>200, 'message'=>'Customer updated Successfully']);   
			}else{
				echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
			}
		}

		public function download_sample(){
			$this->load->helper('download');
			$pth  = file_get_contents(base_url('uploads/sample_csv/lead_sample.csv'));
			$nme   =  "lead_sample.csv";
			force_download($nme ,$pth);  
		}

		public function import_customer(){
	
			$adminID = $this->session->userdata('adminID');
			$userID=$this->input->post('userID');
			$assigned_by=$this->session->userdata('id');
			
			if(empty($userID)){
					echo json_encode(['status'=>403, 'message'=>'Please select user']); 	
					exit();
			}
		
			if (isset($_FILES["file"])) {
				$config['upload_path']   = "uploads/csv/";
				$config['allowed_types'] = 'text/plain|text/csv|csv';
				$config['max_size']      = '2048';
				$config['file_name']     = $_FILES["file"]['name'];
				$config['overwrite']     = TRUE;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload("file")) {
						echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
				} else {
					$file_data = $this->upload->data();
					$file_path = 'uploads/csv/' . $file_data['file_name'];
					if ($this->csvimport->get_array($file_path)) {
						$csv_array = $this->csvimport->get_array($file_path);
           
						$validate_lead = $this->enquiry_model->get_enquiries(array('enquiry.adminID' => $adminID,'enquiry.lead_month'=>date('m-Y')));
						$user = $this->user_model->get_user(array('users.adminID' => $adminID,'user_detail.lead_permission <>'=>0));

						$total_leads = count($validate_lead)+count($csv_array);
						$extra_lead = $total_leads-$user->lead_permission;
							if($user->lead_permission < $total_leads  && $user->lead_permission!=0){
								echo json_encode(['status'=>403, 'message'=>'This month '.$extra_lead.' extra lead is not allowed']); 	
								exit();
							}

							foreach ($csv_array as $key => $value) {

								if(empty($value['name'])){
									echo json_encode(['status'=>403, 'message'=>'Please enter a name']); 	
									exit();
								}

								if(empty($value['phone'])){
									echo json_encode(['status'=>403, 'message'=>'Please enter a phone number']); 	
									exit();
								}

							}

						foreach ($csv_array as $key => $row) {
							$name = $row['name'];
							$company_name = $row['company_name'];
							$email = $row['email'];
							$phone = $row['phone'];
							$lead_sourse = $row['lead_source'];
							$requirement = $row['requirement'];
	
		         $validate_phone = $this->customer_model->get_customer(array('customers.phone' => $phone));
			
			
				if($validate_phone){

					$enquiry_data = array(	
						'adminID'     => $adminID,
						'clientID'    => $validate_phone->id,
						'userID'      => $userID,
						'assigned_by' => $this->session->userdata('id'),
						'lead_sourse' => $lead_sourse,
						'lead_status' => 1,
						'requirement' => $requirement,
						'assigin_date'=> date('d-m-Y'),
						'lead_month'  => date('m-Y'),
					);

					$store_enquiry = $this->enquiry_model->store_enquiry($enquiry_data);

					$enquiry_history = array(	
						'adminID'     => $adminID,
						'enquiryID'   => $store_enquiry,
						'clientID'    => $validate_phone->id,
						'userID'      => $userID,
						'assigned_by' => $this->session->userdata('id'),
						'lead_sourse' => $lead_sourse,
						'lead_status' => 1,
						'requirement' => $requirement,
						'assigin_date'=> date('d-m-Y'),
						'lead_month'  => date('m-Y'),
					);

					$store_enquiry_history = $this->enquiry_model->store_enquiry_history($enquiry_history);

				}else{

					$data = array(	
						'adminID'     => $adminID,
						'userID'      => $userID,
						'name'        => $name,
						'company_name'=> $company_name,
						'email'       => $email,
						'phone'       => $phone,
					);
			
					$customerID = $this->customer_model->store_customer($data);
					if($customerID){

						$enquiry_data = array(	
							'adminID'     => $adminID,
							'clientID'    => $customerID,
							'userID'      => $userID,
							'assigned_by' => $this->session->userdata('id'),
							'lead_sourse' => $lead_sourse,
							'lead_status' => 1,
							'requirement' => $requirement,
							'assigin_date'=> date('d-m-Y'),
							'lead_month'  => date('m-Y'),
						);

						$store_enquiry = $this->enquiry_model->store_enquiry($enquiry_data);

						$enquiry_history = array(	
							'adminID'     => $adminID,
							'enquiryID'   => $store_enquiry,
							'clientID'    => $customerID,
							'userID'      => $userID,
							'assigned_by' => $this->session->userdata('id'),
							'lead_sourse' => $lead_sourse,
							'lead_status' => 1,
							'requirement' => $requirement,
							'assigin_date'=> date('d-m-Y'),
							'lead_month'  => date('m-Y'),
						);

						$store_enquiry_history = $this->enquiry_model->store_enquiry_history($enquiry_history);	
					}				 
						}
					}
						if($store_enquiry_history){
						echo json_encode(['status'=>200, 'message'=>'Lead Import Successfully']);
					} else {
						echo json_encode(['status'=>403, 'message'=>'Lead Import Failure']);
					}
				}
			} 
		}
		else {
			echo json_encode(['status'=>403, 'message'=>'Please upload CSV']);
		}
	}

	public function import_customer_manual()
	{
		$adminID = $this->session->userdata('adminID');
    $userID=$this->input->post('userID');
		$assigned_by=$this->session->userdata('id');

		if(empty($userID)){
				echo json_encode(['status'=>403, 'message'=>'Please select user']); 	
				exit();
		}
	
		if (isset($_FILES["file"])) {
			$config['upload_path']   = "uploads/csv/";
			$config['allowed_types'] = 'text/plain|text/csv|csv';
			$config['max_size']      = '2048';
			$config['file_name']     = $_FILES["file"]['name'];
			$config['overwrite']     = TRUE;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload("file")) {
					echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
			} else {
				$file_data = $this->upload->data();
				$file_path = 'uploads/csv/' . $file_data['file_name'];
				if ($this->csvimport->get_array($file_path)) {
					$csv_array = $this->csvimport->get_array($file_path);
				 
					$validate_lead = $this->enquiry_model->get_enquiries(array('enquiry.adminID' => $adminID,'enquiry.lead_month'=>date('m-Y')));
					$user = $this->user_model->get_user(array('users.adminID' => $adminID,'user_detail.lead_permission <>'=>0));

					$total_leads = count($validate_lead)+count($csv_array);
					$extra_lead = $total_leads-$user->lead_permission;
						// if($user->lead_permission < $total_leads  && $user->lead_permission!=0){
						// 	echo json_encode(['status'=>403, 'message'=>'This month '.$extra_lead.' extra lead is not allowed']); 	
						// 	exit();
						// }

						foreach ($csv_array as $key => $value) {

							if(empty($value['name'])){
								echo json_encode(['status'=>403, 'message'=>'Please enter a name']); 	
								exit();
							}

							if(empty($value['phone'])){
								echo json_encode(['status'=>403, 'message'=>'Please enter a phone number']); 	
								exit();
							}

							if ( filter_var($value['phone'], FILTER_VALIDATE_INT) === false ) {
								echo json_encode(['status'=>403, 'message'=>'Please enter valid phone number']); 	
								exit();
							}

							
							if(!empty($value['email'])){
								if (filter_var($value['email'], FILTER_VALIDATE_EMAIL)==false) { 
									echo json_encode(['status'=>403, 'message'=>'Please enter valid email address']); 	
								exit();
							}  
							}
							
						}
						$duplicat_customer = array();
					foreach ($csv_array as $key => $row) {
					    
					    //print_r($row);
						$name = trim(ucwords($row['name']));
						$company_name = $row['company_name'];
						$email = trim($row['email']);
						$phone = trim($row['phone']);
						$lead_type = $row['lead_type'];
						$lead_sourse = $row['lead_source'];
						$requirement = $row['requirement'];
						$assigin_date = date('Y-m-d');
						$lead_month = date('m-Y');
						$address = $row['company_adress'];

					 $validate_phone = $this->customer_model->get_customer(array('customers.phone' => $phone));
		
			
		
			if($validate_phone){
				//countinue;
        $duplicat_customer[] = array($name=>$phone);

			}else{

				$data = array(	
					'adminID'     => $adminID,
					'userID'      => $userID,
					'name'        => $name,
					'company_name'=> $company_name,
					'email'       => $email,
					'phone'       => $phone,
					'address'     => $address,
				);
		
				$customerID = $this->customer_model->store_customer($data);
				if($customerID){

					$enquiry_data = array(	
						'adminID'     => $adminID,
						'clientID'    => $customerID,
						'userID'      => $userID,
						'assigned_by' => $this->session->userdata('id'),
						'lead_type'   => $lead_type,
						'lead_sourse' => $lead_sourse,
						'lead_status' => 1,
						'requirement' => $requirement,
						'assigin_date'=> $assigin_date,
						'lead_month'  => $lead_month,
					);

					$store_enquiry = $this->enquiry_model->store_enquiry($enquiry_data);

					$enquiry_history = array(	
						'adminID'     => $adminID,
						'enquiryID'   => $store_enquiry,
						'clientID'    => $customerID,
						'userID'      => $userID,
						'assigned_by' => $this->session->userdata('id'),
						'lead_type'   => $lead_type,
						'lead_sourse' => $lead_sourse,
						'lead_status' => 1,
						'requirement' => $requirement,
						'assigin_date'=> $assigin_date,
						'lead_month'  => $lead_month,
					);

					$store_enquiry_history = $this->enquiry_model->store_enquiry_history($enquiry_history);	
				}				 
					}
				}
				  
				
					echo json_encode(['status'=>200, 'message'=>'Lead Import Successfully . Send mail Please wait....','duplicat_customer'=>$duplicat_customer,'id'=>$userID]);
				
			}
		} 
	}
	else {
		echo json_encode(['status'=>403, 'message'=>'Please upload CSV']);
	}
}



	public function import_customer_new()
	{
	
		$adminID = $this->session->userdata('adminID');
		//$userID=$this->input->post('userID');
		//$assigned_by=$this->session->userdata('id');
		
		// if(empty($userID)){
		// 		echo json_encode(['status'=>403, 'message'=>'Please select user']); 	
		// 		exit();
		// }
	
		if (isset($_FILES["file"])) {
			$config['upload_path']   = "uploads/csv/";
			$config['allowed_types'] = 'text/plain|text/csv|csv';
			$config['max_size']      = '2048';
			$config['file_name']     = $_FILES["file"]['name'];
			$config['overwrite']     = TRUE;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload("file")) {
					echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
			} else {
				$file_data = $this->upload->data();
				$file_path = 'uploads/csv/' . $file_data['file_name'];
				if ($this->csvimport->get_array($file_path)) {
					$csv_array = $this->csvimport->get_array($file_path);
				 
					$validate_lead = $this->enquiry_model->get_enquiries(array('enquiry.adminID' => $adminID,'enquiry.lead_month'=>date('m-Y')));
					$user = $this->user_model->get_user(array('users.adminID' => $adminID,'user_detail.lead_permission <>'=>0));

					$total_leads = count($validate_lead)+count($csv_array);
					$extra_lead = $total_leads-$user->lead_permission;
						// if($user->lead_permission < $total_leads  && $user->lead_permission!=0){
						// 	echo json_encode(['status'=>403, 'message'=>'This month '.$extra_lead.' extra lead is not allowed']); 	
						// 	exit();
						// }

						foreach ($csv_array as $key => $value) {

							if(empty($value['name'])){
								echo json_encode(['status'=>403, 'message'=>'Please enter a name']); 	
								exit();
							}

							// if(empty($value['phone'])){
							// 	echo json_encode(['status'=>403, 'message'=>'Please enter a phone number']); 	
							// 	exit();
							// }

						}

					foreach ($csv_array as $key => $row) {
					    
					    //print_r($row);
						$name = trim(ucwords($row['name']));
						$company_name = trim($row['company_name']);
						$email = trim($row['email']);
						$phone = $row['phone'];
						$alternate_no = !empty($row['company_alt_contact']) OR  $row['company_alt_contact'] != NULL? $row['company_alt_contact'] : 0;
						$lead_type = $row['lead_type'];
						$lead_sourse = $row['lead_source'];
						$requirement = $row['requirement'];
						$assigin_date = $row['assigned_date_time'] == NULL ?date('Y-m-d',strtotime($row['added_date_time'])) : date('Y-m-d',strtotime($row['assigned_date_time']));
						$lead_month = date('m-Y',strtotime($row['assigin_date']));
						$address = $row['company_adress'];
						$create_date = date('Y-m-d H:i:',strtotime($row['added_date_time']));
						$update_date = date('Y-m-d H:i:',strtotime($row['added_date_time']));

						$userID = $row['user_id'] == 1 ? 6 : ($row['user_id'] == 3 ? 6 :($row['user_id'] == 7 ? 6 : ($row['user_id'] == 5 ? 5 :($row['user_id'] == 8 ? 5 : 3))));

						$lead_status = $row['lead_status'] == 7 ? 1 : ($row['lead_status'] == 1 ? 2 : ($row['lead_status'] == 2 ? 3 :($row['lead_status'] == 3 ? 4 :($row['lead_status'] == 4 ? 5 :($row['lead_status'] == 5 ? 6 :($row['lead_status'] == 6 ? 7 : 0)))))) ;

					 $validate_phone = $this->customer_model->get_customer(array('customers.phone' => $phone));
		
		


				$data = array(	
					'adminID'     => $adminID,
					'userID'      => $userID,
					'name'        => $name,
					'company_name'=> $company_name,
					'email'       => $email,
					'phone'       => $phone,
					'alternate_no'=> $alternate_no,
					'address'     => $address,
					'created_at'  => $create_date,
					'updated_at'  => $update_date,
				);
		
				$customerID = $this->customer_model->store_customer($data);
				if($customerID){

					$enquiry_data = array(	
						'adminID'     => $adminID,
						'clientID'    => $customerID,
						'userID'      => $userID,
						'assigned_by' => 3,
						'lead_type'   => $lead_type,
						'lead_sourse' => $lead_sourse,
						'lead_status' => $lead_status,
						'requirement' => $requirement,
						'assigin_date'=> $assigin_date,
						'lead_month'  => $lead_month,
						'created_at'  => $create_date,
						'updated_at'  => $update_date,
					);
	
					$store_enquiry = $this->enquiry_model->store_enquiry($enquiry_data);
	
					$enquiry_history = array(	
						'adminID'     => $adminID,
						'enquiryID'   => $store_enquiry,
						'clientID'    => $customerID,
						'userID'      => $userID,
						'assigned_by' => 3,
						'lead_type'   => $lead_type,
						'lead_sourse' => $lead_sourse,
						'lead_status' => $lead_status,
						'requirement' => $requirement,
						'assigin_date'=> $assigin_date,
						'lead_month'  => $lead_month,
						'created_at'  => $create_date,
						'updated_at'  => $update_date,
					);
	
					$store_enquiry_history = $this->enquiry_model->store_enquiry_history($enquiry_history);
				}				 
					
				}
					if($store_enquiry_history){
					echo json_encode(['status'=>200, 'message'=>'Lead Import Successfully']);
				} else {
					echo json_encode(['status'=>403, 'message'=>'Lead Import Failure']);
				}
			}
		} 
	}
	else {
		echo json_encode(['status'=>403, 'message'=>'Please upload CSV']);
	}
}



public function import_folloup()
{

	$adminID = $this->session->userdata('adminID');
	//$userID=$this->input->post('userID');
	//$assigned_by=$this->session->userdata('id');
	
	// if(empty($userID)){
	// 		echo json_encode(['status'=>403, 'message'=>'Please select user']); 	
	// 		exit();
	// }

	if (isset($_FILES["file"])) {
		$config['upload_path']   = "uploads/csv/";
		$config['allowed_types'] = 'text/plain|text/csv|csv';
		$config['max_size']      = '2048';
		$config['file_name']     = $_FILES["file"]['name'];
		$config['overwrite']     = TRUE;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (!$this->upload->do_upload("file")) {
				echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
		} else {
			$file_data = $this->upload->data();
			$file_path = 'uploads/csv/' . $file_data['file_name'];
			if ($this->csvimport->get_array($file_path)) {
				$csv_array = $this->csvimport->get_array($file_path);
			 

				foreach ($csv_array as $key => $row) {
						
		
					$phone = $row['company_contact'];
					$lead_type = $row['lead_type'];
					$lead_sourse = $row['lead_source'];
					$remark = $row['remark'];
					$assigin_date = $row['assigned_date_time'] == NULL ?date('Y-m-d',strtotime($row['added_date_time'])) : date('Y-m-d',strtotime($row['assigned_date_time']));
					$lead_month = date('m-Y',strtotime($row['assigin_date']));
					$create_date = $row['added_date_time'];
					$update_date = $row['added_date_time'];

					$userID = $row['user_id'] == 1 ? 6 : ($row['user_id'] == 3 ? 6 :($row['user_id'] == 7 ? 6 : ($row['user_id'] == 5 ? 5 :($row['user_id'] == 8 ? 5 : 3))));

					$lead_status = $row['lead_status'] == 7 ? 1 : ($row['lead_status'] == 1 ? 2 : ($row['lead_status'] == 2 ? 3 :($row['lead_status'] == 3 ? 4 :($row['lead_status'] == 4 ? 5 :($row['lead_status'] == 5 ? 6 :($row['lead_status'] == 6 ? 7 : 0)))))) ;

					$followup_time = date('h:i:s',strtotime($row['time']));
					$followup_date = date('Y-m-d',strtotime($row['date']));
					$today_followup_status =date('Y-m-d',strtotime($row['date']));
					$create_date = date('Y-m-d H:i:s',strtotime($row['date'].$row['time']));
					$update_date = date('Y-m-d H:i:s',strtotime($row['date'].$row['time']));

					$this->db->select('enquiry.id');
					$this->db->join('enquiry','enquiry.clientID =customers.id','left');
					$this->db->where('customers.phone',$phone);
					$customer =  $this->db->get('customers')->row();
					
				   $enquiryID = $customer->id;
          //   echo "<pre>";
					//  print_r($enquiryID);

        if(!empty($enquiryID)){
					$followup_data = array(	
						'adminID'       => $adminID,
						'enquriyID'     => $enquiryID,
						'userID'        => $userID,
						'remark'        => $remark,
						'lead_status'   => $lead_status,
						'followup_date' => $followup_date,
						'followup_time' => $followup_time,
						'today_followup_status' => $today_followup_status,
						'created_at' => $create_date,
						'updated_at' => $update_date
					);

					$followupID = $this->followup_model->store_followup($followup_data);

					$data = array(	
						'adminID'       => $adminID,
						'enquriyID'     => $enquiryID,
						'followupID'    => $followupID,
						'userID'        => $userID,
						'remark'        => $remark,
						'lead_status'   => $lead_status,
						'followup_date' => $followup_date,
						'followup_time' => $followup_time,
						'created_at'    => $create_date,
						'updated_at'    => $update_date
					);
			
					$followupHistory = $this->followup_model->store_followup_history($data);

					//$sale_page = $sale==1 ? $enquiryID : 0 ;			 
				}
			}die;
				if($store_enquiry_history){
				echo json_encode(['status'=>200, 'message'=>'Lead Import Successfully']);
			} else {
				echo json_encode(['status'=>403, 'message'=>'Lead Import Failure']);
			}
		}
	} 
}
else {
	echo json_encode(['status'=>403, 'message'=>'Please upload CSV']);
}
}


public function import_folloup_history()
{

	$adminID = $this->session->userdata('adminID');
	//$userID=$this->input->post('userID');
	//$assigned_by=$this->session->userdata('id');
	
	// if(empty($userID)){
	// 		echo json_encode(['status'=>403, 'message'=>'Please select user']); 	
	// 		exit();
	// }

	if (isset($_FILES["file"])) {
		$config['upload_path']   = "uploads/csv/";
		$config['allowed_types'] = 'text/plain|text/csv|csv';
		$config['max_size']      = '2048';
		$config['file_name']     = $_FILES["file"]['name'];
		$config['overwrite']     = TRUE;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (!$this->upload->do_upload("file")) {
				echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
		} else {
			$file_data = $this->upload->data();
			$file_path = 'uploads/csv/' . $file_data['file_name'];
			if ($this->csvimport->get_array($file_path)) {
				$csv_array = $this->csvimport->get_array($file_path);
			 

				foreach ($csv_array as $key => $row) {
						
		
					$phone = $row['company_contact'];
					$lead_type = $row['lead_type'];
					$lead_sourse = $row['lead_source'];
					$remark = $row['remark'];
					$assigin_date = $row['assigned_date_time'] == NULL ?date('d-m-Y',strtotime($row['added_date_time'])) : date('d-m-Y',strtotime($row['assigned_date_time']));
					$lead_month = date('m-Y',strtotime($row['assigin_date']));
					$create_date = $row['added_date_time'];
					$update_date = $row['added_date_time'];

					$userID = $row['user_id'] == 1 ? 6 : ($row['user_id'] == 3 ? 6 :($row['user_id'] == 7 ? 6 : ($row['user_id'] == 5 ? 5 :($row['user_id'] == 8 ? 5 : 3))));

					$lead_status = $row['lead_status'] == 7 ? 1 : ($row['lead_status'] == 1 ? 2 : ($row['lead_status'] == 2 ? 3 :($row['lead_status'] == 3 ? 4 :($row['lead_status'] == 4 ? 5 :($row['lead_status'] == 5 ? 6 :($row['lead_status'] == 6 ? 7 : 0)))))) ;

					$followup_time = date('h:i:s',strtotime($row['time']));
					$followup_date = date('Y-m-d',strtotime($row['date']));
					$today_followup_status =date('d-m-Y',strtotime($row['date']));
					$create_date = date('Y-m-d H:i:s',strtotime($row['date'].$row['time']));
					$update_date = date('Y-m-d H:i:s',strtotime($row['date'].$row['time']));

					$this->db->select('enquiry.id,followup.id as followupID');
					$this->db->join('enquiry','enquiry.clientID =customers.id','left');
					$this->db->join('followup','followup.enquriyID =enquiry.id','left');
					$this->db->where('customers.phone',$phone);
					$customer =  $this->db->get('customers')->row();
					
				   $enquiryID = $customer->id;
					 $followupID = $customer->followupID;
          //   echo "<pre>";
					//  print_r("EnquiryID - ".$enquiryID." FollowupID- ".$customer->followupID);

        if(!empty($enquiryID) && !empty($followupID)){

					$data = array(	
						'adminID'       => $adminID,
						'enquriyID'     => $enquiryID,
						'followupID'    => $followupID,
						'userID'        => $userID,
						'remark'        => $remark,
						'lead_status'   => $lead_status,
						'followup_date' => $followup_date,
						'followup_time' => $followup_time,
						'created_at'    => $create_date,
						'updated_at'    => $update_date
					);
			
					$followupHistory = $this->followup_model->store_followup_history($data);

					//$sale_page = $sale==1 ? $enquiryID : 0 ;			 
				}
			}die;
				if($store_enquiry_history){
				echo json_encode(['status'=>200, 'message'=>'Lead Import Successfully']);
			} else {
				echo json_encode(['status'=>403, 'message'=>'Lead Import Failure']);
			}
		}
	} 
}
else {
	echo json_encode(['status'=>403, 'message'=>'Please upload CSV']);
}
}



public function import_sale()
	{
	
		$adminID = $this->session->userdata('adminID');
		// $userID=$this->input->post('userID');
		// $assigned_by=$this->session->userdata('id');
		
		// if(empty($userID)){
		// 		echo json_encode(['status'=>403, 'message'=>'Please select user']); 	
		// 		exit();
		// }
	
		if (isset($_FILES["file"])) {
			$config['upload_path']   = "uploads/csv/";
			$config['allowed_types'] = 'text/plain|text/csv|csv';
			$config['max_size']      = '2048';
			$config['file_name']     = $_FILES["file"]['name'];
			$config['overwrite']     = TRUE;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload("file")) {
					echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
			} else {
				$file_data = $this->upload->data();
				$file_path = 'uploads/csv/' . $file_data['file_name'];
				if ($this->csvimport->get_array($file_path)) {
					$csv_array = $this->csvimport->get_array($file_path);
				 
					$validate_lead = $this->enquiry_model->get_enquiries(array('enquiry.adminID' => $adminID,'enquiry.lead_month'=>date('m-Y')));
					$user = $this->user_model->get_user(array('users.adminID' => $adminID,'user_detail.lead_permission <>'=>0));

					$total_leads = count($validate_lead)+count($csv_array);
					$extra_lead = $total_leads-$user->lead_permission;
						// if($user->lead_permission < $total_leads  && $user->lead_permission!=0){
						// 	echo json_encode(['status'=>403, 'message'=>'This month '.$extra_lead.' extra lead is not allowed']); 	
						// 	exit();
						// }

				// 		foreach ($csv_array as $key => $value) {

				// 			if(empty($value['name'])){
				// 				echo json_encode(['status'=>403, 'message'=>'Please enter a name']); 	
				// 				exit();
				// 			}

				// 			if(empty($value['phone'])){
				// 				echo json_encode(['status'=>403, 'message'=>'Please enter a phone number']); 	
				// 				exit();
				// 			}

				// 		}

					foreach ($csv_array as $key => $row) {
					    //echo "<pre>";
					    //print_r($row);
					    $this->db->select('enquiry.id');
					    $this->db->join('enquiry','enquiry.clientID =customers.id','left');
					    $this->db->where('customers.phone',$row['company_contact']);
					    $customer =  $this->db->get('customers')->row();
					    $userID = $row['user_id'] == 1 ? 3 : ($row['user_id'] == 3 ? 6 :($row['user_id'] == 7 ? 6 : ($row['user_id'] == 5 ? 5 :($row['user_id'] == 8 ? 5 : 3))));
					   $enquiryID = $customer->id;
						 $orderID = $row['order_id'];
					  
					   //print_r($enquiryID);
					   if(!empty($enquiryID)){
					       
					       $gst = $row['gst_applied'];
								 $gst_amount = !empty($row['gst_amount']) ? $row['gst_amount'] : 0;
					       $sale_amount = $row['total_order_value']-$gst_amount;
					       $paid_amount = $row['advance_payment'];
					       $pending_amount = $row['pending_payment'];
					       $payment_mode = $row['payment_mode'];
					       $sale_date = date('Y-m-d',strtotime($row['sale_date']));
								 $sale_month = date('m-Y',strtotime($row['sale_date']));
					       $content_date =  date('Y-m-d',strtotime($row['content_date']));
								 $delivery_date = date('Y-m-d',strtotime($row['delivery_date']));
					       $product = $row['product'];
					       
		                $data = array(	
						'orderID'        => $orderID,
						'adminID'        => $adminID,
						'enquiryID'      => $enquiryID,
						'sale_closed_by' => $userID,
						'sale_create_by' => $userID,
						'website'        => $row['website'],
						'product'        => $product,
						'gst'            => $gst,
						'sale_amount'    => $sale_amount,
						'gst_amount'     => $gst_amount,
						'advance_amount' => $paid_amount,
						'pending_amount' => $pending_amount,
						'total_amount'   => $row['total_order_value'],
						'payment_mode'   => $payment_mode,
						'sale_date'      => $sale_date,
						'sale_month'     => $sale_month,
						'delivery_date'  => $delivery_date,
						'content_date'   => $content_date,
						'created_at'     => date('Y-m-d H:i:s',strtotime($row['added_date'])),
						'updated_at'     => date('Y-m-d H:i:s',strtotime($row['added_date'])),
					);

					$store_sale = $this->sale_model->store_sale($data);
					if($store_sale){
						$payment_data = array(	
						'adminID'      => $adminID,
						'saleID'       => $store_sale,
						'userID'       => $row['added_by'],
						'amount'       => $paid_amount,
						'payment_mode' => $payment_mode,
						'created_at'   => date('Y-m-d H:i:s',strtotime($row['added_date'])),
						'updated_at'   => date('Y-m-d H:i:s',strtotime($row['added_date'])),
						);
						//$store_payment = $this->sale_model->store_payment_history($payment_data);

						$sale_history_data = array(	
							'adminID'        => $adminID,
							'saleID'         => $store_sale,
							'enquiryID'      => $enquiryID,
							'sale_closed_by' => $userID,
							'sale_create_by' => $userID,
							'website'        => $row['website'],
							'product'        => $product,
							'gst'            => $gst,
							'sale_amount'    => $sale_amount,
							'gst_amount'     => $gst_amount,
							'advance_amount' => $paid_amount,
							'pending_amount' => $pending_amount,
							'total_amount'   => $row['total_order_value'],
							'payment_mode'   => $payment_mode,
							'sale_date'      => $sale_date,
							'sale_month'     => $sale_month,
							'delivery_date'  => $delivery_date,
							'content_date'   => $content_date,
							'created_at'     => date('Y-m-d H:i:s',strtotime($row['added_date'])),
						  'updated_at'     => date('Y-m-d H:i:s',strtotime($row['added_date'])),
						);
						$store_sale_history = $this->sale_model->store_sale_history($sale_history_data);

						$enquiry_data = array(
							'sale' =>1
						);

						$update_enquiry = $this->enquiry_model->update_enquiry($enquiry_data,array('id'=>$enquiryID));

						$followup_data = array(
							'status' =>1
						);

						$update_followup = $this->followup_model->update_followup($followup_data,array('enquriyID'=>$enquiryID));
				
				}
			
			}
		} die;
	}

}
}
}


public function import_payment_history()
	{
	
		$adminID = $this->session->userdata('adminID');
		// $userID=$this->input->post('userID');
		// $assigned_by=$this->session->userdata('id');
		
		// if(empty($userID)){
		// 		echo json_encode(['status'=>403, 'message'=>'Please select user']); 	
		// 		exit();
		// }
	
		if (isset($_FILES["file"])) {
			$config['upload_path']   = "uploads/csv/";
			$config['allowed_types'] = 'text/plain|text/csv|csv';
			$config['max_size']      = '2048';
			$config['file_name']     = $_FILES["file"]['name'];
			$config['overwrite']     = TRUE;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload("file")) {
					echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
			} else {
				$file_data = $this->upload->data();
				$file_path = 'uploads/csv/' . $file_data['file_name'];
				if ($this->csvimport->get_array($file_path)) {
					$csv_array = $this->csvimport->get_array($file_path);
				 
					$validate_lead = $this->enquiry_model->get_enquiries(array('enquiry.adminID' => $adminID,'enquiry.lead_month'=>date('m-Y')));
					$user = $this->user_model->get_user(array('users.adminID' => $adminID,'user_detail.lead_permission <>'=>0));

					$total_leads = count($validate_lead)+count($csv_array);
					$extra_lead = $total_leads-$user->lead_permission;


					foreach ($csv_array as $key => $row) {
					    //echo "<pre>";
					    //print_r($row);
					    $this->db->select('enquiry.id,sales.id as saleID');
					    $this->db->join('enquiry','enquiry.clientID =customers.id','left');
							$this->db->join('sales','sales.enquiryID = enquiry.id','left');
					    $this->db->where('customers.phone',$row['company_contact']);
					    $customer =  $this->db->get('customers')->row();
					    $userID = $row['user_id'] == 1 ? 3 : ($row['user_id'] == 3 ? 6 :($row['user_id'] == 7 ? 6 : ($row['user_id'] == 5 ? 5 :($row['user_id'] == 8 ? 5 : 3))));
					   $enquiryID = $customer->id;
					  $saleID = $customer->saleID;
						// echo "<pre>";
					  //  print_r("EnquiryID - ".$enquiryID." saleID- ".$saleID);
					   if(!empty($enquiryID)){
					       
					       $remark = $row['remark'];
								 $payment_mode = $row['payment_mode'];
								 $paid_amount = $row['paid_amount'];
					       

					if(!empty($enquiryID) && !empty($saleID)){
						$payment_data = array(	
						'adminID'      => $adminID,
						'saleID'       => $saleID,
						'userID'       => $userID,
						'amount'       => $paid_amount,
						'payment_mode' => $payment_mode,
						'remark'       => $remark,
						'created_at'   => date('Y-m-d H:i:s',strtotime($row['added_date'])),
						'updated_at'   => date('Y-m-d H:i:s',strtotime($row['added_date'])),
						);
					
						$store_payment = $this->sale_model->store_payment_history($payment_data);
						
				
				}
			
			}
		} die;
	}

}
}
}

public function setSessionCustomer(){
		
	$monthname_customer = $this->input->post('monthname_customer');
	$assign_user_customer = $this->input->post('assign_user_customer');
	$customer_from_date = $this->input->post('customer_from_date');
	$customer_to_date = $this->input->post('customer_to_date');

	$session = array(
			'monthname_customer'   => $monthname_customer,
			'assign_user_customer' => $assign_user_customer,
			'customer_from_date'   => $customer_from_date,
			'customer_to_date'     => $customer_to_date,
			);
			
			$this->session->set_userdata($session);
			
			redirect(base_url('customers'));
			
}

public function resetFromDateCustomer(){
	$this->session->unset_userdata('customer_from_date');
	$this->session->unset_userdata('customer_to_date');
}

public function resetToDateCustomer(){
	$this->session->unset_userdata('customer_to_date');
}

public function resetMonthNameCustomer(){
	$this->session->unset_userdata('monthname_customer');
}

public function resetAssignUserCustomer(){
	$this->session->unset_userdata('assign_user_customer');
}

public function update_query(){
	$query = $this->db->get('followup_history')->result();

	foreach($query as $row){
		// echo "<pre>";
		// print_r($row);
		$data = array(
			'followup_date' => date('Y-m-d',strtotime($row->followup_date1)),
			'followup_time'   => date('H:i:s',strtotime($row->followup_time1)),
		);
		$this->db->where('id',$row->id);
		$this->db->update('followup_history',$data);
	}
}
	
}