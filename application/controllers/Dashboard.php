<?php 
class Dashboard extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
		$this->load->model('auth_model');
		$this->load->model('dashboard_model');
		$this->load->model('user_model');
	}

	public function index()
	{	 

		$data['uri'] ='dashboard';
		$permission = permission($data['uri']);
		$data['user_permission'] = user_role_permission($this->session->userdata('user_type'));
		
		$data['page_title'] = 'Dashboard';
		$condition = $this->session->userdata('user_type') == 2 ? array('users.adminID'=>$this->session->userdata('adminID')) : array('users.id'=>$this->session->userdata('id'),'users.adminID'=>$this->session->userdata('adminID'));
	
		$sales= $this->dashboard_model->get_sale_report($condition);
		

		$received_amounts= $this->dashboard_model->get_sale_received_amount($condition);



		$pending_condition = $this->session->userdata('user_type') == 2 ? array('sales.adminID'=>$this->session->userdata('adminID'),'DATE_FORMAT(sales.sale_date,"%Y-%m")'=>date('Y-m')) : array('sales.sale_closed_by'=>$this->session->userdata('id'),'sales.adminID'=>$this->session->userdata('adminID'),'DATE_FORMAT(sales.sale_date,"%Y-%m")'=>date('Y-m'));

		$pending_amount = $this->dashboard_model->get_pending_amount($pending_condition);
		$data['pending_amount'] = $pending_amount;

		$total_pending_condition = $this->session->userdata('user_type') == 2 ? array('sales.adminID'=>$this->session->userdata('adminID')) : array('sales.sale_closed_by'=>$this->session->userdata('id'),'sales.adminID'=>$this->session->userdata('adminID'));

		$total_pending_amount = $this->dashboard_model->get_total_pending_amount($total_pending_condition);

		$data['total_pending_amount'] = $total_pending_amount;

    $total_sale = 0; 
		$sale_amount = 0;
		$receivedAmount = 0;
		//print_r($sales);die;
		foreach($sales as $sale){
			$total_sale += $sale['object']->totalSales;
			$sale_amount += $sale['object']->totalSalesAmount;
			
		}

		$data['total_sale'] = $total_sale;

		$data['sale_amount'] = $sale_amount;
    
		foreach($received_amounts as $received){
			$receivedAmount += $received['object']->receivedAmount;
		}
		$data['receivedAmount'] = $receivedAmount;


		$users = array();
		$target = 0;
		$target_condition = $this->session->userdata('user_type') == 2 ? array('users.status'=>1,'users.adminID'=>$this->session->userdata('adminID')) : array('users.id'=>$this->session->userdata('id'),'users.adminID'=>$this->session->userdata('adminID'));
		$monthly_target = $this->user_model->get_users($target_condition);

			 foreach($monthly_target as $targets){
				$target +=$targets->monthly_target;
				if(!empty($targets->monthly_target)){
					$users[$targets->id] = $targets->name;
				}
			 }

		$data['traget_percentage'] = !empty($target) ? round(($sale_amount*100)/$target) : 0;
	
		$data['pending_amount_percentage'] = !empty($receivedAmount) && !empty($pending_amount->totalPending) ? ($receivedAmount*100)/$pending_amount->totalPending : 0;

		$data['this_month_sale_amount'] = $this->this_month_sale_amount($target,$sale_amount,$data['permission']);

		$data['this_month_recieve_amount'] = $this->this_month_recieve_amount($target,$receivedAmount,$data['permission']);

		$data['user_monthly_performance'] = $this->user_monthly_performance($target,$sale_amount,$receivedAmount,$data['permission']);

		$data['year_sale_performance'] = $this->year_sale_performance($target,$sale_amount,$receivedAmount,$data['permission']);

		$data['monthly_flow_chart'] = $this->monthly_flow_chart($target,$data['permission']);

		$data['user_wise_monthly_performance'] = $this->user_wise_monthly_performance($data['permission']);

		$data['user_wise_yearly_performance'] = $this->user_wise_yearly_performance($data['permission']);

	  $this->admin_template('dashboard',$data);
		
	}

	public function this_month_sale_amount($target,$sale_amount,$permission){
		$this_month_sale_amount = "['Total Target[₹ $target]',$target],['Sale Amount [₹ $sale_amount]',$sale_amount],";
		return $this_month_sale_amount;
	}

	public function this_month_recieve_amount($target,$receivedAmount,$permission){

		$this_month_recieve_amount = "['Total Target[₹ $target]',$target],['Recieved Amount [₹ $receivedAmount]',$receivedAmount],";
		return  $this_month_recieve_amount;

	}


	public function user_monthly_performance($target,$sale_amount,$receivedAmount,$permission){
		$current_month = strtoupper(date('F'));
		$user_monthly_performance[] = "['".$current_month."',$target,$sale_amount,$receivedAmount],";
		return  $user_monthly_performance;
	}

	public function year_sale_performance($target,$sale_amount,$receivedAmount,$permission){
		$cmonth = date('m');

		$year = $cmonth >= 4 ? date('Y') : date('Y')-1;

		$max_year = $year+1;

		$current_year = $year.' - '.$max_year;

		$begin = new DateTime( $year.'-04 ');
	$end = new DateTime( $year.'-'.$cmonth);
	$end = $end->modify( '+1 month' );

	$interval = DateInterval::createFromDateString('1 month');

	$period = new DatePeriod($begin, $interval, $end);
	$counter = 0;
	foreach($period as $dt) {
			$counter++;
	}

		$condition = $this->session->userdata('user_type') == 2 ? array('users.adminID'=>$this->session->userdata('adminID')) : array('users.id'=>$this->session->userdata('id'),'users.adminID'=>$this->session->userdata('adminID'));

		$monthly_sale = $this->dashboard_model->yearly_sale_report($condition,'single_user');
		//print_r($monthly_sale);die;
		$monthly_payment = $this->dashboard_model->yearly_sale_amount_received($condition,'single_user');
		 $financetarget = $target;
		$financesale = $monthly_sale;
		$financepayment = $monthly_payment;
   
		$finance_report = array();

		$monthly_sale =array();
		$payment = 0;
		$monthly_sale_amount = 0;
		$fmonthname = array();

		foreach($financesale as $sale){
		 $monthly_sale_amount +=$sale['object']->totalSalesAmount;
		}

		foreach($financepayment as $paymentsale){
	  	$payment+=$paymentsale['object']->receivedAmount;
		}
		
		$target = 0;
	   for($i=0; $i<$counter ; $i++) {
			 $target += $financetarget;
		 }

		$year_sale_performance[] = "['".$current_year."',$target,$monthly_sale_amount,$payment],";
		return  $year_sale_performance;

	}

	public function monthly_flow_chart($target,$permission){
		$month = date('m');
		$mont=array();
		if($month >= 4)
		{
			$y = date('Y');
			$pt = date('Y', strtotime('+1 year'));
			$sfy = $y."-04-01";
			$efy = $pt."-03-31";
		}
		else
		{
			$y = date('Y', strtotime('-1 year'));
			$pt = date('Y');
			$sfy=$y."-04-01";
			$efy =$pt."-03-31";
		}

		$start    = new DateTime($sfy);
		$end     = new DateTime($efy);
		$interval = DateInterval::createFromDateString('1 month');
		$period   = new DatePeriod($start, $interval, $end);
		//print_r($period);die;
		foreach ($period as $dt) {
		  $mont[]= $dt->format("m-Y");
		}

		$data['monthly_sale'] = $this->dashboard_model->get_financial_report_monthly('monthly_sale');
		$data['monthly_payment'] = $this->dashboard_model->get_financial_report_monthly('recievedAmount');
		$financetarget = $target;
		$financesale = $data['monthly_sale'];
		$financepayment = $data['monthly_payment'];
    // echo "<pre>";
		// print_r($financesale);

		// print_r($financepayment);
		
		// die;
   
		$finance_report = array();

		$monthly_sale =array();
		$payment = array();
		$monthly_sale_amount =array();
		$fmonthname = array();
		//$months = array('Apr'=>04,'May'=>05,'Jun'=>05,'Jul'=>06,'Aug'=>07,'Sep'=>08,'Oct'=>09);
		foreach($financetarget as $target){
		$target +=$target['target'];
		}
		
		foreach($financesale as $sale){
	
		$monthly_sale_amount[date('m-Y',strtotime($sale['sale_date']))]=$sale['saleamount'];
		$monthly_sale[] = date('m-Y',strtotime($sale['sale_date']));
		}
		//print_r($monthly_sale_amount);die;
			$pay_date = array();
		foreach($financepayment as $paymentsale){
		$payment[date('m-Y',strtotime($paymentsale['created_at']))]=$paymentsale['RecieveAmount'];
		$pay_date[] = date('m-Y',strtotime($paymentsale['created_at']));
		}
		//print_r($payment);die;
		foreach($mont as $monthname){
		//echo '<pre>';
		//$teamtargetbymonth = $this->dashboard_model->team_monthly_target($monthname);

		$total_sales = in_array($monthname,$monthly_sale) ? $monthly_sale_amount[$monthname] : 0;
	
		$total_rec = in_array($monthname,$pay_date)?$payment[$monthname]:0;
		$fmonthname = date('M',strtotime('1-'.$monthname));
		$finance_report[]= '["'.$fmonthname.'", '.$target.','. $total_sales.', '.$total_rec.'],';
		}

  return  $monthly_flow_chart = $finance_report;

		//print_r($monthly_flow_chart);die;

	}



	public function user_wise_monthly_performance($permission){
	$role = role();
	$usersID = user_role_permission($this->session->userdata('user_type'));

	$monthly_target =array();
	$users = array();

	$this->db->select('users.*,user_detail.monthly_target,user_detail.monthly_target_date');
	$this->db->from('users');
	$this->db->join('user_detail','users.id = user_detail.userID','left');
	$this->db->where('users.adminID',$this->session->userdata('adminID'));
	if($this->session->userdata('user_type')!=2){
		if(!empty($usersID)){
			$this->db->where_in('users.id',$usersID );
		}
	}
	$query = $this->db->get()->result();
	$target = 0;
	foreach($query as $row){
			$target +=$row->monthly_target;
			if(!empty($row->monthly_target)){
				$users[$row->id] = $row->name;
			}
	}

	// if($this->session->userdata('user_type') == 2 OR count($usersID) > 1){
	// 	$monthly_target = $this->user_model->get_users(array('users.status'=>1,'users.adminID'=>$this->session->userdata('adminID')));
		
	// 	$target = 0;
	// 	 foreach($monthly_target as $targets){
			
	// 		$target +=$targets->monthly_target;
	// 		if(!empty($targets->monthly_target)){
	// 			$users[$targets->id] = $targets->name;
	// 		}
			
	// 	 }

	// 		$target_month = !empty($data['sales']->totalPayments) ? round(($data['sales']->totalPayments*100)/$target) : 0;

	// }

	// else{
	// 	$monthly_target = $this->user_model->get_user(array('users.id'=>$this->session->userdata('id'),'users.adminID'=>$this->session->userdata('adminID')));
	// 	$target = $monthly_target->monthly_target;
	// 	$target_month = !empty($data['sales']->totalPayments) ? round(($data['sales']->totalPayments*100)/$target) : 0;

	// 		 $users[$monthly_target->id] = $monthly_target->name;
	// }


	$condition =array('users.adminID'=>$this->session->userdata('adminID'));

	$sales = $this->dashboard_model->get_sale_reports_user_wise($condition);

	$recieved_amount = $this->dashboard_model->get_pending_amount_recieve($condition);
	$sale_amount=array();
	$amount_recieve =array();

	foreach($sales as $sale){
		$sale_amount[$sale['user_id']] = !empty($sale['object']->totalSalesAmount) ? $sale['object']->totalSalesAmount : 0 ;
		
	}
  
	foreach($recieved_amount as $recieved){
		$amount_recieve[$recieved['user_id']] += !empty($recieved['object']->receivedAmount) ? $recieved['object']->receivedAmount : 0 ;
	}

	foreach($users as $key=>$user){

		$monthly_target = $this->user_model->get_user(array('users.id'=>$key,'users.adminID'=>$this->session->userdata('adminID')));
		$target = $monthly_target->monthly_target;
	
		$link = base_url('payment-history');
		$user_wise_monthly_performance[] = "['$user', $target, $sale_amount[$key],$amount_recieve[$key],'$link'],";
	}
	return  $user_wise_monthly_performance;
}


public function user_wise_yearly_performance($permission){
	$cmonth = date('m');

		$year = $cmonth >= 4 ? date('Y') : date('Y')-1;

		$max_year = $year+1;

		$current_year = $year.' - '.$max_year;
		$begin = new DateTime( $year.'-04 ');
	$end = new DateTime( $year.'-'.$cmonth);
	$end = $end->modify( '+1 month' );

	$interval = DateInterval::createFromDateString('1 month');

	$period = new DatePeriod($begin, $interval, $end);
	$counter = 0;
	foreach($period as $dt) {
			$counter++;
	}
	$role = role();
	$usersID = user_role_permission($this->session->userdata('user_type'));

	$monthly_target =array();
	$users = array();

	$this->db->select('users.*,user_detail.monthly_target,user_detail.monthly_target_date');
	$this->db->from('users');
	$this->db->join('user_detail','users.id = user_detail.userID','left');
	$this->db->where('users.adminID',$this->session->userdata('adminID'));
	if($this->session->userdata('user_type')!=2){
		if(!empty($usersID)){
			$this->db->where_in('users.id',$usersID );
		}
	}
	$query = $this->db->get()->result();
	$target = 0;
	foreach($query as $row){
			$target +=$row->monthly_target;
			if(!empty($row->monthly_target)){
				$users[$row->id] = $row->name;
			}
	}

	// if($this->session->userdata('user_type') == 2 OR count($usersID) > 1){
	// 	$monthly_target = $this->user_model->get_users(array('users.status'=>1,'users.adminID'=>$this->session->userdata('adminID')));
		
	// 	$target = 0;
	// 	 foreach($monthly_target as $targets){
			
	// 		$target +=$targets->monthly_target;
	// 		if(!empty($targets->monthly_target)){
	// 			$users[$targets->id] = $targets->name;
	// 		}
			
	// 	 }

	// 		$target_month = !empty($data['sales']->totalPayments) ? round(($data['sales']->totalPayments*100)/$target) : 0;

	// }

	// else{
	// 	$monthly_target = $this->user_model->get_user(array('users.id'=>$this->session->userdata('id'),'users.adminID'=>$this->session->userdata('adminID')));
	// 	$target = $monthly_target->monthly_target;
	// 	$target_month = !empty($data['sales']->totalPayments) ? round(($data['sales']->totalPayments*100)/$target) : 0;

	// 		 $users[$monthly_target->id] = $monthly_target->name;
	// }


	$condition =array('users.adminID'=>$this->session->userdata('adminID'));

	$sales = $this->dashboard_model->yearly_sale_report($condition,'All Users');
		//print_r($monthly_sale);die;
	$recieved_amount = $this->dashboard_model->yearly_sale_amount_received($condition,'All Users');

	$sale_amount=array();
	$amount_recieve =array();

	foreach($sales as $sale){
		$sale_amount[$sale['user_id']] = !empty($sale['object']->totalSalesAmount) ? $sale['object']->totalSalesAmount : 0 ;
		
	}
  
	foreach($recieved_amount as $recieved){
		$amount_recieve[$recieved['user_id']] += !empty($recieved['object']->receivedAmount) ? $recieved['object']->receivedAmount : 0 ;
	}
 
	


	foreach($users as $key=>$user){

		$monthly_target = $this->user_model->get_user(array('users.id'=>$key,'users.adminID'=>$this->session->userdata('adminID')));
		$target1 = $monthly_target->monthly_target;
		$target = 0;
	   for($i=0; $i<$counter ; $i++) {
			 $target += $target1;
		 }
		 
		$link = base_url('payment-history');
		$user_wise_yearly_performance[] = "['$user', $target, $sale_amount[$key],$amount_recieve[$key],'$link'],";
	}
	return  $user_wise_yearly_performance;
}
	

}