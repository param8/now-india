<?php 
class Enquiry extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('customer_model');
		$this->load->model('user_model');
		$this->load->model('setting_model');
		$this->load->model('enquiry_model');
		$this->load->model('followup_model');
	}

	public function index()
	{
		$this->not_admin_logged_in();
		$data['uri'] = $this->uri->segment(1);
		$permission = permission($data['uri']);
		if($permission[0]=='View'){
			$data['permission'] = $permission;
			$data['page_title'] = 'Enquiry';
			$data['admins'] = $this->user_model->get_users(array('users.user_type'=>2,'users.status'=>1));
			$data['users'] = get_users_function(array('users.adminID'=>$this->session->userdata('adminID'),'users.status'=>1),'Sale');//$this->user_model->get_users();
		
			$data['enquires'] = $this->enquiry_model->get_enquiries(array('enquiry.adminID' =>$this->session->userdata('adminID'),'enquiry.sale'=>0));
			$data['url'] = $data['uri'];
	    $this->admin_template('enquiry/enquiry',$data);
		}else{
			redirect(base_url('dashboard'));
		}
    
	}

	public function enquiry_generated()
	{
		$this->not_admin_logged_in();
		$data['uri'] = $this->uri->segment(1);
		$permission = permission($data['uri']);
		//print_r($permission);die;
		if($permission[0]=='View'){
			$data['permission'] = $permission;
			$data['page_title'] = 'Enquiry Generated';
			$data['admins'] = $this->user_model->get_users(array('users.user_type'=>2));
			$data['users'] = get_users_function(array('users.adminID'=>$this->session->userdata('adminID'),'users.status'=>1),'Sale');//$this->user_model->get_users();
			$data['url'] = $data['uri'];
	    $this->admin_template('enquiry/enquiry-generated',$data);
		}else{
			redirect(base_url('dashboard'));
		}
    
	}


	public function re_enquires()
	{
		$this->not_admin_logged_in();
		$data['uri'] = $this->uri->segment(1);
		$permission = permission($data['uri']);
		if($permission[0]=='View'){
			$data['permission'] = $permission;
			$data['page_title'] = 'Re Enquiry';
			$data['admins'] = $this->user_model->get_users(array('users.user_type'=>2,'users.status'=>1));
			$data['users'] = get_users_function(array('users.adminID'=>$this->session->userdata('adminID'),'users.status'=>1),'Sale');//$this->user_model->get_users();
			$data['enquires'] = $this->enquiry_model->get_enquiries(array('enquiry.adminID' =>$this->session->userdata('adminID'),'enquiry.sale'=>0));
			$data['url'] = $data['uri'];
	    $this->admin_template('enquiry/re-enquires',$data);
		}else{
			redirect(base_url('dashboard'));
		}
    
	}

	public function ajaxEnquiry(){
		$this->not_admin_logged_in();
		  $data['uri'] = $this->uri->segment(3);
      $permission = permission($data['uri']);
      $role = role();
			$condition = $this->session->userdata('user_type')==1 ? array('enquiry.adminID'=>$this->session->userdata('enquiryAdminID'),'enquiry.lead_status'=>1) : 
			($this->session->userdata('user_type')==2 ? array('enquiry.status'=>1,'enquiry.adminID'=>$this->session->userdata('adminID'),'enquiry.lead_status'=>1) :
			($role->parent_role==0 && $this->session->userdata('user_type')!=2 ? array('enquiry.status'=>1,'enquiry.adminID'=>$this->session->userdata('adminID'),'enquiry.lead_status'=>1) :
			array('enquiry.status'=>1,'enquiry.adminID'=>$this->session->userdata('adminID'),'enquiry.userID'=>$this->session->userdata('id'),'enquiry.lead_status'=>1)));
		  $enquires = $this->enquiry_model->make_datatables($condition); // this will call modal function for fetching data
		  $data = array();
		//print_r($enquires);die;
		foreach($enquires as $key=>$enquiry) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
		   if($this->session->userdata('user_type')!=1){
			$button .= '<a href="'.base_url('create-followup/'.base64_encode($enquiry['id'])).'"  target="_blank" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Followup" class="btn btn-sm btn-white text-warning me-2"> Followup <i class="fa fa-arrow-circle-up"></i></a>';
			 }
   		
			$sub_array[] = $key+1;
			$this->session->userdata('user_type')!=1 ? $sub_array[] = $button : '';
			
			$sub_array[] = $enquiry['customerName'];
			$sub_array[] = $enquiry['customerPhone'];
			$sub_array[] = date('d-M-Y', strtotime($enquiry['assigin_date']));
			$sub_array[] = $enquiry['lead_type'];
			$sub_array[] = $enquiry['lead_sourse'];
			$sub_array[] = '<span class="btn btn-outline-info">'.$enquiry['leadStatus'].'</span>';
			$sub_array[] = $enquiry['assigin_to_name'];
			$sub_array[] = $enquiry['assigin_by_name'];
			$sub_array[] = date('d-M-Y', strtotime($enquiry['created_at']));
		
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->enquiry_model->get_all_data($condition),
			"recordsFiltered"         =>     $this->enquiry_model->get_filtered_data($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	  }


		public function ajaxReEnquiry(){
			$this->not_admin_logged_in();
				$data['uri'] = $this->uri->segment(3);
				$permission = permission($data['uri']);
				$role = role();
				$condition = $this->session->userdata('user_type')==1 ? array('enquiry.adminID'=>$this->session->userdata('enquiryAdminID'),'enquiry.re_enquiry_status'=>1) : 
				($this->session->userdata('user_type')==2 ? array('enquiry.status'=>1,'enquiry.adminID'=>$this->session->userdata('adminID'),'enquiry.re_enquiry_status'=>1) :
				($role->parent_role==0 && $this->session->userdata('user_type')!=2 ? array('enquiry.status'=>1,'enquiry.adminID'=>$this->session->userdata('adminID'),'enquiry.re_enquiry_status'=>1) :
				array('enquiry.status'=>1,'enquiry.adminID'=>$this->session->userdata('adminID'),'enquiry.userID'=>$this->session->userdata('id'),'enquiry.re_enquiry_status'=>1)));
			$enquires = $this->enquiry_model->make_datatables($condition); // this will call modal function for fetching data
			$data = array();
			//print_r($enquires);die;
			foreach($enquires as $key=>$enquiry) // Loop over the data fetched and store them in array
			{
				$button = '';
				$sub_array = array();
				 if($this->session->userdata('user_type')!=1){
				$button .= '<a href="'.base_url('create-followup/'.base64_encode($enquiry['id'])).'"  target="_blank" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Followup" class="btn btn-sm btn-white text-warning me-2"> Followup <i class="fa fa-arrow-circle-up"></i></a>';
				 }
				 
				$sub_array[] = $key+1;
				$this->session->userdata('user_type')!=1 ? $sub_array[] = $button : '';
				
				$sub_array[] = $enquiry['customerName'];
				$sub_array[] = $enquiry['customerPhone'];
				$sub_array[] = date('d-M-Y', strtotime($enquiry['assigin_date']));
				$sub_array[] = $enquiry['lead_type'];
				$sub_array[] = $enquiry['lead_sourse'];
				$sub_array[] = '<span class="btn btn-outline-info">'.$enquiry['leadStatus'].'</span>';
				$sub_array[] = $enquiry['assigin_to_name'];
				$sub_array[] = $enquiry['assigin_by_name'];
				$sub_array[] = date('d-M-Y', strtotime($enquiry['created_at']));
			
				$data[] = $sub_array;
			}
		
			$output = array(
				"draw"                    =>     intval($_POST["draw"]),
				"recordsTotal"            =>     $this->enquiry_model->get_all_data($condition),
				"recordsFiltered"         =>     $this->enquiry_model->get_filtered_data($condition),
				"data"                    =>     $data
			);
			
			echo json_encode($output);
			}

		public function ajaxAssiginEnquiry(){
			$this->not_admin_logged_in();
			$data['uri'] = $this->uri->segment(3);
			$permission = permission($data['uri']);
				
			$condition = array('enquiry.adminID' =>$this->session->userdata('adminID'),'enquiry.sale'=>0);
			$enquires = $this->enquiry_model->make_datatables_assigin_enquiry($condition); // this will call modal function for fetching data
			$data = array();
			//print_r($enquires);die;
			$leads_color = leads_color();
			foreach($enquires as $key=>$enquiry) // Loop over the data fetched and store them in array
			{
				$button = '';
				$sub_array = array();
			
				$button .= '<input type="checkbox" class="chk_cls" name="assigin[]" id="assigin'.$enquiry['id'].'" value="'.$enquiry['id'].'">';
				 
				$sub_array[] = $key+1;
				$sub_array[] = $button;
				$sub_array[] = $enquiry['assigin_to_name'];
				$sub_array[] = '<span class="btn '.$leads_color[$enquiry['lead_status']].' font-weight-bold">'.$enquiry['leadStatus'].'</span>';
				$sub_array[] = $enquiry['customerName'];
				$sub_array[] = $enquiry['customerPhone'];
				$sub_array[] = $enquiry['requirement'];
				$sub_array[] = date('d-M-Y',strtotime($enquiry['created_at']));
			
				$data[] = $sub_array;
			}
		
			$output = array(
				"draw"                    =>     intval($_POST["draw"]),
				"recordsTotal"            =>     $this->enquiry_model->get_all_data_assigin_enquiry($condition),
				"recordsFiltered"         =>     $this->enquiry_model->get_filtered_data_assigin_enquiry($condition),
				"data"                    =>     $data
			);
			
			echo json_encode($output);
			}
	

		public function ajaxEnquiryGenerated(){
			$this->not_admin_logged_in();
		 	$data['uri'] = $this->uri->segment(3);
			$permission = permission($data['uri']);
			$role = role();
			$condition = $this->session->userdata('user_type')==1 ? array('enquiry_history.adminID'=>$this->session->userdata('enquiryAdminID'),'enquiry_history.lead_status<>'=>1) : 
			($this->session->userdata('user_type')==2 ? array('enquiry_history.status'=>1,'enquiry_history.adminID'=>$this->session->userdata('adminID'),'enquiry_history.lead_status<>'=>1) :
			($role->parent_role==0 && $this->session->userdata('user_type')!=2 ? array('enquiry_history.status'=>1,'enquiry_history.adminID'=>$this->session->userdata('adminID'),'enquiry_history.lead_status<>'=>1) :
			array('enquiry_history.status'=>1,'enquiry_history.adminID'=>$this->session->userdata('adminID'),'enquiry_history.userID'=>$this->session->userdata('id'),'enquiry_history.lead_status<>'=>1)));
			$enquires = $this->enquiry_model->make_datatables_history($condition); // this will call modal function for fetching data
			$data = array();
			//print_r($enquires);die;
			$leads_color = leads_color();
			foreach($enquires as $key=>$enquiry) // Loop over the data fetched and store them in array
			{
				$button = '';
				$sub_array = array();
			
				$button .= '<a href="'.base_url('create-followup/'.base64_encode($enquiry['id'])).'"  target="_blank" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View customer Detail" class="btn btn-warning btn-sm"> Followup <i class="fa fa-arrow-up"></i></a>';
				$key = $key+1;
				$sub_array[] = $key;
				// $sub_array[] = $button;
				$sub_array[] = '<a href="create-followup/'.base64_encode($enquiry['id']).'">'.$enquiry['assigin_to_name'].'</a>';
				$sub_array[] = $enquiry['customerName'];
				$sub_array[] = $enquiry['customerPhone'];
				$sub_array[] = date('d-M-Y', strtotime($enquiry['assigin_date']));
				$sub_array[] = '<span class="btn '.$leads_color[$enquiry['lead_status']].' font-weight-bold">'.$enquiry['leadStatus'].'</span>';
				$sub_array[] = $enquiry['lead_type'];
				$sub_array[] = $enquiry['lead_sourse'];
				$sub_array[] = $enquiry['assigin_by_name'];
				$sub_array[] = date('d-M-Y', strtotime($enquiry['created_at']));
				
			
				$data[] = $sub_array;
			}

			$output = array(
				"draw"                    =>     intval($_POST["draw"]),
				"recordsTotal"            =>     $this->enquiry_model->get_all_data_history($condition),
				"recordsFiltered"         =>     $this->enquiry_model->get_filtered_data_history($condition),
				"data"                    =>     $data
			);
			
			echo json_encode($output);
			}


  public function assigin_enquiry(){
		$adminID = $this->session->userdata('adminID');
		$userID = $this->input->post('userID');
		$assigins = $this->input->post('assigin');

		if(empty($userID)){
			echo json_encode(['status'=>403, 'message'=>'Please select user ']); 	
			exit();
		}

		if(count($assigins) > 0){

			foreach($assigins as $key=>$value){
			  $enquiry = $this->enquiry_model->get_enquiry(array('enquiry.id'=>$value,'enquiry.adminID'=>$adminID));

				$data = array(
					'userID'            => $userID,
					'assigned_by'       => $this->session->userdata('id'),
					'assigin_date'      => date('Y-m-d'),
					're_enquiry_status' => 1,
				);
     
				$update = $this->enquiry_model->update_enquiry($data,array('id' => $value));

				if($update){
					$enquiry_history = array(	
						'adminID'     => $adminID,
						'enquiryID'   => $enquiry->id,
						'clientID'    => $enquiry->clientID,
						'userID'      => $userID,
						'assigned_by' => $this->session->userdata('id'),
						'lead_type'   => $enquiry->lead_type,
						'lead_sourse' => $enquiry->lead_sourse,
						'lead_status' => $enquiry->lead_status,
						'requirement' => $enquiry->requirement,
						'assigin_date'=> date('Y-m-d'),
						'lead_month'  => date('m-Y'),
					);

					$store_enquiry = $this->enquiry_model->store_enquiry_history($enquiry_history);

					$followup = $this->followup_model->get_followup(array('followup.enquriyID'=>$value,'followup.adminID'=>$adminID));
          if($followup){
						$followupData = array(
							'userID' => $userID
						);
	
						$update_followup = $this->followup_model->update_followup($followupData,array('enquriyID'=>$value));

						// $followup_history = array(	
						// 	'adminID'       => $adminID,
						// 	'enquiryID'     => $value,
						// 	'followupID'    => $followup->id,
						// 	'userID'        => $userID,
						// 	'remark'        => $this->session->userdata('id'),
						// 	'lead_status'   => $followup->lead_status,
						// 	'followup_date' => $followup->followup_date,
						// 	'followup_time' => $followup->followup_time,
						// );
	
						// $store_followup_history = $this->followup_model->store_followup_history($followup_history);
					}
					
				}

			}

			if($store_enquiry){
		
				echo json_encode(['status'=>200, 'message'=>'Lead assigin Successfully . Send mail please wait','id'=>$userID]);
			}else{
				echo json_encode(['status'=>403, 'message'=>mysqli_error()]);  
			}

		}else{
			echo json_encode(['status'=>403, 'message'=>'Please choose at least one enquiry']); 	
			exit();
		}
	}


	public function setSessionEnquiry(){
		
		$monthname_enquiry = $this->input->post('monthname_enquiry');
		$assign_user_enquiry = $this->input->post('assign_user_enquiry');
		$enquiry_from_date = $this->input->post('enquiry_from_date');
		$enquiry_to_date = $this->input->post('enquiry_to_date');
		$url = $this->input->post('url');

		$session = array(
				'monthname_enquiry'   => $monthname_enquiry,
				'assign_user_enquiry' => $assign_user_enquiry,
				'enquiry_from_date'   => $enquiry_from_date,
				'enquiry_to_date'     => $enquiry_to_date,
				);
				
				$this->session->set_userdata($session);
		    if($url == 'enquiry-generated'){
					redirect(base_url('enquiry-generated'));
				}

				if($url == 'enquires'){
					redirect(base_url('enquires'));
				}
				
				
	}

	public function resetFromDateEnquiry(){
		$this->session->unset_userdata('enquiry_from_date');
		$this->session->unset_userdata('enquiry_to_date');
}

	public function resetToDateEnquiry(){
		$this->session->unset_userdata('enquiry_to_date');
}

	public function resetMonthNameEnquiry(){
		$this->session->unset_userdata('monthname_enquiry');
}

	public function resetAssignUserEnquiry(){
		$this->session->unset_userdata('assign_user_enquiry');
}
		
	
}