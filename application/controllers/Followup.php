<?php 
date_default_timezone_set('Asia/Kolkata');
class Followup extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('customer_model');
		$this->load->model('user_model');
		$this->load->model('setting_model');
		$this->load->model('enquiry_model');
		$this->load->model('followup_model');
	}

	public function index()
	{
		$this->not_admin_logged_in();
		$data['uri'] = $this->uri->segment(1);
		$permission = permission($data['uri']);
		if($permission[0]=='View'){
			$data['permission'] = $permission;
			$data['page_title'] = 'Followup';
			$data['admins'] = $this->user_model->get_users(array('users.user_type'=>2));
			$data['users'] = get_users_function(array('users.adminID'=>$this->session->userdata('adminID'),'users.status'=>1),'Sale');//$this->user_model->get_users();
			$data['enquires'] = $this->enquiry_model->get_enquiries(array('enquiry.adminID' =>$this->session->userdata('adminID'),'enquiry.sale'=>0));
			$data['customers'] = $this->customer_model->get_customers(array('customers.adminID'=>$this->session->userdata('adminID'),'customers.status'=>1));
			$data['leads'] = $this->setting_model->get_lead_status(array('lead_status.status'=>1));
			$data['url'] = $data['uri'];
	    $this->admin_template('followup/followup',$data);
		}else{
			redirect(base_url('dashboard'));
		}
    
	}

	public function ajaxFollowup(){
		$this->not_admin_logged_in();
		  $data['uri'] = $this->uri->segment(3);
      $permission = permission($data['uri']);
      $role = role();
			$condition = $this->session->userdata('user_type')==1 ? array('followup.adminID'=>$this->session->userdata('enquiryAdminID')) : array('followup.status'=>0,'followup.adminID'=>$this->session->userdata('adminID'));
			$followups = $this->followup_model->make_datatables($condition); // this will call modal function for fetching data
			$data = array();
			//print_r($enquires);die;
			$leads_color = leads_color();//array(1=>'text-info',2=>'text-danger',3=>'text-info',4=>'text-warning',5=>'text-success',6=>'text-secondary',7=>'text-dark');
			foreach($followups as $key=>$followup) // Loop over the data fetched and store them in array
			{

				$leave_status = $this->followup_model->get_followups_history(array('followup_history.followupID'=>$followup['followupID'],'followup_history.lead_status'=>7,'followup_history.adminID'=>$this->session->userdata('adminID')));
       $leave_data = array();
				foreach($leave_status as $leaves){
					$leave_data[] = $leaves->assigin_to_name.'  '.date('d-m-Y',strtotime($leaves->FollowupHistoryDate)).'  '.$leaves->leadStatus.'<br>';
				}
    // echo "<pre>";
		// 		print_r($leave_data);

				$button = '';
				$sub_array = array();
		
					$button .= '<a href="'.base_url('create-followup/'.base64_encode($followup['id'])).'"  target="_blank" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Followup" class="btn btn-sm btn-white text-warning me-2"> Followup <i class="fa fa-arrow-circle-up"></i></a>';
					$textarea  = !empty($followup['followupRemark']) ? '<textarea class=""  readonly > '.$followup['followupRemark'].'</textarea>' : '';
						
				$sub_array[] = $key+1;
				$this->session->userdata('user_type')!=1 ? $sub_array[] = $button : '';
				$sub_array[] = $followup['customerName'];
				$sub_array[] = $followup['customerPhone'];
				$sub_array[] = date('d-M-Y', strtotime($followup['assigin_date']));
				$sub_array[] = date('d-M-Y', strtotime($followup['FollowupDate'])) .' '. date('H:i A', strtotime($followup['followup_time']));
				$sub_array[] = $textarea;
				$sub_array[] = '<span class="btn '.$leads_color[$followup['followupLeadStatus']].' font-weight-bold">'.$followup['leadStatus'].'</span>';
				$sub_array[] = implode(',', $leave_data);
				$sub_array[] = $followup['assigin_to_name'];
				
				$sub_array[] = $followup['assigin_by_name'];
				$sub_array[] = date('d-M-Y', strtotime($followup['created_at']));
				$sub_array[] = $followup['lead_type'];
				$sub_array[] = $followup['lead_sourse'];
				$sub_array[] = date('d-M-Y', strtotime($followup['followupCreatedAt']));
			
				$data[] = $sub_array;
			}
		
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->followup_model->get_all_data($condition),
			"recordsFiltered"         =>     $this->followup_model->get_filtered_data($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	  }

		public function create(){
			$this->not_admin_logged_in();
			$id = base64_decode($this->uri->segment(2));
			$data['uri'] = 'followup';
			$permission = permission($data['uri']);
			if($permission[1]=='Add'){
				$data['page_title'] = 'Followup';
				$adminID = $this->session->userdata('adminID');
				$data['enquiry'] = $this->enquiry_model->get_enquiry(array('enquiry.id'=>$id));
				$data['lead_status'] = $this->setting_model->get_lead_status(array('id<>'=>1,'status'=>1));
				$data['enquiryID'] = $id;
				$data['followup'] = $this->followup_model->get_followup(array('followup.adminID'=>$adminID,'followup.enquriyID'=>$id));
				//print_r($data['followup']);die;
				$this->admin_template('followup/create-followup',$data);
			}else{
				redirect(base_url('dashboard'));
			}
			
		}

		public function store()
		{
			$adminID = $this->session->userdata('adminID');
			$enquiryID = $this->input->post('enquiryID');
			$lead_status = $this->input->post('lead_status');
			$company_name = $this->input->post('company_name');
			$email = $this->input->post('email');
			$alternative_no = $this->input->post('alternative_no');
			$followup_date = date('Y-m-d',strtotime($this->input->post('followup_date')));
			$followup_time = date('H:i:s',strtotime($this->input->post('followup_time')));
			$requirement = $this->input->post('requirement');
			$remark = $this->input->post('remark');
			$sale = !empty($this->input->post('sale')) ? $this->input->post('sale') : 0;
			$today_followup_status = date('d-m-Y');
      $followup = $this->followup_model->get_followup(array('followup.adminID' => $adminID,'followup.enquriyID'=>$enquiryID));
			

			if(empty($lead_status)){
				echo json_encode(['status'=>403, 'message'=>'Please select lead status ']); 	
				exit();
			}
				
			if(empty($followup_date)){
				echo json_encode(['status'=>403, 'message'=>'Please enter followup date ']); 	
				exit();
			}

			if(empty($followup_time)){
				echo json_encode(['status'=>403, 'message'=>'Please enter followup time ']);  	
				exit();
			}
				
		
			// if(empty($requirement)){
			// 	echo json_encode(['status'=>403, 'message'=>'Please enter requirement ']);   	
			// 	exit();
			// }

			if(empty($remark)){
				echo json_encode(['status'=>403, 'message'=>'Please enter remark ']);   	
				exit();
			}

			 $enquiry = $this->enquiry_model->get_enquiry(array('enquiry.id'=>$enquiryID));

			   $clientID = $enquiry->clientID;

				$customer_data = array(	
					'company_name' => $company_name,
					'email'        => $email,
          'alternate_no' => $alternative_no
				);

				$customer_update = $this->customer_model->update_customer($customer_data,array('id'=>$clientID));

				if($followup){
					$enquiry_data = array(	
						'lead_status'       => $lead_status,
						'requirement'       => $requirement,
						're_enquiry_status' => 0,
					);
					$store_enquiry = $this->enquiry_model->update_enquiry($enquiry_data,array('id'=>$enquiryID));

					$followup_data = array(	
						'userID'        => $this->session->userdata('id'),
						'remark'        => $remark,
						'lead_status'   => $lead_status,
						'followup_date' => $followup_date,
						'followup_time' => $followup_time,
						'today_followup_status' => $today_followup_status
					);

					$followupID = $this->followup_model->update_followup($followup_data,array('enquriyID'=>$enquiryID));

					$data = array(	
						'adminID'       => $adminID,
						'enquriyID'     => $enquiryID,
						'followupID'    => $followup->followupID,
						'userID'        => $this->session->userdata('id'),
						'remark'        => $remark,
						'lead_status'   => $lead_status,
						'followup_date' => $followup_date,
						'followup_time' => $followup_time,
					);
			
					$followupHistory = $this->followup_model->store_followup_history($data);
					$sale_page = $sale==1 ? $enquiryID : 0 ;
					if($followupHistory){
						echo json_encode(['status'=>200, 'message'=>'Followup Added Successfully','sale_page'=>$sale_page]);   
					}else{
						 echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
					}

				}else{

					$enquiry_data = array(	
						'lead_status'       => $lead_status,
						'requirement'       => $requirement,
						're_enquiry_status' => 0,
					);
					$store_enquiry = $this->enquiry_model->update_enquiry($enquiry_data,array('id'=>$enquiryID));

					$followup_data = array(	
						'adminID'       => $adminID,
						'enquriyID'     => $enquiryID,
						'userID'        => $this->session->userdata('id'),
						'remark'        => $remark,
						'lead_status'   => $lead_status,
						'followup_date' => $followup_date,
						'followup_time' => $followup_time,
						'today_followup_status' => $today_followup_status
					);

					$followupID = $this->followup_model->store_followup($followup_data);

					$data = array(	
						'adminID'       => $adminID,
						'enquriyID'     => $enquiryID,
						'followupID'    => $followupID,
						'userID'        => $this->session->userdata('id'),
						'remark'        => $remark,
						'lead_status'   => $lead_status,
						'followup_date' => $followup_date,
						'followup_time' => $followup_time,
					);
			
					$followupHistory = $this->followup_model->store_followup_history($data);

					$sale_page = $sale==1 ? $enquiryID : 0 ;
					
        if($followupHistory)
				{
					 echo json_encode(['status'=>200, 'message'=>'Followup Added Successfully','sale_page'=>$sale_page]);   
					}else{
						echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
					}
				}
		}


		public function ajaxFollowupHistory(){
			$this->not_admin_logged_in();
			$enquiryID = $this->uri->segment(3);
			$condition = $this->session->userdata('user_type')==1 ? array('followup_history.enquriyID'=>$enquiryID) : array('followup_history.adminID'=>$this->session->userdata('adminID'),'followup_history.enquriyID'=>$enquiryID);
			$followups = $this->followup_model->make_datatables_history($condition); // this will call modal function for fetching data
			$data = array();
		//	print_r($followups);die;
		$leads_color = leads_color();//array(1=>'text-info',2=>'text-danger',3=>'text-info',4=>'text-warning',5=>'text-success',6=>'text-secondary',7=>'text-dark');
			foreach($followups as $key=>$followup) // Loop over the data fetched and store them in array
			{
				$sub_array = array();
				$sub_array[] = $key+1;
				$sub_array[] = $followup['customerName'];
				$sub_array[] = '<span class="btn '.$leads_color[$followup['followupHistoryLeadStatus']].' font-weight-bold">'.$followup['leadStatus'].'</span>';//$followup['leadStatus'];
				$sub_array[] = '<textarea class="form-control" readonly>'.$followup['requirement'].'</textarea>';
				$sub_array[] = '<textarea class="form-control" readonly>'.$followup['followupHistoryRemark'].'</textarea>';
				$sub_array[] = date('d-M-Y', strtotime($followup['FollowupHistoryDate'])) .' '. date('h:i A', strtotime($followup['FollowupHistoryTime']));
				$sub_array[] = $followup['assigin_to_name'];
				//$sub_array[] = date('d-M-Y', strtotime($followup['followupHistoryCreatedAt']));
			
				$data[] = $sub_array;
			}
		
			$output = array(
				"draw"                    =>     intval($_POST["draw"]),
				"recordsTotal"            =>     $this->followup_model->get_all_data_history($condition),
				"recordsFiltered"         =>     $this->followup_model->get_filtered_data_history($condition),
				"data"                    =>     $data
			);
			
			echo json_encode($output);
			}

	public function followup_report()
	{
		$this->not_admin_logged_in();
		$data['uri'] = $this->uri->segment(1);
		$permission = permission($data['uri']);
		if($permission[0]=='View'){
			$data['permission'] = $permission;
			$data['page_title'] = 'Followup Report';
			$data['admins'] = $this->user_model->get_users(array('users.user_type'=>2));
			$data['users'] = get_users_function(array('users.adminID'=>$this->session->userdata('adminID'),'users.status'=>1),'Sale');
			$data['url'] = $data['uri'];
	    $this->admin_template('followup/followup-report',$data);
		}else{
			redirect(base_url('dashboard'));
		}
    
	}


	public function ajaxFollowupReport(){
		$this->not_admin_logged_in();
		  $data['uri'] = $this->uri->segment(3);
      $permission = permission($data['uri']);
      $role = role();
			$condition = $this->session->userdata('user_type')==1 ? array('followup.adminID'=>$this->session->userdata('enquiryAdminID')) : 
			($this->session->userdata('user_type')==2 ? array('followup.status'=>0,'followup.adminID'=>$this->session->userdata('adminID')) :
			($role->parent_role==0 && $this->session->userdata('user_type')!=2 ? array('followup.status'=>0,'followup.adminID'=>$this->session->userdata('adminID')) :
			array('followup.status'=>0,'followup.adminID'=>$this->session->userdata('adminID'),'followup.userID'=>$this->session->userdata('id'))));
		$followups = $this->followup_model->make_datatables($condition); // this will call modal function for fetching data
		$data = array();
		//print_r($enquires);die;
		$leads_color = leads_color();
		foreach($followups as $key=>$followup) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
	
        $button .= '<a href="javascript:void(0)" onclick="view_report('.$followup['id'].')"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Report" class="btn btn-sm btn-white text-warning me-2">  <i class="fa fa-eye"></i></a>';
				$textarea  = !empty($followup['followupRemark']) ? '<textarea class=""  readonly > '.$followup['followupRemark'].'</textarea>' : '';
			$sub_array[] = $key+1;
			$sub_array[] = $button;
			$sub_array[] = $followup['customerName'];
			$sub_array[] = $followup['customerPhone'];
			$sub_array[] = date('d-M-Y', strtotime($followup['assigin_date']));
			$sub_array[] = date('d-M-Y', strtotime($followup['FollowupDate'])) .' '. date('h:i A', strtotime($followup['followup_time']));
			$sub_array[] = $textarea;
			$sub_array[] = '<span class="btn '.$leads_color[$followup['followupLeadStatus']].' font-weight-bold">'.$followup['leadStatus'].'</span>';

			$sub_array[] = $followup['assigin_to_name'];
			$sub_array[] = $followup['assigin_by_name'];
			$sub_array[] = date('d-M-Y', strtotime($followup['created_at']));
			$sub_array[] = $followup['lead_type'];
			$sub_array[] = $followup['lead_sourse'];
			$sub_array[] = date('d-M-Y', strtotime($followup['followupCreatedAt']));
		
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->followup_model->get_all_data($condition),
			"recordsFiltered"         =>     $this->followup_model->get_filtered_data($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	  }


		public function setSessionFollowup(){
			$customers_filter = $this->input->post('customers_filter');
			$monthname_followup = $this->input->post('monthname_followup');
			$assign_user_followup = $this->input->post('assign_user_followup');
			$followup_from_date = $this->input->post('followup_from_date');
			$followup_to_date = $this->input->post('followup_to_date');
			$lead_status_followup = $this->input->post('lead_status_followup');
			$url = $this->input->post('url');
			$split = explode('/', $url);
			
			$session = array(
					'customers_filter'     => $customers_filter,
					'monthname_followup'   => $monthname_followup,
					'assign_user_followup' => $assign_user_followup,
					'followup_from_date'   => $followup_from_date,
					'followup_to_date'     => $followup_to_date,
					'lead_status_followup' => $lead_status_followup,
					);
					
					$this->session->set_userdata($session);
					if($split[0]=='followup-status'){
						redirect(base_url('followup-status/'.$split[1]));
					}else{
						redirect(base_url('followup'));
					}
					
					
		}

		public function resetCustomerFilter(){
			$this->session->unset_userdata('customers_filter');
	}
	
		public function resetFromDateFollowup(){
			$this->session->unset_userdata('followup_from_date');
			$this->session->unset_userdata('followup_to_date');
	}
	
		public function resetToDateFollowup(){
			$this->session->unset_userdata('followup_to_date');
	}
	
		public function resetMonthNameFollowup(){
			$this->session->unset_userdata('monthname_followup');
	}
	
		public function resetAssignUserFollowup(){
			$this->session->unset_userdata('assign_user_followup');
	}
	
		public function resetLeadStatusFollowup(){
			$this->session->unset_userdata('lead_status_followup');
	}


	public function followup_status(){
		$this->not_admin_logged_in();
		$data['uri'] = 'followup';
		$status = base64_decode($this->uri->segment(2));
		$permission = permission($data['uri']);
		if($permission[0]=='View'){
			$data['permission'] = $permission;
			$data['page_title'] = $status;
			$data['admins'] = $this->user_model->get_users(array('users.user_type'=>2));
			$data['users'] = get_users_function(array('users.adminID'=>$this->session->userdata('adminID'),'users.status'=>1),'Sale');//$this->user_model->get_users();
			$data['customers'] = $this->customer_model->get_customers(array('customers.adminID'=>$this->session->userdata('adminID'),'customers.status'=>1));
			$data['leads'] = $this->setting_model->get_lead_status(array('lead_status.status'=>1));
			$data['url'] = $data['uri'];
			$data['folowup_status'] = $this->uri->segment(2);
	    $this->admin_template('followup/followup-status',$data);
		}else{
			redirect(base_url('dashboard'));
		}
	}

	public function ajaxFollowupStatus(){
		$this->not_admin_logged_in();
		  $data['uri'] = $this->uri->segment(3);
			$folowup_status = base64_decode($this->uri->segment(4));
      $permission = permission($data['uri']);
      $role = role();
			// $condition = $this->session->userdata('user_type')==1 ? array('followup.adminID'=>$this->session->userdata('enquiryAdminID')) : 
			// ($this->session->userdata('user_type')==2 ? array('followup.status'=>0,'followup.adminID'=>$this->session->userdata('adminID')) :
			// ($permission[7]=='Like-admin' ? array('followup.status'=>0,'followup.adminID'=>$this->session->userdata('adminID')) :
			// array('followup.status'=>0,'followup.adminID'=>$this->session->userdata('adminID'),'followup.userID'=>$this->session->userdata('id'))));
			if($folowup_status=='Re Enquiry'){
				$condition = $this->session->userdata('user_type')==1 ? array('enquiry.adminID'=>$this->session->userdata('enquiryAdminID'),'enquiry.re_enquiry_status'=>1) : 
				($this->session->userdata('user_type')==2 ? array('enquiry.status'=>1,'enquiry.adminID'=>$this->session->userdata('adminID'),'enquiry.re_enquiry_status'=>1) :
				($role->parent_role==0 && $this->session->userdata('user_type')!=2 ? array('enquiry.status'=>1,'enquiry.adminID'=>$this->session->userdata('adminID'),'enquiry.re_enquiry_status'=>1) :
				array('enquiry.status'=>1,'enquiry.adminID'=>$this->session->userdata('adminID'),'enquiry.userID'=>$this->session->userdata('id'),'enquiry.re_enquiry_status'=>1)));
				$enquires = $this->enquiry_model->make_datatables($condition);
				$all_count_data =  $this->enquiry_model->get_all_data($condition);
        $all_filter_data = $this->enquiry_model->get_filtered_data($condition);
			}

			if($folowup_status=='New Enquiry'){
				$condition = $this->session->userdata('user_type')==1 ? array('enquiry.adminID'=>$this->session->userdata('enquiryAdminID'),'enquiry.lead_status'=>1) : 
		   ($this->session->userdata('user_type')==2 ? array('enquiry.status'=>1,'enquiry.adminID'=>$this->session->userdata('adminID'),'enquiry.lead_status'=>1) :
		  ($role->parent_role==0 && $this->session->userdata('user_type')!=2 ? array('enquiry.status'=>1,'enquiry.adminID'=>$this->session->userdata('adminID'),'enquiry.lead_status'=>1) :
		  array('enquiry.status'=>1,'enquiry.adminID'=>$this->session->userdata('adminID'),'enquiry.userID'=>$this->session->userdata('id'),'enquiry.lead_status'=>1)));
			$enquires = $this->enquiry_model->make_datatables($condition);
			$all_count_data =  $this->enquiry_model->get_all_data($condition);
      $all_filter_data = $this->enquiry_model->get_filtered_data($condition);
			}


			if($folowup_status=='Today FollowUp'){
				
				$condition = $this->session->userdata('user_type')==1 ? array('followup.adminID'=>$this->session->userdata('adminID'),'DATE_FORMAT(followup.followup_date,"%Y-%m-%d")'=>date('Y-m-d')) : 
		   ($this->session->userdata('user_type')==2 ? array('followup.status'=>0,'followup.adminID'=>$this->session->userdata('adminID'),'DATE_FORMAT(followup.followup_date,"%Y-%m-%d")'=>date('Y-m-d')) :
		   ($role->parent_role==0 && $this->session->userdata('user_type')!=2 ? array('followup.status'=>0,'followup.adminID'=>$this->session->userdata('adminID'),'DATE_FORMAT(followup.followup_date,"%Y-%m-%d")'=>date('Y-m-d')) :
		   array('followup.status'=>0,'followup.adminID'=>$this->session->userdata('adminID'),'followup.userID'=>$this->session->userdata('id'),'DATE_FORMAT(followup.followup_date,"%Y-%m-%d")'=>date('Y-m-d'))));

			 $enquires = $this->followup_model->make_datatables($condition);
			 $all_count_data =  $this->followup_model->get_all_data($condition);
      $all_filter_data = $this->followup_model->get_filtered_data($condition);
			}


			if($folowup_status=='Hot Lead'){
				$condition = $this->session->userdata('user_type')==1 ? array('followup.adminID'=>$this->session->userdata('enquiryAdminID'),'followup.lead_status'=>2) : 
		   ($this->session->userdata('user_type')==2 ? array('followup.status'=>0,'followup.adminID'=>$this->session->userdata('adminID'),'followup.lead_status'=>2) :
		   ($role->parent_role==0 && $this->session->userdata('user_type')!=2 ? array('followup.status'=>0,'followup.adminID'=>$this->session->userdata('adminID'),'followup.lead_status'=>2) :
		  array('followup.status'=>0,'followup.adminID'=>$this->session->userdata('adminID'),'followup.userID'=>$this->session->userdata('id'),'followup.lead_status'=>2)));
			$enquires = $this->followup_model->make_datatables($condition);
			$all_count_data =  $this->followup_model->get_all_data($condition);
      $all_filter_data = $this->followup_model->get_filtered_data($condition);
			}


			if($folowup_status=='Warm Lead'){
				$condition = $this->session->userdata('user_type')==1 ? array('followup.adminID'=>$this->session->userdata('enquiryAdminID'),'followup.lead_status'=>4) : 
		    ($this->session->userdata('user_type')==2 ? array('followup.status'=>0,'followup.adminID'=>$this->session->userdata('adminID'),'followup.lead_status'=>4) :
		   ($role->parent_role==0 && $this->session->userdata('user_type')!=2 ? array('followup.status'=>0,'followup.adminID'=>$this->session->userdata('adminID'),'followup.lead_status'=>4) :
		   array('followup.status'=>0,'followup.adminID'=>$this->session->userdata('adminID'),'followup.userID'=>$this->session->userdata('id'),'followup.lead_status'=>4)));

			 $enquires = $this->followup_model->make_datatables($condition);

			$all_count_data =  $this->followup_model->get_all_data($condition);
      $all_filter_data = $this->followup_model->get_filtered_data($condition);
			}


			//$enquires = $this->followup_model->make_datatables($condition); // this will call modal function for fetching data
			$data = array();
			//print_r($enquires);die;
			$leads_color = leads_color();
			foreach($enquires as $key=>$enquiry) // Loop over the data fetched and store them in array
			{
				if($folowup_status=='Today FollowUp' OR $folowup_status=='Hot Lead' OR $folowup_status=='Warm Lead'){
           $folloup_date_time = date('d-M-Y', strtotime($enquiry['FollowupDate'])) .' '.date('H:i', strtotime($enquiry['FollowupTime']));
					 $remark =  !empty($enquiry['followupRemark']) ? '<textarea class=""  readonly > '.$enquiry['followupRemark'].'</textarea>' : '';		
				}else{
					$folloup_date_time = "";
					$remark = " ";
				}
				$button = '';
				$sub_array = array();
		      $button_css = date('d-m-Y',strtotime($enquiry['today_followup_status'])) == date('d-m-Y') ? 'text-success' : 'text-warning';
					$button .= '<a href="'.base_url('create-followup/'.base64_encode($enquiry['id'])).'"  target="_blank" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Followup" class="btn btn-sm btn-white '.$button_css.' me-2"> Followup <i class="fa fa-arrow-circle-up"></i></a>';
					
					$sub_array[] = $key+1;
				$this->session->userdata('user_type')!=1 ? $sub_array[] = $button : '';
				
				$sub_array[] = $enquiry['customerName'];
				$sub_array[] = $enquiry['customerPhone'];
				$sub_array[] = date('d-M-Y', strtotime($enquiry['assigin_date']));
				$sub_array[] = $folloup_date_time;
				$sub_array[] = $remark;
				$sub_array[] = $enquiry['lead_type'];
				$sub_array[] = $enquiry['lead_sourse'];
				$sub_array[] = '<span class="btn '.$leads_color[$enquiry['lead_status']].' font-weight-bold">'.$enquiry['leadStatus'].'</span>';
				$sub_array[] = $enquiry['assigin_to_name'];
				$sub_array[] = $enquiry['assigin_by_name'];
				$sub_array[] = date('d-M-Y', strtotime($enquiry['created_at']));
			
				$data[] = $sub_array;
			}
			
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $all_count_data,
			"recordsFiltered"         =>     $all_filter_data,
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	  }

	
}