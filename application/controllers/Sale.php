<?php 
date_default_timezone_set('Asia/Kolkata');
class Sale extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('customer_model');
		$this->load->model('user_model');
		$this->load->model('setting_model');
		$this->load->model('enquiry_model');
		$this->load->model('sale_model');
		$this->load->model('followup_model');
		$this->load->model('service_model');
	}

	public function index()
	{
		$this->not_admin_logged_in();
		$data['uri'] = $this->uri->segment(1);
		$permission = permission($data['uri']);
		if($permission[0]=='View'){
			$data['permission'] = $permission;
			$data['page_title'] = 'Sales';
			$data['url'] = $data['uri'];
			$data['admins'] = $this->user_model->get_users(array('users.user_type'=>2));
			//$data['users'] = $this->user_model->get_users();
			$data['users'] = get_users_function(array('users.adminID'=>$this->session->userdata('adminID'),'users.status'=>1),'Sale');
	    $this->admin_template('sale/sales',$data);
		}else{
			redirect(base_url('dashboard'));
		}
    
	}


	public function ajaxSale(){
	  $this->not_admin_logged_in();

	  $data['uri'] = $this->uri->segment(3);
		$permission = permission($data['uri']);

		$payment_permission = 'payment-history';
		$paymentPermission = permission($payment_permission);
		
		$service_permission = 'services';
		$servicePermission =  permission($service_permission);

		$task_permission = 'task';
		$taskPermission = permission($task_permission);

		$total_sale_amount = 0;
		$total_gst = 0;
		$total_with_gst = 0;
		$total_paid = 0;
		$total_pending_amount = 0;
		
		$grand_total_sale_amount = 0;
		$grand_total_gst = 0;
		$grand_total_with_gst = 0;
		$grand_total_paid = 0;
		$grand_total_pending_amount = 0;
		$role = role();
		$condition = $this->session->userdata('user_type')==1 ? array('sales.adminID'=>$this->session->userdata('adminID'),'sales.status'=>1) : 
			($this->session->userdata('user_type')==2 ? array('sales.status'=>1,'sales.adminID'=>$this->session->userdata('adminID')) :
			($role->parent_role==0 && $this->session->userdata('user_type')!=2 ? array('sales.status'=>1,'sales.adminID'=>$this->session->userdata('adminID')) :
			array('sales.status'=>1,'sales.adminID'=>$this->session->userdata('adminID'),'sales.sale_closed_by'=>$this->session->userdata('id'))));
			$grand_total = $this->sale_model->grand_total_sale($condition);
			$sales = $this->sale_model->make_datatables($condition); // this will call modal function for fetching data
			$data = array();
			//print_r($enquires);die;
			foreach($sales as $key=>$sale) // Loop over the data fetched and store them in array
			{
				$service = $this->service_model->get_service(array('services.saleID'=>$sale['id'],'services.adminID'=>$this->session->userdata('adminID')));
				$paid_amount = 0;
				$payment_histories = $this->sale_model->get_payment_histories(array('payment_history.saleID'=>$sale['id']));
				foreach($payment_histories as $payment){
					$paid_amount += $payment->amount;
				}
				$payment_button_css = $sale['pending_amount'] == 0 ? 'btn btn-outline-success' : 'btn btn-outline-secondary';
				$button = '';
				$sub_array = array();
				if($permission[3]=='Delete' && $this->session->userdata('user_type')!=1){
					$button .= '<a href="javascript:void(0)" onclick="delete_sale('.$sale['id'].')"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete sale Detail" class="btn btn-outline-danger btn-sm m-2"> <i class="fa fa-trash"></i></a>';
				}
				if($permission[0]=='View'){
					$button .= '<a href="javascript:void(0)" onclick="view_sales('.$sale['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View sale Detail" class="btn btn-outline-warning btn-sm"> <i class="fa fa-eye"></i></a>';
				}

				if($permission[2]=='Edit' && $this->session->userdata('user_type')!=1){
					$button .= '<a href="'.base_url('edit-sale/'.base64_encode($sale['id'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit sale Detail" class="btn btn-outline-primary btn-sm m-2"> <i class="fa fa-edit"></i></a>';
				}

				if($paymentPermission[1]=='Add' && $this->session->userdata('user_type')!=1){
					$button .= '<a href="javascript:void(0)" onclick="add_payment('.$sale['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Add Sale Payment" class="'.$payment_button_css.' btn-sm "> Payment <i class="fa fa-credit-card"></i></a>';
				}

		

				if($servicePermission[1]=='Add' && $this->session->userdata('user_type')!=1){
					if($service){
						$button .= '<a href="javscript:void(0)" onclick="view_services('.$sale['id'].')"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Service" class="btn btn-outline-success btn-sm m-2"> Service <i class="fa fa-eye"></i></a>';
					}else{
						$button .= '<a href="'.base_url('create-service/'.base64_encode($sale['id']).'/'.base64_encode($sale['enquiryID'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Add Service" class="btn btn-outline-dark btn-sm m-2"> Service <i class="fa fa-wrench"></i></a>';
					}
					
				}
        //echo $sale['sale_work_status'];
				if($taskPermission[1]=='Add' && $this->session->userdata('user_type')!=1){
					if($sale['task_status']==0){
						$button .= '<a href="'.base_url('create-task/'.base64_encode($sale['id'])).'"   data-bs-toggle="tooltip" data-bs-placement="bottom" title="Add Task" class="btn btn-outline-primary btn-sm m-2"> Task <i class="fa fa-clock-o"></i></a>';
					}elseif($sale['task_status']==2){
						$button .= '<a href="javscript:void(0)"   data-bs-toggle="tooltip" data-bs-placement="bottom" title="Add Task" class="btn btn-outline-warning btn-sm m-2"> Task <i class="fas fa-spinner fa-spin"></i></a>';
					}elseif($sale['task_status']==3){
						$button .= '<a href="'.base_url('edit-task/'.base64_encode($sale['id']).'/'.base64_encode('sale')).'"   data-bs-toggle="tooltip" data-bs-placement="bottom" title="Add Task" class="btn btn-outline-danger btn-sm m-2"> Task <i class="fa fa-ban"></i></a>';
					}else{
						$button .= '<a href="javascript:void()"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Task Completed" class="btn btn-outline-success btn-sm m-2"> Task <i class="fa fa-check-circle"></i></a>';
					}
					
				}

				

				$total_sale_amount  += $sale['sale_amount'];
				$total_gst += $sale['gst_amount'];
				$total_with_gst += $sale['total_amount'];
				$total_paid += $paid_amount;
				$total_pending_amount  += $sale['pending_amount'];

				$sub_array[] = $key+1;
				$sub_array[] = $button;

				$sub_array[] = $sale['company_name'];
				$sub_array[] = date('d-M-Y', strtotime($sale['created_at']));
				$sub_array[] = '₹'.$sale['sale_amount'];
				$sub_array[] = '₹'.$sale['gst_amount'];
				$sub_array[] = '₹'.$sale['total_amount'];
				$sub_array[] = '₹'.$paid_amount;
				$sub_array[] = '₹'.$sale['pending_amount'];
				$sub_array[] = $sale['customerName'];
				$sub_array[] = $sale['customerPhone'];
				$sub_array[] = !empty($sale['delivery_date']) ? date('d-M-Y', strtotime($sale['delivery_date'])) : '';
				$sub_array[] = $sale['assigin_to_name'];
				
			
				$data[] = $sub_array;
		}

		$value[] = array('','','','<b>Total</b>','<b>'.'₹'.$total_sale_amount.'</b>','<b>'.'₹'.$total_gst.'</b>','<b>'.'₹'.$total_with_gst.'</b>','<b>'.'₹'.$total_paid.'</b>','<b>'.'₹'.$total_pending_amount.'</b>','','','','');

    $data = array_merge($data,$value);

		foreach($grand_total as $all_total){
			$grand_total_sale_amount  += $all_total['sale_amount'];
			$grand_total_gst += $all_total['gst_amount'];
			$grand_total_with_gst += $all_total['total_amount'];
			$grand_total_paid += $all_total['total_paid']['total_amount_paid'];
			$grand_total_pending_amount += $all_total['pending_amount'];
		}
      $total[] = array('','','','<b class="text-success">Grand Total</b>','<b class="text-success">'.'₹'.$grand_total_sale_amount.'</b>','<b class="text-success">'.'₹'.$grand_total_gst.'</b>','<b class="text-success">'.'₹'.$grand_total_with_gst.'</b>','<b class="text-success">'.'₹'.$grand_total_paid.'</b>','<b class="text-success">'.'₹'.$grand_total_pending_amount.'</b>','','','','','');
      
      
      $data = array_merge($data,$total);
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->sale_model->get_all_data($condition),
			"recordsFiltered"         =>     $this->sale_model->get_filtered_data($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	  }

		
		public function create(){
			$this->not_admin_logged_in();
			$data['uri'] = 'sales';
			$enquiryID = base64_decode($this->uri->segment(2));
			$permission = permission($data['uri']);
			if($permission[1]=='Add'){
				$data['page_title'] = 'Sale';
				$data['enquiry'] = $this->enquiry_model->get_enquiry(array('enquiry.id'=>$enquiryID));
				$this->admin_template('sale/create-sale',$data);
			}else{
				redirect(base_url('dashboard'));
			}
			
		}

		public function store(){

			$adminID = $this->session->userdata('adminID');
			$userID = $this->session->userdata('id');
			$enquiryID = $this->input->post('enquiryID');
			$company_name = $this->input->post('company_name');
			$email = $this->input->post('email');
			$website = $this->input->post('website');
			$sale_date = date('Y-m-d',strtotime($this->input->post('sale_date')));
			$sale_amount = $this->input->post('sale_amount');
			$gst = !empty($this->input->post('gst')) ? $this->input->post('gst') : 0;
			$gst_amount = !empty($this->input->post('gst_amount')) ? $this->input->post('gst_amount') : 0;
			$paid_amount = $this->input->post('paid_amount');
			$pending_amount = $this->input->post('pending_amount');
			$payment_mode = $this->input->post('payment_mode');
			$delivery_date = date('Y-m-d',strtotime($this->input->post('delivery_date')));
			$content_date = date('Y-m-d',strtotime($this->input->post('content_date')));
			$sale_month = date('m-Y');
			//$product = $this->input->post('product');
			$userID = $this->session->userdata('id');
			$sale_create_by = $this->input->post('sale_create_by');

			$customer = $this->enquiry_model->get_enquiry(array('enquiry.id'=>$enquiryID));
	    
			$sale = $this->sale_model->get_sale(array('sales.adminID'=>$this->session->userdata('adminID')));
			$sale_id = $sale->id+1;
			$orderID = 'WC'.date('Ymd').$sale_id;

			if(empty($sale_date)){
				echo json_encode(['status'=>403, 'message'=>'Please enter sale date ']); 	
				exit();
			}
				
		 if(empty($sale_amount)){
				echo json_encode(['status'=>403, 'message'=>'Please enter sale amount ']); 	
				exit();
			}

			if(!empty($gst)){
          if(empty($gst_amount)){
						echo json_encode(['status'=>403, 'message'=>'Please enter gst amount ']);  	
						exit();
					}
			}
				
			if($paid_amount==""){
				echo json_encode(['status'=>403, 'message'=>'Please enter advance amount ']); 	
				exit();
			}
				// if($pending_amount!=""){
				// 	echo json_encode(['status'=>403, 'message'=>'Please enter pending amount ']); 	
				// 	exit();
				// }
				
		
				if(empty($payment_mode)){
					echo json_encode(['status'=>403, 'message'=>'Please enter payment mode']);  	
					exit();
				}

				// if(empty($product)){
				// 	echo json_encode(['status'=>403, 'message'=>'Please enter product']);  	
				// 	exit();
				// }


					$data = array(	
						'adminID'        => $adminID,
						'enquiryID'      => $enquiryID,
						'orderID'        => $orderID,
						'sale_closed_by' => $userID,
						'sale_create_by' => $sale_create_by,
						'website'        => $website,
						//'product'        => $product,
						'gst'            => $gst,
						'sale_amount'    => $sale_amount,
						'gst_amount'     => $gst_amount,
						'advance_amount' => $paid_amount,
						'pending_amount' => $pending_amount,
						'total_amount'   => $sale_amount+$gst_amount,
						'payment_mode'   => $payment_mode,
						'sale_date'      => $sale_date,
						'sale_month'     => $sale_month,
						'content_date'   => $content_date,
						'sale_type'      => 'New'
					);


					  
					$store_sale = $this->sale_model->store_sale($data);
					if($store_sale){
						$payment_data = array(	
						'adminID'      => $adminID,
						'saleID'       => $store_sale,
						'userID'       => $userID,
						'amount'       => $paid_amount,
						'payment_mode' => $payment_mode,
						//'remark'       => $product,
						);
						$store_payment = $this->sale_model->store_payment_history($payment_data);

						$sale_history_data = array(	
							'adminID'        => $adminID,
							'saleID'         => $store_sale,
							'enquiryID'      => $enquiryID,
							'sale_closed_by' => $userID,
							'sale_create_by' => $sale_create_by,
							'website'        => $website,
							//'product'        => $product,
							'gst'            => $gst,
							'sale_amount'    => $sale_amount,
							'gst_amount'     => $gst_amount,
							'advance_amount' => $paid_amount,
							'pending_amount' => $pending_amount,
							'total_amount'   => $sale_amount+$gst_amount,
							'payment_mode'   => $payment_mode,
							'sale_date'      => $sale_date,
							'sale_month'     => $sale_month,
							'content_date'   => $content_date,
							'sale_type'      => 'New'
						);
						$store_sale_history = $this->sale_model->store_sale_history($sale_history_data);

						$enquiry_data = array(
							'sale' =>1
						);

						$update_enquiry = $this->enquiry_model->update_enquiry($enquiry_data,array('id'=>$enquiryID));

						
						$followup_data = array(
							'status' =>1
						);

						$update_followup = $this->followup_model->update_followup($followup_data,array('enquriyID'=>$enquiryID));

						$customer_data = array(
							'company_name' => $company_name,
							'email' => $email,
						);

						$update_customer = $this->customer_model->update_customer($customer_data,array('id'=>$customer->customerID));

						echo json_encode(['status'=>200, 'message'=>'Sale added Successfully','saleID'=>base64_encode($store_sale),'enquiryID'=>base64_encode($enquiryID)]);   
					}else{
						echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
					}

	
		}




		public function edit(){

			$this->not_admin_logged_in();
			$data['uri'] = 'sales';
			$permission = permission($data['uri']);
			if($permission[2]=='Edit'){
				$data['page_title'] = 'Sale';
				$id = base64_decode($this->uri->segment(2));
				$data['page_title'] = 'Sale';
				$data['sale'] = $this->sale_model->get_sale(array('sales.id'=>$id));
				$this->admin_template('sale/edit-sale',$data);
			}else{
				redirect(base_url('dashboard'));
			}

		}


		public function update(){
			$saleID = $this->input->post('id');
			$adminID = $this->session->userdata('adminID');
			$userID = $this->session->userdata('id');
			$enquiryID = $this->input->post('enquiryID');
			$company_name = $this->input->post('company_name');
			$email = $this->input->post('email');
			$website = $this->input->post('website');
			$sale_date = date('Y-m-d',strtotime($this->input->post('sale_date')));
			$sale_amount = $this->input->post('sale_amount');
			$gst = !empty($this->input->post('gst')) ? $this->input->post('gst') : 0;
			$gst_amount = !empty($this->input->post('gst_amount')) ? $this->input->post('gst_amount') : 0;
			$paid_amount = $this->input->post('paid_amount');
			$pending_amount = $this->input->post('pending_amount');
			$payment_mode = $this->input->post('payment_mode');
			$delivery_date = date('Y-m-d',strtotime($this->input->post('delivery_date')));
			$content_date = date('Y-m-d',strtotime($this->input->post('content_date')));
			// $product = $this->input->post('product');
			$userID = $this->session->userdata('id');
			$sale_create_by = $this->input->post('sale_create_by');
			$sale_month = date('m-Y');
			
			$customer = $this->enquiry_model->get_enquiry(array('enquiry.id'=>$enquiryID));

			if(empty($sale_date)){
				echo json_encode(['status'=>403, 'message'=>'Please enter sale date ']); 	
				exit();
			}
				
		 if(empty($sale_amount)){
				echo json_encode(['status'=>403, 'message'=>'Please enter sale amount ']); 	
				exit();
			}
			
  

			if(!empty($gst)){
          if(empty($gst_amount)){
						echo json_encode(['status'=>403, 'message'=>'Please enter gst amount ']);  	
						exit();
					}
			}
				
				if($paid_amount==""){
					echo json_encode(['status'=>403, 'message'=>'Please enter advance amount ']); 	
					exit();
				}
				// if(empty($pending_amount)){
				// 	echo json_encode(['status'=>403, 'message'=>'Please enter pending amount ']); 	
				// 	exit();
				// }
				
		
				if(empty($payment_mode)){
					echo json_encode(['status'=>403, 'message'=>'Please enter payment mode']);  	
					exit();
				}

				// if(empty($product)){
				// 	echo json_encode(['status'=>403, 'message'=>'Please enter product']);  	
				// 	exit();
				// }


					$data = array(	
						'website'        => $website,
						//'product'        => $product,
						'gst'            => $gst,
						'sale_amount'    => $sale_amount,
						'gst_amount'     => $gst_amount,
						'advance_amount' => $paid_amount,
						'pending_amount' => $pending_amount,
						'total_amount'   => $sale_amount+$gst_amount,
						'payment_mode'   => $payment_mode,
						'sale_date'      => $sale_date,
						'content_date'   => $content_date,
					);

					$update = $this->sale_model->update_sale($data,array('id'=>$saleID));
					if($update){

						$sale_history_data = array(	
							'adminID'        => $adminID,
							'saleID'         => $saleID,
							'enquiryID'      => $enquiryID,
							'sale_closed_by' => $userID,
							'sale_create_by' => $sale_create_by,
							'website'        => $website,
							//'product'        => $product,
							'gst'            => $gst,
							'sale_amount'    => $sale_amount,
							'gst_amount'     => $gst_amount,
							'advance_amount' => $paid_amount,
							'pending_amount' => $pending_amount,
							'total_amount'   => $sale_amount+$gst_amount,
							'payment_mode'   => $payment_mode,
							'sale_date'      => $sale_date,
							'sale_month'     => $sale_date,
							'content_date'   => $content_date,
						);
						$store_sale_history = $this->sale_model->store_sale_history($sale_history_data);

						$customer_data = array(
							'company_name' => $company_name,
							'email' => $email,
						);

						$update_customer = $this->customer_model->update_customer($customer_data,array('id'=>$customer->customerID));

						echo json_encode(['status'=>200, 'message'=>'Sale updated Successfully']);   
					}else{
						echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
					}
		}

		public function viewSaleForm(){
			$data['uri'] = 'sales';
			$permission = permission($data['uri']);
			if($permission[0]=='View'){

				$id = $this->input->post('id');
				$sale = $this->sale_model->get_sale(array('sales.id'=>$id));

				$product =	get_services_detail($sale->id);	
				$product_details = !empty($product) ? implode(', ',$product) : $sale->product;
				?>
<div class="row">

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Company Name </label>
      <input type="text" class="form-control" value="<?=$sale->company_name?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Full Name </label>
      <input type="text" class="form-control" value="<?=$sale->customerName?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Phone No. </label>
      <input type="text" class="form-control" minlength="10" maxlength="10"
        oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
        onkeyup="get_customer(this.value)" value="<?=$sale->customerPhone?>" readonly>
    </div>
  </div>


  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Email </label>
      <input type="email" class="form-control" value="<?=$sale->customerEmail?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Website </label>
      <input type="text" class="form-control" name="website" id="website" placeholder="Website"
        value="<?=$sale->website?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Sale Date <span class="text-danger">*</span></label>
      <input type="text" onfocus="(this.type='date')" class="form-control" name="sale_date" id="sale_date"
        value="<?=date('d-m-Y')?>" placeholder="Sale Date" value="<?=$sale->sale_date?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Sale Amount <span class="text-danger">*</span></label>
      <input type="text" class="form-control" name="sale_amount" id="sale_amount"
        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Sale Amount"
        onkeyup="amountCalculation()" value="<?=$sale->sale_amount?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="row">
      <div class="col-md-4 col-lg-4 col-xl-4">
        <div class="form-group">
          <label>GST Apply </label><br>
          <div class="btn btn-rounded btn-outline-success mr-5" title="Apply
																				GST">
            <input type="checkbox" name="gst" id="gst" class="" value="1" onclick="checkGst()"
              <?=$sale->gst==1 ? 'checked' : ''?> disabled> <span class=" ml-5 mr-5">Apply
              GST</span>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-lg-4 col-xl-4" id="gst_amount_div" style="display:none">
        <div class="form-group">
          <label>GST Amount </label>
          <input type="text" class="form-control" name="gst_amount" id="gst_amount"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
            placeholder="GST Amount" onkeyup="amountCalculation()" readonly value="<?=$sale->gst_amount?>">
        </div>
      </div>
      <div class="col-md-4 col-lg-4 col-xl-4">
        <div class="form-group">
          <label>Total Amount </label>
          <input type="text" class="form-control" name="total_amount" id="total_amount"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
            placeholder="Total Amount" value="<?=$sale->total_amount?>" readonly>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Advance Payment Amount <span class="text-danger">*</span></label>
      <input type="text" class="form-control" name="paid_amount" id="paid_amount"
        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
        placeholder="Advance Payment Amount" value="<?=$sale->advance_amount?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Pending Amount <span class="text-danger">*</span></label>
      <input type="text" class="form-control" name="pending_amount" id="pending_amount"
        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
        placeholder="Pending Amount" value="<?=$sale->pending_amount?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Payment Mode <span class="text-danger">*</span></label>
      <input type="text" class="form-control" name="payment_mode" id="payment_mode" placeholder="Payment Mode"
        value="<?=$sale->payment_mode?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Lead source </label>
      <input type="text" class="form-control" value="<?=$sale->lead_sourse?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Delivery Date </label>
      <input type="text" onfocus="(this.type='date')" class="form-control" name="delivery_date" id="delivery_date"
        placeholder="Delivery Date" value="<?=$sale->delivery_date?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Content Date </label>
      <input type="text" onfocus="(this.type='date')" class="form-control" name="content_date" id="content_date"
        placeholder="Content Date" value="<?=$sale->content_date?>" readonly>
    </div>
  </div>



  <div class="col-md-12 col-lg-12 col-xl-12">
    <div class="form-group">
      <label>Product/Requirement <span class="text-danger">*</span></label>
      <textarea class="form-control" name="product" id="product" placeholder="Product/Requirement"
        readonly><?=$product_details?></textarea>
    </div>
  </div>

</div>
<script>
$(document).ready(function() {
  checkGst();
});
</script>
<?php
			}else{
				redirect(base_url('dashboard'));
			}
			
			 
		}

		// Payment

		public function payment_history()
	{
		 $this->not_admin_logged_in();
		 $data['uri'] = $this->uri->segment(1);
		 $permission = permission($data['uri']);
		if($permission[0]=='View'){
			$data['permission'] = $permission;
			$data['page_title'] = 'Payment History';
			$data['url']    = $data['uri'];
			$data['admins'] = $this->user_model->get_users(array('users.user_type'=>2));
			//$data['users']  = $this->user_model->get_users();
			$data['users'] = get_users_function(array('users.adminID'=>$this->session->userdata('adminID'),'users.status'=>1),'Sale');
	    $this->admin_template('payment/payment-history',$data);
		}else{
			redirect(base_url('dashboard'));
		}
    
	}


	public function ajaxPaymentHistory(){
		$this->not_admin_logged_in();
		  $data['uri'] = $this->uri->segment(3);
			$permission = permission($data['uri']);
			$payment_permission = 'payment-history';
      $paymentMenu = permission($payment_permission);
      

			$total_amount = 0;
      $grand_total_amount = 0;
			$role = role();
			$condition = $this->session->userdata('user_type')==1 ? array('payment_history.adminID'=>$this->session->userdata('adminID'),'payment_history.status'=>1) : array('payment_history.status'=>1,'payment_history.adminID'=>$this->session->userdata('adminID'));
			$grand_total = $this->sale_model->grand_total_payment_history($condition);

			$payments = $this->sale_model->make_datatables_payment($condition); // this will call modal function for fetching data
			$data = array();
			$paid_amount = 0;
	
			foreach($payments as $key=>$payment) // Loop over the data fetched and store them in array
			{
		
				$button = '';
				$sub_array = array();

				$total_amount +=$payment['amount'];
				$sub_array[] = $key+1;
				//$sub_array[] = $button;
				$sub_array[] = $payment['assigin_to_name'];
				$sub_array[] = $payment['customerName'];
				$sub_array[] = $payment['customerPhone'];
				$sub_array[] = '₹'.$payment['amount'];
				$sub_array[] =$payment['payment_mode'];
				$sub_array[] = date('d-M-Y', strtotime($payment['created_at']));
				$data[] = $sub_array;
			}

			$value[] = array('','','','<b>Total Received</b>','<b>'.'₹'.$total_amount.'</b>','','');

    $data = array_merge($data,$value);


			$grand_total_amount = !empty($grand_total['total_amount_paid']) ? $grand_total['total_amount_paid'] : 0;
	
      $total[] = array('','','','<b class="text-success">Grand Total</b>','<b class="text-success">'.'₹'.$grand_total_amount.'</b>','','');
      
      
      $data = array_merge($data,$total);
		
			$output = array(
				"draw"                    =>     intval($_POST["draw"]),
				"recordsTotal"            =>     $this->sale_model->get_all_data_payment($condition),
				"recordsFiltered"         =>     $this->sale_model->get_filtered_data_payment($condition),
				"data"                    =>     $data
			);
			
			echo json_encode($output);
	  }

		public function viewPaymentForm(){
			$data['uri'] = 'payment-history';
			$permission = permission($data['uri']);
			if($permission[0]=='View'){

				$id         = $this->input->post('id');
				$sale       = $this->sale_model->get_sale(array('sales.id'=>$id));
				$payments   = $this->sale_model->get_payment_histories(array('payment_history.saleID'=>$id));
				$total_paid = 0;
				$gstAmount  = !empty($sale->gst_amount) ? $sale->gst_amount : 0;
				?>
<div class="row">
  <div class="table-responsive">
    <table class=" table table-hover table-center mb-0">
      <thead>
        <tr>
          <th>S.no.</th>
          <th>Added By</th>
          <th>Amount</th>
          <th>Payment Mode</th>
          <th>Payment Date</th>
        </tr>
      </thead>
      <tbody>
        <?php 
					 foreach($payments as $key=>$payment){
							$total_paid += $payment->amount;
				?>
        <tr>
          <td><?=$key+1?></td>
          <td><?=$payment->user_name?></td>
          <td> ₹ <?=$payment->amount?></td>
          <td><?=$payment->payment_mode?></td>
          <td><?=date('d-m-Y h:i A',strtotime($payment->created_at))?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>
<hr>
<div class="row">
  <div class="table-responsive">
    <table class=" table table-hover table-center mb-0">
      <thead>
        <tr>
          <th>Sale Amount</th>
          <?= !empty($sale->gst_amount) ? '<th>GST Amount</th>' : ''?>
          <th>Total Amount</th>
          <th>Paid Amount</th>
          <th>Pending Amount</th>
          <?= !empty($sale->pending_amount) && $sale->pending_amount >0 && $this->session->userdata('user_type') != 1 ? '<th>Add Payment</th>' : ''?>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>₹ <?=$sale->sale_amount?></td>
          <?= !empty($sale->gst_amount) ? '<td>₹'.$sale->gst_amount : 0 .'</td>'?>
          <td>₹<?=$sale->sale_amount + $gstAmount ?></td>
          <td>₹<?=$total_paid?></td>
          <td>₹ <?=$sale->pending_amount?></td>
          <?=  !empty($sale->pending_amount) && $sale->pending_amount >0 && $this->session->userdata('user_type') != 1 ? '<td><button type="button" class="btn btn-block btn-outline-success" onclick="add_payment_history()">Add Payment <i class="fa fa-plus"> </i></button></td>' : ''?>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<hr>
<div class="row" style="display:none" id="add_payment_div">
  <div class="col-md-4 col-lg-4 col-xl-4">
    <div class="form-group">
      <label>Amount <span class="text-danger">*</span></label>
      <input type="text" class="form-control" name="amount" id="amount"
        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Amount">
    </div>
  </div>
  <div class="col-md-4 col-lg-4 col-xl-4">
    <div class="form-group">
      <label>Payment Mode <span class="text-danger">*</span></label>
      <input type="text" class="form-control" name="payment_mode" id="payment_mode" placeholder="Payment Mode">
    </div>
  </div>
  <input type="hidden" name="saleID" id="saleID" value="<?=$id?>">
  <div class="col-md-4 col-lg-4 col-xl-4 mt-2">
    <div class="form-group">
      <label> </label>
      <input type="submit" class="btn btn-block btn-outline-primary " name="submit" value="Add Payment">
    </div>
  </div>
</div>
<?php

			}else{
				redirect(base_url('dashboard'));
			}	
		 }

		 public function store_payment_history()
		 {
			$adminID = $this->session->userdata('adminID');
			$userID = $this->session->userdata('id');
			$saleID = $this->input->post('saleID');
			$amount = $this->input->post('amount');
			$payment_mode = $this->input->post('payment_mode');
      $sale = $this->sale_model->get_sale(array('sales.id'=>$saleID));
			$pending_amount = $sale->pending_amount;
			$advance_amount = $sale->advance_amount;
			if(empty($amount))
			{
				echo json_encode(['status'=>403, 'message'=>'Please enter amount']);  	
				exit();
			}



			if(empty($payment_mode))
			{
				echo json_encode(['status'=>403, 'message'=>'Please enter payment mode']);  	
				exit();
			}

			 $data = array(	
				'adminID'      => $adminID,
				'saleID'       => $saleID,
				'userID'       => $userID,
				'amount'       => $amount,
				'payment_mode' => $payment_mode,
				'remark'       => $payment_mode,
				);
				$store = $this->sale_model->store_payment_history($data);
				if($store)
				{
					$date_sale = array(
						'advance_amount' => $advance_amount+$amount,
            'pending_amount' => $pending_amount-$amount
					);
					$update = $this->sale_model->update_sale($date_sale,array('id'=>$saleID));

				  echo json_encode(['status'=>200, 'message'=>'Payment addes Successfully.Send mail please wait ....','saleID'=>$saleID,'id'=>$userID]);   
				}else{
					echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
				}
		  }


		 public function setSession()
		 {
				$pending_balance = $this->input->post('pending_balance');
				$from_date       = $this->input->post('sale_from_date');
				$to_date         = $this->input->post('sale_to_date');
				$monthname       = $this->input->post('monthname');
				$assign_user     = $this->input->post('assign_user');
				
				$session = array(
						'pending_balance' => $pending_balance,
						'sale_from_date'  => $from_date,
						'sale_to_date'    => $to_date,
						'monthname'       => $monthname,
						'assign_user'     => $assign_user,
					);
						
				$this->session->set_userdata($session);		
				redirect(base_url('sales'));
					
	    }

	public function setSessionPayment()
	{
		$pending_balance = $this->input->post('pending_balance');
		$from_date       = $this->input->post('sale_from_date');
		$to_date         = $this->input->post('sale_to_date');
		$monthname       = $this->input->post('monthname');
		$assign_user     = $this->input->post('assign_user');
		
		$session = array(
				'pending_balance' => $pending_balance,
				'sale_from_date'  => $from_date,
				'sale_to_date'    => $to_date,
				'monthname'       => $monthname,
				'assign_user'     => $assign_user,
			);
				
				$this->session->set_userdata($session);
				
				redirect(base_url('payment-history'));
				
   }
	
		public function resetPending_balance()
		{
			$this->session->unset_userdata('pending_balance');
		}
	
		public function resetFromDate()
		{
			$this->session->unset_userdata('sale_from_date');
			$this->session->unset_userdata('sale_to_date');
	  }
	
		public function resetToDate()
		{
			$this->session->unset_userdata('sale_to_date');
	  }
	
		public function resetMonthName()
		{
			$this->session->unset_userdata('monthname');
	  }
	
		public function resetAssignUser()
		{
			$this->session->unset_userdata('assign_user');
	  }
	
		public function resetContactPerson()
		{
			$this->session->unset_userdata('contact_person');
	  }

	public function delete()
	{
		$id = $this->input->post('id');
		$sale = $this->sale_model->get_sale(array('sales.id' => $id));

		$delete_service      = $this->service_model->delete_service(array('saleID'=>$id));

		$delete_payment      = $this->sale_model->delete_payment(array('saleID'=>$id));

		$delete_sale_history = $this->sale_model->delete_sale_history(array('saleID'=>$id));

		$enquiry_data = array(
			'sale' => 0
		);
		$update_enquiry = $this->enquiry_model->update_enquiry($enquiry_data,array('id'=>$sale->enquiryID));
		$followup_data = array(
			'status' => 0
		);
		$update_followup = $this->followup_model->update_followup($followup_data,array('enquriyID'=>$sale->enquiryID));

		$delete = $this->sale_model->delete_sale(array('id'=>$id));
    if($delete){
		   echo json_encode(['status'=>200, 'message'=>'Sale deleted Successfully']);   
	  }else{
		   echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
	  }

	}

	
}