<?php 
date_default_timezone_set('Asia/Kolkata');
class Send_mail extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('customer_model');
		$this->load->model('user_model');
		$this->load->model('setting_model');
		$this->load->model('enquiry_model');
		$this->load->model('followup_model');
		$this->load->model('sale_model');
    $this->load->model('task_model');
		$this->load->library('csvimport');
	}

  public function customerSendMail(){
    $userID = $this->input->post('id');
    $assigin_user = $this->user_model->get_user(array('users.id'=>$userID));
    $users = send_mail_users($userID);
    $email_data['siteinfo'] = $this->siteinfo();
    $subject = 'Assign Customer';
    $site_data = array('site_name'=>$email_data['siteinfo']->site_name);
    $email_data['assigin_by'] = $assigin_user->name;
    $html = $this->load->view('email-template/assigin-lead',$email_data, true);

    foreach($users as $user)
    {
      $email = $user->email;
      $email_data['name'] = $user->name;
      sendEmail($email,$subject,$html,$site_data);
    }
  }

  public function saleSendMail(){
    $userID = $this->input->post('id');
    $saleID = $this->input->post('saleID');
    $sale = $this->sale_model->get_sale(array('sales.id'=>$saleID));
    $product =	get_services_detail($sale->id);	
		$product_details = !empty($product) ? implode(', ',$product) : '';
    $assigin_user = $this->user_model->get_user(array('users.id'=>$userID));
    $users = send_mail_users($userID);
    $email_data['siteinfo'] = $this->siteinfo();
    $subject = 'Sale';
    $site_data = array('site_name'=>$email_data['siteinfo']->site_name);
    $email_data['name'] = $assigin_user->name;
    $email_data['client_name'] = $sale->customerName;
    $email_data['product_sold'] = $product_details;
    $email_data['total_price'] = $sale->total_amount;
    $email_data['advance_rcvd'] = $sale->advance_amount;
    $html = $this->load->view('email-template/sale-template',$email_data, true);

    foreach($users as $user)
    {
      $email = $user->email;
      sendEmail($email,$subject,$html,$site_data);
    }
  }

  public function paymentSendMail(){
    $userID = $this->input->post('id');
    $saleID = $this->input->post('saleID');
    $sale = $this->sale_model->get_sale(array('sales.id'=>$saleID));
    $product =	get_services_detail($sale->id);	
		$product_details = !empty($product) ? implode(', ',$product) : '';
    $payments   = $this->sale_model->get_payment_histories(array('payment_history.saleID'=>$saleID));
    $assigin_user = $this->user_model->get_user(array('users.id'=>$userID));
    $users = send_mail_users($userID);
    $email_data['siteinfo'] = $this->siteinfo();
    $subject = 'Payment Report';
    $site_data = array('site_name'=>$email_data['siteinfo']->site_name);
    $email_data['name'] = $assigin_user->name;
    $email_data['sale'] = $sale;
    $email_data['payments'] = $payments;
    $email_data['services'] = $product_details;
    $html = $this->load->view('email-template/payment-report',$email_data, true);
    //print_r($html);die;
    $user_ids=array();
    foreach($users as $user)
    {
      $email = $user->email;
      sendEmail($email,$subject,$html,$site_data);
    }
  }

  public function assignEnquerySendMail(){
    $userID = $this->input->post('id');
    $assigin_user = $this->user_model->get_user(array('users.id'=>$userID));
    $users = send_mail_users($userID);
    $email_data['siteinfo'] = $this->siteinfo();
    $subject = 'Assign Customer';
    $site_data = array('site_name'=>$email_data['siteinfo']->site_name);
    $email_data['name'] = $assigin_user->name;
    $html = $this->load->view('email-template/assigin-old-lead',$email_data, true);

    $user_ids=array();
    foreach($users as $user)
    {
      $email = $user->email;
      sendEmail($email,$subject,$html,$site_data);
    }
  }

  public function createTaskSendMail(){
    $assign_to = $this->input->post('assign_to');
    $assign_by = $this->input->post('assign_by');
    $saleID = $this->input->post('saleID');
    $sale = $this->sale_model->get_sale(array('sales.id'=>$saleID));
    $product =	implode(',',get_services_detail($sale->id));	
    $assgin_to_name = array();
    $assgin_to_email = array();
      foreach($assign_to as $user){
          $users = $this->user_model->get_user(array('users.id'=>$user));
          $assgin_to_name[$user] = $users->name;
          $assgin_to_email[$user] = $users->email;
      }
					
      $assigin_by_user = $this->user_model->get_user(array('users.id'=>$assign_by));
      $users = send_mail_users($assign_by);
      $email_data['siteinfo'] = $this->siteinfo();
      $subject = 'Task Assignment of project';
      $site_data = array('site_name'=>$email_data['siteinfo']->site_name);
      $email_data['assgin_to'] = implode(',',$assgin_to_name);
      $email_data['assgin_by'] = $assigin_by_user->name;
      $email_data['product'] = $product ;
      $email_data['heading'] = 'Task Assign to developer/designer';
      $email_data['mail_detail']	= '<div style="font-family: inherit; text-align: inherit"><span style="font-size: 14px">&nbsp; New Task '.$product.'have been assign to '.$email_data['assgin_to'].' by '.$email_data['assgin_by'].' . Kindly check the panel and do the needful</span> </div>';
      //print_r($html);die;
      $user_ids=array();
      foreach($users as $user)
      {
        if($user->id!=$this->session->userdata('id')){
          $email_data['name'] = $user->name ;
          $email = $user->email;
          
          $html = $this->load->view('email-template/task-assign',$email_data, true);
          sendEmail($email,$subject,$html,$site_data);
        }
        
      }

  }

  public function taskUpdateSendMail(){
      $assign_to = $this->input->post('assign_to');
      $taskID = $this->input->post('taskID');
      $task = $this->task_model->get_task(array('task_maneger.id'=>$taskID));
      $task_status = $this->input->post('task_status');
      $product = $task->product;
      $assgin_to_name = array();
      $assgin_to_email = array();
      foreach($assign_to as $user)
      {
        $users = $this->user_model->get_user(array('users.id'=>$user));
        $assgin_to_name[$user] = $users->name;
        $assgin_to_email[$user] = $users->email;
      }
      $assign_by = $task->assign_by;
      if(!empty($task_status) && $this->session->userdata('user_type')==2){
      $assigin_by_user = $this->user_model->get_user(array('users.id'=>$task->assign_by));
      $users = send_mail_users($assign_by);
      $email_data['siteinfo'] = $this->siteinfo();
      
      $site_data = array('site_name'=>$email_data['siteinfo']->site_name);
      $email_data['assgin_to'] = implode(',',$assgin_to_name);
      $email_data['assgin_by'] = $assigin_by_user->name;
      $email_data['product']   = $task->product;

      $user_ids=array();
      if($task_status==1){
        $subject = 'New Task Assigined';
        $email_data['heading'] = 'New Task Assigined' ;
        foreach($assgin_to_email as $key=>$assigin_tos)
        {
          $email_data['name'] = $assgin_to_name[$key] ;
          $email = $assigin_tos;
          $email_data['mail_detail']	= '<div style="font-family: inherit; text-align: inherit"><span style="font-size: 14px">&nbsp; New Task '.$product.'have been assign to '.$email_data['assgin_to'].' by '.$email_data['assgin_by'].' . Kindly check the panel and do the needful</span> </div>';
          $html = $this->load->view('email-template/task-assign',$email_data, true);
          sendEmail($email,$subject,$html,$site_data);
        }
        $subject = 'Task Approved';
        $email_data['heading'] = 'Task Approved' ;
        $mail_detail	= '<div style="font-family: inherit; text-align: inherit"><span style="font-size: 14px">&nbsp; This task ' .$product. ' Approved by Admin.
          </div>';
      }

      if($task_status==2){
        $subject = 'Task Approved';
        $email_data['heading'] = 'Task Disapproved' ;
        $mail_detail	= '<div style="font-family: inherit; text-align: inherit"><span style="font-size: 14px">&nbsp; This task ' .$product. ' Disapproved by Admin.
        </div>';
      }

      foreach($users as $user)
      {
        if($user->user_type!=2)
        {
          $email = $user->email;
          $email_data['name'] = $user->name;
          
          $email_data['mail_detail'] = $mail_detail;
          $html = $this->load->view('email-template/task-assign',$email_data, true);
          sendEmail($email,$subject,$html,$site_data);
        }
      }
    }elseif($task->status==2){
    $assigin_by_user = $this->user_model->get_user(array('users.id'=>$task->assign_by));
    $users = send_mail_users($assign_by);
    $email_data['siteinfo'] = $this->siteinfo();
    
    $site_data = array('site_name'=>$email_data['siteinfo']->site_name);
    $email_data['assgin_to'] = implode(',',$assgin_to_name);
    $email_data['assgin_by'] = $assigin_by_user->name;
    $email_data['product']   = $product;

    $subject = 'New Changes in task';
    $email_data['heading'] = 'New Changes in task' ;
    $user_ids=array();
    $mail_detail	= '<div style="font-family: inherit; text-align: inherit"><span style="font-size: 14px">&nbsp; This task ' .$product. ' new changes by '.$email_data['assgin_by'].'.
    </div>';

    foreach($users as $user)
    {
      if($user->user_type!=$this->session->userdata('id'))
      {
        $email = $user->email;
        $email_data['name'] = $user->name;
        $email_data['mail_detail'] = $mail_detail;
        $html = $this->load->view('email-template/task-assign',$email_data, true);
        sendEmail($email,$subject,$html,$site_data);
      }
    }
  }

  else{
          
    $assigin_by_user = $this->user_model->get_user(array('users.id'=>$this->session->userdata('id')));
    $users = send_mail_users($assign_by);
    $email_data['siteinfo'] = $this->siteinfo();
    $subject = 'Task updated by user';
    $site_data = array('site_name'=>$email_data['siteinfo']->site_name);
    $email_data['assgin_to'] = implode(',',$assgin_to_name);
    $email_data['assgin_by'] = $assigin_by_user->name;
    $email_data['product'] = $product ;
    $email_data['heading'] = 'Task Update';
    $email_data['mail_detail']	= '<div style="font-family: inherit; text-align: inherit"><span style="font-size: 14px">&nbsp; New Task '.$product.'have been updated by '.$email_data['assgin_by'].' . Kindly check the panel and do the needful</span>
            </div>';
    //print_r($html);die;
    $user_ids=array();
    foreach($users as $user)
    {
      if($user->user_type!=$this->session->userdata('id'))
      {
        $email_data['name'] = $user->name ;
        $email = $user->email;
        $html = $this->load->view('email-template/task-assign',$email_data, true);
        sendEmail($email,$subject,$html,$site_data);
      }
    }

    // if($this->session->userdata('user_type')==2)
    // {
    // 	$email_data['name'] = $this->session->userdata('name');
    // 	$email = $this->session->userdata('email');
    // 	$html = $this->load->view('email-template/task-assign',$email_data, true);
    // 	sendEmail($email,$subject,$html,$site_data);
    // }

    // foreach($assgin_to_email as $key=>$assigin_tos)
    // {
    //   $email_data['name'] = $assgin_to_name[$key] ;
    //   $email = $assigin_tos;
    //   $html = $this->load->view('email-template/task-assign',$email_data, true);
    //   sendEmail($email,$subject,$html,$site_data);
    // }

    }
  }

  public function workReportSendEmail(){
    $userID = $this->input->post('id');
    $assigin_by_user = $this->user_model->get_user(array('users.id'=>$userID));
		$user = $this->user_model->get_user(array('users.adminID'=>$this->session->userdata('adminID'),'users.user_type'=>2));
		$email_data['siteinfo'] = $this->siteinfo();
		$subject = 'Work report submiit by '.$assigin_by_user->name;
		$site_data = array('site_name'=>$email_data['siteinfo']->site_name);
		$email_data['assgin_by'] = $assigin_by_user->name;
		//  $email_data['product'] = $product ;
		$email_data['heading'] = 'Work report submiit by '.$assigin_by_user->name;
		$email_data['mail_detail']	= '<div style="font-family: inherit; text-align: inherit"><span style="font-size: 14px">&nbsp; New Task report  submiit by '.$assigin_by_user->name.' . Kindly check the panel and do the needful</span> </div>';
		//print_r($html);die;
		$user_ids=array();

    if($user->id!=$this->session->userdata('id')){
      $email_data['name'] = $user->name ;
      $email = $user->email;
      
      $html = $this->load->view('email-template/task-assign',$email_data, true);
      sendEmail($email,$subject,$html,$site_data);
    }		
  }


}