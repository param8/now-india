<?php 
class Services extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('customer_model');
		$this->load->model('user_model');
		$this->load->model('setting_model');
		$this->load->model('enquiry_model');
		$this->load->model('sale_model');
		$this->load->model('followup_model');
		$this->load->model('service_model');
	}

	public function index()
	{
		$this->not_admin_logged_in();
		$data['uri'] = $this->uri->segment(1);
		$permission = permission($data['uri']);
		if($permission[0]=='View'){
			$data['permission'] = $permission[1];
			$data['page_title'] = 'Services';
			$data['url'] = $data['uri'];
			$data['admins'] = $this->user_model->get_users(array('users.user_type'=>2,'users.status'=>1));
	    $this->admin_template('service/services',$data);
		}else{
			redirect(base_url('dashboard'));
		}
    
	}


	public function ajaxServices(){
		$this->not_admin_logged_in();
		  $data['uri'] = $this->uri->segment(3);
      $permission = permission($data['uri']);
			$role = role();
			$condition = $this->session->userdata('user_type')==1 ? array('services.adminID'=>$this->session->userdata('serviceAdminID'),'services.status'=>1) : 
			($this->session->userdata('user_type')==2 ? array('services.status'=>1,'services.adminID'=>$this->session->userdata('adminID')) :
			($role->parent_role==0 && $this->session->userdata('user_type')!=2 ? array('services.status'=>1,'services.adminID'=>$this->session->userdata('adminID')) :
			array('services.status'=>1,'services.adminID'=>$this->session->userdata('adminID'),'services.userID'=>$this->session->userdata('id'))));
		$services = $this->service_model->make_datatables($condition); // this will call modal function for fetching data
		$data = array();
		//print_r($enquires);die;
		foreach($services as $key=>$service) // Loop over the data fetched and store them in array
		{
	
			$button = '';
			$sub_array = array();
		   if($permission[0]=='View'){
				$button .= '<a href="javascript:void(0)" onclick="view_services('.$service['saleID'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Services Detail" class="btn btn-outline-warning btn-sm"> <i class="fa fa-eye"></i></a>';
			 }

			 if($permission[2]=='Edit' && $this->session->userdata('user_type')!=1){
				$button .= '<a href="'.base_url('edit-service/'.base64_encode($service['id'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit services Detail" class="btn btn-outline-primary btn-sm m-2"> <i class="fa fa-edit"></i></a>';
			 }

			$sub_array[] = $key+1;
			$sub_array[] = $button;
			$sub_array[] = $service['customerName'];
			$sub_array[] = $service['customerPhone'];
			$sub_array[] = $service['company_name'];
			$sub_array[] = $service['assigin_to_name'];
			$sub_array[] = date('d-M-Y', strtotime($service['created_at']));
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->service_model->get_all_data($condition),
			"recordsFiltered"         =>     $this->service_model->get_filtered_data($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	  }

		
		public function create(){
			$this->not_admin_logged_in();
			$data['uri'] = 'services';
			$permission = permission($data['uri']);
			if($permission[1]=='Add'){
				$saleID = base64_decode($this->uri->segment(2));
			  $enquiryID = base64_decode($this->uri->segment(3));
				$data['page_title'] = 'Services';
				$data['saleID'] = $saleID;
				$data['enquiryID'] = $enquiryID;
				$data['service_types'] = $this->service_model->get_service_types(array('status'=>1));
				$this->admin_template('service/create-service',$data);
			}else{
				redirect(base_url('dashboard'));
			}
			
		}

		public function store()
		{
			$adminID      = $this->session->userdata('adminID');		
			$counter      = $this->input->post('counter');
		  $sale_id      = $this->input->post('saleID');
		  $enq_id       = $this->input->post('enquiryID');
		  $services     = $this->input->post('services');
		  $domain       = $this->input->post('domain');
	

			$sale = $this->sale_model->get_sale(array('sales.id'=>$sale_id));
			$user_id = $sale->sale_closed_by;
			if(empty($services))
			{
				echo json_encode(['status'=>403, 'message'=>'Please Select Any service']);
						exit();
			}


			if(in_array('Domain',$services))
			{
				$email_Domain = $this->input->post('domain');
				$hostDomain   = $this->input->post('domain');
				$sslDomain    = $this->input->post('domain'); 
			}else{
				$email_Domain = $this->input->post('emailDomain');
				$hostDomain = $this->input->post('hostDomain');
				$sslDomain = $this->input->post('sslDomain');
			}

				
			if(in_array('Domain',$services)){
				$booking_date = $this->input->post('booking_date');
				$year         = $this->input->post('year');
				$renew_date   = $this->input->post('rDate');
				$domainprice  = $this->input->post('price');
				$domaintprice = $this->input->post('dtotalprice');
				$domainStatus = $this->input->post('domainStatus');
				if(empty($domain)){
					echo json_encode(['status'=>403, 'message'=>'Please enter domain name']);
					exit();
				}
				// $checkDomain = $this->service_model->get_services(array('services.domain_name'=>$domain));
				// if(count($checkDomain) > 0){
				// 	echo json_encode(['status'=>403,'message'=>'This Domain already Purchased']);
				// 	exit();
				// }
				if(empty($booking_date)){
					echo json_encode(['status'=>403, 'message'=>'Please choose booking date']);
					exit();
				}
				if(empty($domainprice)){
					echo json_encode(['status'=>403, 'message'=>'Please enter domain price']);
					exit();
				}
			}
		
			 if(in_array('Email',$services)){

				$emailservice = $this->input->post('emailservice');
				$box_size = $this->input->post('box_size');	
				$box_size_type =  $this->input->post('emailsizetype');
				$numEmail = $this->input->post('numEmail');
				$costperemail = $this->input->post('costpemail');
				$tcostemail = $this->input->post('tcostemail');
				$adminmail = $this->input->post('adminmail');
				$email_booking_date = $this->input->post('email_booking_date');
				$email_year = $this->input->post('email_year');
				$email_rDate = $this->input->post('email_rDate');
				$emailcounter = $this->input->post('emailcounter');
				$emailStatus = $this->input->post('emailStatus');
				$no_email = array();
				//print_r($emailDomain);die;
				for($i=0; $i<$emailcounter; $i++){
					$no_email[]=array($box_size[$i],$box_size_type[$i],$emailservice[$i],$numEmail[$i],$costperemail[$i]);
					$costpemail[$emailDomain]	=	$no_email;
				}

			$email_json_data = json_encode($costpemail);
			     
				if(empty($email_Domain)){
					echo json_encode(['status'=>403, 'message'=>'Please enter emailDomain']);
					exit();
				}
				if(empty($emailservice)){
				    echo json_encode(['status'=>403,'message'=>'Please select Service provider']);
				    exit();
				}

				if(empty(array_filter($box_size))){
					echo json_encode(['status'=>403, 'message'=>'Please enter box size']);
					exit();
				}
				if(empty(array_filter($numEmail))){
					echo json_encode(['status'=>403, 'message'=>'Please enter number of mails']);
					exit();
				}
				if(empty(array_filter($costperemail))){
					echo json_encode(['status'=>403, 'message'=>'Please enter cost per mail']);
					exit();
				}
				if(empty($adminmail)){
					echo json_encode(['status'=>403, 'message'=>'Please enter admin mail id']);
					exit();
				}
				if(empty($email_booking_date)){
					echo json_encode(['status'=>403, 'message'=>'Please enter booking date']);
					exit();
				}

				if(empty($email_year)){
					echo json_encode(['status'=>403, 'message'=>'Please enter email year']);
					exit();
				}
			}
		//	print_r($email_Domain);

			 if(in_array('Hosting',$services)){
				$hostingspace= $this->input->post('hostingspace');
				$hostingspace_type = $this->input->post('hspacetype');
				$hpanel= $this->input->post('hpanel');
				$otherhpanel= $this->input->post('otherpanel');
				$host_booking_date = $this->input->post('host_booking_date');
				$host_ren_year = $this->input->post('hostingyear');
				$host_ren_date = $this->input->post('host_ren_date');
				$hostingcost = $this->input->post('hostingcost');
				$hostingStatus = $this->input->post('hostingStatus');
				if($service != 'domain'){
					if(empty($hostDomain)){
						echo json_encode(['status'=>403, 'message'=>'Please enter hosting Domain']);
						exit();
					}
				}
				if(empty($hostingspace)){
					echo json_encode(['status'=>403, 'message'=>'Please enter hostingspace']);
					exit();
				}
				if(empty($hpanel)){
					echo json_encode(['status'=>403, 'message'=>'Please enter hosting panel']);
					exit();
				}
				if($hpanel=='other'){
					if(empty($otherhpanel)){
						echo json_encode(['status'=>403, 'message'=>'Please Enter hosting panel']);
						exit();
					}
				}
				if(empty($hostingcost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter hosting price']);
					exit();
				}
			}
			
			 if(in_array('SSL',$services)){
				$ssl_booking_date = $this->input->post('ssl_booking_date');
				$ssl_ren_year = $this->input->post('sslyear');
				$ssl_ren_date = $this->input->post('ssl_ren_date');
				$sslcost = $this->input->post('sslcost');
				$sslStatus = $this->input->post('sslStatus');
				if($service != 'domain'){
					if(empty($sslDomain)){
						echo json_encode(['status'=>403, 'message'=>'Please enter SSL Domain']);
							exit();
					}
				}
				if(empty($sslcost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter SSL price']);
					exit();
				}
			}

			 if(in_array('Web Designing',$services)){
				$urlwebdesign= $this->input->post('urlwebdesign');
				$webcost= $this->input->post('webcost');
				$desiginStatus= $this->input->post('desiginStatus');
				if(empty($urlwebdesign)){
					echo json_encode(['status'=>403, 'message'=>'Please enter url for website']);
					exit();
				}
				if(empty($webcost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter website price']);
					exit();
				}
			}

			 if(in_array('Software Development',$services)){
				$softdetail= $this->input->post('softdetail');
				$softcost= $this->input->post('softcost');
				$maintenence= $this->input->post('maintenence');
				$softmaintence= $this->input->post('maintcost');
				$softmaintyear= $this->input->post('maintyear');
				$developStatus= $this->input->post('developStatus');
				
				if(empty($softdetail)){
					echo json_encode(['status'=>403, 'message'=>'Please enter software detail']);
					exit();
				}
				if(empty($softcost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter software price']);
					exit();
				}
				if(!empty($maintenence)){
					if(empty($softmaintence)){
						echo json_encode(['status'=>403, 'message'=>'Please enter maintenence price']);
						exit();
					}
					if(empty($softmaintyear)){
						echo json_encode(['status'=>403, 'message'=>'Please enter maintenence years']);
						exit();
					}
				}
			}

			 if(in_array('Whatsapp',$services)){
				$nWhatsapp= $this->input->post('nWhatsapp');
				$priceWhatsapp= $this->input->post('pWhatsapp');
				$twhatsprice= $this->input->post('twhatsprice');
				$whatsappStatus= $this->input->post('whatsappStatus');

				if(empty($nWhatsapp)){
					echo json_encode(['status'=>403, 'message'=>'Please enter quantity of whatsapp']);
					exit();
				}
				if(empty($priceWhatsapp)){
					echo json_encode(['status'=>403, 'message'=>'Please enter whatsapp price']);
					exit();
				}
			}
			 if(in_array('SMS',$services)){
				$smsqty= $this->input->post('smsqty');
				$pricesms= $this->input->post('pricesms');
				$smsTotalprice= $this->input->post('smsTotalprice');
				$smsStatus= $this->input->post('smsStatus');

				if(empty($smsqty)){
					echo json_encode(['status'=>403, 'message'=>'Please enter sms quantity']);
					exit();
				}
				if(empty($pricesms)){
					echo json_encode(['status'=>403, 'message'=>'Please enter price per sms']);
					exit();
				}
			}

			 if(in_array('SEO',$services)){
				$seourl= $this->input->post('seourl');
				$seocost= $this->input->post('seocost');
				$seodate= $this->input->post('seodate');
				$seo_month= $this->input->post('seo_month');
				$seo_rDate= $this->input->post('seo_rDate');
				$tseocost= $this->input->post('tseocost');
				$seoStatus= $this->input->post('seoStatus');

				if(empty($seourl)){
					echo json_encode(['status'=>403, 'message'=>'Please enter url for seo']);
					exit();
				}
				if(empty($seocost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter seo price']);
					exit();
				}
				if(empty($seodate)){
					echo json_encode(['status'=>403, 'message'=>'Please choose seo date']);
					exit();
				}
				if(empty($seo_month)){
					echo json_encode(['status'=>403, 'message'=>'Please enter seo month']);
					exit();
				}

				if(empty($seo_rDate)){
					echo json_encode(['status'=>403, 'message'=>'Please choose seo renew date']);
					exit();
				}

				if(empty($tseocost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter total seo cost']);
					exit();
				}
			}

			 if(in_array('SMO',$services)){
				$smourl= $this->input->post('smourl');
				$smocost= $this->input->post('smocost');
				$smodate= $this->input->post('smodate');
				$smo_month= $this->input->post('smo_month');
				$smo_rDate= $this->input->post('smo_rDate');
				$tsmocost= $this->input->post('tsmocost');
				$smoStatus= $this->input->post('smoStatus');
				if(empty($smourl)){
						echo json_encode(['status'=>403, 'message'=>'Please enter url for smo']);
						exit();
					}
					if(empty($smocost)){
						echo json_encode(['status'=>403, 'message'=>'Please enter smo price']);
						exit();
					}
					if(empty($smodate)){
						echo json_encode(['status'=>403, 'message'=>'Please choose smo date']);
						exit();
					}
					if(empty($smo_month)){
						echo json_encode(['status'=>403, 'message'=>'Please enter smo month']);
						exit();
					}
	
					if(empty($smo_rDate)){
						echo json_encode(['status'=>403, 'message'=>'Please choose smo renew date']);
						exit();
					}
	
					if(empty($tsmocost)){
						echo json_encode(['status'=>403, 'message'=>'Please enter total smo cost']);
						exit();
					}
			}
			 if(in_array('SMM',$services)){
				$smmurl= $this->input->post('smmurl');
				$smmcost= $this->input->post('smmcost');
				$smmdate= $this->input->post('smmdate');
				$smm_month= $this->input->post('smm_month');
				$smm_rDate= $this->input->post('smm_rDate');
				$tsmmcost= $this->input->post('tsmmcost');
				$smmStatus= $this->input->post('smmStatus');
				if(empty($smmurl)){
					echo json_encode(['status'=>403, 'message'=>'Please enter url for smm']);
					exit();
				}
				if(empty($smmcost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter smm price']);
					exit();
				}
				if(empty($smmdate)){
					echo json_encode(['status'=>403, 'message'=>'Please choose smm date']);
					exit();
				}
				if(empty($smm_month)){
					echo json_encode(['status'=>403, 'message'=>'Please enter smm month']);
					exit();
				}

				if(empty($smm_rDate)){
					echo json_encode(['status'=>403, 'message'=>'Please choose smm renew date']);
					exit();
				}

				if(empty($tsmmcost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter total smm cost']);
					exit();
				}
			}
			 if(in_array('PPC',$services)){
				$ppcurl= $this->input->post('ppcurl');
				$ppccost= $this->input->post('ppccost');
				$ppcdate= $this->input->post('ppcdate');
				$ppc_month= $this->input->post('ppc_month');
				$ppc_rDate= $this->input->post('ppc_rDate');
				$tppccost= $this->input->post('tppccost');
				$ppcStatus= $this->input->post('ppcStatus');
				if(empty($ppcurl)){
					echo json_encode(['status'=>403, 'message'=>'Please enter url for ppc']);
					exit();
				}
				if(empty($ppccost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter ppc price']);
					exit();
				}
				if(empty($ppcdate)){
					echo json_encode(['status'=>403, 'message'=>'Please choose ppc date']);
					exit();
				}
				if(empty($ppc_month)){
					echo json_encode(['status'=>403, 'message'=>'Please enter ppc month']);
					exit();
				}

				if(empty($ppc_rDate)){
					echo json_encode(['status'=>403, 'message'=>'Please choose ppc renew date']);
					exit();
				}

				if(empty($tppccost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter total ppc cost']);
					exit();
				}
			}


			if(in_array('Mass Email',$services)){
				$massEmail_domain= $this->input->post('massEmail_domain');
			  $no_of_email= $this->input->post('no_of_email');
			  $massEmailcost= $this->input->post('massEmailcost');
			  $t_mass_email_ccost= $this->input->post('t_mass_email_ccost');
			  $mass_email_Status= $this->input->post('mass_email_Status');
				if(empty($massEmail_domain)){
					echo json_encode(['status'=>403, 'message'=>'Please enter Domain for Mass Email']);
					exit();
				}

				if(empty($no_of_email)){
					echo json_encode(['status'=>403, 'message'=>'Please Enter No of Mass Email']);
					exit();
				}

				if(empty($massEmailcost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter Mass Email price']);
					exit();
				}

				if(empty($t_mass_email_ccost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter total Mass Email cost']);
					exit();
				}
			}
			

			if(in_array('Maintenence',$services)){

				$websiteMaintenenceDetail= $this->input->post('websiteMaintenenceDetail');
				$maintenencecost= $this->input->post('maintenencecost');
				$maintenenceyear= $this->input->post('maintenenceyear');
				$maintenencedate= $this->input->post('maintenencedate');
				$maintenence_rDate= $this->input->post('maintenence_rDate');
				$tpricemaintenence= $this->input->post('tpricemaintenence');
				$maintenenceStatus= $this->input->post('maintenenceStatus');
					if(empty($websiteMaintenenceDetail)){
						echo json_encode(['status'=>403, 'message'=>'Please enter Maintenence detail']);
						exit();
					}
					if(empty($maintenencecost)){
						echo json_encode(['status'=>403, 'message'=>'Please enter Maintenence price']);
						exit();
					}
					if(empty($maintenencedate)){
						echo json_encode(['status'=>403, 'message'=>'Please choose Maintenence date']);
						exit();
					}
					if(empty($maintenenceyear)){
						echo json_encode(['status'=>403, 'message'=>'Please enter Maintenence month']);
						exit();
					}
	
					if(empty($maintenence_rDate)){
						echo json_encode(['status'=>403, 'message'=>'Please choose Maintenence renew date']);
						exit();
					}
	
					if(empty($tpricemaintenence)){
						echo json_encode(['status'=>403, 'message'=>'Please enter total Maintenence cost']);
						exit();
					}
				}

				if(in_array('Other',$services)){
					$other_detail= $this->input->post('other_detail');
				  $no_of_other= $this->input->post('no_of_other');
				  $othercost= $this->input->post('othercost');
				  $t_other_ccost= $this->input->post('t_other_ccost');
				  $other_Status= $this->input->post('other_Status');
					if(empty($other_detail)){
						echo json_encode(['status'=>403, 'message'=>'Please enter Other detail']);
						exit();
					}
	
					if(empty($no_of_other)){
						echo json_encode(['status'=>403, 'message'=>'Please Enter No of Mass Email']);
						exit();
					}
	
					if(empty($othercost)){
						echo json_encode(['status'=>403, 'message'=>'Please enter Mass Email price']);
						exit();
					}
	
					if(empty($t_other_ccost)){
						echo json_encode(['status'=>403, 'message'=>'Please enter total Mass Email cost']);
						exit();
					}
				}
				

			

			$domain_data =array();
			if(in_array('Domain',$services)){
				$total_price = $domainprice*$year;
				$domain_data['Domain'] =array('domain_name'=>$domain,'domain_booking_date'=>$booking_date,'domain_year'=>$year,'doamin_renew_date'=>$renew_date,'doamin_price'=>$domainprice,'domain_total_price'=>$total_price,'domainStatus'=>$domainStatus);
			}

			$email_data =array();
			if(in_array('Email',$services)){
				$total_price = $domainprice*$year;
				$email_data['Email'] =array('email_domain_name'=>$email_Domain,'email_admin_mail_id'=>$adminmail,'email_booking_date'=>$email_booking_date,'email_year'=>$email_year,'email_rDate'=>$email_rDate,'email_data'=>$email_json_data,'email_total_price'=>$tcostemail,'emailStatus'=>$emailStatus);
			}

			$hosting_data =array();
			if(in_array('Hosting',$services)){
				$total_price = $hostingcost*$host_ren_year;
			  $hosting_panel = !empty($otherhpanel) ? $otherhpanel : $hpanel;
				$hosting_data['Hosting'] =array('hosting_domain_name'=>$hostDomain,'hosting_panel'=>$hpanel,'hosting_panel_name'=>$hosting_panel,'hosting_space'=>$hostingspace,'hosting_space_type'=>$hostingspace_type,'hosting_booking_date'=>$host_booking_date,'hosting_renew_year'=>$host_ren_year,'hosting_renew_date'=>$host_ren_date,'hosting_price'=>$hostingcost,'hosting_total_price' =>$total_price,'hostingStatus'=>$hostingStatus);
			}

			$ssl_data =array();
			if(in_array('SSL',$services)){
				$total_price = $sslcost*$ssl_ren_year;
				$ssl_data['SSL'] =array('ssl_domain_name'=>$sslDomain,'ssl_booking_date'=>$ssl_booking_date,'ssl_renew_year'=>$ssl_ren_year,'ssl_renew_date'=>$ssl_ren_date,'ssl_price'=>$sslcost,'ssl_total_price' =>$total_price,'sslStatus'=>$sslStatus);
			}

			$web_desigin_data =array();
			if(in_array('Web Designing',$services)){
				//$total_price = $sslcost*$ssl_ren_year;
				$web_desigin_data['Web Designing'] =array('web_desigin_url'=>$urlwebdesign,'web_desigin_price'=>$webcost,'desiginStatus'=>$desiginStatus);
			}

			$Software_development_data =array();
			if(in_array('Software Development',$services))
			{
				$mentinance_price_t = !empty($maintenence) ? $softmaintence : 1;
				$mentinance_year_t  = !empty($maintenence) ? $softmaintyear : 1;
				$total_price        = $softcost*$mentinance_price_t*$mentinance_year_t;
				$mentinance_price   = !empty($maintenence) ? $softmaintence : 0;
				$mentinance_year    = !empty($maintenence) ? $softmaintyear : 0;
				
				$Software_development_data['Software Development'] =array('Software_development_detail'=>$softdetail,'Software_development_price'=>$softcost,'Software_development_maintenence'=>$maintenence,'Software_development_maintenence_price'=>$mentinance_price,'Software_development_maintenence_year'=>$mentinance_year,'Software_development_total_price'=>$total_price,'developStatus'=>$developStatus);
			}

			$whatsapp_data =array();
			if(in_array('Whatsapp',$services)){
				$whatsapp_data['Whatsapp'] =array('whatsapp_qty'=>$nWhatsapp,'whatsapp_price'=>$priceWhatsapp,'whatsappStatus'=>$whatsappStatus);
			}

			$sms_data =array();
			if(in_array('SMS',$services)){
				$sms_data['SMS'] =array('sms_qty'=>$smsqty,'sms_price'=>$pricesms,'smsStatus'=>$smsStatus);
			}

			$seo_data =array();
			if(in_array('SEO',$services)){
				$seo_data['SEO'] =array('seo_url'=>$seourl,'seo_price'=>$seocost,'seo_date'=>$seodate,'seo_month'=>$seo_month,'seo_rDate'=>$seo_rDate,'tseocost'=>$tseocost,'seoStatus'=>$seoStatus);
			}

			$smo_data =array();
			if(in_array('SMO',$services)){
				$smo_data['SMO'] =array('smo_url'=>$smourl,'smo_price'=>$smocost,'smo_date'=>$smodate,'smo_month'=>$smo_month,'smo_rDate'=>$smo_rDate,'tsmocost'=>$tsmocost,'smoStatus'=>$smoStatus);
			}

			$smm_data =array();
			if(in_array('SMM',$services)){
				$smm_data['SMM'] =array('smm_url'=>$smmurl,'smm_price'=>$smmcost,'smm_date'=>$smmdate,'smm_month'=>$smm_month,'smm_rDate'=>$smm_rDate,'tsmmcost'=>$tsmmcost,'smmStatus'=>$smmStatus);
			}

			$ppc_data =array();
			if(in_array('PPC',$services)){
				$ppc_data['PPC'] =array('ppc_url'=>$ppcurl,'ppc_price'=>$ppccost,'ppc_date'=>$ppcdate,'ppc_month'=>$ppc_month,'ppc_rDate'=>$ppc_rDate,'tppccost'=>$tppccost,'ppcStatus'=>$ppcStatus);
			}

			$mass_email_data =array();
			if(in_array('Mass Email',$services)){
				$mass_email_data['Mass Email'] =array('mass_email_domain'=>$massEmail_domain,'no_of_email'=>$no_of_email,'mass_email_price'=>$massEmailcost,'t_mass_email_ccost'=>$t_mass_email_ccost,'mass_email_Status'=>$mass_email_Status);
			}


			$maintenence_data =array();
			if(in_array('Maintenence',$services)){
				$maintenence_data['Maintenence'] =array('websiteMaintenenceDetail'=>$websiteMaintenenceDetail,'maintenencecost'=>$maintenencecost,'maintenenceyear'=>$maintenenceyear,'maintenencedate'=>$maintenencedate,'maintenence_rDate'=>$maintenence_rDate,'tpricemaintenence'=>$tpricemaintenence,'maintenenceStatus'=>$maintenenceStatus);
			}

			$other_data =array();
			if(in_array('Other',$services)){
				$other_data['Other'] =array('other_detail'=>$other_detail,'no_of_other'=>$no_of_other,'othercost'=>$othercost,'t_other_ccost'=>$t_other_ccost,'other_Status'=>$other_Status);
			}

			

    $service_array = array_merge($domain_data,$email_data,$hosting_data,$ssl_data,$web_desigin_data,$Software_development_data,$whatsapp_data,$sms_data,$sms_data,$seo_data,$smo_data,$smm_data,$ppc_data,$mass_email_data,$maintenence_data,$other_data);
      
		$service_data = json_encode($service_array);


		$data =array(
			'adminID'        => $adminID,
			'userID'         => $user_id,
			'saleID'         => $sale_id,
			'enquiryID'      => $enq_id,
			'service_detail' => $service_data,
		);

	   $add =  $this->service_model->store_service($data);
	   if($add){
		    echo json_encode(['status'=>200, 'message'=>'Services added successfully. Send mail please wait...','saleID'=>$sale_id,'id'=>$user_id]);
		}else{
			echo json_encode(['status'=>302, 'message'=>'somthing went wrong!']);   
	   	}
	
	}

		public function edit(){

		 $this->not_admin_logged_in();
		 $id = base64_decode($this->uri->segment(2));
		 $data['uri'] = 'services';
		 $permission = permission($data['uri']);
			if($permission[2]=='Edit'){
				$data['permission'] = $permission;
				$data['page_title'] = 'Edit Services';
				$data['url'] = $data['uri'];
				$data['admins'] = $this->user_model->get_users(array('users.user_type'=>2,'users.status'=>1));
				$data['service'] = $this->service_model->get_service(array('services.id'=>$id,'services.adminID'=>$this->session->userdata('adminID')));
				$data['service_types'] = $this->service_model->get_service_types(array('status'=>1));
				$this->admin_template('service/edit-service',$data);
			}else{
				redirect(base_url('dashboard'));
			}
			 
		}


		public function update(){
			$adminID = $this->session->userdata('adminID');
			$serviceID = $this->input->post('serviceID');
			//$user_id = $this->session->userdata('id');
			$counter      = $this->input->post('counter');
		  $sale_id      = $this->input->post('saleID');
		  $enq_id       = $this->input->post('enquiryID');
		  $services     = $this->input->post('services');
			$domain       = $this->input->post('domain');
	

			$sale = $this->sale_model->get_sale(array('sales.id'=>$sale_id));
			$user_id = $sale->sale_closed_by;
			if(empty($services))
			{
				echo json_encode(['status'=>403, 'message'=>'Please Select Any service']);
						exit();
			}


			if(in_array('Domain',$services))
			{
				$email_Domain = $this->input->post('domain');
				$hostDomain   = $this->input->post('domain');
				$sslDomain    = $this->input->post('domain'); 
			}else{
				$email_Domain = $this->input->post('emailDomain');
				$hostDomain = $this->input->post('hostDomain');
				$sslDomain = $this->input->post('sslDomain');
			}

				
			if(in_array('Domain',$services)){
				$booking_date = $this->input->post('booking_date');
				$year         = $this->input->post('year');
				$renew_date   = $this->input->post('rDate');
				$domainprice  = $this->input->post('price');
				$domaintprice = $this->input->post('dtotalprice');
				$domainStatus = $this->input->post('domainStatus');
				if(empty($domain)){
					echo json_encode(['status'=>403, 'message'=>'Please enter domain name']);
					exit();
				}
				// $checkDomain = $this->service_model->get_services(array('services.domain_name'=>$domain));
				// if(count($checkDomain) > 0){
				// 	echo json_encode(['status'=>403,'message'=>'This Domain already Purchased']);
				// 	exit();
				// }
				if(empty($booking_date)){
					echo json_encode(['status'=>403, 'message'=>'Please choose booking date']);
					exit();
				}
				if(empty($domainprice)){
					echo json_encode(['status'=>403, 'message'=>'Please enter domain price']);
					exit();
				}
			}
		
			 if(in_array('Email',$services)){

				$emailservice = $this->input->post('emailservice');
				$box_size = $this->input->post('box_size');	
				$box_size_type =  $this->input->post('emailsizetype');
				$numEmail = $this->input->post('numEmail');
				$costperemail = $this->input->post('costpemail');
				$tcostemail = $this->input->post('tcostemail');
				$adminmail = $this->input->post('adminmail');
				$email_booking_date = $this->input->post('email_booking_date');
				$email_year = $this->input->post('email_year');
				$email_rDate = $this->input->post('email_rDate');
				$emailcounter = $this->input->post('emailcounter');
				$emailStatus = $this->input->post('emailStatus');
				$no_email = array();
				//print_r($emailDomain);die;
				for($i=0; $i<$emailcounter; $i++){
					$no_email[]=array($box_size[$i],$box_size_type[$i],$emailservice[$i],$numEmail[$i],$costperemail[$i]);
					$costpemail[$emailDomain]	=	$no_email;
				}

			$email_json_data = json_encode($costpemail);
			     
				if(empty($email_Domain)){
					echo json_encode(['status'=>403, 'message'=>'Please enter emailDomain']);
					exit();
				}
				if(empty($emailservice)){
				    echo json_encode(['status'=>403,'message'=>'Please select Service provider']);
				    exit();
				}

				if(empty(array_filter($box_size))){
					echo json_encode(['status'=>403, 'message'=>'Please enter box size']);
					exit();
				}
				if(empty(array_filter($numEmail))){
					echo json_encode(['status'=>403, 'message'=>'Please enter number of mails']);
					exit();
				}
				if(empty(array_filter($costperemail))){
					echo json_encode(['status'=>403, 'message'=>'Please enter cost per mail']);
					exit();
				}
				if(empty($adminmail)){
					echo json_encode(['status'=>403, 'message'=>'Please enter admin mail id']);
					exit();
				}
				if(empty($email_booking_date)){
					echo json_encode(['status'=>403, 'message'=>'Please enter booking date']);
					exit();
				}

				if(empty($email_year)){
					echo json_encode(['status'=>403, 'message'=>'Please enter email year']);
					exit();
				}
			}
		//	print_r($email_Domain);

			 if(in_array('Hosting',$services)){
				$hostingspace= $this->input->post('hostingspace');
				$hostingspace_type = $this->input->post('hspacetype');
				$hpanel= $this->input->post('hpanel');
				$otherhpanel= $this->input->post('otherpanel');
				$host_booking_date = $this->input->post('host_booking_date');
				$host_ren_year = $this->input->post('hostingyear');
				$host_ren_date = $this->input->post('host_ren_date');
				$hostingcost = $this->input->post('hostingcost');
				$hostingStatus = $this->input->post('hostingStatus');
				if($service != 'domain'){
					if(empty($hostDomain)){
						echo json_encode(['status'=>403, 'message'=>'Please enter hosting Domain']);
						exit();
					}
				}
				if(empty($hostingspace)){
					echo json_encode(['status'=>403, 'message'=>'Please enter hostingspace']);
					exit();
				}
				if(empty($hpanel)){
					echo json_encode(['status'=>403, 'message'=>'Please enter hosting panel']);
					exit();
				}
				if($hpanel=='other'){
					if(empty($otherhpanel)){
						echo json_encode(['status'=>403, 'message'=>'Please Enter hosting panel']);
						exit();
					}
				}
				if(empty($hostingcost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter hosting price']);
					exit();
				}
			}
			
			 if(in_array('SSL',$services)){
				$ssl_booking_date = $this->input->post('ssl_booking_date');
				$ssl_ren_year = $this->input->post('sslyear');
				$ssl_ren_date = $this->input->post('ssl_ren_date');
				$sslcost = $this->input->post('sslcost');
				$sslStatus = $this->input->post('sslStatus');
				if($service != 'domain'){
					if(empty($sslDomain)){
						echo json_encode(['status'=>403, 'message'=>'Please enter SSL Domain']);
							exit();
					}
				}
				if(empty($sslcost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter SSL price']);
					exit();
				}
			}

			 if(in_array('Web Designing',$services)){
				$urlwebdesign= $this->input->post('urlwebdesign');
				$webcost= $this->input->post('webcost');
				$desiginStatus= $this->input->post('desiginStatus');
				if(empty($urlwebdesign)){
					echo json_encode(['status'=>403, 'message'=>'Please enter url for website']);
					exit();
				}
				if(empty($webcost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter website price']);
					exit();
				}
			}

			 if(in_array('Software Development',$services)){
				$softdetail= $this->input->post('softdetail');
				$softcost= $this->input->post('softcost');
				$maintenence= $this->input->post('maintenence');
				$softmaintence= $this->input->post('maintcost');
				$softmaintyear= $this->input->post('maintyear');
				$developStatus= $this->input->post('developStatus');
				
				if(empty($softdetail)){
					echo json_encode(['status'=>403, 'message'=>'Please enter software detail']);
					exit();
				}
				if(empty($softcost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter software price']);
					exit();
				}
				if(!empty($maintenence)){
					if(empty($softmaintence)){
						echo json_encode(['status'=>403, 'message'=>'Please enter maintenence price']);
						exit();
					}
					if(empty($softmaintyear)){
						echo json_encode(['status'=>403, 'message'=>'Please enter maintenence years']);
						exit();
					}
				}
			}

			 if(in_array('Whatsapp',$services)){
				$nWhatsapp= $this->input->post('nWhatsapp');
				$priceWhatsapp= $this->input->post('pWhatsapp');
				$twhatsprice= $this->input->post('twhatsprice');
				$whatsappStatus= $this->input->post('whatsappStatus');

				if(empty($nWhatsapp)){
					echo json_encode(['status'=>403, 'message'=>'Please enter quantity of whatsapp']);
					exit();
				}
				if(empty($priceWhatsapp)){
					echo json_encode(['status'=>403, 'message'=>'Please enter whatsapp price']);
					exit();
				}
			}
			 if(in_array('SMS',$services)){
				$smsqty= $this->input->post('smsqty');
				$pricesms= $this->input->post('pricesms');
				$smsTotalprice= $this->input->post('smsTotalprice');
				$smsStatus= $this->input->post('smsStatus');

				if(empty($smsqty)){
					echo json_encode(['status'=>403, 'message'=>'Please enter sms quantity']);
					exit();
				}
				if(empty($pricesms)){
					echo json_encode(['status'=>403, 'message'=>'Please enter price per sms']);
					exit();
				}
			}

			 if(in_array('SEO',$services)){
				$seourl= $this->input->post('seourl');
				$seocost= $this->input->post('seocost');
				$seodate= $this->input->post('seodate');
				$seo_month= $this->input->post('seo_month');
				$seo_rDate= $this->input->post('seo_rDate');
				$tseocost= $this->input->post('tseocost');
				$seoStatus= $this->input->post('seoStatus');

				if(empty($seourl)){
					echo json_encode(['status'=>403, 'message'=>'Please enter url for seo']);
					exit();
				}
				if(empty($seocost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter seo price']);
					exit();
				}
				if(empty($seodate)){
					echo json_encode(['status'=>403, 'message'=>'Please choose seo date']);
					exit();
				}
				if(empty($seo_month)){
					echo json_encode(['status'=>403, 'message'=>'Please enter seo month']);
					exit();
				}

				if(empty($seo_rDate)){
					echo json_encode(['status'=>403, 'message'=>'Please choose seo renew date']);
					exit();
				}

				if(empty($tseocost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter total seo cost']);
					exit();
				}
			}

			 if(in_array('SMO',$services)){
				$smourl= $this->input->post('smourl');
				$smocost= $this->input->post('smocost');
				$smodate= $this->input->post('smodate');
				$smo_month= $this->input->post('smo_month');
				$smo_rDate= $this->input->post('smo_rDate');
				$tsmocost= $this->input->post('tsmocost');
				$smoStatus= $this->input->post('smoStatus');
				if(empty($smourl)){
						echo json_encode(['status'=>403, 'message'=>'Please enter url for smo']);
						exit();
					}
					if(empty($smocost)){
						echo json_encode(['status'=>403, 'message'=>'Please enter smo price']);
						exit();
					}
					if(empty($smodate)){
						echo json_encode(['status'=>403, 'message'=>'Please choose smo date']);
						exit();
					}
					if(empty($smo_month)){
						echo json_encode(['status'=>403, 'message'=>'Please enter smo month']);
						exit();
					}
	
					if(empty($smo_rDate)){
						echo json_encode(['status'=>403, 'message'=>'Please choose smo renew date']);
						exit();
					}
	
					if(empty($tsmocost)){
						echo json_encode(['status'=>403, 'message'=>'Please enter total smo cost']);
						exit();
					}
			}
			 if(in_array('SMM',$services)){
				$smmurl= $this->input->post('smmurl');
				$smmcost= $this->input->post('smmcost');
				$smmdate= $this->input->post('smmdate');
				$smm_month= $this->input->post('smm_month');
				$smm_rDate= $this->input->post('smm_rDate');
				$tsmmcost= $this->input->post('tsmmcost');
				$smmStatus= $this->input->post('smmStatus');
				if(empty($smmurl)){
					echo json_encode(['status'=>403, 'message'=>'Please enter url for smm']);
					exit();
				}
				if(empty($smmcost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter smm price']);
					exit();
				}
				if(empty($smmdate)){
					echo json_encode(['status'=>403, 'message'=>'Please choose smm date']);
					exit();
				}
				if(empty($smm_month)){
					echo json_encode(['status'=>403, 'message'=>'Please enter smm month']);
					exit();
				}

				if(empty($smm_rDate)){
					echo json_encode(['status'=>403, 'message'=>'Please choose smm renew date']);
					exit();
				}

				if(empty($tsmmcost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter total smm cost']);
					exit();
				}
			}
			 if(in_array('PPC',$services)){
				$ppcurl= $this->input->post('ppcurl');
				$ppccost= $this->input->post('ppccost');
				$ppcdate= $this->input->post('ppcdate');
				$ppc_month= $this->input->post('ppc_month');
				$ppc_rDate= $this->input->post('ppc_rDate');
				$tppccost= $this->input->post('tppccost');
				$ppcStatus= $this->input->post('ppcStatus');
				if(empty($ppcurl)){
					echo json_encode(['status'=>403, 'message'=>'Please enter url for ppc']);
					exit();
				}
				if(empty($ppccost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter ppc price']);
					exit();
				}
				if(empty($ppcdate)){
					echo json_encode(['status'=>403, 'message'=>'Please choose ppc date']);
					exit();
				}
				if(empty($ppc_month)){
					echo json_encode(['status'=>403, 'message'=>'Please enter ppc month']);
					exit();
				}

				if(empty($ppc_rDate)){
					echo json_encode(['status'=>403, 'message'=>'Please choose ppc renew date']);
					exit();
				}

				if(empty($tppccost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter total ppc cost']);
					exit();
				}
			}


			if(in_array('Mass Email',$services)){
				$massEmail_domain= $this->input->post('massEmail_domain');
			  $no_of_email= $this->input->post('no_of_email');
			  $massEmailcost= $this->input->post('massEmailcost');
			  $t_mass_email_ccost= $this->input->post('t_mass_email_ccost');
			  $mass_email_Status= $this->input->post('mass_email_Status');
				if(empty($massEmail_domain)){
					echo json_encode(['status'=>403, 'message'=>'Please enter Domain for Mass Email']);
					exit();
				}

				if(empty($no_of_email)){
					echo json_encode(['status'=>403, 'message'=>'Please Enter No of Mass Email']);
					exit();
				}

				if(empty($massEmailcost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter Mass Email price']);
					exit();
				}

				if(empty($t_mass_email_ccost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter total Mass Email cost']);
					exit();
				}
			}
			

			if(in_array('Maintenence',$services)){

				$websiteMaintenenceDetail= $this->input->post('websiteMaintenenceDetail');
				$maintenencecost= $this->input->post('maintenencecost');
				$maintenenceyear= $this->input->post('maintenenceyear');
				$maintenencedate= $this->input->post('maintenencedate');
				$maintenence_rDate= $this->input->post('maintenence_rDate');
				$tpricemaintenence= $this->input->post('tpricemaintenence');
				$maintenenceStatus= $this->input->post('maintenenceStatus');
					if(empty($websiteMaintenenceDetail)){
						echo json_encode(['status'=>403, 'message'=>'Please enter Maintenence detail']);
						exit();
					}
					if(empty($maintenencecost)){
						echo json_encode(['status'=>403, 'message'=>'Please enter Maintenence price']);
						exit();
					}
					if(empty($maintenencedate)){
						echo json_encode(['status'=>403, 'message'=>'Please choose Maintenence date']);
						exit();
					}
					if(empty($maintenenceyear)){
						echo json_encode(['status'=>403, 'message'=>'Please enter Maintenence month']);
						exit();
					}
	
					if(empty($maintenence_rDate)){
						echo json_encode(['status'=>403, 'message'=>'Please choose Maintenence renew date']);
						exit();
					}
	
					if(empty($tpricemaintenence)){
						echo json_encode(['status'=>403, 'message'=>'Please enter total Maintenence cost']);
						exit();
					}
				}

				if(in_array('Other',$services)){
					$other_detail= $this->input->post('other_detail');
				  $no_of_other= $this->input->post('no_of_other');
				  $othercost= $this->input->post('othercost');
				  $t_other_ccost= $this->input->post('t_other_ccost');
				  $other_Status= $this->input->post('other_Status');
					if(empty($other_detail)){
						echo json_encode(['status'=>403, 'message'=>'Please enter Other detail']);
						exit();
					}
	
					if(empty($no_of_other)){
						echo json_encode(['status'=>403, 'message'=>'Please Enter No of Mass Email']);
						exit();
					}
	
					if(empty($othercost)){
						echo json_encode(['status'=>403, 'message'=>'Please enter Mass Email price']);
						exit();
					}
	
					if(empty($t_other_ccost)){
						echo json_encode(['status'=>403, 'message'=>'Please enter total Mass Email cost']);
						exit();
					}
				}
				

			

			$domain_data =array();
			if(in_array('Domain',$services)){
				$total_price = $domainprice*$year;
				$domain_data['Domain'] =array('domain_name'=>$domain,'domain_booking_date'=>$booking_date,'domain_year'=>$year,'doamin_renew_date'=>$renew_date,'doamin_price'=>$domainprice,'domain_total_price'=>$total_price,'domainStatus'=>$domainStatus);
			}

			$email_data =array();
			if(in_array('Email',$services)){
				$total_price = $domainprice*$year;
				$email_data['Email'] =array('email_domain_name'=>$email_Domain,'email_admin_mail_id'=>$adminmail,'email_booking_date'=>$email_booking_date,'email_year'=>$email_year,'email_rDate'=>$email_rDate,'email_data'=>$email_json_data,'email_total_price'=>$tcostemail,'emailStatus'=>$emailStatus);
			}

			$hosting_data =array();
			if(in_array('Hosting',$services)){
				$total_price = $hostingcost*$host_ren_year;
			  $hosting_panel = !empty($otherhpanel) ? $otherhpanel : $hpanel;
				$hosting_data['Hosting'] =array('hosting_domain_name'=>$hostDomain,'hosting_panel'=>$hpanel,'hosting_panel_name'=>$hosting_panel,'hosting_space'=>$hostingspace,'hosting_space_type'=>$hostingspace_type,'hosting_booking_date'=>$host_booking_date,'hosting_renew_year'=>$host_ren_year,'hosting_renew_date'=>$host_ren_date,'hosting_price'=>$hostingcost,'hosting_total_price' =>$total_price,'hostingStatus'=>$hostingStatus);
			}

			$ssl_data =array();
			if(in_array('SSL',$services)){
				$total_price = $sslcost*$ssl_ren_year;
				$ssl_data['SSL'] =array('ssl_domain_name'=>$sslDomain,'ssl_booking_date'=>$ssl_booking_date,'ssl_renew_year'=>$ssl_ren_year,'ssl_renew_date'=>$ssl_ren_date,'ssl_price'=>$sslcost,'ssl_total_price' =>$total_price,'sslStatus'=>$sslStatus);
			}

			$web_desigin_data =array();
			if(in_array('Web Designing',$services)){
				//$total_price = $sslcost*$ssl_ren_year;
				$web_desigin_data['Web Designing'] =array('web_desigin_url'=>$urlwebdesign,'web_desigin_price'=>$webcost,'desiginStatus'=>$desiginStatus);
			}

			$Software_development_data =array();
			if(in_array('Software Development',$services))
			{
				$mentinance_price_t = !empty($maintenence) ? $softmaintence : 1;
				$mentinance_year_t  = !empty($maintenence) ? $softmaintyear : 1;
				$total_price        = $softcost*$mentinance_price_t*$mentinance_year_t;
				$mentinance_price   = !empty($maintenence) ? $softmaintence : 0;
				$mentinance_year    = !empty($maintenence) ? $softmaintyear : 0;
				
				$Software_development_data['Software Development'] =array('Software_development_detail'=>$softdetail,'Software_development_price'=>$softcost,'Software_development_maintenence'=>$maintenence,'Software_development_maintenence_price'=>$mentinance_price,'Software_development_maintenence_year'=>$mentinance_year,'Software_development_total_price'=>$total_price,'developStatus'=>$developStatus);
			}

			$whatsapp_data =array();
			if(in_array('Whatsapp',$services)){
				$whatsapp_data['Whatsapp'] =array('whatsapp_qty'=>$nWhatsapp,'whatsapp_price'=>$priceWhatsapp,'whatsappStatus'=>$whatsappStatus);
			}

			$sms_data =array();
			if(in_array('SMS',$services)){
				$sms_data['SMS'] =array('sms_qty'=>$smsqty,'sms_price'=>$pricesms,'smsStatus'=>$smsStatus);
			}

			$seo_data =array();
			if(in_array('SEO',$services)){
				$seo_data['SEO'] =array('seo_url'=>$seourl,'seo_price'=>$seocost,'seo_date'=>$seodate,'seo_month'=>$seo_month,'seo_rDate'=>$seo_rDate,'tseocost'=>$tseocost,'seoStatus'=>$seoStatus);
			}

			$smo_data =array();
			if(in_array('SMO',$services)){
				$smo_data['SMO'] =array('smo_url'=>$smourl,'smo_price'=>$smocost,'smo_date'=>$smodate,'smo_month'=>$smo_month,'smo_rDate'=>$smo_rDate,'tsmocost'=>$tsmocost,'smoStatus'=>$smoStatus);
			}

			$smm_data =array();
			if(in_array('SMM',$services)){
				$smm_data['SMM'] =array('smm_url'=>$smmurl,'smm_price'=>$smmcost,'smm_date'=>$smmdate,'smm_month'=>$smm_month,'smm_rDate'=>$smm_rDate,'tsmmcost'=>$tsmmcost,'smmStatus'=>$smmStatus);
			}

			$ppc_data =array();
			if(in_array('PPC',$services)){
				$ppc_data['PPC'] =array('ppc_url'=>$ppcurl,'ppc_price'=>$ppccost,'ppc_date'=>$ppcdate,'ppc_month'=>$ppc_month,'ppc_rDate'=>$ppc_rDate,'tppccost'=>$tppccost,'ppcStatus'=>$ppcStatus);
			}

			$mass_email_data =array();
			if(in_array('Mass Email',$services)){
				$mass_email_data['Mass Email'] =array('mass_email_domain'=>$massEmail_domain,'no_of_email'=>$no_of_email,'mass_email_price'=>$massEmailcost,'t_mass_email_ccost'=>$t_mass_email_ccost,'mass_email_Status'=>$mass_email_Status);
			}


			$maintenence_data =array();
			if(in_array('Maintenence',$services)){
				$maintenence_data['Maintenence'] =array('websiteMaintenenceDetail'=>$websiteMaintenenceDetail,'maintenencecost'=>$maintenencecost,'maintenenceyear'=>$maintenenceyear,'maintenencedate'=>$maintenencedate,'maintenence_rDate'=>$maintenence_rDate,'tpricemaintenence'=>$tpricemaintenence,'maintenenceStatus'=>$maintenenceStatus);
			}

			$other_data =array();
			if(in_array('Other',$services)){
				$other_data['Other'] =array('other_detail'=>$other_detail,'no_of_other'=>$no_of_other,'othercost'=>$othercost,'t_other_ccost'=>$t_other_ccost,'other_Status'=>$other_Status);
			}

			

    $service_array = array_merge($domain_data,$email_data,$hosting_data,$ssl_data,$web_desigin_data,$Software_development_data,$whatsapp_data,$sms_data,$sms_data,$seo_data,$smo_data,$smm_data,$ppc_data,$mass_email_data,$maintenence_data,$other_data);
      
		$service_data = json_encode($service_array);

    //print_r($service_data);die;
		$data =array(
			'service_detail' => $service_data,
		);

	   $update =  $this->service_model->update_service($data,array('id'=>$serviceID));

		if($update){
		    echo json_encode(['status'=>200, 'message'=>'Services added successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>'somthing went wrong!']);   
	   	}
	
		}

		

		public function get_service_detail(){
				 $id = $this->input->post('id');
				$adminID = $this->session->userdata('adminID');
				$service = $this->service_model->get_service(array('services.saleID'=>$id,'services.adminID'=>$adminID));
        $service_types = $this->service_model->get_service_types(array('status'=>1));
        $details = json_decode($service->service_detail);
				$service_type =array();
				foreach($service_types as $type){
					$service_type[] = $type->name;
				}
        ?>
<div class="row">
  <?php
		foreach($details as $key=>$value){
			if(in_array($key, $service_type)){
		?>
  <div class="col-12 col-md-4 col-lg-4 d-flex">
    <div class="card flex-fill">
      <div class="card-header">
        <h6 class="card-title mb-0"><?=$key?></h6>
      </div>
      <ul class="list-group list-group-flush">
        <?php if($key=='Domain'){?>
        <li class="list-group-item"><strong>Domain Name</strong> : <?=$value->domain_name?></li>
        <li class="list-group-item"><strong>Amount</strong> : ₹ <?=$value->doamin_price?></li>
        <li class="list-group-item text-primary"><strong>Total Amount</strong> : ₹ <?=$value->domain_total_price?></li>
        <li class="list-group-item"><strong>Domain Period</strong> : <?=$value->domain_year?> Year</li>
        <li class="list-group-item text-success"><strong>Booking Date</strong> :
          <?=date('d-M-Y',strtotime($value->domain_booking_date))?></li>
        <li class="list-group-item text-danger"><strong>Expiry Date</strong> :
          <?=date('d-M-Y',strtotime($value->doamin_renew_date))?></li>

        <?php } if($key=='Email'){?>
        <li class="list-group-item"><strong>Domain Name</strong> : <?=$value->email_domain_name?></li>
        <li class="list-group-item"><strong>Email Services</strong> : <?=$value->email_services?></li>
        <?php   
					$email_data = json_decode($value->email_data);
					foreach($email_data as $rows){
					 // print_r($rows);
						foreach($rows as $key1=>$row){
				?>
        <li class="list-group-item"><strong>Box Size <?=$key1+1?></strong> : <?=$row[0] .' '.$value->email_size_type?>
        </li>
        <li class="list-group-item"><strong>Emails <?=$key1+1?></strong> : <?=$row[2]?></li>
        <li class="list-group-item"><strong>Total Emails <?=$key1+1?></strong> : <?=$row[3]?></li>
        <li class="list-group-item"><strong>Email Amount <?=$key1+1?></strong> : ₹ <?=$row[4]?></li>
        <?php } } ?>
        <li class="list-group-item text-primary"><strong>Total Amount</strong> : ₹ <?=$value->email_total_price?></li>
        <li class="list-group-item text-success"><strong>Booking Date</strong> :
          <?=date('d-M-Y',strtotime($value->email_booking_date))?></li>
        <li class="list-group-item text-danger"><strong>Expiry Date</strong> :
          <?=date('d-M-Y',strtotime($value->email_rDate))?></li>

        <?php } if($key=='Hosting'){?>
        <li class="list-group-item"><strong>Domain Name</strong> : <?=$value->hosting_domain_name?></li>
        <li class="list-group-item"><strong>Hosting Panel</strong> : <?=$value->hosting_panel?></li>
        <li class="list-group-item"><strong>Hosting Space</strong> :
          <?=$value->hosting_space .' '.$value->hosting_space_type?></li>
        <li class="list-group-item"><strong>Amount</strong> : ₹ <?=$value->hosting_price?></li>
        <li class="list-group-item"><strong>Total Amount</strong> : ₹ <?=$value->hosting_total_price?></li>
        <li class="list-group-item"><strong>Hosting Period</strong> : <?=$value->hosting_renew_year?> Year</li>
        <li class="list-group-item text-success"><strong>Booking Date</strong> :
          <?=date('d-M-Y',strtotime($value->hosting_booking_date))?></li>
        <li class="list-group-item text-danger"><strong>Expiry Date</strong> :
          <?=date('d-M-Y',strtotime('+'.$value->hosting_renew_year.' year',strtotime($value->hosting_booking_date)))?>
        </li>

        <?php } if($key=='SSL'){?>
        <li class="list-group-item"><strong>Domain Name</strong> : <?=$value->ssl_domain_name?></li>

        <li class="list-group-item"><strong>Amount</strong> : ₹ <?=$value->ssl_price?></li>
        <li class="list-group-item text-primary"><strong>Total Amount</strong> : ₹ <?=$value->ssl_total_price?></li>
        <li class="list-group-item"><strong>SSL Period</strong> : <?=$value->ssl_renew_year?> Year</li>
        <li class="list-group-item text-success"><strong>Booking Date</strong> :
          <?=date('d-M-Y',strtotime($value->ssl_booking_date))?></li>
        <li class="list-group-item text-danger"><strong>Expiry Date</strong> :
          <?=date('d-M-Y',strtotime($value->ssl_renew_date))?></li>

        <?php } if($key=='Web Designing'){?>
        <li class="list-group-item"><strong>Website URL</strong> : <?=$value->web_desigin_url?></li>

        <li class="list-group-item"><strong>Amount</strong> : ₹ <?=$value->web_desigin_price?></li>

        <?php } if($key=='Software Development'){?>
        <li class="list-group-item"><strong>Software Detail</strong> : <?=$value->Software_development_detail?></li>

        <li class="list-group-item"><strong>Amount</strong> : ₹ <?=$value->Software_development_price?></li>
        <?php if($value->Software_development_price=='maintenence'){?>
        <li class="list-group-item"><strong>Maintenance Price</strong> : ₹
          <?=$value->Software_development_maintenence_price?></li>
        <li class="list-group-item"><strong>Maintenance Period</strong> : ₹
          <?=$value->Software_development_maintenence_year?></li>
        <?php } ?>
        <li class="list-group-item text-primary"><strong>Total Amount</strong> : ₹
          <?=$value->Software_development_total_price?></li>
        <?php }  if($key=='Whatsapp'){?>
        <li class="list-group-item"><strong>Whatsapp Qty</strong> : <?=$value->whatsapp_qty?></li>
        <li class="list-group-item"><strong>Amount</strong> : ₹ <?=$value->whatsapp_price?></li>
        <li class="list-group-item text-primary"><strong>Total Amount</strong> : ₹
          <?=$value->whatsapp_price*$value->whatsapp_qty?></li>

        <?php } if($key=='SMS'){?>
        <li class="list-group-item"><strong>SMS Qty</strong> : <?=$value->sms_qty?></li>
        <li class="list-group-item"><strong>Amount</strong> : ₹ <?=$value->sms_price?></li>
        <li class="list-group-item text-primary"><strong>Total Amount</strong> : ₹
          <?=$value->sms_price*$value->sms_qty?></li>

        <?php } if($key=='SEO'){?>
        <li class="list-group-item"><strong>SEO URL</strong> : <?=$value->seo_url?></li>
        <li class="list-group-item"><strong>Total Amount</strong> : ₹ <?=$value->tseocost?></li>
        <li class="list-group-item text-success"><strong>SEO Date</strong> :
          <?=date('d-M-Y',strtotime($value->seo_date))?></li>
        <li class="list-group-item text-danger"><strong>Expiry Date</strong> :
          <?=date('d-M-Y',strtotime($value->seo_rDate))?>
        </li>

        <?php } if($key=='SMO'){?>
        <li class="list-group-item"><strong>SMO URL</strong> : <?=$value->smo_url?></li>
        <li class="list-group-item"><strong>Total Amount</strong> : ₹ <?=$value->tsmocost?></li>
        <li class="list-group-item text-success"><strong>SMO Date</strong> :
          <?=date('d-M-Y',strtotime($value->smo_date))?></li>
        <li class="list-group-item text-danger"><strong>Expiry Date</strong> :
          <?=date('d-M-Y',strtotime($value->smo_rDate))?>
        </li>

        <?php } if($key=='SMM'){?>
        <li class="list-group-item"><strong>SMM URL</strong> : <?=$value->smm_url?></li>
        <li class="list-group-item"><strong>Total Amount</strong> : ₹ <?=$value->tsmmcost?></li>
        <li class="list-group-item text-success"><strong>SMM Date</strong> :
          <?=date('d-M-Y',strtotime($value->smm_date))?></li>
        <li class="list-group-item text-danger"><strong>Expiry Date</strong> :
          <?=date('d-M-Y',strtotime($value->smm_rDate))?>
        </li>

        <?php }  if($key=='PPC'){ ?>
        <li class="list-group-item"><strong>PPC URL</strong> : <?=$value->ppc_url?></li>
        <li class="list-group-item"><strong>Total Amount</strong> : ₹ <?=$value->tppccost?></li>
        <li class="list-group-item text-success"><strong>PPC Date</strong> :
          <?=date('d-M-Y',strtotime($value->ppc_date))?></li>
        <li class="list-group-item text-danger"><strong>Expiry Date</strong> :
          <?=date('d-M-Y',strtotime($value->ppc_rDate))?>
        </li>
        <?php } if($key=='Mass Email'){ ?>
        <li class="list-group-item"><strong>Mass Email Domain</strong> : <?=$value->mass_email_domain?></li>
				<li class="list-group-item"><strong>Mass Email Qty</strong> : <?=$value->no_of_email?></li>
        <li class="list-group-item"><strong>Amount</strong> : ₹ <?=$value->mass_email_price?></li>
        <li class="list-group-item text-primary"><strong>Total Amount</strong> : ₹ <?=$value->t_mass_email_ccost?></li>

        <?php } if($key=='Maintenence'){ ?>
        <li class="list-group-item"><strong>Detail</strong> : <?=$value->websiteMaintenenceDetail?></li>
        <li class="list-group-item"><strong>Total Amount</strong> : ₹ <?=$value->tpricemaintenence?></li>
        <li class="list-group-item text-success"><strong>Maintenence Date</strong> :
          <?=date('d-M-Y',strtotime($value->maintenencedate))?></li>
        <li class="list-group-item text-danger"><strong>Expiry Date</strong> :
          <?=date('d-M-Y',strtotime($value->maintenence_rDate))?>
        </li>
        <?php }  if($key=='Other'){ ?>
					<li class="list-group-item"><strong>Detail</strong> : <?=$value->other_detail?></li>
				<li class="list-group-item"><strong>Qty</strong> : <?=$value->no_of_other?></li>
        <li class="list-group-item"><strong>Amount</strong> : ₹ <?=$value->othercost?></li>
        <li class="list-group-item text-primary"><strong>Total Amount</strong> : ₹ <?=$value->t_other_ccost?></li>
        <?php }  ?>
      </ul>
    </div>
  </div>
  <?php
	
			}
		}
		?>
</div>
<?php
    }


		public function service_renewal()
	{
		$this->not_admin_logged_in();
		$data['uri'] = $this->uri->segment(1);
		$permission = permission($data['uri']);
		$role = role();
		if($permission[0]=='View'){
			$data['permission'] = $permission[1];
			$data['page_title'] = 'Services Renewal';
			$data['url'] = $data['uri'];
			$data['admins'] = $this->user_model->get_users(array('users.user_type'=>2,'users.status'=>1));
			$condition = $this->session->userdata('user_type')==1 ? array('services.adminID'=>$this->session->userdata('serviceAdminID'),'services.status'=>1)  : array('services.status'=>1,'services.adminID'=>$this->session->userdata('adminID'));
			$data['services'] = $this->service_model->make_datatables_renew($condition);
	    $this->admin_template('service/services-renewal',$data);
		}else{
			redirect(base_url('dashboard'));
		}
    
	}

public function create_renewal(){
	$this->not_admin_logged_in();
	$data['uri'] = 'services';
	$permission = permission($data['uri']);
	if($permission[1]=='Add'){
		$id = base64_decode($this->uri->segment(2));
		$data['page_title'] = 'Renewal';
		$data['admins'] = $this->user_model->get_users(array('users.user_type'=>2,'users.status'=>1));
		$data['service'] = $this->service_model->get_service(array('services.id'=>$id,'services.adminID'=>$this->session->userdata('adminID')));
		$data['service_types'] = $this->service_model->get_service_types(array('status'=>1));
		$this->admin_template('service/create-renewal',$data);
	}else{
		redirect(base_url('dashboard'));
	}
	
}

public function store_renewal()
{
		$adminID      = $this->session->userdata('adminID');
		$user_id      = $this->session->userdata('id');
		$counter      = $this->input->post('counter');
		$serviceID    = $this->input->post('serviceID');
	//echo	$oldSaleID    = $this->input->post('oldSaleID');
	//echo	$enq_id       = $this->input->post('enquiryID');die;
		$services     = $this->input->post('services');
		$domain       = $this->input->post('domain');

		$old_service = $this->service_model->get_service(array('services.id'=>$serviceID,'services.adminID'=>$adminID));
		$oldSaleID    = $old_service->saleID;
		$enq_id       = $old_service->enquiryID;

	  $renew_data = 	$this->old_service_data($old_service,$services);

			if(empty($services))
			{
				echo json_encode(['status'=>403, 'message'=>'Please Select Any service']);
						exit();
			}


			if(in_array('Domain',$services))
			{
				$email_Domain = $this->input->post('domain');
				$hostDomain   = $this->input->post('domain');
				$sslDomain    = $this->input->post('domain'); 
			}else{
				$email_Domain = $this->input->post('emailDomain');
				$hostDomain = $this->input->post('hostDomain');
				$sslDomain = $this->input->post('sslDomain');
			}

				
			if(in_array('Domain',$services)){
				$booking_date = $this->input->post('booking_date');
				$year         = $this->input->post('year');
				$renew_date   = $this->input->post('rDate');
				$domainprice  = $this->input->post('price');
				$domaintprice = $this->input->post('dtotalprice');
				$domainStatus = $this->input->post('domainStatus');
				if(empty($domain)){
					echo json_encode(['status'=>403, 'message'=>'Please enter domain name']);
					exit();
				}
				// $checkDomain = $this->service_model->get_services(array('services.domain_name'=>$domain));
				// if(count($checkDomain) > 0){
				// 	echo json_encode(['status'=>403,'message'=>'This Domain already Purchased']);
				// 	exit();
				// }
				if(empty($booking_date)){
					echo json_encode(['status'=>403, 'message'=>'Please choose booking date']);
					exit();
				}
				if(empty($domainprice)){
					echo json_encode(['status'=>403, 'message'=>'Please enter domain price']);
					exit();
				}
			}
		
			 if(in_array('Email',$services)){

				$emailservice = $this->input->post('emailservice');
				$box_size = $this->input->post('box_size');	
				$box_size_type =  $this->input->post('emailsizetype');
				$numEmail = $this->input->post('numEmail');
				$costperemail = $this->input->post('costpemail');
				$tcostemail = $this->input->post('tcostemail');
				$adminmail = $this->input->post('adminmail');
				$email_booking_date = $this->input->post('email_booking_date');
				$email_year = $this->input->post('email_year');
				$email_rDate = $this->input->post('email_rDate');
				$emailcounter = $this->input->post('emailcounter');
				$emailStatus = $this->input->post('emailStatus');
				$no_email = array();
				//print_r($emailDomain);die;
				for($i=0; $i<$emailcounter; $i++){
					$no_email[]=array($box_size[$i],$box_size_type[$i],$emailservice[$i],$numEmail[$i],$costperemail[$i]);
					$costpemail[$emailDomain]	=	$no_email;
				}

			$email_json_data = json_encode($costpemail);
			     
				if(empty($email_Domain)){
					echo json_encode(['status'=>403, 'message'=>'Please enter emailDomain']);
					exit();
				}
				if(empty($emailservice)){
				    echo json_encode(['status'=>403,'message'=>'Please select Service provider']);
				    exit();
				}

				if(empty(array_filter($box_size))){
					echo json_encode(['status'=>403, 'message'=>'Please enter box size']);
					exit();
				}
				if(empty(array_filter($numEmail))){
					echo json_encode(['status'=>403, 'message'=>'Please enter number of mails']);
					exit();
				}
				if(empty(array_filter($costperemail))){
					echo json_encode(['status'=>403, 'message'=>'Please enter cost per mail']);
					exit();
				}
				if(empty($adminmail)){
					echo json_encode(['status'=>403, 'message'=>'Please enter admin mail id']);
					exit();
				}
				if(empty($email_booking_date)){
					echo json_encode(['status'=>403, 'message'=>'Please enter booking date']);
					exit();
				}

				if(empty($email_year)){
					echo json_encode(['status'=>403, 'message'=>'Please enter email year']);
					exit();
				}
			}
		//	print_r($email_Domain);

			 if(in_array('Hosting',$services)){
				$hostingspace= $this->input->post('hostingspace');
				$hostingspace_type = $this->input->post('hspacetype');
				$hpanel= $this->input->post('hpanel');
				$otherhpanel= $this->input->post('otherpanel');
				$host_booking_date = $this->input->post('host_booking_date');
				$host_ren_year = $this->input->post('hostingyear');
				$host_ren_date = $this->input->post('host_ren_date');
				$hostingcost = $this->input->post('hostingcost');
				$hostingStatus = $this->input->post('hostingStatus');
				if($service != 'domain'){
					if(empty($hostDomain)){
						echo json_encode(['status'=>403, 'message'=>'Please enter hosting Domain']);
						exit();
					}
				}
				if(empty($hostingspace)){
					echo json_encode(['status'=>403, 'message'=>'Please enter hostingspace']);
					exit();
				}
				if(empty($hpanel)){
					echo json_encode(['status'=>403, 'message'=>'Please enter hosting panel']);
					exit();
				}
				if($hpanel=='other'){
					if(empty($otherhpanel)){
						echo json_encode(['status'=>403, 'message'=>'Please Enter hosting panel']);
						exit();
					}
				}
				if(empty($hostingcost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter hosting price']);
					exit();
				}
			}
			
			 if(in_array('SSL',$services)){
				$ssl_booking_date = $this->input->post('ssl_booking_date');
				$ssl_ren_year = $this->input->post('sslyear');
				$ssl_ren_date = $this->input->post('ssl_ren_date');
				$sslcost = $this->input->post('sslcost');
				$sslStatus = $this->input->post('sslStatus');
				if($service != 'domain'){
					if(empty($sslDomain)){
						echo json_encode(['status'=>403, 'message'=>'Please enter SSL Domain']);
							exit();
					}
				}
				if(empty($sslcost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter SSL price']);
					exit();
				}
			}

			 if(in_array('Web Designing',$services)){
				$urlwebdesign= $this->input->post('urlwebdesign');
				$webcost= $this->input->post('webcost');
				$desiginStatus= $this->input->post('desiginStatus');
				if(empty($urlwebdesign)){
					echo json_encode(['status'=>403, 'message'=>'Please enter url for website']);
					exit();
				}
				if(empty($webcost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter website price']);
					exit();
				}
			}

			 if(in_array('Software Development',$services)){
				$softdetail= $this->input->post('softdetail');
				$softcost= $this->input->post('softcost');
				$maintenence= $this->input->post('maintenence');
				$softmaintence= $this->input->post('maintcost');
				$softmaintyear= $this->input->post('maintyear');
				$developStatus= $this->input->post('developStatus');
				
				if(empty($softdetail)){
					echo json_encode(['status'=>403, 'message'=>'Please enter software detail']);
					exit();
				}
				if(empty($softcost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter software price']);
					exit();
				}
				if(!empty($maintenence)){
					if(empty($softmaintence)){
						echo json_encode(['status'=>403, 'message'=>'Please enter maintenence price']);
						exit();
					}
					if(empty($softmaintyear)){
						echo json_encode(['status'=>403, 'message'=>'Please enter maintenence years']);
						exit();
					}
				}
			}

			 if(in_array('Whatsapp',$services)){
				$nWhatsapp= $this->input->post('nWhatsapp');
				$priceWhatsapp= $this->input->post('pWhatsapp');
				$twhatsprice= $this->input->post('twhatsprice');
				$whatsappStatus= $this->input->post('whatsappStatus');

				if(empty($nWhatsapp)){
					echo json_encode(['status'=>403, 'message'=>'Please enter quantity of whatsapp']);
					exit();
				}
				if(empty($priceWhatsapp)){
					echo json_encode(['status'=>403, 'message'=>'Please enter whatsapp price']);
					exit();
				}
			}
			 if(in_array('SMS',$services)){
				$smsqty= $this->input->post('smsqty');
				$pricesms= $this->input->post('pricesms');
				$smsTotalprice= $this->input->post('smsTotalprice');
				$smsStatus= $this->input->post('smsStatus');

				if(empty($smsqty)){
					echo json_encode(['status'=>403, 'message'=>'Please enter sms quantity']);
					exit();
				}
				if(empty($pricesms)){
					echo json_encode(['status'=>403, 'message'=>'Please enter price per sms']);
					exit();
				}
			}

			 if(in_array('SEO',$services)){
				$seourl= $this->input->post('seourl');
				$seocost= $this->input->post('seocost');
				$seodate= $this->input->post('seodate');
				$seo_month= $this->input->post('seo_month');
				$seo_rDate= $this->input->post('seo_rDate');
				$tseocost= $this->input->post('tseocost');
				$seoStatus= $this->input->post('seoStatus');

				if(empty($seourl)){
					echo json_encode(['status'=>403, 'message'=>'Please enter url for seo']);
					exit();
				}
				if(empty($seocost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter seo price']);
					exit();
				}
				if(empty($seodate)){
					echo json_encode(['status'=>403, 'message'=>'Please choose seo date']);
					exit();
				}
				if(empty($seo_month)){
					echo json_encode(['status'=>403, 'message'=>'Please enter seo month']);
					exit();
				}

				if(empty($seo_rDate)){
					echo json_encode(['status'=>403, 'message'=>'Please choose seo renew date']);
					exit();
				}

				if(empty($tseocost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter total seo cost']);
					exit();
				}
			}

			 if(in_array('SMO',$services)){
				$smourl= $this->input->post('smourl');
				$smocost= $this->input->post('smocost');
				$smodate= $this->input->post('smodate');
				$smo_month= $this->input->post('smo_month');
				$smo_rDate= $this->input->post('smo_rDate');
				$tsmocost= $this->input->post('tsmocost');
				$smoStatus= $this->input->post('smoStatus');
				if(empty($smourl)){
						echo json_encode(['status'=>403, 'message'=>'Please enter url for smo']);
						exit();
					}
					if(empty($smocost)){
						echo json_encode(['status'=>403, 'message'=>'Please enter smo price']);
						exit();
					}
					if(empty($smodate)){
						echo json_encode(['status'=>403, 'message'=>'Please choose smo date']);
						exit();
					}
					if(empty($smo_month)){
						echo json_encode(['status'=>403, 'message'=>'Please enter smo month']);
						exit();
					}
	
					if(empty($smo_rDate)){
						echo json_encode(['status'=>403, 'message'=>'Please choose smo renew date']);
						exit();
					}
	
					if(empty($tsmocost)){
						echo json_encode(['status'=>403, 'message'=>'Please enter total smo cost']);
						exit();
					}
			}
			 if(in_array('SMM',$services)){
				$smmurl= $this->input->post('smmurl');
				$smmcost= $this->input->post('smmcost');
				$smmdate= $this->input->post('smmdate');
				$smm_month= $this->input->post('smm_month');
				$smm_rDate= $this->input->post('smm_rDate');
				$tsmmcost= $this->input->post('tsmmcost');
				$smmStatus= $this->input->post('smmStatus');
				if(empty($smmurl)){
					echo json_encode(['status'=>403, 'message'=>'Please enter url for smm']);
					exit();
				}
				if(empty($smmcost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter smm price']);
					exit();
				}
				if(empty($smmdate)){
					echo json_encode(['status'=>403, 'message'=>'Please choose smm date']);
					exit();
				}
				if(empty($smm_month)){
					echo json_encode(['status'=>403, 'message'=>'Please enter smm month']);
					exit();
				}

				if(empty($smm_rDate)){
					echo json_encode(['status'=>403, 'message'=>'Please choose smm renew date']);
					exit();
				}

				if(empty($tsmmcost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter total smm cost']);
					exit();
				}
			}
			 if(in_array('PPC',$services)){
				$ppcurl= $this->input->post('ppcurl');
				$ppccost= $this->input->post('ppccost');
				$ppcdate= $this->input->post('ppcdate');
				$ppc_month= $this->input->post('ppc_month');
				$ppc_rDate= $this->input->post('ppc_rDate');
				$tppccost= $this->input->post('tppccost');
				$ppcStatus= $this->input->post('ppcStatus');
				if(empty($ppcurl)){
					echo json_encode(['status'=>403, 'message'=>'Please enter url for ppc']);
					exit();
				}
				if(empty($ppccost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter ppc price']);
					exit();
				}
				if(empty($ppcdate)){
					echo json_encode(['status'=>403, 'message'=>'Please choose ppc date']);
					exit();
				}
				if(empty($ppc_month)){
					echo json_encode(['status'=>403, 'message'=>'Please enter ppc month']);
					exit();
				}

				if(empty($ppc_rDate)){
					echo json_encode(['status'=>403, 'message'=>'Please choose ppc renew date']);
					exit();
				}

				if(empty($tppccost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter total ppc cost']);
					exit();
				}
			}


			if(in_array('Mass Email',$services)){
				$massEmail_domain= $this->input->post('massEmail_domain');
			  $no_of_email= $this->input->post('no_of_email');
			  $massEmailcost= $this->input->post('massEmailcost');
			  $t_mass_email_ccost= $this->input->post('t_mass_email_ccost');
			  $mass_email_Status= $this->input->post('mass_email_Status');
				if(empty($massEmail_domain)){
					echo json_encode(['status'=>403, 'message'=>'Please enter Domain for Mass Email']);
					exit();
				}

				if(empty($no_of_email)){
					echo json_encode(['status'=>403, 'message'=>'Please Enter No of Mass Email']);
					exit();
				}

				if(empty($massEmailcost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter Mass Email price']);
					exit();
				}

				if(empty($t_mass_email_ccost)){
					echo json_encode(['status'=>403, 'message'=>'Please enter total Mass Email cost']);
					exit();
				}
			}
			

			if(in_array('Maintenence',$services)){

				$websiteMaintenenceDetail= $this->input->post('websiteMaintenenceDetail');
				$maintenencecost= $this->input->post('maintenencecost');
				$maintenenceyear= $this->input->post('maintenenceyear');
				$maintenencedate= $this->input->post('maintenencedate');
				$maintenence_rDate= $this->input->post('maintenence_rDate');
				$tpricemaintenence= $this->input->post('tpricemaintenence');
				$maintenenceStatus= $this->input->post('maintenenceStatus');
					if(empty($websiteMaintenenceDetail)){
						echo json_encode(['status'=>403, 'message'=>'Please enter Maintenence detail']);
						exit();
					}
					if(empty($maintenencecost)){
						echo json_encode(['status'=>403, 'message'=>'Please enter Maintenence price']);
						exit();
					}
					if(empty($maintenencedate)){
						echo json_encode(['status'=>403, 'message'=>'Please choose Maintenence date']);
						exit();
					}
					if(empty($maintenenceyear)){
						echo json_encode(['status'=>403, 'message'=>'Please enter Maintenence month']);
						exit();
					}
	
					if(empty($maintenence_rDate)){
						echo json_encode(['status'=>403, 'message'=>'Please choose Maintenence renew date']);
						exit();
					}
	
					if(empty($tpricemaintenence)){
						echo json_encode(['status'=>403, 'message'=>'Please enter total Maintenence cost']);
						exit();
					}
				}

				if(in_array('Other',$services)){
					$other_detail= $this->input->post('other_detail');
				  $no_of_other= $this->input->post('no_of_other');
				  $othercost= $this->input->post('othercost');
				  $t_other_ccost= $this->input->post('t_other_ccost');
				  $other_Status= $this->input->post('other_Status');
					if(empty($other_detail)){
						echo json_encode(['status'=>403, 'message'=>'Please enter Other detail']);
						exit();
					}
	
					if(empty($no_of_other)){
						echo json_encode(['status'=>403, 'message'=>'Please Enter No of Mass Email']);
						exit();
					}
	
					if(empty($othercost)){
						echo json_encode(['status'=>403, 'message'=>'Please enter Mass Email price']);
						exit();
					}
	
					if(empty($t_other_ccost)){
						echo json_encode(['status'=>403, 'message'=>'Please enter total Mass Email cost']);
						exit();
					}
				}
				
		
		$domain_data =array();
		if(in_array('Domain',$services))
		{
			$total_price = $domainprice*$year;
			$domain_data['Domain'] =array('domain_name'=>$domain,'domain_booking_date'=>$booking_date,'domain_year'=>$year,'doamin_renew_date'=>$renew_date,'doamin_price'=>$domainprice,'domain_total_price'=>$total_price,'domainStatus'=>$domainStatus);
		}

		$domain_sale_price = in_array('Domain',$services) ? $domainprice*$year : 0;

		$email_data =array();
		if(in_array('Email',$services))
		{
			$email_data['Email'] =array('email_domain_name'=>$email_Domain,'email_admin_mail_id'=>$adminmail,'email_booking_date'=>$email_booking_date,'email_year'=>$email_year,'email_rDate'=>$email_rDate,'email_data'=>$email_json_data,'email_total_price'=>$tcostemail,'emailStatus'=>$emailStatus);
		}

		$email_sale_price = in_array('Email',$services) ? $tcostemail : 0;

		$hosting_data =array();
		if(in_array('Hosting',$services))
		{
			$total_price   = $hostingcost*$host_ren_year;
			$hosting_panel = !empty($otherhpanel) ? $otherhpanel : $hpanel;
			$hosting_data['Hosting'] = array('hosting_domain_name'=>$hostDomain,'hosting_panel'=>$hpanel,'hosting_panel_name'=>$hosting_panel,'hosting_space'=>$hostingspace,'hosting_space_type'=>$hostingspace_type,'hosting_booking_date'=>$host_booking_date,'hosting_renew_year'=>$host_ren_year,'hosting_renew_date'=>$host_ren_date,'hosting_price'=>$hostingcost,'hosting_total_price' =>$total_price,'hostingStatus'=>$hostingStatus);
		}

		$hosting_sale_price = in_array('Hosting',$services) ? $hostingcost*$host_ren_year : 0;

		$ssl_data =array();
		if(in_array('SSL',$services))
		{
			$total_price = $sslcost*$ssl_ren_year;
			$ssl_data['SSL'] =array('ssl_domain_name'=>$sslDomain,'ssl_booking_date'=>$ssl_booking_date,'ssl_renew_year'=>$ssl_ren_year,'ssl_renew_date'=>$ssl_ren_date,'ssl_price'=>$sslcost,'ssl_total_price' =>$total_price,'sslStatus'=>$sslStatus);
		}
		$ssl_sale_price = in_array('SSL',$services) ? $sslcost*$ssl_ren_year : 0;
		$web_desigin_data =array();
		if(in_array('Web Designing',$services))
		{
			$web_desigin_data['Web Designing'] =array('web_desigin_url'=>$urlwebdesign,'web_desigin_price'=>$webcost,'desiginStatus'=>$desiginStatus);
		}

		$desigin_sale_price = in_array('Web Designing',$services) ? $webcost : 0;

		$Software_development_data =array();
		if(in_array('Software Development',$services))
		{
			$mentinance_price_t = !empty($maintenence) ? $softmaintence : 1;
			$mentinance_year_t = !empty($maintenence) ? $softmaintyear : 1;
			$total_price = $softcost*$mentinance_price_t*$mentinance_year_t;
			$mentinance_price = !empty($maintenence) ? $softmaintence : 0;
			$mentinance_year = !empty($maintenence) ? $softmaintyear : 0;
			
			$Software_development_data['Software Development'] = array('Software_development_detail'=>$softdetail,'Software_development_price'=>$softcost,'Software_development_maintenence'=>$maintenence,'Software_development_maintenence_price'=>$mentinance_price,'Software_development_maintenence_year'=>$mentinance_year,'Software_development_total_price'=>$total_price,'developStatus'=>$developStatus);
		}

		$developer_sale_price = in_array('Software Development',$services) ? $softcost*$mentinance_price_t*$mentinance_year_t : 0;

		$whatsapp_data =array();
		if(in_array('Whatsapp',$services))
		{
			$whatsapp_data['Whatsapp'] = array('whatsapp_qty'=>$nWhatsapp,'whatsapp_price'=>$priceWhatsapp,'whatsappStatus'=>$whatsappStatus);
		}

		$whatsapp_sale_price = in_array('Whatsapp',$services) ? $priceWhatsapp : 0;

		$sms_data =array();
		if(in_array('SMS',$services))
		{
			$sms_data['SMS'] =array('sms_qty'=>$smsqty,'sms_price'=>$pricesms,'smsStatus'=>$smsStatus);
		}

		$sms_sale_price = in_array('SMS',$services) ? $pricesms : 0;

		$seo_data =array();
		if(in_array('SEO',$services))
		{
			$seo_data['SEO'] =array('seo_url'=>$seourl,'seo_price'=>$seocost,'seo_date'=>$seodate,'seo_month'=>$seo_month,'seo_rDate'=>$seo_rDate,'tseocost'=>$tseocost,'seoStatus'=>$seoStatus);
		}

		$seo_sale_price = in_array('SEO',$services) ? $tseocost : 0;

		$smo_data =array();
		if(in_array('SMO',$services))
		{
			$smo_data['SMO'] =array('smo_url'=>$smourl,'smo_price'=>$smocost,'smo_date'=>$smodate,'smo_month'=>$smo_month,'smo_rDate'=>$smo_rDate,'tsmocost'=>$tsmocost,'smoStatus'=>$smoStatus);
		}

		$smo_sale_price = in_array('SMO',$services) ? $tsmocost : 0;

		$smm_data =array();
		if(in_array('SMM',$services))
		{
			$smm_data['SMM'] =array('smm_url'=>$smmurl,'smm_price'=>$smmcost,'smm_date'=>$smmdate,'smm_month'=>$smm_month,'smm_rDate'=>$smm_rDate,'tsmmcost'=>$tsmmcost,'smmStatus'=>$smmStatus);
		}

		$smm_sale_price = in_array('SMM',$services) ? $tsmmcost : 0;

		$ppc_data =array();
		if(in_array('PPC',$services))
		{
			$ppc_data['PPC'] =array('ppc_url'=>$ppcurl,'ppc_price'=>$ppccost,'ppc_date'=>$ppcdate,'ppc_month'=>$ppc_month,'ppc_rDate'=>$ppc_rDate,'tppccost'=>$tppccost,'ppcStatus'=>$ppcStatus);
		}

		$ppc_sale_price = in_array('PPC',$services) ? $tppccost : 0;

		$mass_email_data =array();
			if(in_array('Mass Email',$services)){
				$mass_email_data['Mass Email'] =array('mass_email_domain'=>$massEmail_domain,'no_of_email'=>$no_of_email,'mass_email_price'=>$massEmailcost,'t_mass_email_ccost'=>$t_mass_email_ccost,'mass_email_Status'=>$mass_email_Status);
			}

			$mass_email_sale_price = in_array('Mass Email',$services) ? $t_mass_email_ccost : 0;

			$maintenence_data =array();
			if(in_array('Maintenence',$services)){
				$maintenence_data['Maintenence'] =array('websiteMaintenenceDetail'=>$websiteMaintenenceDetail,'maintenencecost'=>$maintenencecost,'maintenenceyear'=>$maintenenceyear,'maintenencedate'=>$maintenencedate,'maintenence_rDate'=>$maintenence_rDate,'tpricemaintenence'=>$tpricemaintenence,'maintenenceStatus'=>$maintenenceStatus);
			}

		$maintenence_sale_price = in_array('Maintenence',$services) ? $tpricemaintenence : 0;

		$other_data =array();
			if(in_array('Other',$services)){
				$other_data['Other'] =array('other_detail'=>$other_detail,'no_of_other'=>$no_of_other,'othercost'=>$othercost,'t_other_ccost'=>$t_other_ccost,'other_Status'=>$other_Status);
			}

			$other_sale_price = in_array('Other',$services) ? $t_other_ccost : 0;

    $service_array = array_merge($domain_data,$email_data,$hosting_data,$ssl_data,$web_desigin_data,$Software_development_data,$whatsapp_data,$sms_data,$sms_data,$seo_data,$smo_data,$smm_data,$ppc_data,$mass_email_data,$maintenence_data,$other_data);
		
		
	$service_data = json_encode($service_array);

	$sale_amount = $domain_sale_price+$email_sale_price+$hosting_sale_price+$ssl_sale_price+$desigin_sale_price+$developer_sale_price+$whatsapp_sale_price+$sms_sale_price+$seo_sale_price+$smo_sale_price+$smm_sale_price+$ppc_sale_price+$mass_email_sale_price+$maintenence_sale_price+$other_sale_price;

	$gst_amount = ($sale_amount*18)/100;

	$sale = $this->sale_model->get_sale(array('sales.adminID'=>$this->session->userdata('adminID')));
	$sale_id = $sale->id+1;
    $orderID = 'WC'.date('Ymd').$sale_id;

	// print_r($gst_amount);die;
	// print_r($sale_amount);die;
	
	$sale_data = array(	
		'adminID'        => $adminID,
		'enquiryID'      => $enq_id,
		'orderID'        => $orderID,
		'sale_closed_by' => $user_id,
		'sale_create_by' => $user_id,
		'website'        => 'Renewal',
		'product'        => implode(',',$services),
		'gst'            => 1,
		'sale_amount'    => $sale_amount,
		'gst_amount'     => $gst_amount,
		'advance_amount' => $sale_amount+$gst_amount,
		'pending_amount' => 0,
		'total_amount'   => $sale_amount+$gst_amount,
		'payment_mode'   => 'Offline',
		'sale_date'      => date('Y-m-d H:i:s'),
		'content_date'   => date('Y-m-d H:i:s'),
		'sale_type'      => 'Renewal'
	);

	$store_sale = $this->sale_model->store_sale($sale_data);

		$payment_data = array(	
		'adminID'      => $adminID,
		'saleID'       => $store_sale,
		'userID'       => $user_id,
		'amount'       => $sale_amount+$gst_amount,
		'payment_mode' => 'Offline',
		//'remark'       => $product,
		);
		$store_payment = $this->sale_model->store_payment_history($payment_data);

		$sale_history_data = array(	
			'adminID'        => $adminID,
			'saleID'         => $store_sale,
			'enquiryID'      => $enq_id,
			'orderID'        => $orderID,
			'sale_closed_by' => $user_id,
			'sale_create_by' => $user_id,
			'website'        => 'Renewal',
			//'product'        => $product,
			'gst'            => 1,
			'sale_amount'    => $sale_amount,
			'gst_amount'     => $gst_amount,
			'advance_amount' => $sale_amount+$gst_amount,
			'pending_amount' => 0,
			'total_amount'   => $sale_amount+$gst_amount,
			'payment_mode'   => 'Offline',
			'sale_date'      => date('Y-m-d H:i:s'),
			'content_date'   => date('Y-m-d H:i:s'),
			'sale_type'      => 'Renewal'
		);
		$store_sale_history = $this->sale_model->store_sale_history($sale_history_data);



	$data =array(
		'adminID'        => $adminID,
		'userID'         => $user_id,
		'saleID'         => $store_sale,
		'enquiryID'      => $enq_id,
		'service_detail' => $service_data,
	);

	$add =  $this->service_model->store_service($data);
	if($add)
	{
   $update_service= array(	
		'service_detail' => $renew_data
	 );
	 //print_r($update_service);die;
		 $this->service_model->update_service($update_service,array('id'=>$serviceID));

	 echo json_encode(['status'=>200, 'message'=>'Services added successfully!']);
	}else{
	 echo json_encode(['status'=>302, 'message'=>'somthing went wrong!']);   
	}

}

public function old_service_data($old_service,$service){

	$service_list = json_decode($old_service->service_detail);
	// echo "<pre>";
	// print_r($service_list->Domain);die;
	$domain_data_old =array();
		if(!empty($service_list->Domain->domain_total_price))
		{
			$domainStatus = in_array('Domain',$service) ? 'Renewed' : $service_list->Domain->domainStatus;
			$total_price = $domainprice*$year;
			$domain_data_old['Domain'] =array('domain_name'=>$service_list->Domain->domain_name,'domain_booking_date'=>$service_list->Domain->domain_booking_date,'domain_year'=>$service_list->Domain->domain_year,'doamin_renew_date'=>$service_list->Domain->doamin_renew_date,'doamin_price'=>$service_list->Domain->doamin_price,'domain_total_price'=>$service_list->Domain->domain_total_price,'domainStatus'=>$domainStatus);
		}

		$email_data_old =array();
		if(!empty($service_list->Email->email_total_price))
		{
			$emailStatus = in_array('Email',$service) ? 'Renewed' : $service_list->Email->emailStatus;
			$email_data_old['Email'] =array('email_domain_name'=>$service_list->Email->email_domain_name,'email_admin_mail_id'=>$service_list->Email->email_admin_mail_id,'email_booking_date'=>$service_list->Email->email_booking_date,'email_year'=>$service_list->Email->email_year,'email_rDate'=>$service_list->Email->email_rDate,'email_data'=>$service_list->Email->email_data,'email_total_price'=>$service_list->Email->email_total_price,'emailStatus'=>$emailStatus);
		}


		$hosting_data_old =array();
		if(!empty($service_list->Email->hosting_total_price))
		{
			$hostingStatus = in_array('Hosting',$service) ? 'Renewed' : $service_list->Hosting->hostingStatus;
			$total_price   = $hostingcost*$host_ren_year;
			$hosting_panel = !empty($otherhpanel) ? $otherhpanel : $hpanel;
			$hosting_data_old['Hosting'] = array('hosting_domain_name'=>$service_list->Hosting->hosting_domain_name,'hosting_panel'=>$service_list->Hosting->hosting_panel,'hosting_panel_name'=>$service_list->Hosting->hosting_panel_name,'hosting_space'=>$service_list->Hosting->hosting_space,'hosting_space_type'=>$service_list->Hosting->hosting_space_type,'hosting_booking_date'=>$service_list->Hosting->hosting_booking_date,'hosting_renew_year'=>$service_list->Hosting->hosting_renew_year,'hosting_renew_date'=>$service_list->Hosting->hosting_renew_date,'hosting_price'=>$service_list->Hosting->hosting_price,'hosting_total_price' =>$service_list->Hosting->hosting_total_price,'hostingStatus'=>$hostingStatus);
		}


		$ssl_data_old =array();
		if(!empty($service_list->SSL->ssl_total_price))
		{
			$sslStatus = in_array('SSL',$service) ? 'Renewed' : $service_list->SSL->sslStatus;
			$total_price = $sslcost*$ssl_ren_year;
			$ssl_data_old['SSL'] =array('ssl_domain_name'=>$service_list->SSL->ssl_domain_name,'ssl_booking_date'=>$service_list->SSL->ssl_booking_date,'ssl_renew_year'=>$service_list->SSL->ssl_renew_year,'ssl_renew_date'=>$service_list->SSL->ssl_renew_date,'ssl_price'=>$service_list->SSL->ssl_price,'ssl_total_price' =>$service_list->SSL->ssl_total_price,'sslStatus'=>$sslStatus);
		}

		$web_desigin_data_old =array();
		if(!empty($service_list->$web_boject->web_desigin_price))
		{
			$web_boject = 'Web Designing';
			$desiginStatus = in_array('Web Designing',$service) ? 'Renewed' : $service_list->$web_boject->desiginStatus;
			$web_desigin_data_old['Web Designing'] =array('web_desigin_url'=>$service_list->$web_boject->web_desigin_url,'web_desigin_price'=>$service_list->$web_boject->web_desigin_price,'desiginStatus'=>$desiginStatus);
		}



		$Software_development_data_old =array();
		if(!empty($service_list->$Software_development_data->Software_development_total_price))
		{
			$mentinance_price_t = !empty($maintenence) ? $softmaintence : 1;
			$mentinance_year_t = !empty($maintenence) ? $softmaintyear : 1;
			$total_price = $softcost*$mentinance_price_t*$mentinance_year_t;
			$mentinance_price = !empty($maintenence) ? $softmaintence : 0;
			$mentinance_year = !empty($maintenence) ? $softmaintyear : 0;

			$develop_object = 'Software Development';
			$developStatus = in_array('Software Development',$service) ? 'Renewed' : $service_list->$develop_object->developStatus;
			$Software_development_data_old['Software Development'] = array('Software_development_detail'=>$service_list->$Software_development_data->Software_development_detail,'Software_development_price'=>$service_list->$Software_development_data->Software_development_price,'Software_development_maintenence'=>$service_list->$Software_development_data->Software_development_maintenence,'Software_development_maintenence_price'=>$service_list->$Software_development_data->Software_development_maintenence_price,'Software_development_maintenence_year'=>$service_list->$Software_development_data->Software_development_maintenence_year,'Software_development_total_price'=>$service_list->$Software_development_data->Software_development_total_price,'developStatus'=>$developStatus);
		}

	

		$whatsapp_data_old =array();
		if(!empty($service_list->Whatsapp->whatsapp_price))
		{
			$whatsappStatus = in_array('Whatsapp',$service) ? 'Renewed' : $service_list->Whatsapp->whatsappStatus;
			$whatsapp_data_old['Whatsapp'] = array('whatsapp_qty'=>$service_list->Whatsapp->whatsapp_qty,'whatsapp_price'=>$service_list->Whatsapp->whatsapp_price,'whatsappStatus'=>$whatsappStatus);
		}


		$sms_data_old =array();
		if(!empty($service_list->SMS->sms_price))
		{
			$smsStatus = in_array('SMS',$service) ? 'Renewed' : $service_list->SMS->smsStatus;
			$sms_data_old['SMS'] =array('sms_qty'=>$service_list->SMS->sms_qty,'sms_price'=>$service_list->SMS->sms_price,'smsStatus'=>$smsStatus);
		}


		$seo_data_old =array();
		if(!empty($service_list->SEO->tseocost))
		{
			$seoStatus = in_array('SEO',$service) ? 'Renewed' : $service_list->SEO->seoStatus;
			$seo_data_old['SEO'] =array('seo_url'=>$service_list->SEO->seo_url,'seo_price'=>$service_list->SEO->seo_price,'seo_date'=>$service_list->SEO->seo_date,'seo_month'=>$service_list->SEO->seo_month,'seo_rDate'=>$service_list->SEO->seo_rDate,'tseocost'=>$service_list->SEO->tseocost,'seoStatus'=>$seoStatus);
		}


		$smo_data_old =array();
		if(!empty($service_list->SMO->tsmocost))
		{
			$smoStatus = in_array('SMO',$service) ? 'Renewed' : $service_list->SMO->smoStatus;
			$smo_data_old['SMO'] =array('smo_url'=>$service_list->SMO->smo_url,'smo_price'=>$service_list->SMO->smo_price,'smo_date'=>$service_list->SMO->smo_date,'smo_month'=>$service_list->SMO->smo_month,'smo_rDate'=>$service_list->SMO->smo_rDate,'tsmocost'=>$service_list->SMO->tsmocost,'smoStatus'=>$smoStatus);
		}


		$smm_data_old =array();
		if(!empty($service_list->SMM->tsmmcost))
		{
			$smmStatus = in_array('SMM',$service) ? 'Renewed' : $service_list->SMM->smmStatus;
			$smm_data_old['SMM'] =array('smm_url'=>$service_list->SMM->smm_url,'smm_price'=>$service_list->SMM->smm_price,'smm_date'=>$service_list->SMM->smm_date,'smm_month'=>$service_list->SMM->smm_month,'smm_rDate'=>$service_list->SMM->smm_rDate,'tsmmcost'=>$service_list->SMM->tsmmcost,'smmStatus'=>$smmStatus);
		}


		$ppc_data_old =array();
		if(!empty($service_list->PPC->tppccost))
		{
			$ppcStatus = in_array('PPC',$service) ? 'Renewed' : $service_list->PPC->ppcStatus;
			$ppc_data_old['PPC'] =array('ppc_url'=>$service_list->PPC->ppc_url,'ppc_price'=>$service_list->PPC->ppc_price,'ppc_date'=>$service_list->PPC->ppc_date,'ppc_month'=>$service_list->PPC->ppc_month,'ppc_rDate'=>$service_list->PPC->ppc_rDate,'tppccost'=>$service_list->PPC->tppccost,'ppcStatus'=>$ppcStatus);
		}

//print_r($domain_data_old);

	$service_array_old = array_merge($domain_data_old,$email_data_old,$hosting_data_old,$ssl_data_old,$web_desigin_data_old,$Software_development_data_old,$whatsapp_data_old,$sms_data_old,$seo_data_old,$smo_data_old,$smm_data_old,$ppc_data_old);
	
	$service_data_old = json_encode($service_array_old);
	//print_r($service_data_old);die;
	return $service_data_old;
	
}

	
}