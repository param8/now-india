<?php
class Setting extends MY_Controller 
{
    public function __construct()
    {
      parent::__construct();
     $this->not_admin_logged_in();
     $this->load->model('setting_model');
     $this->load->model('user_model');
     $this->load->library('csvimport');
    }

    public function index(){
      $this->not_admin_logged_in();
		  $data['uri'] = $this->uri->segment(1);
		  $permission = permission($data['uri']);
      if($permission[0]=='View'){
        $data['permission'] = $permission[1];
        $data['page_title'] = 'General-Setting';
        $this->admin_template('components/breadcrumb',$data);
        $this->admin_template('setting/setting',$data);
      }else{
        redirect(base_url('dashboard'));
      }
      
    }


   
    public function store_siteInfo(){
      //print_r($_FILES);print_r($_POST);die;
       $id = $this->input->post('adminID');
       $site_name = $this->input->post('site_name');
       $site_contact = $this->input->post('mobile');
       $whatsapp_no = $this->input->post('whatsapp');
       $site_email = $this->input->post('email');
       $site_address = $this->input->post('address');
       $footer_contant = $this->input->post('footer_content');
       $discription = $this->input->post('description');
       $siteinfo = $this->Common_model->get_site_info(array('site_info.adminID'=>$id));
       if(empty($site_name)){
        echo json_encode(['status'=>403, 'message'=>'Please enter site name']); 	
        exit();
       }
       if(empty($site_contact)){
        echo json_encode(['status'=>403, 'message'=>'Please enter site mobile']); 	
        exit();
       }
       if(empty($site_email)){
        echo json_encode(['status'=>403, 'message'=>'Please enter site email']); 	
        exit();
       }

       if(strlen((string)$site_contact) != 10){
        echo json_encode(['status'=>403, 'message'=>'Please enter mobile number 10 digits']);
        exit();
       }
       if(!empty($whatsapp_no)){
       if(strlen((string)$whatsapp_no) != 10){
        echo json_encode(['status'=>403, 'message'=>'Please enter whatsapp number 10 digits']);
        exit();
       }
      }
      // if(empty($footer_contant)){
      //   echo json_encode(['status'=>403, 'message'=>'Please enter footer content']); 	
      //   exit();
      //  }

      //  if(empty($discription)){
      //   echo json_encode(['status'=>403, 'message'=>'Please enter description']); 	
      //   exit();
      //  }

       
     $this->load->library('upload');
    if(!empty($_FILES['image']['name'])){
     $config = array(
      'upload_path' 	=> 'uploads/siteInfo',
      'file_name' 	=> str_replace(' ','',$site_name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif',
      'max_size' 		=> '10000000',
     );
     $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]); 	
          exit();
      }
      else
      {
        $type = explode('.', $_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/siteInfo/'.$config['file_name'].'.'.$type;
      }
    }elseif(!empty($siteinfo->site_logo)){
      $image = $siteinfo->site_logo;
    }else{
      $image = 'public/website/images/dummy_image.jpg';
    }
      $data = array(
       'site_name' => $site_name,
       'site_email' => $site_email,
       'site_contact' => $site_contact,
       'whatsapp_no' => $whatsapp_no,
       'site_address' => $site_address,
       'discription' => $discription,
       'footer_contant' => $footer_contant,
       'site_logo' => $image,
       'facebook_url' => $facebook_url,
       'youtube_url' => $youtube_url,
       'linkedin_url' => $linkedin_url,
       'twitter_url' => $twitter_url,
       'insta_url' => $insta_url,
      
      );

      $update = $this->setting_model->update_siteInfo($data,$id);

      if($update){
        echo json_encode(['status'=>200, 'message'=>'Site-info update successfully!']);
    }else{
        echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
    }

    }


    public function role()
    {  	
      $this->not_admin_logged_in();
      $data['uri'] = $this->uri->segment(1);
      $permission = permission($data['uri']);
      if($permission[0]=='View'){
        $data['page_title'] = 'Role';
        $data['permission'] = $permission[1];
        $data['parent_roles'] = $this->setting_model->get_roles(array('role.status'=>1,'role.id <>'=>1,'role.adminID'=>$this->session->userdata('adminID'),'role.parent_role'=>0));
        $data['admins'] = $this->user_model->get_users(array('users.user_type'=>2));
        $data['uri'] = $data['uri'];
        $this->admin_template('setting/role',$data);
      }else{
        redirect(base_url('dashboard'));
      }
        
    }


    public function ajaxRole(){
      $data['uri'] = $this->uri->segment(3);
      $permission = permission($data['uri']);
      $role = role();
      $condition = $this->session->userdata('user_type')==1 ? array('adminID'=>$this->session->userdata('roleAdminID')) : array('status'=>1,'adminID'=>$this->session->userdata('adminID')); 
		$roles = $this->setting_model->make_datatables_role($condition); // this will call modal function for fetching data
		$data = array();
		//print_r($admins);die;
		foreach($roles as $key=>$role) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
			if($permission[2]=='Edit'){   
        $button .= '<a href="javascript:void(o)" onclick="editRoleModal('.$role['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Role Detail" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
      }

      if($permission[3]=='Delete'){   
        $button .= '<a href="javascript:void(0)" onclick="delete_role('.$role['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Role" class="btn  btn-sm  text-danger"><i class="fa fa-trash"></i> </a>';
      }
			
      		
			$sub_array[] = $key+1;
			$sub_array[] = $button;
			$sub_array[] = $role['name'];
			$sub_array[] = date('d-m-Y', strtotime($role['created_at']));
		
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->setting_model->get_all_data_role($condition),
			"recordsFiltered"         =>     $this->setting_model->get_filtered_data_role($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
    }

    public function store_role(){
      $adminID = $this->session->userdata('adminID');
      $name = $this->input->post('name');
      $parent_role = $this->input->post('parent_role');
      $sale_heading_countion = !empty($this->input->post('sale_heading_countion')) ? $this->input->post('sale_heading_countion') : 0;
      $task_heading_countion = !empty($this->input->post('task_heading_countion')) ? $this->input->post('task_heading_countion') : 0;
      $task_sale = !empty($this->input->post('task_sale')) ? $this->input->post('task_sale') : 0;
      $task_developer = !empty($this->input->post('task_developer')) ? $this->input->post('task_developer') : 0;
      $role = $this->setting_model->get_role(array('name' => $name,'adminID' => $adminID));
      if(empty($name)){
        echo json_encode(['status'=>403, 'message'=>'Please enter role name']); 	
        exit();
       }

       if($name == 'Admin' || $name == 'Super Admin'){
        echo json_encode(['status'=>403, 'message'=>'You have not permissions to add '.$name]); 	
        exit();
       }

       if($role){
        echo json_encode(['status'=>403, 'message'=>'This role is already in use']); 	
        exit();
       }

       $menus = $this->menus();
			 $permission = array();
				foreach($menus as $menu){
			
					$view = !empty($this->input->post('view')[$menu['id']]) ? $this->input->post('view')[$menu['id']] : '';
					$add = !empty($this->input->post('add')[$menu['id']]) ? $this->input->post('add')[$menu['id']] : '';
					$edit = !empty($this->input->post('edit')[$menu['id']]) ? $this->input->post('edit')[$menu['id']] : '';
					$delete = !empty($this->input->post('delete')[$menu['id']]) ? $this->input->post('delete')[$menu['id']] : '';
					$upload = !empty($this->input->post('upload')[$menu['id']]) ? $this->input->post('upload')[$menu['id']] : '';
					$export = !empty($this->input->post('export')[$menu['id']]) ? $this->input->post('export')[$menu['id']] : '';
					$download = !empty($this->input->post('download')[$menu['id']]) ? $this->input->post('download')[$menu['id']] : '';
          $like_admin = !empty($this->input->post('like_admin')[$menu['id']]) ? $this->input->post('like_admin')[$menu['id']] : '';
          $filter = !empty($this->input->post('filter')[$menu['id']]) ? $this->input->post('filter')[$menu['id']] : '';
					$permission[$menu['id']] = array($view,$add,$edit,$delete,$upload,$export,$download,$like_admin,$filter);
					
				}
				
			$permission_json = json_encode($permission);

      $other_permission_array = array('sale_heading_countion'=>$sale_heading_countion,'task_heading_countion'=>$task_heading_countion,'task_sale'=>$task_sale,'task_developer'=>$task_developer);
      $other_permission = json_encode($other_permission_array);

       $data = array(
        'adminID'     => $adminID,
        'name'        => $name,
        'parent_role' => $parent_role,
        'permission'  => $permission_json,
        'other_permission' => $other_permission
       );

       $store = $this->setting_model->store_role($data);
       if($store){
        echo json_encode(['status'=>200, 'message'=>'Role add successfully!']);
      }else{
        echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
      }
    }

    public function editRoleForm(){
      $menues = $this->menus();
		  $permissions = permissions();
      $id = $this->input->post('id');
      $parent_roles = $this->setting_model->get_roles(array('role.status'=>1,'role.id <>'=>1,'role.adminID'=>$this->session->userdata('adminID'),'role.parent_role'=>0));
      //print_r($data['parent_roles']);die;
      $role = $this->setting_model->get_role(array('role.id' => $id));
      $role_other_permission = json_decode($role->other_permission);

      ?>

<div class="row">
  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Role Name <span class="text-danger">*</span></label>
      <input type="text" class="form-control" id="name" name="name" value="<?=$role->name?>"
        placeholder="Enter Role Name">
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Parent Role </label>
      <select class="form-control" name="parent_role" id="parent_role">
        <option value="0">Select Parent</option>
        <?php foreach($parent_roles as $parent_role){?>
        <option value="<?=$parent_role->id?>" <?=$role->parent_role==$parent_role->id ? 'selected' : ''?>>
          <?=$parent_role->name?>
        </option>
        <?php } ?>
      </select>
    </div>
  </div>

  <div class="col-md-12 col-lg-12 col-xl-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Provide Permission</h4>
      </div>

      <div class="card-body">
        <div class="table-responsive">
          <table class=" table table-hover table-center mb-0">
            <thead>
              <tr>
                <th>Menu</th>
                <th>View</th>
                <th>Add</th>
                <th>Edit</th>
                <th>Delete</th>
                <th>Upload</th>
                <th>Export</th>
                <th>Download</th>
                <th>Like Admin</th>
                <th>Filter</th>
              </tr>
            </thead>
            <tbody>
              <?php
                    foreach($menues as $menu){
                      $menuID = $menu['id'];
                      $permission = $role->permission;
                      $permission = json_decode($permission)->$menuID;
                      if($menu['parent_menu']!='Setting'){
                      ?>
              <tr>
                <td><?=$menu['parent_menu']?></td>
                <td><input type="checkbox" name="view[<?=$menu['id']?>]"
                    <?=in_array('View',$permission) ? 'checked' : ''?> value="View"></td>
                <td><input type="checkbox" name="add[<?=$menu['id']?>]"
                    <?=in_array('Add',$permission) ? 'checked' : ''?> value="Add"></td>
                <td><input type="checkbox" name="edit[<?=$menu['id']?>]"
                    <?=in_array('Edit',$permission) ? 'checked' : ''?> value="Edit"></td>
                <td><input type="checkbox" name="delete[<?=$menu['id']?>]"
                    <?=in_array('Delete',$permission) ? 'checked' : ''?> value="Delete"></td>
                <td><input type="checkbox" name="upload[<?=$menu['id']?>]"
                    <?=in_array('Upload',$permission) ? 'checked' : ''?> value="Upload"></td>
                <td><input type="checkbox" name="export[<?=$menu['id']?>]"
                    <?=in_array('Export',$permission) ? 'checked' : ''?> value="Export"></td>
                <td><input type="checkbox" name="download[<?=$menu['id']?>]"
                    <?=in_array('Download',$permission) ? 'checked' : ''?> value="Download"></td>
                <td><input type="checkbox" name="like_admin[<?=$menu['id']?>]" <?=in_array('Like Admin',$permission) ? 'checked' : ''?>  value="Like Admin"></td>
                <td><input type="checkbox" name="filter[<?=$menu['id']?>]" <?=in_array('Filter',$permission) ? 'checked' : ''?> value="Filter"></td>
              </tr>
              <?php } } ?>
            </tbody>
          </table>
        </div>
        <input type="hidden" name="id" id="id" value="<?=$id?>">

      </div>
      <div class="col-md-12 col-lg-12 col-xl-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Provide Other Permission</h4>
          </div>

          <div class="card-body">
            <div class="col-md-6 col-lg-6 col-xl-6">
              <div class="form-group">

                <input type="checkbox" class="form-check-input" id="sale_heading_countion" name="sale_heading_countion"
                  placeholder="Sale Heading Counting" value="1"
                  <?=$role_other_permission->sale_heading_countion==1 ? 'checked' : ''?>>
                <label class="form-check-label" for="sale_heading_countion ml-5">Sale Heading Counting </label>
              </div>
            </div>

            <div class="col-md-6 col-lg-6 col-xl-6">
              <div class="form-group">

                <input type="checkbox" class="form-check-input" id="task_heading_countion" name="task_heading_countion"
                  placeholder="Sale Heading Counting"
                  <?=$role_other_permission->task_heading_countion==1 ? 'checked' : ''?> value="1">
                <label class="form-check-label" for="task_heading_countion ml-5">Task Heading Counting </label>
              </div>
            </div>

            <div class="col-md-6 col-lg-6 col-xl-6">
              <div class="form-group">

                <input type="checkbox" class="form-check-input" id="task_sale" name="task_sale"
                  <?=$role_other_permission->task_sale==1 ? 'checked' : ''?> placeholder="Task Sale" value="1">
                <label class="form-check-label" for="task_sale ml-5">Task Sale</label>
              </div>
            </div>

            <div class="col-md-6 col-lg-6 col-xl-6">
              <div class="form-group">

                <input type="checkbox" class="form-check-input" id="task_developer"
                  <?=$role_other_permission->task_developer==1 ? 'checked' : ''?> name="task_developer"
                  placeholder="Task For Developer" value="1">
                <label class="form-check-label" for="task_developer ml-5">Task For Developer</label>
              </div>
            </div>


          </div>
        </div>
      </div>
    </div>

    <?php
    }
    public function update_role(){
      $adminID = $this->session->userdata('adminID');
      $id = $this->input->post('id');
      $name = $this->input->post('name');
      $parent_role = $this->input->post('parent_role');
      $sale_heading_countion = !empty($this->input->post('sale_heading_countion')) ? $this->input->post('sale_heading_countion') : 0;
      $task_heading_countion = !empty($this->input->post('task_heading_countion')) ? $this->input->post('task_heading_countion') : 0;
      $task_sale = !empty($this->input->post('task_sale')) ? $this->input->post('task_sale') : 0;
      $task_developer = !empty($this->input->post('task_developer')) ? $this->input->post('task_developer') : 0;
      $role = $this->setting_model->get_role(array('name' => $name,'adminID' => $adminID,'id<>'=>$id));

      if(empty($name)){
        echo json_encode(['status'=>403, 'message'=>'Please enter role name']); 	
        exit();
       }

       if($name == 'Admin' || $name == 'Super Admin'){
        echo json_encode(['status'=>403, 'message'=>'You have not permissions to add '.$name]); 	
        exit();
       }

       if($role){
        echo json_encode(['status'=>403, 'message'=>'This role is already in use']); 	
        exit();
       }

      $menus = $this->menus();
			$permission = array();
				foreach($menus as $menu){
		
					$view = !empty($this->input->post('view')[$menu['id']]) ? $this->input->post('view')[$menu['id']] : '';
					$add = !empty($this->input->post('add')[$menu['id']]) ? $this->input->post('add')[$menu['id']] : '';
					$edit = !empty($this->input->post('edit')[$menu['id']]) ? $this->input->post('edit')[$menu['id']] : '';
					$delete = !empty($this->input->post('delete')[$menu['id']]) ? $this->input->post('delete')[$menu['id']] : '';
					$upload = !empty($this->input->post('upload')[$menu['id']]) ? $this->input->post('upload')[$menu['id']] : '';
					$export = !empty($this->input->post('export')[$menu['id']]) ? $this->input->post('export')[$menu['id']] : '';
					$download = !empty($this->input->post('download')[$menu['id']]) ? $this->input->post('download')[$menu['id']] : '';
					$like_admin = !empty($this->input->post('like_admin')[$menu['id']]) ? $this->input->post('like_admin')[$menu['id']] : '';
          $filter = !empty($this->input->post('filter')[$menu['id']]) ? $this->input->post('filter')[$menu['id']] : '';
					$permission[$menu['id']] = array($view,$add,$edit,$delete,$upload,$export,$download,$like_admin,$filter);
					
				}
				
			$permission_json = json_encode($permission);

      $other_permission_array = array('sale_heading_countion'=>$sale_heading_countion,'task_heading_countion'=>$task_heading_countion,'task_sale'=>$task_sale,'task_developer'=>$task_developer);
      $other_permission = json_encode($other_permission_array);

       $data = array(
        'name'        => $name,
        'parent_role' => $parent_role,
        'permission'  => $permission_json,
        'other_permission' => $other_permission
       );

       $update = $this->setting_model->update_role($data,$id);
       if($update){
        echo json_encode(['status'=>200, 'message'=>'Role update successfully!']);
      }else{
        echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
      }
    }

  public function delete_role(){
    $id = $this->input->post('id');
    $data = array(
     'status' => 0,
    );
   $update = $this->user_model->update_user($data,array('user_type',$id));
   if($update){
    $update = $this->setting_model->update_role($data,$id);
   }
  }


  // Email Setting

  public function email_setting(){
      $this->not_admin_logged_in();
		  $data['uri'] = 'setting';
		  $permission = permission($data['uri']);
      if($permission[0]=='View'){
        $data['permission'] = $permission[1];
        $data['page_title'] = 'Email Setting';
        //$data['users'] = $this->user_model->get_users();
        $data['users'] = get_users_function(array('users.status'=>1,'users.adminID'=>$this->session->userdata('adminID')),'Sale');
       
        $this->admin_template('components/breadcrumb',$data);
        $this->admin_template('setting/email-setting',$data);
      }else{
        redirect(base_url('dashboard'));
      }
    
  }


  public function ajaxEmailSetting(){
    $data['uri'] = $this->uri->segment(3);
    $permission = permission($data['uri']);
    $role = role();
    $condition = $this->session->userdata('user_type')==1 ? array('adminID'=>$this->session->userdata('roleAdminID')) : array('status'=>1,'adminID'=>$this->session->userdata('adminID')); 
  $emails = $this->setting_model->make_datatables_email_setting($condition); // this will call modal function for fetching data
  $data = array();
  //print_r($admins);die;
  
  foreach($emails as $key=>$email) // Loop over the data fetched and store them in array
  {
    $user_data =array();
    $users = $this->user_model->get_users_in_condition(array('users.adminID'=>$this->session->userdata('adminID')),explode(',',$email['asigin_email']));
   
    foreach($users as $user){
       $user_data[] =  $user->name . '(<b>'.$user->email.'</b>)';
    }
    $button = '';
    $sub_array = array();
    if($permission[2]=='Edit'){   
      $button .= '<a href="javascript:void(o)" onclick="editEmailSettingModal('.$email['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Role Detail" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
    }

    // if($permission[3]=='Delete'){   
    //   $button .= '<a href="javascript:void(0)" onclick="delete_role('.$role['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Role" class="btn  btn-sm  text-danger"><i class="fa fa-trash"></i> </a>';
    // }
    
        
    $sub_array[] = $key+1;
    $sub_array[] = $button;
    $sub_array[] = $email['type'];
    $sub_array[] = implode(',',$user_data);
    $sub_array[] = $email['subject'];
    $sub_array[] = date('d-m-Y', strtotime($email['created_at']));
  
    $data[] = $sub_array;
  }

  $output = array(
    "draw"                    =>     intval($_POST["draw"]),
    "recordsTotal"            =>     $this->setting_model->get_all_data_email_setting($condition),
    "recordsFiltered"         =>     $this->setting_model->get_filtered_data_email_setting($condition),
    "data"                    =>     $data
  );
  
  echo json_encode($output);
  }

  public function store_email_setting(){
     $adminID = $this->session->userdata('adminID');
     $asigin_email = implode(',',$this->input->post('asigin_email'));
     $type = $this->input->post('type');
     $subject = $this->input->post('subject');
     $email_setting = $this->setting_model->get_email_setting(array('type' => $type,'adminID' => $adminID));
      if(empty($type)){
        echo json_encode(['status'=>403, 'message'=>'Please enter role name']); 	
        exit();
       }

       if($email_setting){
        echo json_encode(['status'=>403, 'message'=>'This email type already exists ']); 	
        exit();
       }

       if(empty($asigin_email)){
        echo json_encode(['status'=>403, 'message'=>'This select assigin email']); 	
        exit();
       }

       if(empty($subject)){
        echo json_encode(['status'=>403, 'message'=>'This enter subject']); 	
        exit();
       }


       $data = array(
        'adminID'      => $adminID,
        'type'         => $type,
        'asigin_email' => $asigin_email,
        'subject'      => $subject
       );

       $store = $this->setting_model->store_email_setting($data);
       if($store){
        echo json_encode(['status'=>200, 'message'=>'Email sessting add successfully!']);
      }else{
        echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
      }
  }


  public function editEmailSettingForm(){

    $id = $this->input->post('id');
    $users = $this->user_model->get_users(array('users.status'=>1,'users.adminID'=>$this->session->userdata('adminID')));
    $email_setting = $this->setting_model->get_email_setting(array('email_setting.id'=>$id));
    $user_id = explode(',',$email_setting->asigin_email);
    
    ?>

    <div class="row">
      <div class="col-md-6 col-lg-6 col-xl-6">
        <div class="form-group">
          <label>Email Type <span class="star-red">*</span></label>
          <input type="text" class="form-control" name="type" value="<?=$email_setting->type?>" readonly>
        </div>
      </div>

      <div class="col-md-6 col-lg-6 col-xl-6">
        <div class="form-group">
          <label>Assigin Email <span class="star-red">*</span></label>
          <select class="form-control " name="asigin_email[]">
            <option value="">Select User</option>
            <?php
                  
                  foreach($users as $user){?>
            <option value="<?=$user->id?>" <?=in_array($user->id,$user_id) ? 'selected': ''?>>
              <?=$user->name.'('.$user->email.')'?></option>
            <?php } ?>

          </select>
        </div>
      </div>

      <div class="col-md-6 col-lg-6 col-xl-6">
        <div class="form-group">
          <label>Subject <span class="star-red">*</span></label>
          <input type="text" class="form-control" name="subject" value="<?=$email_setting->subject?>">
        </div>
      </div>


      <input type="hidden" name="id" id="id" value="<?=$id?>">
    </div>

    <?php
  }

  public function send_email(){

    $data['siteinfo'] = $this->siteinfo();

    $this->load->view('email-template/assigin-lead',$data);
  }



}

 




?>