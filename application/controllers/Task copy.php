<?php 
date_default_timezone_set('Asia/Kolkata');
class Task extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('customer_model');
		$this->load->model('user_model');
		$this->load->model('setting_model');
		$this->load->model('enquiry_model');
		$this->load->model('sale_model');
		$this->load->model('followup_model');
		$this->load->model('service_model');
		$this->load->model('task_model');
	}

	public function index()
	{
		$this->not_admin_logged_in();
		$data['uri'] = $this->uri->segment(1);
		$menu = $this->Common_model->get_menu(array('url'=>$data['uri']));
		$menuID = $menu->id;
		$permission = permissions()->$menuID;
		if($permission[0]=='View'){
			$data['permission'] = $permission;
			$data['page_title'] = 'Task';
			$data['url'] = $data['uri'];
			$data['admins'] = $this->user_model->get_users(array('users.user_type'=>2));
			$data['users'] = $this->user_model->get_users(array('users.adminID'=>$this->session->userdata('adminID'),'users.status'=>1));
	    $this->admin_template('task/task',$data);
		}else{
			redirect(base_url('dashboard'));
		}
    
	}


	public function ajaxTask(){
	  $this->not_admin_logged_in();
	  $data['uri'] = $this->uri->segment(3);
      $menu = $this->Common_model->get_menu(array('url'=>$data['uri']));
      $menuID = $menu->id;
      $permission = permissions()->$menuID;

			$condition = $this->session->userdata('user_type')==1 ? array('sales.adminID'=>$this->session->userdata('saleAdminID'),'sales.status'=>1) : 	array('sales.status'=>1,'sales.adminID'=>$this->session->userdata('adminID'));	
			$tasks = $this->task_model->make_datatables($condition); // this will call modal function for fetching data
			
			$data = array();
			foreach($tasks as $key=>$task) // Loop over the data fetched and store them in array
			{
				
				$sub_array = array();
				$userID = explode(',',$task['assign_to']);
	
        $user_name = array();
				$users = $this->user_model->get_users_in_condition(array('users.adminID'=>$this->session->userdata('adminID')),$userID);
      
				foreach($users as $user){
					$user_name[] = $user->name;
				}

				$button = '';
				
				
				if($permission[0]=='View'){
					$button .= '<a href="javascript:void(0)" onclick="view_task('.$task['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View task Detail" class="btn btn-outline-warning btn-sm"> <i class="fa fa-eye"></i></a>';
				}

				if($permission[2]=='Edit' && $this->session->userdata('user_type')!=1){
					$button .= '<a href="'.base_url('edit-task/'.base64_encode($task['id'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit task Detail" class="btn btn-outline-primary btn-sm m-2"> <i class="fa fa-edit"></i></a>';
				}

				if($permission[3]=='Delete' && $this->session->userdata('user_type')!=1){
					$button .= '<a href="javascript:void(0)" onclick="delete_task('.$task['id'].')"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete task Detail" class="btn btn-outline-danger btn-sm m-2"> <i class="fa fa-trash"></i></a>';
				}

        $task_status = $task['task_status']=='Completed' ? '<a class="btn btn-outline-success btn-sm">Completed <i class="fa fa-check-circle"></i></a>' : ($task['task_status']=='Work In Progress' ? '<a class="btn btn-outline-warning btn-sm">In Progress <i class="fas fa-spinner fa-spin"></i></a>' : '<a class="btn btn-outline-danger btn-sm">Not Started <i class="fa fa-clock-o"></i></a>');
				$sub_array[] = $key+1;
				$sub_array[] = $button;

				$sub_array[] = implode(', ',$user_name);
				$sub_array[] = $task['assigin_by_name'];
				$sub_array[] = '<textarea class="form-control" disabled>'.$task['product'].'</textarea>';
				$sub_array[] = $task['priority'];
				$sub_array[] = $task_status;
				$sub_array[] =  $task['assign_date'] != '0000-00-00' ? date('d-M-Y', strtotime($task['assign_date'])) : '';
				
			
				$data[] = $sub_array;
		
	}

		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->task_model->get_all_data($condition),
			"recordsFiltered"         =>     $this->task_model->get_filtered_data($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	  }

		
		public function create(){
			$this->not_admin_logged_in();
			$data['uri'] = 'task';
			$menu = $this->Common_model->get_menu(array('parent_menu'=>$data['uri']));
			$menuID = $menu->id;
			$permission = permissions()->$menuID;
			if($permission[1]=='Add'){
				$data['page_title'] = 'Task';
				$saleID = base64_decode($this->uri->segment(2));
				$data['sale'] = $this->sale_model->get_sale(array('sales.id' => $saleID));
				$product =	get_services_detail($data['sale']->id);
				$data['roles'] = $this->setting_model->get_roles(array('role.adminID'=>$this->session->userdata('adminID'),'role.status'=>1));	
				$data['product_details'] = !empty($product) ? implode(', ',$product) : $data['sale']->product;
				$this->admin_template('task/create-task',$data);
			}else{
				redirect(base_url('dashboard'));
			}
			
		}

	

		public function store(){

			$adminID = $this->session->userdata('adminID');
			$assign_by = $this->session->userdata('id');
			$saleID = $this->input->post('saleID');
			$product = $this->input->post('product');
			$role = $this->input->post('role');
			$client_detail = $this->input->post('client_detail');
			$priority = $this->input->post('priority');
			$work_status = $this->input->post('work_status');
			$requirement = $this->input->post('requirement');
			$work_status = 'Not Started';


			if(empty($this->input->post('assign_to'))){
					echo json_encode(['status'=>403, 'message'=>'Please select assign to user ']);  	
					exit();
			}
				
			if(empty($priority)){
				echo json_encode(['status'=>403, 'message'=>'Please select priority ']);   	
				exit();
			}


			if(empty($requirement)){
				echo json_encode(['status'=>403, 'message'=>'Please enter work requirement ']);  	
				exit();
			}
			   $assign_to = implode(',', $this->input->post('assign_to'));

					$data = array(	
						'adminID'         => $adminID,
						'role'            => $role,
						'assign_by'       => $assign_by,
						'assign_to'       => $assign_to,
						'saleID'          => $saleID,
						'product'         => $product,
						'priority'        => $priority,
						'work_status'     => $work_status,
						'assign_date'     => date('Y-m-d'),
						'requirement'     => $requirement,
					);

					$store_task = $this->task_model->store_task($data);
					if($store_task){

						$data_task_history = array(	
							'adminID'         => $adminID,
							'taskID'          => $store_task,
							'role'            => $role,
							'assign_by'       => $assign_by,
							'assign_to'       => $assign_to,
							'saleID'          => $saleID,
							'product'         => $product,
							'client_detail'   => $client_detail,
							'priority'        => $priority,
							'work_status'     => $work_status,
							'assign_date'     => date('Y-m-d'),
							'requirement'     => $requirement,
						);
						$store_task_history = $this->task_model->store_task_history($data_task_history);

						$sale_data = array(
							'task_status' => 2
						);

						$this->sale_model->update_sale($sale_data,array('id'=>$saleID));


						echo json_encode(['status'=>200, 'message'=>'Task added Successfully']);   
					}else{
						echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
					}
	
		}




		public function edit(){

			$this->not_admin_logged_in();
			$data['uri'] = 'task';
			$menu = $this->Common_model->get_menu(array('parent_menu'=>$data['uri']));
			$menuID = $menu->id;
			$permission = permissions()->$menuID;
			if($permission[2]=='Edit'){
				$data['page_title'] = 'Task';
				$id = base64_decode($this->uri->segment(2));
				$data['task'] = $this->task_model->get_task(array('task_maneger.id'=>$id));
				$data['sale'] = $this->sale_model->get_sale(array('sales.id' => $data['task']->saleID));
				$data['roles'] = $this->setting_model->get_roles(array('role.adminID'=>$this->session->userdata('adminID'),'role.status'=>1));
				$this->admin_template('task/edit-task',$data);
			}else{
				redirect(base_url('dashboard'));
			}

		}


		public function update(){
			$adminID = $this->session->userdata('adminID');
			$assign_by = $this->session->userdata('id');
			$task_name = $this->input->post('task_name');
			$saleID = $this->input->post('saleID');
			$assign_to = $this->input->post('assign_to');
			$priority = $this->input->post('priority');
			$work_status = $this->input->post('work_status');
			$start_date = !empty($this->input->post('start_date')) ? date('Y-m-d',strtotime($this->input->post('start_date'))) : '';
			$end_date =   !empty($this->input->post('end_date')) ? date('Y-m-d',strtotime($this->input->post('end_date'))) : '';
			$work_percentage = json_encode(array('work'=>$this->input->post('work'),'remaning_work'=>$this->input->post('remaning_work'),'complete_work'=>$this->input->post('complete_work')));
			$tools = implode(',',$this->input->post('tools'));
			$requirement   = $this->input->post('requirement');
			$work_progress = $this->input->post('work_progress');
			

			if(empty($task_name)){
				echo json_encode(['status'=>403, 'message'=>'Please enter task name ']); 	
				exit();
			}
				
		  if(empty($saleID)){
				echo json_encode(['status'=>403, 'message'=>'Please select sale ']); 	
				exit();
			}

			if(empty($assign_to)){
					echo json_encode(['status'=>403, 'message'=>'Please select assign to user ']);  	
					exit();
			}
				
			if(empty($priority)){
				echo json_encode(['status'=>403, 'message'=>'Please select priority ']);   	
				exit();
			}

			if(empty($work_status)){
				echo json_encode(['status'=>403, 'message'=>'Please select work status ']);  	
				exit();
			}

			if(empty($requirement)){
				echo json_encode(['status'=>403, 'message'=>'Please enter work requirement ']);  	
				exit();
			}

					$data = array(	
						'adminID'         => $adminID,
						'assign_by'       => $assign_by,
						'assign_to'       => $assign_to,
						'saleID'          => $saleID,
						'task_name'       => $task_name,
						'work_status'     => $work_status,
						'work_percentage' => $work_percentage,
						'work_progress'   => $work_progress,
						'priority'        => $priority,
						'start_date'      => $start_date,
						'end_date'        => $end_date,
						'tools'           => $tools,
						'requirement'     => $requirement,
					);

					$store_task = $this->task_model->store_task($data);
					if($store_task){

						$data_task_history = array(	
							'adminID'         => $adminID,
							'taskID'          => $store_task,
							'assign_by'       => $assign_by,
							'assign_to'       => $assign_to,
							'saleID'          => $saleID,
							'task_name'       => $task_name,
							'work_status'     => $work_status,
							'work_percentage' => $work_percentage,
							'work_progress'   => $work_progress,
							'priority'        => $priority,
							'start_date'      => $start_date,
							'end_date'        => $end_date,
							'tools'           => $tools,
							'requirement'     => $requirement,
						);
						$store_task_history = $this->task_model->store_task_history($data_task_history);


						echo json_encode(['status'=>200, 'message'=>'Task added Successfully']);   
					}else{
						echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
					}
	
		}

		public function viewSaleForm(){
			$data['uri'] = 'Sales';
			$menu = $this->Common_model->get_menu(array('parent_menu'=>$data['uri']));
			$menuID = $menu->id;
			$permission = permissions()->$menuID;
			if($permission[0]=='View'){

				$id = $this->input->post('id');
				$sale = $this->sale_model->get_sale(array('sales.id'=>$id));
				?>
<div class="row">

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Company Name </label>
      <input type="text" class="form-control" value="<?=$sale->company_name?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Full Name </label>
      <input type="text" class="form-control" value="<?=$sale->customerName?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Phone No. </label>
      <input type="text" class="form-control" minlength="10" maxlength="10"
        oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
        onkeyup="get_customer(this.value)" value="<?=$sale->customerPhone?>" readonly>
    </div>
  </div>


  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Email </label>
      <input type="email" class="form-control" value="<?=$sale->customerEmail?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Website </label>
      <input type="text" class="form-control" name="website" id="website" placeholder="Website"
        value="<?=$sale->website?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Sale Date <span class="text-danger">*</span></label>
      <input type="text" onfocus="(this.type='date')" class="form-control" name="sale_date" id="sale_date"
        value="<?=date('d-m-Y')?>" placeholder="Sale Date" value="<?=$sale->sale_date?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Sale Amount <span class="text-danger">*</span></label>
      <input type="text" class="form-control" name="sale_amount" id="sale_amount"
        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Sale Amount"
        onkeyup="amountCalculation()" value="<?=$sale->sale_amount?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="row">
      <div class="col-md-4 col-lg-4 col-xl-4">
        <div class="form-group">
          <label>GST Apply </label><br>
          <div class="btn btn-rounded btn-outline-success mr-5" title="Apply
																				GST">
            <input type="checkbox" name="gst" id="gst" class="" value="1" onclick="checkGst()"
              <?=$sale->gst==1 ? 'checked' : ''?> disabled> <span class=" ml-5 mr-5">Apply
              GST</span>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-lg-4 col-xl-4" id="gst_amount_div" style="display:none">
        <div class="form-group">
          <label>GST Amount </label>
          <input type="text" class="form-control" name="gst_amount" id="gst_amount"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
            placeholder="GST Amount" onkeyup="amountCalculation()" readonly value="<?=$sale->gst_amount?>">
        </div>
      </div>
      <div class="col-md-4 col-lg-4 col-xl-4">
        <div class="form-group">
          <label>Total Amount </label>
          <input type="text" class="form-control" name="total_amount" id="total_amount"
            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
            placeholder="Total Amount" value="<?=$sale->total_amount?>" readonly>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Advance Payment Amount <span class="text-danger">*</span></label>
      <input type="text" class="form-control" name="paid_amount" id="paid_amount"
        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
        placeholder="Advance Payment Amount" value="<?=$sale->advance_amount?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Pending Amount <span class="text-danger">*</span></label>
      <input type="text" class="form-control" name="pending_amount" id="pending_amount"
        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
        placeholder="Pending Amount" value="<?=$sale->pending_amount?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Payment Mode <span class="text-danger">*</span></label>
      <input type="text" class="form-control" name="payment_mode" id="payment_mode" placeholder="Payment Mode"
        value="<?=$sale->payment_mode?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Lead source </label>
      <input type="text" class="form-control" value="<?=$sale->lead_sourse?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Delivery Date </label>
      <input type="text" onfocus="(this.type='date')" class="form-control" name="delivery_date" id="delivery_date"
        placeholder="Delivery Date" value="<?=$sale->delivery_date?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Content Date </label>
      <input type="text" onfocus="(this.type='date')" class="form-control" name="content_date" id="content_date"
        placeholder="Content Date" value="<?=$sale->content_date?>" readonly>
    </div>
  </div>



  <div class="col-md-12 col-lg-12 col-xl-12">
    <div class="form-group">
      <label>Product/Requirement <span class="text-danger">*</span></label>
      <textarea class="form-control" name="product" id="product" placeholder="Product/Requirement"
        readonly><?=$sale->product?></textarea>
    </div>
  </div>

</div>
<script>
$(document).ready(function() {
  checkGst();
});
</script>
<?php
			}else{
				redirect(base_url('dashboard'));
			}
			
			 
		}

		// Payment

		public function payment_history()
	{
		 $this->not_admin_logged_in();
		 $data['uri'] = $this->uri->segment(1);
		 $menu        = $this->Common_model->get_menu(array('url'=>$data['uri']));
		 $menuID      = $menu->id;
		 $permission  = permissions()->$menuID;
		if($permission[0]=='View'){
			$data['permission'] = $permission[1];
			$data['page_title'] = 'Payment History';
			$data['url']    = $data['uri'];
			$data['admins'] = $this->user_model->get_users(array('users.user_type'=>2));
			$data['users']  = $this->user_model->get_users(array('users.adminID'=>$this->session->userdata('adminID'),'users.status'=>1));
	    $this->admin_template('payment/payment-history',$data);
		}else{
			redirect(base_url('dashboard'));
		}
    
	}


	public function ajaxPaymentHistory(){
		$this->not_admin_logged_in();
		  $data['uri'] = $this->uri->segment(3);
      $menu = $this->Common_model->get_menu(array('url'=>$data['uri']));
      $menuID = $menu->id;
      $permission = permissions()->$menuID;

			$payment_permission = 'payment-history';
      $paymentMenu = $this->Common_model->get_menu(array('url'=>$payment_permission));
      $paymentMenuID = $paymentMenu->id;
      $paymentPermission = permissions()->$paymentMenuID;

			$total_amount = 0;
      $grand_total_amount = 0;
			$role = role();
			$condition = $this->session->userdata('user_type')==1 ? array('payment_history.adminID'=>$this->session->userdata('adminID'),'payment_history.status'=>1) : array('payment_history.status'=>1,'payment_history.adminID'=>$this->session->userdata('adminID'));
			$grand_total = $this->sale_model->grand_total_payment_history($condition);

			$payments = $this->sale_model->make_datatables_payment($condition); // this will call modal function for fetching data
			$data = array();
			$paid_amount = 0;
	
			foreach($payments as $key=>$payment) // Loop over the data fetched and store them in array
			{
		
				$button = '';
				$sub_array = array();

				$total_amount +=$payment['amount'];
				$sub_array[] = $key+1;
				//$sub_array[] = $button;
				$sub_array[] = $payment['assigin_to_name'];
				$sub_array[] = $payment['customerName'];
				$sub_array[] = $payment['customerPhone'];
				$sub_array[] = '₹'.$payment['amount'];
				$sub_array[] =$payment['payment_mode'];
				$sub_array[] = date('d-M-Y', strtotime($payment['created_at']));
				$data[] = $sub_array;
			}

			$value[] = array('','','','<b>Total Received</b>','<b>'.'₹'.$total_amount.'</b>','','');

    $data = array_merge($data,$value);


			$grand_total_amount = !empty($grand_total['total_amount_paid']) ? $grand_total['total_amount_paid'] : 0;
	
      $total[] = array('','','','<b class="text-success">Grand Total</b>','<b class="text-success">'.'₹'.$grand_total_amount.'</b>','','');
      
      
      $data = array_merge($data,$total);
		
			$output = array(
				"draw"                    =>     intval($_POST["draw"]),
				"recordsTotal"            =>     $this->sale_model->get_all_data_payment($condition),
				"recordsFiltered"         =>     $this->sale_model->get_filtered_data_payment($condition),
				"data"                    =>     $data
			);
			
			echo json_encode($output);
	  }

		public function viewPaymentForm(){
			$data['uri'] = 'Payment History';
			$menu = $this->Common_model->get_menu(array('parent_menu'=>$data['uri']));
			$menuID = $menu->id;
			$permission = permissions()->$menuID;
			if($permission[0]=='View'){

				$id         = $this->input->post('id');
				$sale       = $this->sale_model->get_sale(array('sales.id'=>$id));
				$payments   = $this->sale_model->get_payment_histories(array('payment_history.saleID'=>$id));
				$total_paid = 0;
				$gstAmount  = !empty($sale->gst_amount) ? $sale->gst_amount : 0;
				?>
<div class="row">
  <div class="table-responsive">
    <table class=" table table-hover table-center mb-0">
      <thead>
        <tr>
          <th>S.no.</th>
          <th>Added By</th>
          <th>Amount</th>
          <th>Payment Mode</th>
          <th>Payment Date</th>
        </tr>
      </thead>
      <tbody>
        <?php 
								  foreach($payments as $key=>$payment){
											$total_paid += $payment->amount;
								?>
        <tr>
          <td><?=$key+1?></td>
          <td><?=$payment->user_name?></td>
          <td> ₹ <?=$payment->amount?></td>
          <td><?=$payment->payment_mode?></td>
          <td><?=date('d-m-Y h:i A',strtotime($payment->created_at))?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>
<hr>
<div class="row">
  <div class="table-responsive">
    <table class=" table table-hover table-center mb-0">
      <thead>
        <tr>
          <th>Sale Amount</th>
          <?= !empty($sale->gst_amount) ? '<th>GST Amount</th>' : ''?>
          <th>Total Amount</th>
          <th>Paid Amount</th>
          <th>Pending Amount</th>
          <?= !empty($sale->pending_amount) && $sale->pending_amount >0 && $this->session->userdata('user_type') != 1 ? '<th>Add Payment</th>' : ''?>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>₹ <?=$sale->sale_amount?></td>
          <?= !empty($sale->gst_amount) ? '<td>₹'.$sale->gst_amount : 0 .'</td>'?>
          <td>₹<?=$sale->sale_amount + $gstAmount ?></td>
          <td>₹<?=$total_paid?></td>
          <td>₹ <?=$sale->pending_amount?></td>
          <?=  !empty($sale->pending_amount) && $sale->pending_amount >0 && $this->session->userdata('user_type') != 1 ? '<td><button type="button" class="btn btn-block btn-outline-success" onclick="add_payment_history()">Add Payment <i class="fa fa-plus"> </i></button></td>' : ''?>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<hr>
<div class="row" style="display:none" id="add_payment_div">
  <div class="col-md-4 col-lg-4 col-xl-4">
    <div class="form-group">
      <label>Amount <span class="text-danger">*</span></label>
      <input type="text" class="form-control" name="amount" id="amount"
        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Amount">
    </div>
  </div>
  <div class="col-md-4 col-lg-4 col-xl-4">
    <div class="form-group">
      <label>Payment Mode <span class="text-danger">*</span></label>
      <input type="text" class="form-control" name="payment_mode" id="payment_mode" placeholder="Payment Mode">
    </div>
  </div>
  <input type="hidden" name="saleID" id="saleID" value="<?=$id?>">
  <div class="col-md-4 col-lg-4 col-xl-4 mt-2">
    <div class="form-group">
      <label> </label>
      <input type="submit" class="btn btn-block btn-outline-primary " name="submit" value="Add Payment">
    </div>
  </div>
</div>
<?php

			}else{
				redirect(base_url('dashboard'));
			}	
		 }

		 public function store_payment_history()
		 {
			$adminID = $this->session->userdata('adminID');
			$userID = $this->session->userdata('id');
			$saleID = $this->input->post('saleID');
			$amount = $this->input->post('amount');
			$payment_mode = $this->input->post('payment_mode');
      $sale = $this->sale_model->get_sale(array('sales.id'=>$saleID));
			$pending_amount = $sale->pending_amount;
			$advance_amount = $sale->advance_amount;
			if(empty($amount))
			{
				echo json_encode(['status'=>403, 'message'=>'Please enter amount']);  	
				exit();
			}



			if(empty($payment_mode))
			{
				echo json_encode(['status'=>403, 'message'=>'Please enter payment mode']);  	
				exit();
			}

			 $data = array(	
				'adminID'      => $adminID,
				'saleID'       => $saleID,
				'userID'       => $userID,
				'amount'       => $amount,
				'payment_mode' => $payment_mode,
				'remark'       => $payment_mode,
				);
				$store = $this->sale_model->store_payment_history($data);
				if($store)
				{
					$date_sale = array(
						'advance_amount' => $advance_amount+$amount,
            'pending_amount' => $pending_amount-$amount
					);
					$update = $this->sale_model->update_sale($date_sale,array('id'=>$saleID));

					$sale       = $this->sale_model->get_sale(array('sales.id'=>$saleID));
					$payments   = $this->sale_model->get_payment_histories(array('payment_history.saleID'=>$saleID));
					$assigin_user = $this->user_model->get_user(array('users.id'=>$userID));
					$users = send_mail_users($userID);
					$email_data['siteinfo'] = $this->siteinfo();
					$subject = 'Payment Report';
					$site_data = array('site_name'=>$email_data['siteinfo']->site_name);
					$email_data['name'] = $assigin_user->name;
					$email_data['sale'] = $sale;
				 	$email_data['payments'] = $payments ;
					$html = $this->load->view('email-template/payment-report',$email_data, true);
	        //print_r($html);die;
					$user_ids=array();
					foreach($users as $user)
					{
						$email = $user->email;
						sendEmail($email,$subject,$html,$site_data);
					}
			

				  echo json_encode(['status'=>200, 'message'=>'Payment addes Successfully']);   
				}else{
					echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
				}
		  }


		 public function setSession()
		 {
				$pending_balance = $this->input->post('pending_balance');
				$from_date       = $this->input->post('sale_from_date');
				$to_date         = $this->input->post('sale_to_date');
				$monthname       = $this->input->post('monthname');
				$assign_user     = $this->input->post('assign_user');
				
				$session = array(
						'pending_balance' => $pending_balance,
						'sale_from_date'  => $from_date,
						'sale_to_date'    => $to_date,
						'monthname'       => $monthname,
						'assign_user'     => $assign_user,
					);
						
				$this->session->set_userdata($session);		
				redirect(base_url('sales'));
					
	    }

	public function setSessionPayment()
	{
		$pending_balance = $this->input->post('pending_balance');
		$from_date       = $this->input->post('sale_from_date');
		$to_date         = $this->input->post('sale_to_date');
		$monthname       = $this->input->post('monthname');
		$assign_user     = $this->input->post('assign_user');
		
		$session = array(
				'pending_balance' => $pending_balance,
				'sale_from_date'  => $from_date,
				'sale_to_date'    => $to_date,
				'monthname'       => $monthname,
				'assign_user'     => $assign_user,
			);
				
				$this->session->set_userdata($session);
				
				redirect(base_url('payment-history'));
				
   }
	
		public function resetPending_balance()
		{
			$this->session->unset_userdata('pending_balance');
		}
	
		public function resetFromDate()
		{
			$this->session->unset_userdata('sale_from_date');
			$this->session->unset_userdata('sale_to_date');
	  }
	
		public function resetToDate()
		{
			$this->session->unset_userdata('sale_to_date');
	  }
	
		public function resetMonthName()
		{
			$this->session->unset_userdata('monthname');
	  }
	
		public function resetAssignUser()
		{
			$this->session->unset_userdata('assign_user');
	  }
	
		public function resetContactPerson()
		{
			$this->session->unset_userdata('contact_person');
	  }

	public function delete()
	{
		$id = $this->input->post('id');
		$sale = $this->sale_model->get_sale(array('sales.id' => $id));

		$delete_service      = $this->service_model->delete_service(array('saleID'=>$id));

		$delete_payment      = $this->sale_model->delete_payment(array('saleID'=>$id));

		$delete_sale_history = $this->sale_model->delete_sale_history(array('saleID'=>$id));

		$enquiry_data = array(
			'sale' => 0
		);
		$update_enquiry = $this->enquiry_model->update_enquiry($enquiry_data,array('id'=>$sale->enquiryID));
		$followup_data = array(
			'status' => 0
		);
		$update_followup = $this->followup_model->update_followup($followup_data,array('enquriyID'=>$sale->enquiryID));

		$delete = $this->sale_model->delete_sale(array('id'=>$id));
    if($delete){
		   echo json_encode(['status'=>200, 'message'=>'Sale deleted Successfully']);   
	  }else{
		   echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
	  }

	}

	
}