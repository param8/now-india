<?php 
date_default_timezone_set('Asia/Kolkata');
class Task extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('customer_model');
		$this->load->model('user_model');
		$this->load->model('setting_model');
		$this->load->model('enquiry_model');
		$this->load->model('sale_model');
		$this->load->model('followup_model');
		$this->load->model('service_model');
		$this->load->model('task_model');
	}

	public function index()
	{
		$this->not_admin_logged_in();
		$data['uri'] = $this->uri->segment(1);
		$permission = permission($data['uri']);
		if($permission[0]=='View'){
			$data['permission'] = $permission;
			$data['page_title'] = 'Task';
			$data['url'] = $data['uri'];
			$data['admins'] = $this->user_model->get_users(array('users.user_type'=>2));
			//$data['users'] = $this->user_model->get_users();
			$data['users'] = get_users_function(array('users.adminID'=>$this->session->userdata('adminID'),'users.status'=>1),'Sale');
	    $this->admin_template('task/task',$data);
		}else{
			redirect(base_url('dashboard'));
		}
    
	}


	public function ajaxTask(){
	  $this->not_admin_logged_in();
	  $data['uri'] = $this->uri->segment(3);
    $permission = permission($data['uri']);
  
		$condition = $this->session->userdata('user_type')==1 ? array('task_maneger.adminID'=>$this->session->userdata('adminID'),'task_maneger.status'=>1) : 	(($this->session->userdata('user_type')==2 OR $permission[7]=='Like Admin') ? array('task_maneger.adminID'=>$this->session->userdata('adminID')) : 	array('task_maneger.status'=>1,'task_maneger.adminID'=>$this->session->userdata('adminID')));	

			$tasks = $this->task_model->make_datatables($condition); // this will call modal function for fetching data
			
			$data = array();
			foreach($tasks as $key=>$task) // Loop over the data fetched and store them in array
			{
				$task_report = $this->task_model->get_task_report(array('task_report.taskID'=>$task['id']));
				$sub_array = array();
				$userID = explode(',',$task['assign_to']);
	
        $user_name = array();
				$users = $this->user_model->get_users_in_condition(array('users.adminID'=>$this->session->userdata('adminID')),$userID);
      
				foreach($users as $user){
					$user_name[] = $user->name;
				}

				$button = '';
				
				if($task['work_status']!='Completed'){
					if($permission[2]=='Edit' && $this->session->userdata('user_type')!=1){

						$button .= '<a href="'.base_url('edit-task/'.base64_encode($task['id']).'/'.base64_encode('task')).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit task Detail" class="btn btn-outline-primary btn-sm m-2"> <i class="fa fa-edit"></i></a>';
					}
	
				
				}
         if($task['work_status']=='Not Started' OR $task['status']==0 OR $task['status']==2){
				if($permission[3]=='Delete' && $this->session->userdata('user_type')!=1){
					$button .= '<a href="javascript:void(0)" onclick="delete_task('.$task['id'].')"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete task Detail" class="btn btn-outline-danger btn-sm m-2"> <i class="fa fa-trash"></i></a>';
				}
		  	}
				
				if($task['work_status']=='Completed'){
					$button .= '<a href="'.base_url('view-task/'.base64_encode($task['id'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Task" class="btn btn-secondary btn-sm m-2"> <i class="fa fa-eye"></i></a>';
				}
				
				if($permission[0]=='View' AND count($task_report) > 0){
					$button .= '<a href="'.base_url('task-report/'.base64_encode($task['id'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="View task report" class="btn btn-outline-warning btn-sm">Task Report <i class="fa fa-eye"></i></a>';
				}
	

				if(in_array($this->session->userdata('id'), $userID) AND $task['work_status']=='Work In Progress'){
					$button .= '<a href="javascript:void(0)" onclick="daily_task_report('.$task['id'].')"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Task Report" class="btn btn-outline-success btn-sm m-2"> Daliy Report <i class="fa fa-plus"></i></a>';
				}

				if($task['status']==1 && $this->session->userdata('user_type')==2){
					$button .= '<a href="javascript:void(0)"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Task Approved" class="btn btn-outline-success btn-sm m-2"> Approved <i class="fa fa-check"></i></a>';
				}
				if($task['status']==2 && $this->session->userdata('user_type')==2){
					$button .= '<a href="javascript:void(0)" onclick="approve_or_disapprove_task('.$task['id'].')"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Task Disapproved" class="btn btn-outline-danger btn-sm m-2"> Disapproved <i class="fa fa-ban"></i></a>';
				}

				if(empty($task['status']) && $this->session->userdata('user_type')==2){
					$button .= '<a href="javascript:void(0)" onclick="approve_or_disapprove_task('.$task['id'].')"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Task Approve/Disapproved" class="btn btn-outline-primary btn-sm m-2">  Task Status <i class="fas fa-spinner fa-spin"></i></a>';
				}

        $task_status = $task['work_status']=='Completed' ? '<a class="btn btn-outline-success btn-sm">Completed <i class="fa fa-check-circle"></i></a>' : ($task['work_status']=='Work In Progress' ? '<a class="btn btn-outline-warning btn-sm">In Progress <i class="fas fa-spinner fa-spin"></i></a>' : '<a class="btn btn-outline-danger btn-sm">Not Started <i class="fa fa-clock-o"></i></a>');
				$sub_array[] = $key+1;
				$sub_array[] = $button;

				$sub_array[] = implode(', ',$user_name);
				$sub_array[] = $task['assigin_by_name'];
				$sub_array[] = '<textarea class="form-control" disabled>'.$task['product'].'</textarea>';
				$sub_array[] = $task['priority'];
				$sub_array[] = $task_status;
				$sub_array[] =  $task['assign_date'] != '0000-00-00' ? date('d-M-Y', strtotime($task['assign_date'])) : '';
				
				$data[] = $sub_array;
		
	}

		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->task_model->get_all_data($condition),
			"recordsFiltered"         =>     $this->task_model->get_filtered_data($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	  }

		
		public function create(){
			$this->not_admin_logged_in();
			$data['uri'] = 'task';
			$permission = permission($data['uri']);
			if($permission[1]=='Add'){
				$data['page_title'] = 'Task';
				$saleID = base64_decode($this->uri->segment(2));
				$data['sale'] = $this->sale_model->get_sale(array('sales.id' => $saleID));
				$product =	get_services_detail($data['sale']->id);
				$data['roles'] = $this->setting_model->get_roles(array('role.adminID'=>$this->session->userdata('adminID'),'role.status'=>1));	
				$data['product_details'] = !empty($product) ? implode(', ',$product) : $data['sale']->product;
				$this->admin_template('task/create-task',$data);
			}else{
				redirect(base_url('dashboard'));
			}
			
		}


		public function store(){

			$adminID = $this->session->userdata('adminID');
			$assign_by = $this->session->userdata('id');
			$saleID = $this->input->post('saleID');
			$product = $this->input->post('product');
			$role = $this->input->post('role');
			$client_detail = $this->input->post('client_detail');
			$priority = $this->input->post('priority');
			$work_status = $this->input->post('work_status');
			$requirement = $this->input->post('requirement');
			$work_status = 'Not Started';
			$status = $this->session->userdata('user_type') == 2 ? 1 : 0;

			if(empty($this->input->post('assign_to'))){
					echo json_encode(['status'=>403, 'message'=>'Please select assign to user ']);  	
					exit();
			}
				
			if(empty($priority)){
				echo json_encode(['status'=>403, 'message'=>'Please select priority ']);   	
				exit();
			}

			if(empty($requirement)){
				echo json_encode(['status'=>403, 'message'=>'Please enter work requirement ']);  	
				exit();
			}
			   $assign_to = implode(',', $this->input->post('assign_to'));

					$data = array(	
						'adminID'         => $adminID,
						'role'            => $role,
						'assign_by'       => $assign_by,
						'assign_to'       => $assign_to,
						'saleID'          => $saleID,
						'product'         => $product,
						'priority'        => $priority,
						'work_status'     => $work_status,
						'assign_date'     => date('Y-m-d'),
						'requirement'     => $requirement,
						'status'          => $status,
					);

					$store_task = $this->task_model->store_task($data);
					if($store_task){

						$data_task_history = array(	
							'adminID'         => $adminID,
							'taskID'          => $store_task,
							'role'            => $role,
							'assign_by'       => $assign_by,
							'assign_to'       => $assign_to,
							'saleID'          => $saleID,
							'product'         => $product,
							'client_detail'   => $client_detail,
							'priority'        => $priority,
							'work_status'     => $work_status,
							'assign_date'     => date('Y-m-d'),
							'requirement'     => $requirement,
							'status'          => $status,
						);
						$store_task_history = $this->task_model->store_task_history($data_task_history);

						$sale_data = array(
							'task_status' => 2
						);

						$this->sale_model->update_sale($sale_data,array('id'=>$saleID));

						echo json_encode(['status'=>200, 'message'=>'Task added Successfully . Send mail please wait....','assign_to'=>$this->input->post('assign_to'),'assign_by'=>$assign_by,'saleID'=>$saleID]);   
					}else{
						echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
					}
	
		}


		public function edit(){

			$this->not_admin_logged_in();
			$data['uri'] = 'task';
			$permission = permission($data['uri']);
			if($permission[2]=='Edit'){
				$data['page_title'] = 'Task';
				$id = base64_decode($this->uri->segment(2));
				$type = base64_decode($this->uri->segment(3));
				if($type=='sale'){
					$data['task'] = $this->task_model->get_task(array('task_maneger.saleID'=>$id));
					$data['sale'] = $this->sale_model->get_sale(array('sales.id' => $data['task']->saleID));
					//print_r($data['task']);die;
				}
				
				if($type=='task'){
					$data['task'] = $this->task_model->get_task(array('task_maneger.id'=>$id));
					$data['sale'] = $this->sale_model->get_sale(array('sales.id' => $data['task']->saleID));
				}
				$data['type'] = $type;
				$data['roles'] = $this->setting_model->get_roles(array('role.adminID'=>$this->session->userdata('adminID'),'role.status'=>1));
				$this->admin_template('task/edit-task',$data);
			}else{
				redirect(base_url('dashboard'));
			}

		}


		public function update(){
			$adminID = $this->session->userdata('adminID');
			$taskID = $this->input->post('taskID');
			$saleID = $this->input->post('saleID');
			$product = $this->input->post('product');
			$role = $this->input->post('role');
			$client_detail = $this->input->post('client_detail');
			$assign_to = $this->input->post('assign_to');
			$priority = $this->input->post('priority');
			$work_status = $this->input->post('work_status');
			$start_date = !empty($this->input->post('start_date')) ? date('Y-m-d',strtotime($this->input->post('start_date'))) : '';
			$end_date =   !empty($this->input->post('end_date')) ? date('Y-m-d',strtotime($this->input->post('end_date'))) : '';
			//$work_percentage = json_encode(array('work'=>$this->input->post('work'),'remaning_work'=>$this->input->post('remaning_work'),'complete_work'=>$this->input->post('complete_work')));
			
			$task = $this->task_model->get_task(array('task_maneger.id'=>$taskID));
			$assign_by = $task->assign_by;
			$tools = !empty($this->input->post('tools')) ? implode(',',$this->input->post('tools')) : (!empty($task->tools) ? $task->tools : '');
			$requirement = $this->input->post('requirement');
			$task_status = !empty($this->input->post('task_status')) ? $this->input->post('task_status') : $task->task_status;
			$reason =!empty($this->input->post('reason'))? $this->input->post('reason'): $task->reason ;
			$status = !empty($this->input->post('task_status')) ? $this->input->post('task_status') : $task->status;

       if($task->work_status==1){
			if(empty($this->input->post('assign_to'))){
					echo json_encode(['status'=>403, 'message'=>'Please select assign to user ']);  	
					exit();
			}
				
			if(empty($priority)){
				echo json_encode(['status'=>403, 'message'=>'Please select priority ']);   	
				exit();
			}

			if(empty($work_status)){
				echo json_encode(['status'=>403, 'message'=>'Please select work status ']);  	
				exit();
			}

			if(empty($start_date)){
				echo json_encode(['status'=>403, 'message'=>'Please select start date ']);  	
				exit();
			}

			if(empty($end_date)){
				echo json_encode(['status'=>403, 'message'=>'Please select end date ']);   	
				exit();
			}

			if(empty($tools)){
				echo json_encode(['status'=>403, 'message'=>'Please select tools ']);   	
				exit();
			}

			if(empty($requirement)){
				echo json_encode(['status'=>403, 'message'=>'Please enter work requirement ']);  	
				exit();
			}
		}
      if($this->session->userdata('user_type')==2){
			if(empty($task_status)){
				echo json_encode(['status'=>403, 'message'=>'Please select at least one task status']);  	
				exit();
			}

			if($task_status==2){

				if(empty($reason)){
					echo json_encode(['status'=>403, 'message'=>'Please enter disapproved reason']);  	
					exit();
				}

			}
		}

			$assign_to = implode(',', $this->input->post('assign_to'));
					$data = array(	
						'assign_to'       => $assign_to,
						'role'            => $role,
						'work_status'     => $work_status,
						//'work_percentage' => $work_percentage,
						//'work_progress'   => $work_progress,
						'priority'        => $priority,
						'start_date'      => $start_date,
						'end_date'        => $end_date,
						'tools'           => $tools,
						'requirement'     => $requirement,
						'task_status'     => $task_status,
						'reason'          => $reason,
						'status'          => $status
					);

					$update = $this->task_model->update_task($data,array('id'=>$taskID));
					if($update){
						$data_task_history = array(	
							'adminID'         => $adminID,
							'taskID'          => $taskID,
							'assign_by'       => $assign_by,
							'assign_to'       => $assign_to,
							'saleID'          => $saleID,
							'product'         => $product,
							'client_detail'   => $client_detail,
							'role'            => $role,
							'work_status'     => $work_status,
							//'work_percentage' => $work_percentage,
							//'work_progress'   => $work_progress,
							'priority'        => $priority,
							'start_date'      => $start_date,
							'end_date'        => $end_date,
							'tools'           => $tools,
							'requirement'     => $requirement,
							'task_status'     => $task_status,
						  'reason'          => $reason,
							'status'          => $status
						);
						
						$store_task_history = $this->task_model->store_task_history($data_task_history);

						if($work_status=='Completed'){
							$sale_data = array(
								'task_status' => 1
							);
	
							$this->sale_model->update_sale($sale_data,array('id'=>$saleID));
						}elseif($task_status==2 AND $this->session->userdata('user_type')==2){
							$sale_data = array(
								'task_status' => 3
							);
	
							$this->sale_model->update_sale($sale_data,array('id'=>$saleID));
						}else{
							$sale_data = array(
								'task_status' => 2
							);
	
							$this->sale_model->update_sale($sale_data,array('id'=>$saleID));
						}


						echo json_encode(['status'=>200, 'message'=>'Task updated Successfully. Send mail please wait....','taskID'=>$taskID,'assign_to'=>$this->input->post('assign_to'),'task_status'=>$this->input->post('task_status')]);   
					}else{
						echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
					}
	
		}

		public function view(){
			$this->not_admin_logged_in();
			$data['uri'] = 'task';
			$permission = permission($data['uri']);
			if($permission[0]=='View'){
				$data['page_title'] = 'Task';
				$id = base64_decode($this->uri->segment(2));
				$data['task'] = $this->task_model->get_task(array('task_maneger.id'=>$id));
				$data['sale'] = $this->sale_model->get_sale(array('sales.id' => $data['task']->saleID));
				$data['roles'] = $this->setting_model->get_roles(array('role.adminID'=>$this->session->userdata('adminID'),'role.status'=>1));
				$this->admin_template('task/view-task',$data);
			}else{
				redirect(base_url('dashboard'));
			}
		
		}

		public function daily_task_report(){

				$id = $this->input->post('id');
				$task = $this->task_model->get_task(array('task_maneger.id'=>$id));
				$sale = $this->sale_model->get_sale(array('sales.id' => $task->saleID));
				?>
<div class="row">

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Product Name </label>
      <input type="text" class="form-control" value="<?=$task->product?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Client Detail</label>
      <input type="text" class="form-control"
        value="<?=$sale->customerName.'('.$sale->customerPhone.'/'.$sale->company_name.') '?>" readonly>
    </div>
  </div>

  <div class="col-md-12 col-lg-12 col-xl-12">
    <div class="form-group">
      <label>Work estimates and progress </label>
      <div class="row">
        <div class="col-md-3 col-lg-3 col-xl-3">
          <label>Work in %</label>
          <input type="text" class="form-control" name="work" id="work"
            oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" value="100" readonly>
        </div>
        <div class="col-md-3 col-lg-3 col-xl-3">
          <label>Remaining work in %</label>
          <?php 
							$work_percentage = json_decode($task->work_percentage);
							$remaning_work = $work_percentage->remaning_work;
							$complete_work = $work_percentage->complete_work;
						?>
          <input type="text" class="form-control" name="remaning_work" id="remaning_work"
            oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
            onkeyup="get_work_percentage(this.value)">
        </div>
        <div class="col-md-3 col-lg-3 col-xl-3">
          <label>Complete work in %</label>
          <input type="text" class="form-control" name="complete_work" id="complete_work"
            oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" value="" readonly>
        </div>
        <div class="col-md-3 col-lg-3 col-xl-3">
          <div class="form-group">
            <label>Work Time in Minutes <span class="text-danger">*</span></label><br>
            <input type="text" class="form-control" name="work_time" id="work_time" placeholder="Work Time in Minutes"
              oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
          </div>
        </div>
      </div>
    </div>
  </div>



  <div class="col-md-12 col-lg-12 col-xl-12">
    <div class="form-group">
      <label>Work Progress Description <span class="text-danger">*</span></label>
      <textarea class="form-control" name="task_description" id="task_description"
        placeholder="Work Progress Description"></textarea>
    </div>
  </div>
  <input type="hidden" name="taskID" id="taskID" value="<?=$id?>">
</div>
<script>
$(document).ready(function() {
  get_work_percentage();
});
</script>
<?php
		
			 
}


  public function store_task_report(){
		$adminID = $this->session->userdata('adminID');
		$userID = $this->session->userdata('id');
		$taskID = $this->input->post('taskID');
		$work_report_percentage = json_encode(array('work'=>$this->input->post('work'),'remaning_work'=>$this->input->post('remaning_work'),'complete_work'=>$this->input->post('complete_work')));
		$work_time = $this->input->post('work_time');
		$task_description = $this->input->post('task_description');
			
		if(empty($this->input->post('remaning_work'))){
			echo json_encode(['status'=>403, 'message'=>'Please enter remaning work ']);   	
				exit();
		}

		if(empty($task_description)){
			echo json_encode(['status'=>403, 'message'=>'Please enter task work progress']);   	
				exit();
		}



		$data = array(
			'adminID'                => $adminID,
			'taskID'                 => $taskID,
			'userID'                 => $userID,
			'work_report_percentage' => $work_report_percentage,
			'work_time'              => $work_time,
			'task_description'       => $task_description
		);

		$store = $this->task_model->store_task_report($data);

		if($store){
			echo json_encode(['status'=>200, 'message'=>'Report submit Successfully. Send mail please wait .....','id'=>$userID]);   
		}else{
			echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
		}

	}


	public function task_report(){
		$this->not_admin_logged_in();
		$data['uri'] = 'task';
		$permission = permission($data['uri']);
		if($permission[0]=='View'){
			$data['page_title'] = 'Task Report';
			$id = base64_decode($this->uri->segment(2));
			$data['task'] = $this->task_model->get_task(array('task_maneger.id'=>$id));
			$data['reports'] = $this->task_model->get_task_reports(array('task_maneger.id'=>$id));
			$report_users = explode(',',$data['reports']['assign_to']);
			
			// echo "<pre>";
			 $users =array();
			foreach($report_users as $user){
				
				$users[] = $this->user_model->get_user(array('users.id'=>$user,'users.adminID'=>$this->session->userdata('adminID')),'Developer');
			}
			$data['users'] =$users;
			$data['sale'] = $this->sale_model->get_sale(array('sales.id' => $data['task']->saleID));
			$data['roles'] = $this->setting_model->get_roles(array('role.adminID'=>$this->session->userdata('adminID'),'role.status'=>1));
			$this->admin_template('task/task-report',$data);
		}else{
			redirect(base_url('dashboard'));
		}
	}

	public function setSession()
	{
		 $task_status = $this->input->post('task_status');
		 $taskMonthName       = $this->input->post('taskMonthName');
		 $task_assign_user         = $this->input->post('task_assign_user');
		 $task_from_date       = $this->input->post('task_from_date');
		 $task_to_date     = $this->input->post('task_to_date');
		 
		 $session = array(
				 'task_status'      => $task_status,
				 'taskMonthName'    => $taskMonthName,
				 'task_assign_user' => $task_assign_user,
				 'task_from_date'   => $task_from_date,
				 'task_to_date'     => $task_to_date,
			 );
				 
		 $this->session->set_userdata($session);		
		 redirect(base_url('task'));
			 
	 }

	 public function approve_or_disapprove_task_form(){
		$id = $this->input->post('id');
		$task = $this->task_model->get_task(array('task_maneger.id' => $id));
		$sale = $this->sale_model->get_sale(array('sales.id' => $task->saleID));
		?>
<div class="row">

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Product Name </label>
      <input type="text" class="form-control" value="<?=$task->product?>" readonly>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Client Detail</label>
      <input type="text" class="form-control"
        value="<?=$sale->customerName.'('.$sale->customerPhone.'/'.$sale->company_name.') '?>" readonly>
    </div>
  </div>
  <div class="col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <label>Task Approve/Disapprove <span class="text-danger">*</span></label><br>
      <?php $task_status = array(1 =>'Approve', 2 =>'Disapprove'); 
          foreach($task_status as $key=>$statu){
      ?>
      <span class="p-2"><input type="radio" name="task_status" onclick="check_task_status(this.value)"
          <?=($key==$task->status) ? 'checked' : ''?> <?=($task->status==1) ? 'disabled' : ''?> value="<?=$key?>"> <span
          class="m-1"><b><?=$statu?></b></span></span>
      <?php 
                      }
                    ?>
    </div>
  </div>

  <div class="col-md-6 col-lg-6 col-xl-6" id="reason_div" <?=($task->status!=2) ? 'style="display:none" ' : ''?>>
    <div class="form-group">
      <label>Disapprove Reason </label>
      <textarea class="form-control" name="reason" id="reason"><?=$task->reason?></textarea>
    </div>
  </div>
  <input type="hidden" name="taskID" id="taskID" value="<?=$id?>">
</div>
<?php
	 }

	 public function approve_disapprove_task(){
      $adminID = $this->session->userdata('adminID');
		  $taskID = $this->input->post('taskID');
			$task = $this->task_model->get_task(array('task_maneger.id'=>$taskID));
			$assign_by = $task->assign_by;
			$tools = !empty($this->input->post('tools')) ? implode(',',$this->input->post('tools')) : (!empty($task->tools) ? $task->tools : '');
			$requirement = $this->input->post('requirement');
			$task_status = !empty($this->input->post('task_status')) ? $this->input->post('task_status') : $task->task_status;
			$reason =!empty($this->input->post('reason'))? $this->input->post('reason'): $task->reason ;
			$status = !empty($this->input->post('task_status')) ? $this->input->post('task_status') : $task->status;

			if(empty($task_status)){
				echo json_encode(['status'=>403, 'message'=>'Please select at least one task status']);  	
				exit();
			}


				if(empty($reason)){
					echo json_encode(['status'=>403, 'message'=>'Please enter disapproved reason']);  	
					exit();
				}

					$data = array(	
						'task_status'     => $task_status,
						'reason'          => $reason,
						'status'          => $status
					);

					$update = $this->task_model->update_task($data,array('id'=>$taskID));
					if($update){
						$data_task_history = array(	
							'adminID'         => $adminID,
							'taskID'          => $taskID,
							'assign_by'       => $assign_by,
							'assign_to'       => $task->assign_to,
							'saleID'          => $task->saleID,
							'product'         => $task->product,
							'role'            => $task->role,
							'work_status'     => $task->work_status,
							'priority'        => $task->priority,
						  'reason'          => $reason,
							'task_status'     => $task_status,
							'status'          => $status
						);
						
						$store_task_history = $this->task_model->store_task_history($data_task_history);

				    if($task_status==2 AND $this->session->userdata('user_type')==2){
							$sale_data = array(
								'task_status' => 3
							);
	
							$this->sale_model->update_sale($sale_data,array('id'=>$saleID));
						}else{
							$sale_data = array(
								'task_status' => 2
							);
	
							$this->sale_model->update_sale($sale_data,array('id'=>$saleID));
						}
     
						$task_type = $this->input->post('task_status')==1 ? 'Approved' : 'Disapproved';

						echo json_encode(['status'=>200, 'message'=>'Task '.$task_type.' Successfully. Send mail please wait....','taskID'=>$taskID,'assign_to'=>$task->assign_to,'task_status'=>$this->input->post('task_status')]);   
					}else{
						echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
					}
	

	 }


	public function resetTaskStatus()
		{
			$this->session->unset_userdata('task_status');
		}
	
		public function resetTaskFromDate()
		{
			$this->session->unset_userdata('task_from_date');
			$this->session->unset_userdata('task_to_date');
	  }
	
		public function resetTaskToDate()
		{
			$this->session->unset_userdata('task_to_date');
	  }
	
		public function resetMonthName()
		{
			$this->session->unset_userdata('taskMonthName');
	  }
	
		public function resetTaskAssignUser()
		{
			$this->session->unset_userdata('task_assign_user');
	  }

	
}