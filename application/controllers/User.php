<?php 
class User extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();


		$this->load->model('user_model');
	}

	public function index()
	{
		$this->not_admin_logged_in();
		$data['uri'] = 'Users';
		$menu = $this->Common_model->get_menu(array('parent_menu'=>$data['uri']));
		$menuID = $menu->id;
		$permission = permissions()->$menuID;
		if($permission[0]=='View'){
			$data['permission'] = $permission[1];
			$data['page_title'] = 'Users';
		    $validate_user_create = $this->user_model->get_users(array('users.adminID' => $this->session->userdata('id'),'users.user_type<>'=>2));
            $user = $this->user_model->get_user(array('users.adminID' => $this->session->userdata('id')));
		    $data['message'] = $user->user_permission == 0 ? '<span class="text-success">Unlimited User access</span>'  : ( $user->user_permission <= count($validate_user_create) ? '<span class="text-danger">Your user permission limit up please upgrade</span>': '') ;
	        $this->admin_template('users/users',$data);
		}else{
			redirect(base_url('dashboard'));
		}
    
	}

	public function ajaxUser(){
		$this->not_admin_logged_in();
		  $data['uri'] = $this->uri->segment(3);
      $menu = $this->Common_model->get_menu(array('parent_menu'=>$data['uri']));
      $menuID = $menu->id;
      $permission = permissions()->$menuID;
      $role = role();
		$condition = array('users.status'=>1,'users.user_type<>'=>'2','users.adminID'=>$this->session->userdata('adminID'));
		$users = $this->user_model->make_datatables($condition); // this will call modal function for fetching data
		$data = array();
		//print_r($admins);die;
		foreach($users as $key=>$user) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
			if($permission[0]=='View'){
        $button .= '<a href="'.base_url('view-user/'.base64_encode($user['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View User Detail" class="btn  btn-sm  text-warning"><i class="fa fa-eye"></i></a>';
      }
			if($permission[1]=='Add'){
        $button .= '<a href="javascript:void(0)" onclick="add_target('.$user['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Add Traget" class="btn  btn-outline-success btn-sm  ">Add Traget</a>';
      }
			if($permission[2]=='Edit'){
				$button .= '<a href="'.base_url('edit-user/'.base64_encode($user['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit User Detail" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
      }
			if($permission[3]=='Delete'){
				$button .= '<a href="javascript:void(0)" onclick="delete_user('.$user['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete User" class="btn  btn-sm  text-danger"><i class="fa fa-trash"></i> </a>';
      }
      		
			$profile_pic = $user['profile_pic'];
			$sub_array[] = $key+1;
			$sub_array[] = $button;
			$sub_array[] = '<img src="'.base_url($profile_pic).'" height="50" width="50">';
			$sub_array[] = $user['name'];
			$sub_array[] = $user['phone'];
			$sub_array[] = $user['email'];
			$sub_array[] = $user['address'];
			$sub_array[] = $user['cityName'];
			$sub_array[] = $user['stateName'];
			$sub_array[] = !empty($user['pincode'])?$user['pincode']:'-';
			$sub_array[] = date('d-m-Y', strtotime($user['created_at']));
		
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->user_model->get_all_data($condition),
			"recordsFiltered"         =>     $this->user_model->get_filtered_data($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	  }

		public function create(){
			$this->not_admin_logged_in();
			$data['uri'] = 'Users';
			$menu = $this->Common_model->get_menu(array('parent_menu'=>$data['uri']));
			$menuID = $menu->id;
			$permission = permissions()->$menuID;
			if($permission[1]=='Add'){
				$data['page_title'] = 'Create User';
				$this->admin_template('users/create-user',$data);
			}else{
				redirect(base_url('dashboard'));
			}
			
		}

		public function store(){
			$name = $this->input->post('name');
			$email = $this->input->post('email');
			$phone = $this->input->post('phone');
			$alternative_no = $this->input->post('alternative_no');
			$user_type = $this->input->post('user_type');
			$profile_pic = 'public/dummy_user.png';	
			$state = $this->input->post('state');
			$city = $this->input->post('city');
			$address = $this->input->post('address');
			$pincode = $this->input->post('pincode');
			$gender = $this->input->post('gender');
			$password = $this->input->post('password');
			// permission start
			$validate_email = $this->user_model->get_user(array('users.email' => $email));
		  $validate_phone = $this->user_model->get_user(array('users.phone' => $phone));
			$validate_user_create = $this->user_model->get_users(array('users.adminID' => $this->session->userdata('id'),'users.user_type<>'=>2));
      $user = $this->user_model->get_user(array('users.adminID' => $this->session->userdata('id'),'users.user_type<>'=>2));
			// $menus = $this->menus();
			// $permission = array();
			// 	foreach($menus as $menu){
			
			// 		$view = !empty($this->input->post('view')[$menu['id']]) ? $this->input->post('view')[$menu['id']] : '';
			// 		$add = !empty($this->input->post('add')[$menu['id']]) ? $this->input->post('add')[$menu['id']] : '';
			// 		$edit = !empty($this->input->post('edit')[$menu['id']]) ? $this->input->post('edit')[$menu['id']] : '';
			// 		$delete = !empty($this->input->post('delete')[$menu['id']]) ? $this->input->post('delete')[$menu['id']] : '';
			// 		$upload = !empty($this->input->post('upload')[$menu['id']]) ? $this->input->post('upload')[$menu['id']] : '';
			// 		$export = !empty($this->input->post('export')[$menu['id']]) ? $this->input->post('export')[$menu['id']] : '';
			// 		$download = !empty($this->input->post('download')[$menu['id']]) ? $this->input->post('download')[$menu['id']] : '';
			// 		$like_admin = !empty($this->input->post('like_admin')[$menu['id']]) ? $this->input->post('like_admin')[$menu['id']] : '';
			// 		$permission[$menu['id']] = array($view,$add,$edit,$delete,$upload,$export,$download,$like_admin);
					
			// 	}
				
			// $permission_json = json_encode($permission);
			if($user->user_permission != 0){
    			if($user->user_permission < count($validate_user_create) ){
    				echo json_encode(['status'=>403, 'message'=>'User Create limit up please upgrade permissions']); 	
    				exit();
    			}
			}
				
		 if(empty($name)){
				echo json_encode(['status'=>403, 'message'=>'Please enter name']); 	
				exit();
			}
			if(empty($email)){
				echo json_encode(['status'=>403, 'message'=>'Please enter your email ']); 	
				exit();
			}
			
			if($validate_email){
				echo json_encode(['status'=>403, 'message'=>'This email address is already in use.']); 	
				exit();
			}
	
			if(empty($phone)){
				echo json_encode(['status'=>403, 'message'=>'Please enter your phone ']); 	
				exit();
			}
		 
			if($validate_phone){
				echo json_encode(['status'=>403, 'message'=>'This phone number is already in use.']); 	
				exit();
			}
	
			if(empty($user_type)){
				echo json_encode(['status'=>403, 'message'=>'Please select role ']); 	
				exit();
			}

			if(empty($address)){
				echo json_encode(['status'=>403, 'message'=>'Please enter address ']); 	
				exit();
			}
				
				if(empty($state)){
					echo json_encode(['status'=>403, 'message'=>'Please select state ']); 	
					exit();
				}
				if(empty($city)){
					echo json_encode(['status'=>403, 'message'=>'Please select city ']); 	
					exit();
				}
				
		
				if(empty($pincode)){
					echo json_encode(['status'=>403, 'message'=>'Please enter pincode']);  	
					exit();
				}

				if(empty($password)){
					echo json_encode(['status'=>403, 'message'=>'Please enter password']);  	
					exit();
				}

			$unique_id =  uniqid(date('dmYHis'));
			$data = array(	
				'unique_id'   => $unique_id,
				'adminID'     => $this->session->userdata('adminID'),
				'name'        => $name,
				'email'       => $email,
				'phone'       => $phone,
				'address'     => $address,
				'city'        => $city,
				'state'       => $state,
				'pincode'     => $pincode,
				'profile_pic' => $profile_pic,
				'user_type'   => $user_type,
				'password'    => md5($password),
				//'permission'  => $permission_json,
				'status'      => 1,
			);
	
			$user_id = $this->user_model->store_user($data);
			if($user_id){
				$user_detail = array(
					'userID'          => $user_id,
					'alternative_no'  => $alternative_no,
					'gender'          => $gender,
					'user_permission' => 0,
					'lead_permission' => 0,
				);
			 $store_user_detail = $this->user_model->store_user_detail($user_detail);
			 
			 echo json_encode(['status'=>200, 'message'=>'Admin Added Successfully']);   
			}else{
				echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
			}
	
		}

	public function delete(){

		$id = $this->input->post("id");
		$update = $this->user_model->update_user(array('status'=>0),array('adminID'=>$id));
		if($update){
			echo json_encode(['status'=>200, 'message'=>'Admin Deleted Successfully']);   
		}else{
			echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
		}
		
	}


		public function edit(){
			$this->not_admin_logged_in();
			$data['uri'] = 'Users';
			$menu = $this->Common_model->get_menu(array('parent_menu'=>$data['uri']));
			$menuID = $menu->id;
			 $permission = permissions()->$menuID;
			if($permission[2]=='Edit'){
				$data['page_title'] = 'Edit User';
			  $id = base64_decode($this->uri->segment(2));
				$data['user'] = $this->user_model->get_user(array('users.id'=>$id));
				$data['menus'] = $this->Common_model->get_menues(array('status'=>1));
				$this->admin_template('users/edit-user',$data);
			}else{
				redirect(base_url('dashboard'));
			}
			 
		}


		public function update(){
			$id = $this->input->post('id');
			$name = $this->input->post('name');
			$email = $this->input->post('email');
			$phone = $this->input->post('phone');
			$alternative_no = $this->input->post('alternative_no');
			$user_type = $this->input->post('user_type');
			$profile_pic = 'public/dummy_user.png';	
			$state = $this->input->post('state');
			$city = $this->input->post('city');
			$address = $this->input->post('address');
			$pincode = $this->input->post('pincode');
			$gender = $this->input->post('gender');
			$password = $this->input->post('password');
			// permission start
			$validate_email = $this->user_model->get_user(array('users.email' => $email,'users.id<>'=>$id));
		  $validate_phone = $this->user_model->get_user(array('users.phone' => $phone ,'users.id<>'=>$id));
			// $menus = $this->menus();
			// $permission = array();
			// 	foreach($menus as $menu){
		
			// 		$view = !empty($this->input->post('view')[$menu['id']]) ? $this->input->post('view')[$menu['id']] : '';
			// 		$add = !empty($this->input->post('add')[$menu['id']]) ? $this->input->post('add')[$menu['id']] : '';
			// 		$edit = !empty($this->input->post('edit')[$menu['id']]) ? $this->input->post('edit')[$menu['id']] : '';
			// 		$delete = !empty($this->input->post('delete')[$menu['id']]) ? $this->input->post('delete')[$menu['id']] : '';
			// 		$upload = !empty($this->input->post('upload')[$menu['id']]) ? $this->input->post('upload')[$menu['id']] : '';
			// 		$export = !empty($this->input->post('export')[$menu['id']]) ? $this->input->post('export')[$menu['id']] : '';
			// 		$download = !empty($this->input->post('download')[$menu['id']]) ? $this->input->post('download')[$menu['id']] : '';
			// 		$like_admin = !empty($this->input->post('like_admin')[$menu['id']]) ? $this->input->post('like_admin')[$menu['id']] : '';
			// 		$permission[$menu['id']] = array($view,$add,$edit,$delete,$upload,$export,$download,$like_admin);
					
			// 	}
				
			// $permission_json = json_encode($permission);
				
		 if(empty($name)){
				echo json_encode(['status'=>403, 'message'=>'Please enter name']); 	
				exit();
			}
			if(empty($email)){
				echo json_encode(['status'=>403, 'message'=>'Please enter your email ']); 	
				exit();
			}
			
			if($validate_email){
				echo json_encode(['status'=>403, 'message'=>'This email address is already in use.']); 	
				exit();
			}
	
			if(empty($phone)){
				echo json_encode(['status'=>403, 'message'=>'Please enter your phone ']); 	
				exit();
			}
		 
			if($validate_phone){
				echo json_encode(['status'=>403, 'message'=>'This phone number is already in use.']); 	
				exit();
			}
	
			if(empty($user_type)){
				echo json_encode(['status'=>403, 'message'=>'Please select Role ']); 	
				exit();
			}

			if(empty($address)){
				echo json_encode(['status'=>403, 'message'=>'Please enter address ']); 	
				exit();
			}
				
				if(empty($state)){
					echo json_encode(['status'=>403, 'message'=>'Please select state ']); 	
					exit();
				}
				if(empty($city)){
					echo json_encode(['status'=>403, 'message'=>'Please select city ']); 	
					exit();
				}
				
		
				if(empty($pincode)){
					echo json_encode(['status'=>403, 'message'=>'Please enter pincode']);  	
					exit();
				}

				
			$unique_id =  uniqid(date('dmYHis'));
			$data = array(	
				'name'        => $name,
				'email'       => $email,
				'phone'       => $phone,
				'address'     => $address,
				'city'        => $city,
				'state'       => $state,
				'pincode'     => $pincode,
				'user_type'  => $user_type,
			);
	
			$update = $this->user_model->update_user($data,array('id'=>$id));
			if($update){
				$user_detail = array(
					'alternative_no'  => $alternative_no,
					'gender'          => $gender,
				);
			 $update_user_detail = $this->user_model->update_user_detail($user_detail,$id);
			 
			 
			 echo json_encode(['status'=>200, 'message'=>'User updated Successfully']);   
			}else{
				echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
			}
	
		}


		public function view(){

			$this->not_admin_logged_in();
			$data['uri'] = 'Users';
			$menu = $this->Common_model->get_menu(array('parent_menu'=>$data['uri']));
			$menuID = $menu->id;
			 $permission = permissions()->$menuID;
			if($permission[0]=='View'){
				$data['page_title'] = 'View User';
			  $id = base64_decode($this->uri->segment(2));
				$data['user'] = $this->user_model->get_user(array('users.id'=>$id));
				$data['menus'] = $this->Common_model->get_menues(array('status'=>1));
				
				$this->admin_template('users/view-user',$data);
			}else{
				redirect(base_url('dashboard'));
			}
			
	 }

	//  public function profile(){
	// 	$this->not_logged_in();
	// 	$data['page_title'] = 'Profile';
	// 	$data['panel'] = 'Panel';
	// 	$id = $this->session->userdata('id');
	// 	$data['user'] =$this->user_model->get_user(array('users.id'=>$id));
	// 	$data['states'] = $this->Common_model->get_states(array('country_id'=>101));
	// 	$this->template('web/panel/profile',$data);
	//  }

		public function forget_password()
		{
			$data['page_title'] = 'Forget Password';	
		  $this->template('web/forget-password',$data);
		}

		

		public function change_password(){
			$this->not_admin_logged_in();
			$data['page_title'] = 'Change Password';
			$id = $this->session->userdata('id');
			$this->admin_template('components/profile-breadcrumb',$data);
		  $data['user'] =$this->user_model->get_user(array('users.id'=>$id));
	    $this->admin_template('users/change-password',$data);
		  
		}

		public function update_password(){
			$userID = $this->session->userdata('id');
			$old_password = $this->input->post('old_password');
			$new_password = $this->input->post('new_password');
			$confirm_password = $this->input->post('confirm_password');
      
			$user = $this->user_model->get_user(array('users.id' =>$userID));

			if(empty($old_password)){
				echo json_encode(['status'=>403, 'message'=>'Please enter old password']);  	
				exit();
			}

			if($user->password != md5($old_password)){
				echo json_encode(['status'=>403, 'message'=>'Old password is incorrect']);  	
				exit();
			}

			if(empty($new_password)){
				echo json_encode(['status'=>403, 'message'=>'Please enter new password']);  	
				exit();
			}

			if(strlen($new_password)<6){
				echo json_encode(['status'=>403, 'message'=>'Password minimum length must be at least 6 characters']);  	
				exit();
			}

			if(empty($confirm_password)){
				echo json_encode(['status'=>403, 'message'=>'Please enter confirm password']);  	
				exit();
			}


			if($new_password != $confirm_password){
				echo json_encode(['status'=>403, 'message'=>'New Password and Confirm Password must be the same']);  	
				exit();
			}

			$data = array(
				'password' => md5($new_password)
			);
   
			$update = $this->user_model->update_user($data,array('id'=>$userID));
			if($update){
		  	echo json_encode(['status'=>200, 'message'=>'Password updated successfully']); 
			}else{
				echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
			}
		}

		public function password_change(){
			$verify_otp = $this->session->userdata('verify_otp');
			$detail = $this->session->userdata('detail');
			$otp = $this->input->post('otp');
			$new_password = $this->input->post('new_password');
			$confirm_password = $this->input->post('confirm_password');
      
			$users = $this->user_model->get_user_detail($detail);

			$userID = $users->row()->id;

			if(empty($otp)){
				echo json_encode(['status'=>403, 'message'=>'Please enter  OTP']);  	
				exit();
			}

			if($verify_otp != $otp){
				echo json_encode(['status'=>403, 'message'=>'Please enter valid OTP']);  	
				exit();
			}

			if(empty($new_password)){
				echo json_encode(['status'=>403, 'message'=>'Please enter new password']);  	
				exit();
			}

			if(strlen($new_password)<6){
				echo json_encode(['status'=>403, 'message'=>'Password minimum length must be at least 6 characters']);  	
				exit();
			}

			if(empty($confirm_password)){
				echo json_encode(['status'=>403, 'message'=>'Please enter confirm password']);  	
				exit();
			}

			if($new_password != $confirm_password){
				echo json_encode(['status'=>403, 'message'=>'New Password and Confirm Password must be the same']);  	
				exit();
			}

			$data = array(
				'password' => md5($new_password)
			);
   
			$update = $this->user_model->update_user($data,$userID);
			if($update){
		  	echo json_encode(['status'=>200, 'message'=>'Password updated successfully please login again !..']); 
			}else{
				echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
			}
		}


		public function profile(){
			$this->not_admin_logged_in();

			$data['page_title'] = 'Profile';
			$id = $this->session->userdata('id');
			$this->admin_template('components/profile-breadcrumb',$data);
		  $data['user'] =$this->user_model->get_user(array('users.id'=>$id));
		  $data['states'] = $this->Common_model->get_states(array('country_id'=>101));
	    $this->admin_template('users/profile',$data);
	
		}


		public function update_profile(){
			$id = $this->input->post('id');
			$name = $this->input->post('name');
			$email = $this->input->post('email');
			$phone = $this->input->post('phone');
			$alternative_no = $this->input->post('alternative_no');
			$state = $this->input->post('state');
			$city = $this->input->post('city');
			$address = $this->input->post('address');
			$pincode = $this->input->post('pincode');
			$gender = $this->input->post('gender');
			$user = $this->user_model->get_user(array('users.id'=>$id));
		  $validate_phone = $this->user_model->get_user(array('users.phone' => $phone ,'users.id<>'=>$id));


			$this->load->library('upload');
			if(!empty($_FILES['image']['name'])){
			 $config = array(
				'upload_path' 	=> 'uploads/user',
				'file_name' 	=> str_replace(' ','',$name).uniqid(),
				'allowed_types' => 'jpg|jpeg|png|gif',
				'max_size' 		=> '10000000',
			 );
			 $this->upload->initialize($config);
			if ( ! $this->upload->do_upload('image'))
				{
						$error = $this->upload->display_errors();
						echo json_encode(['status'=>403, 'message'=>$error]); 	
						exit();
				}
				else
				{
					$type = explode('.', $_FILES['image']['name']);
					$type = $type[count($type) - 1];
					$profile_pic = 'uploads/user/'.$config['file_name'].'.'.$type;
				}
			}elseif(!empty($user->profile_pic)){
				$profile_pic = $user->profile_pic;;	
			}else{
				$profile_pic = 'public/dummy_user.png';	
			}


				
		 if(empty($name)){
				echo json_encode(['status'=>403, 'message'=>'Please enter name']); 	
				exit();
			}

	
			if(empty($phone)){
				echo json_encode(['status'=>403, 'message'=>'Please enter your phone ']); 	
				exit();
			}
		 
			if($validate_phone){
				echo json_encode(['status'=>403, 'message'=>'This phone number is already in use.']); 	
				exit();
			}
	

			if(empty($address)){
				echo json_encode(['status'=>403, 'message'=>'Please enter address ']); 	
				exit();
			}
				
				// if(empty($state)){
				// 	echo json_encode(['status'=>403, 'message'=>'Please select state ']); 	
				// 	exit();
				// }
				// if(empty($city)){
				// 	echo json_encode(['status'=>403, 'message'=>'Please select city ']); 	
				// 	exit();
				// }
				
		
				// if(empty($pincode)){
				// 	echo json_encode(['status'=>403, 'message'=>'Please enter pincode']);  	
				// 	exit();
				// }

				
			$unique_id =  uniqid(date('dmYHis'));
			$data = array(	
				'name'        => $name,
				'phone'       => $phone,
				'address'     => $address,
				'city'        => $city,
				'state'       => $state,
				'pincode'     => $pincode,
				'profile_pic' => $profile_pic,
			);
	
			$update = $this->user_model->update_user($data,array('id'=>$id));
			if($update){
				$user_detail = array(
					'alternative_no'  => $alternative_no,
					'gender'          => $gender,
				);
			 $update_user_detail = $this->user_model->update_user_detail($user_detail,$id);
			 
			 
			 echo json_encode(['status'=>200, 'message'=>'Your profile update Successfully']);   
			}else{
				echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
			}
	
		}

		public function add_target_form(){
			$id = $this->input->post('id');
			$user = $this->user_model->get_user(array('users.id'=>$id));
			?>
			<div class="col-lg-12 col-sm-12 col-xl-12">
				<div class="form-group ">
					<label class="col-form-label">Monthly Target</label>
					<div class="col-lg-9">
						<input type="text" class="form-control" id="monthly_target" name="monthly_target" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" value="<?=$user->monthly_target?>">
					</div>
				</div>
			</div>
			<div class="col-lg-12 col-sm-12 col-xl-12">
				<div class="form-group ">
					<label class="col-form-label">Monthly Target Date</label>
					<div class="col-lg-9">
						<input type="date" class="form-control" id="monthly_target_date" name="monthly_target_date" value="<?=$user->monthly_target_date?>">
					</div>
				</div>
			</div>
			<input type="hidden" name="id" id="id" value="<?=$id?>">
<?php
		}

		public function store_target(){
			$id = $this->input->post('id');
			$adminID = $this->session->userdata('adminID');
			$monthly_target = $this->input->post('monthly_target');
			$monthly_target_date = $this->input->post('monthly_target_date');

			if(empty($monthly_target)){
				echo json_encode(['status'=>403, 'message'=>'Please enter monthly target ']); 	
				exit();
			}

			if(empty($monthly_target_date)){
				echo json_encode(['status'=>403, 'message'=>'Please enter monthly target date ']); 	
				exit();
			}

			$data = array(
				'monthly_target'      => $monthly_target,
				'monthly_target_date' => $monthly_target_date
			);

			$update = $this->user_model->update_user_detail($data,$id);

			if($update){
				$data_target = array(
					'adminID'     => $adminID,
					'userID'      => $id,
					'target'      => $monthly_target,
					'target_date' => $monthly_target_date,
				);
	
				$add = $this->user_model->store_monthly_target($data_target);
		  	echo json_encode(['status'=>200, 'message'=>'Monthly target added successfully..']); 
			}else{
				echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
			}

		}

	
}