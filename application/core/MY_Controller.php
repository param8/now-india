<?php 
class MY_Controller extends CI_Controller 
{
	var $permission = array();

	public function __construct() 
	{
	   parent::__construct();

		$this->load->helper('permission_helper');
    $this->load->model('Common_model');
		$this->load->model('user_model');
		$this->load->helper('mail');
		$this->load->helper('sms_helper');
		$this->load->helper('common_helper');
		$this->load->model('enquiry_model');
		$this->load->model('followup_model');
		$this->load->model('setting_model');
		if(empty($this->session->userdata('logged_in'))) 
		{
			$session_data = array('logged_in' => FALSE);
			$this->session->set_userdata($session_data);
		}
	}

	public function logged_in()
	{
		$session_data = $this->session->userdata();
		
		if($session_data['logged_in'] == TRUE) {
			if($session_data['user_type'] == 'Admin' || $session_data['user_type']=='Staff' || $session_data['user_type']=='Faculties')
			{
				redirect('dashboard', 'refresh');	
			}else{
				redirect('home', 'refresh');
			}	
		}
	}

	public function not_logged_in()
	{
		$session_data = $this->session->userdata();
		if($session_data['logged_in'] == FALSE) {
			redirect('home', 'refresh');
		}
	}

	public function user_verification()
	{
		$session_data = $this->session->userdata();
		if($session_data['verification_user'] == 0) {
			redirect('verification', 'refresh');
		}
	}

	public function not_admin_logged_in()
	{
		$session_data = $this->session->userdata();
		//print_r($session_data);die;
		//print_r($session_data);die;
		if($session_data['logged_in'] == FALSE ) {
			redirect('Authantication', 'refresh');
		}
	}


	public function siteinfo()
	{
		$siteinfo = $this->Common_model->get_site_info(array('adminID'=>$this->session->userdata('adminID')));
	  return $siteinfo;
	}

	 public function menus()
	 {
		$menus = $this->Common_model->get_menues(array('status'=>1));
	  return $menus;
	 }

	 public function states()
	 {
		$states = $this->Common_model->get_states(array('country_id'=>101));
	  return $states;
	 }


	 public function login_template($page = null, $data = array())
	 {
		$data['siteinfo'] = $this->siteinfo();
		$this->load->view('components/head',$data);
		$this->load->view(''.$page);
		$this->load->view('components/footer');
	 }


	 public function admin_template($page = null, $data = array())
	 {
		$data['siteinfo'] = $this->siteinfo();
		$data['menues'] = $this->menus();
		$data['permissions'] = permissions();
		$data['roles'] = roles();
		$data['role'] = role();
		// $data['user_role_permission'] = user_role_permission();
		$data['states'] = $this->states();
		$data['total_re_enquiry'] = total_re_enquiry();
		//print_r($data['role']);die;
		$data['total_new_enquiry'] = total_new_enquiry();
		$data['total_todays_followup'] = total_todays_followup();
		$data['total_hot_lead'] = total_hot_lead();
		$data['total_warm_lead'] = total_warm_lead();

		$this->load->view('components/head',$data);
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view($page);
		$this->load->view('components/footer');
	 }

	 


}