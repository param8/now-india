<?php
    
    function verify_reCaptcha($secret,$recaptcha)
    {
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$recaptcha;
        $responce = file_get_contents($url);
        $responceData =  json_decode($responce, true);
        return $responceData;
    }

    function leads_color(){
      return  $leads_color = array(1=>'btn-outline-info',2=>'btn-outline-danger',3=>'btn-outline-info',4=>'btn-outline-warning',5=>'btn-outline-success',6=>'btn-outline-secondary',7=>'btn-outline-muted');
    }

    function get_services_detail($saleID){

      $ci=& get_instance();
      $ci->load->database();
      
      $adminID = $ci->session->userdata('adminID');

      $ci->db->select('services.*,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus,enquiry.lead_type,enquiry.lead_sourse,enquiry.requirement');
    $ci->db->from('services');
    $ci->db->join('sales','sales.id = services.saleID','left');
    $ci->db->join('enquiry','enquiry.id = services.enquiryID','left');
    $ci->db->join('customers','customers.id = enquiry.clientID','left');
    $ci->db->join('lead_status','lead_status.id = enquiry.lead_status','left');
    $ci->db->join('users','users.id = services.userID','left');
    $ci->db->join('users as assigned','assigned.id = enquiry.assigned_by','left');
    $ci->db->where(array('services.adminID'=>$adminID,'services.saleID'=>$saleID));
    $service = $ci->db->get()->row();
    $service_detail = json_decode($service->service_detail);
				$product = array();
				foreach($service_detail as $key=>$value){
          $product[] = $key=='Domain' ? $key.' : '.$value->domain_name :($key=='Email' ? $key.' : '.$value->email_domain_name :($key=='Hosting' ? $key.' : '.$value->hosting_domain_name :($key=='SSL' ? $key.' : '.$value->ssl_domain_name :($key=='Web Designing' ? $key.' : '.$value->web_desigin_url :($key=='Software Development' ? $key.' : '.$value->Software_development_detail :($key=='Whatsapp' ? $key.' : '.$key :($key=='SMS' ? $key.' : '.$key :($key=='SEO' ? $key.' : '.$value->seo_url :($key=='SMO' ? $key.' : '.$value->smo_url :($key=='SMM' ? $key.' : '.$value->smm_url :($key=='PPC' ? $key.' : '.$value->ppc_url :($key=='Mass Email' ? $key.' : '.$value->mass_email_domain :($key=='Maintenence' ? $key.' : '.$value->websiteMaintenenceDetail :($key=='Other' ? $key.' : '.$value->other_detail :''))))))))))))));
        }

        return $product ;

    }

  
?>