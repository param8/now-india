<?php
 function roles(){
  $ci=& get_instance();
  $ci->load->database();

  $ci->db->where('status',1);
  $ci->db->where('id<>',1);
  $ci->db->where('adminID',$ci->session->userdata('adminID'));
  return $ci->db->get('role')->result();

 }


 function role(){
  $ci=& get_instance();
  $ci->load->database();

  $ci->db->where('id',$ci->session->userdata('user_type'));
  return $ci->db->get('role')->row();

 }

 function role_other_permission(){
  $ci=& get_instance();
  $ci->load->database();

  $ci->db->where('id',$ci->session->userdata('user_type'));
  $role =  $ci->db->get('role')->row();
  $task_permission = json_decode($role->other_permission);
  return  $task_permission->task_developer;

 }

 function get_users_function($condition,$type){
  $ci=& get_instance();
  $ci->load->database();
  $users = $ci->user_model->get_users($condition);
  $user_data = array();
  foreach($users as $user){
    $user_task_permission = json_decode($user->roleOtherPermission);
    if($type=='Sale'){
      $task_permission =  $user_task_permission->task_sale;
      if($task_permission==1){
        $user_data[] = $user;
      }
    }

    if($type=='Developer'){
      $task_permission =  $user_task_permission->task_developer;
      if($task_permission==1){
        $user_data[] = $user;
      }
    }
  }
   return $user_data;
 }


 function permission($url){
  $ci=& get_instance();
  $ci->load->database();

  $session_data = $ci->session->userdata();
  $permissions = array();
  if($session_data['logged_in'] == TRUE){
    $ci->db->where('id',$ci->session->userdata('user_type'));
    $user = $ci->db->get('role')->row();
    $permissions = json_decode($user->permission);
  }
  
  $menu = $ci->Common_model->get_menu(array('url'=>$url));
    $menuID = $menu->id;
    return $permissions->$menuID;
   //return $permission = permissions()->$menuID;
 }

 function permissions()
 {
  $ci=& get_instance();
  $ci->load->database();
  
  $session_data = $ci->session->userdata();
  $permissions = array();
  if($session_data['logged_in'] == TRUE){
    $ci->db->where('id',$ci->session->userdata('user_type'));
    $user = $ci->db->get('role')->row();
    $permissions = json_decode($user->permission);
  }
 // print_r($permissions);die;
  return $permissions; 
 }

 function users_in_condition($condition,$users){
  $ci=& get_instance();
  $ci->load->database();

  $ci->db->select('users.*,cities.name as cityName,states.name as stateName,user_detail.alternative_no as user_alternative_no,user_detail.dob,user_detail.gender,user_detail.monthly_target,user_detail.monthly_target_date,user_detail.user_permission,user_detail.lead_permission,role.name as roleName');
  $ci->db->from('users');
  $ci->db->join('cities','cities.id = users.city','left');
  $ci->db->join('states','states.id = users.state','left'); 
  $ci->db->join('user_detail','user_detail.userID = users.id','left');
  $ci->db->join('role','role.id = users.user_type','left');
  $ci->db->where($condition);
  $ci->db->where_in('users.id',$users);
  return $this->db->get()->result();

 }

  function user_role_permission($user_type)
	 {
    $ci=& get_instance();
    $ci->load->database();
    $adminID = $ci->session->userdata('adminID');
    // $ci->db->where('parent_role',$user_type);
    // $roles = $ci->db->get('role')->result();
    if(!empty($adminID)){

    $roles =  $ci->db->query("WITH RECURSIVE EmpCTE AS (
      Select id,name,parent_role,adminID
      From role 
      Where parent_role = $user_type  AND adminID = $adminID
      UNION ALL
      Select role.id ,role.name,role.parent_role,role.adminID
      From role
      JOIN EmpCTE
      ON role.id = EmpCTE.parent_role  WHERE role.adminID = $adminID
      )
      Select * From EmpCTE ")->result();
   
		$role_id = array();
		foreach($roles as $role){
      $role_id[] = $role->id;
		}
  
		if(!empty($role_id)){
			$ci->db->where_in('user_type',$role_id);
		}
		
		$ci->db->or_where('user_type',$ci->session->userdata('user_type'));
		$users = $ci->db->get('users')->result();
		$data =  $ci->db->last_query();
    $user_role_permission = array();
		foreach($users as $user)
		{
			$user_role_permission[] = $user->id;
		}		
		 return $user_role_permission;
      }else{
          return 0;
      }
	 }




    function total_re_enquiry()
	 {
    $ci=& get_instance();
    $ci->load->database();

		$session_data = $ci->session->userdata();
    $ci->db->where('url','enquires');
    $ci->db->order_by('ordering','asc');
    $menu = $ci->db->get('menues')->result_array();
	 
		$menuID = $menu->id; 
		$permission = permissions()->$menuID;
		$role = role();
		$user = user_role_permission($ci->session->userdata('user_type'));

    //print_r($user);die;
		
		$condition = $ci->session->userdata('user_type')==1 ? array('enquiry.adminID'=>$ci->session->userdata('enquiryAdminID'),'enquiry.re_enquiry_status'=>1) : 
		array('enquiry.status'=>1,'enquiry.adminID'=>$ci->session->userdata('adminID'),'enquiry.re_enquiry_status'=>1);

    $ci->db->where($condition);
    if($ci->session->userdata('user_type')!=2){
      $ci->db->where_in('userID',$user);
    }
    return $ci->db->get('enquiry')->result();

    //echo $ci->db->last_query();die;
 

	  // return	$total_re_enquiry = $this->enquiry_model->get_enquiries($condition);
	 }

    function total_new_enquiry()
	 {
		$ci=& get_instance();
    $ci->load->database();

		$ci=& get_instance();
    $ci->load->database();

		$session_data = $ci->session->userdata();
    $ci->db->where('url','enquires');
    $ci->db->order_by('ordering','asc');
    $menu = $ci->db->get('menues')->result_array();
	 
		$menuID = $menu->id; 
		$permission = permissions()->$menuID;
		$role = role();
		$user = user_role_permission($ci->session->userdata('user_type'));
		$condition = $ci->session->userdata('user_type')==1 ? array('enquiry.adminID'=>$ci->session->userdata('enquiryAdminID'),'enquiry.lead_status'=>1) : 
		array('enquiry.status'=>1,'enquiry.adminID'=>$ci->session->userdata('adminID'),'enquiry.lead_status'=>1);
    $ci->db->where($condition);
    if($ci->session->userdata('user_type')!=2){
      $ci->db->where_in('userID',$user);
    }
    return $ci->db->get('enquiry')->result();
	 }

    function total_todays_followup(){
      $ci=& get_instance();
      $ci->load->database();
  
      $session_data = $ci->session->userdata();
      $ci->db->where('url','enquires');
      $ci->db->order_by('ordering','asc');
      $menu = $ci->db->get('menues')->result_array();
     
      $menuID = $menu->id; 
      $permission = permissions()->$menuID;
      $role = role();
      $user = user_role_permission($ci->session->userdata('user_type'));
		$condition = $ci->session->userdata('user_type')==1 ? array('followup.adminID'=>$ci->session->userdata('enquiryAdminID'),'DATE_FORMAT(followup.followup_date,"%Y-%m-%d")'=>date('Y-m-d')) : 
		array('followup.status'=>0,'followup.adminID'=>$ci->session->userdata('adminID'),'DATE_FORMAT(followup.followup_date,"%Y-%m-%d")'=>date('Y-m-d'));
		$ci->db->where($condition);
    if($ci->session->userdata('user_type')!=2){
      $ci->db->where_in('userID',$user);
    }
    return $ci->db->get('followup')->result();
		
	 }


    function total_hot_lead(){
      $ci=& get_instance();
      $ci->load->database();
  
      $ci=& get_instance();
      $ci->load->database();
  
      $session_data = $ci->session->userdata();
      $ci->db->where('url','enquires');
      $ci->db->order_by('ordering','asc');
      $menu = $ci->db->get('menues')->result_array();
     
      $menuID = $menu->id; 
      $permission = permissions()->$menuID;
      $role = role();
      $user = user_role_permission($ci->session->userdata('user_type'));
		$condition = $ci->session->userdata('user_type')==1 ? array('followup.adminID'=>$ci->session->userdata('enquiryAdminID'),'followup.lead_status'=>2) : array('followup.status'=>0,'followup.adminID'=>$ci->session->userdata('adminID'),'followup.lead_status'=>2);
       $ci->db->where($condition);
    if($ci->session->userdata('user_type')!=2){
      $ci->db->where_in('userID',$user);
    }
    return $ci->db->get('followup')->result();
		
	 }

	  function total_warm_lead(){
      $ci=& get_instance();
      $ci->load->database();
  
      $ci=& get_instance();
      $ci->load->database();
  
      $session_data = $ci->session->userdata();
      $ci->db->where('url','enquires');
      $ci->db->order_by('ordering','asc');
      $menu = $ci->db->get('menues')->result_array();
     
      $menuID = $menu->id; 
      $permission = permissions()->$menuID;
      $role = role();
      $user = user_role_permission($ci->session->userdata('user_type'));
		$condition = $ci->session->userdata('user_type')==1 ? array('followup.adminID'=>$ci->session->userdata('enquiryAdminID'),'followup.lead_status'=>4) : array('followup.status'=>0,'followup.adminID'=>$ci->session->userdata('adminID'),'followup.lead_status'=>4);
		$ci->db->where($condition);
    if($ci->session->userdata('user_type')!=2){
      $ci->db->where_in('userID',$user);
    }
    return $ci->db->get('followup')->result();
	 }

   function email_setting($condition){
    $ci=& get_instance();
    $ci->load->database();

    $ci->db->where($condition);
    $ci->db->where('adminID',$ci->session->userdata('adminID'));
    return $ci->db->get('email_setting')->row();
  
   }

   function send_mail_users($userID){
    $ci=& get_instance();
    $ci->load->database();
    $adminID = $ci->session->userdata('adminID');

    $ci->db->where(array('adminID'=>$adminID ,'user_type'=>2));
    $admin =  $ci->db->get('users')->row();
    $ci->db->where(array('adminID'=>$ci->session->userdata('adminID'),'id'=>$userID));
    $user =  $ci->db->get('users')->row();
    $user_type = $user->user_type;

  $roles =  $ci->db->query("WITH RECURSIVE EmpCTE AS (
      Select id,name,parent_role,adminID
      From role 
      Where id = $user_type AND adminID = $adminID
      UNION ALL
      Select role.id ,role.name,role.parent_role,role.adminID
      From role
      JOIN EmpCTE
      ON role.id = EmpCTE.parent_role WHERE role.adminID = $adminID
      )
      Select * From EmpCTE")->result();
    $roleID = array();
    
    foreach($roles as $role){
      if($role->id != $user_type){
        $roleID[] =$role->id;
      }
      
    }
    $roleID = array_merge($roleID,[2]); 
    $ci->db->where('adminID',$ci->session->userdata('adminID'));
    $ci->db->where_in('user_type',$roleID);
    $users_data =  $ci->db->get('users')->result();
    $users_id =array();
    foreach($users_data as $user_data){
      $users_id[] = $user_data->id;
    }

    $users_id =array_merge($users_id,[$user->id]);

    $ci->db->where('adminID',$ci->session->userdata('adminID'));
    $ci->db->where_in('id',$users_id);
   return $users_data =  $ci->db->get('users')->result();

   }


  //  

?>