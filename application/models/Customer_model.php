<?php 

class Customer_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}

  
	function make_query($condition)
  {
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('customers.*,cities.name as cityName,states.name as stateName');
    $this->db->from('customers');
    $this->db->join('cities','cities.id = customers.city','left');
    $this->db->join('states','states.id = customers.state','left'); 
    $this->db->where($condition);
    // if($this->session->userdata('user_type')!=2){
    //   if(!empty($usersID)){
    //     $this->db->where_in('customers.userID',$usersID );
    //   }
    // }

    if(!empty($this->session->userdata('customer_from_date')) AND !empty($this->session->userdata('customer_to_date')) ){
      $this->db->group_start();
        $this->db->where('DATE_FORMAT(customers.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('customer_from_date'))));
        $this->db->where('DATE_FORMAT(customers.created_at,"%Y-%m-%d") <=', date('Y-m-d',strtotime($this->session->userdata('customer_to_date'))));
        $this->db->group_end();
    }elseif(!empty($this->session->userdata('customer_from_date'))){
        $this->db->where('DATE_FORMAT(customers.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('customer_from_date'))));
        
    }
    if(!empty($this->session->userdata('monthname_customer'))){ 
        $this->db->where('DATE_FORMAT(customers.created_at,"%m")', $this->session->userdata('monthname_customer'));
        $this->db->where('DATE_FORMAT(customers.created_at,"%Y")', date('Y'));
    }

  if(!empty($this->session->userdata('assign_user_customer'))){
    $this->db->where('customers.userID', $this->session->userdata('assign_user_customer'));
   }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('customers.name', $_POST["search"]["value"]);
    $this->db->or_like('customers.email', $_POST["search"]["value"]);
    $this->db->or_like('customers.phone', $_POST["search"]["value"]);
    $this->db->or_like('cities.name', $_POST["search"]["value"]);
    $this->db->or_like('states.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('customers.id','desc');
    
  }
    function make_datatables($condition){
	  $this->make_query($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data($condition){
	  $this->make_query($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data($condition)
  {
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('customers.*,cities.name as cityName,states.name as stateName');
    $this->db->from('customers');
    $this->db->join('cities','cities.id = customers.city','left');
    $this->db->join('states','states.id = customers.state','left'); 
    $this->db->where($condition);
    // if($this->session->userdata('user_type')!=2){
    //   if(!empty($usersID)){
    //     $this->db->where_in('customers.userID',$usersID );
    //   }
    // }

    if(!empty($this->session->userdata('customer_from_date')) AND !empty($this->session->userdata('customer_to_date')) ){
      $this->db->group_start();
        $this->db->where('DATE_FORMAT(customers.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('customer_from_date'))));
        $this->db->where('DATE_FORMAT(customers.created_at,"%Y-%m-%d") <=', date('Y-m-d',strtotime($this->session->userdata('customer_to_date'))));
        $this->db->group_end();
    }elseif(!empty($this->session->userdata('customer_from_date'))){
        $this->db->where('DATE_FORMAT(customers.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('customer_from_date'))));
        
    }
    if(!empty($this->session->userdata('monthname_customer'))){ 
        $this->db->where('DATE_FORMAT(customers.created_at,"%m")', $this->session->userdata('monthname_customer'));
        $this->db->where('DATE_FORMAT(customers.created_at,"%Y")', date('Y'));
    }

  if(!empty($this->session->userdata('assign_user_customer'))){
    $this->db->where('customers.userID', $this->session->userdata('assign_user_customer'));
   }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('customers.name', $_POST["search"]["value"]);
    $this->db->or_like('customers.email', $_POST["search"]["value"]);
    $this->db->or_like('customers.phone', $_POST["search"]["value"]);
    $this->db->or_like('cities.name', $_POST["search"]["value"]);
    $this->db->or_like('states.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('customers.id','desc');
	   return $this->db->count_all_results();
  }


  public function get_customer($condition){
    $this->db->select('customers.*,cities.name as cityName,states.name as stateName');
    $this->db->from('customers');
    $this->db->join('cities','cities.id = customers.city','left');
    $this->db->join('states','states.id = customers.state','left'); 
    $this->db->where($condition);
	  return $this->db->get()->row();
  }

  public function get_customers($condition,$type=""){
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('customers.*,cities.name as cityName,states.name as stateName');
    $this->db->from('customers');
    $this->db->join('cities','cities.id = customers.city','left');
    $this->db->join('states','states.id = customers.state','left'); 
    if($type=='Like'){
      $this->db->like($condition);
      //$this->db->where('customers.status',1);
    }else{
      $this->db->where($condition);
    }
    // if($this->session->userdata('user_type')!=2){
    //   if(!empty($usersID)){
    //     $this->db->where_in('customers.userID',$usersID );
    //   }
    // }
	return $this->db->get()->result();
   //echo $this->db->last_query();
  }

  public function get_customers_like($condition){

  }

  public function store_customer($data){
	 $this->db->insert('customers',$data);
   return $this->db->insert_id();
  }


  public function update_customer($data,$condition){
	$this->db->where($condition);
	return $this->db->update('customers',$data);
  }
  
  public function delete_customer($condition){
	$this->db->where($condition);
	return $this->db->delete('customers');
  }



  
}