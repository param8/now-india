<?php 

class Enquiry_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}

  
	function make_query($condition)
  {
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('enquiry.*,customers.id as customerID,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus');
    $this->db->from('enquiry');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = enquiry.lead_status','left');
    $this->db->join('users','users.id = enquiry.userID','left');
    $this->db->join('users as assigned','assigned.id = enquiry.assigned_by','left');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('enquiry.userID',$usersID );
      }
    }

    if(!empty($this->session->userdata('enquiry_from_date')) AND !empty($this->session->userdata('enquiry_to_date')) ){
      $this->db->group_start();
        $this->db->where('DATE_FORMAT(enquiry.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('enquiry_from_date'))));
        $this->db->where('DATE_FORMAT(enquiry.created_at,"%Y-%m-%d") <=', date('Y-m-d',strtotime($this->session->userdata('enquiry_to_date'))));
        $this->db->group_end();
    }elseif(!empty($this->session->userdata('enquiry_from_date'))){
        $this->db->where('DATE_FORMAT(enquiry.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('enquiry_from_date'))));
        
    }
    if(!empty($this->session->userdata('monthname_enquiry'))){ 
        $this->db->where('DATE_FORMAT(enquiry.created_at,"%m")', $this->session->userdata('monthname_enquiry'));
        $this->db->where('DATE_FORMAT(enquiry.created_at,"%Y")', date('Y'));
    }

  if(!empty($this->session->userdata('assign_user_enquiry'))){
    $this->db->where('enquiry.userID', $this->session->userdata('assign_user_enquiry'));
   }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('customers.name', $_POST["search"]["value"]);
    $this->db->or_like('customers.email', $_POST["search"]["value"]);
    $this->db->or_like('customers.phone', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_type', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_sourse', $_POST["search"]["value"]);
    $this->db->or_like('lead_status.name', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('assigned.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('enquiry.id','desc');
    
  }
    function make_datatables($condition){
	  $this->make_query($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data($condition){
	  $this->make_query($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data($condition)
  {
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('enquiry.*,customers.id as customerID,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus');
    $this->db->from('enquiry');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = enquiry.lead_status','left');
    $this->db->join('users','users.id = enquiry.userID','left');
    $this->db->join('users as assigned','assigned.id = enquiry.assigned_by','left');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('enquiry.userID',$usersID );
      }
    }

    if(!empty($this->session->userdata('enquiry_from_date')) AND !empty($this->session->userdata('enquiry_to_date')) ){
      $this->db->group_start();
        $this->db->where('DATE_FORMAT(enquiry.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('enquiry_from_date'))));
        $this->db->where('DATE_FORMAT(enquiry.created_at,"%Y-%m-%d") <=', date('Y-m-d',strtotime($this->session->userdata('enquiry_to_date'))));
        $this->db->group_end();
    }elseif(!empty($this->session->userdata('enquiry_from_date'))){
        $this->db->where('DATE_FORMAT(enquiry.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('enquiry_from_date'))));
        
    }
    if(!empty($this->session->userdata('monthname_enquiry'))){ 
        $this->db->where('DATE_FORMAT(enquiry.created_at,"%m")', $this->session->userdata('monthname_enquiry'));
        $this->db->where('DATE_FORMAT(enquiry.created_at,"%Y")', date('Y'));
    }

  if(!empty($this->session->userdata('assign_user_enquiry'))){
    $this->db->where('enquiry.userID', $this->session->userdata('assign_user_enquiry'));
   }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('customers.name', $_POST["search"]["value"]);
    $this->db->or_like('customers.email', $_POST["search"]["value"]);
    $this->db->or_like('customers.phone', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_type', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_sourse', $_POST["search"]["value"]);
    $this->db->or_like('lead_status.name', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('assigned.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('enquiry.id','desc');
	   return $this->db->count_all_results();
  }


  public function get_enquiry($condition)
  {
    $this->db->select('enquiry.*,customers.id as customerID,customers.userID as customer_assigin_id,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus');
    $this->db->from('enquiry');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = enquiry.lead_status','left');
    $this->db->join('users','users.id = enquiry.userID','left');
    $this->db->join('users as assigned','assigned.id = enquiry.assigned_by','left');
    $this->db->where($condition);
	  return $this->db->get()->row();
  }
  public function get_enquiries($condition){
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('enquiry.*,customers.id as customerID,customers.userID as customer_assigin_id,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus');
    $this->db->from('enquiry');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = enquiry.lead_status','left');
    $this->db->join('users','users.id = enquiry.userID','left');
    $this->db->join('users as assigned','assigned.id = enquiry.assigned_by','left');
    $this->db->where($condition);

    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('enquiry.userID',$usersID );
      }
    }
	  return $this->db->get()->result();
  }

  public function store_enquiry($data){
	 $this->db->insert('enquiry',$data);
   return $this->db->insert_id();
  }


  public function update_enquiry($data,$condition){
	$this->db->where($condition);
	return $this->db->update('enquiry',$data);
  }

  public function store_enquiry_history($data){
   return $this->db->insert('enquiry_history',$data);
     
   }

   function make_query_history($condition)
  {
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('enquiry_history.*,customers.id as customerID,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus');
    $this->db->from('enquiry_history');
    $this->db->join('customers','customers.id = enquiry_history.clientID','left');
    $this->db->join('lead_status','lead_status.id = enquiry_history.lead_status','left');
    $this->db->join('users','users.id = enquiry_history.userID','left');
    $this->db->join('users as assigned','assigned.id = enquiry_history.assigned_by','left');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('enquiry_history.userID',$usersID );
      }
    }

    if(!empty($this->session->userdata('enquiry_from_date')) AND !empty($this->session->userdata('enquiry_to_date')) ){
      $this->db->group_start();
        $this->db->where('DATE_FORMAT(enquiry_history.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('enquiry_from_date'))));
        $this->db->where('DATE_FORMAT(enquiry_history.created_at,"%Y-%m-%d") <=', date('Y-m-d',strtotime($this->session->userdata('enquiry_to_date'))));
        $this->db->group_end();
    }elseif(!empty($this->session->userdata('enquiry_from_date'))){
        $this->db->where('DATE_FORMAT(enquiry_history.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('enquiry_from_date'))));
        
    }
    if(!empty($this->session->userdata('monthname_enquiry'))){ 
        $this->db->where('DATE_FORMAT(enquiry_history.created_at,"%m")', $this->session->userdata('monthname_enquiry'));
        $this->db->where('DATE_FORMAT(enquiry_history.created_at,"%Y")', date('Y'));
    }

  if(!empty($this->session->userdata('assign_user_enquiry'))){
    $this->db->where('enquiry_history.userID', $this->session->userdata('assign_user_enquiry'));
   }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('customers.name', $_POST["search"]["value"]);
    $this->db->or_like('customers.email', $_POST["search"]["value"]);
    $this->db->or_like('customers.phone', $_POST["search"]["value"]);
    $this->db->or_like('enquiry_history.lead_type', $_POST["search"]["value"]);
    $this->db->or_like('enquiry_history.lead_sourse', $_POST["search"]["value"]);
    $this->db->or_like('lead_status.name', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('assigned.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('enquiry_history.id','desc');
    
  }
    function make_datatables_history($condition){
	  $this->make_query_history($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data_history($condition){
	  $this->make_query_history($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_history($condition)
  {
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('enquiry_history.*,customers.id as customerID,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus');
    $this->db->from('enquiry_history');
    $this->db->join('customers','customers.id = enquiry_history.clientID','left');
    $this->db->join('lead_status','lead_status.id = enquiry_history.lead_status','left');
    $this->db->join('users','users.id = enquiry_history.userID','left');
    $this->db->join('users as assigned','assigned.id = enquiry_history.assigned_by','left');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('enquiry_history.userID',$usersID );
      }
    }

    if(!empty($this->session->userdata('enquiry_from_date')) AND !empty($this->session->userdata('enquiry_to_date')) ){
      $this->db->group_start();
        $this->db->where('DATE_FORMAT(enquiry_history.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('enquiry_from_date'))));
        $this->db->where('DATE_FORMAT(enquiry_history.created_at,"%Y-%m-%d") <=', date('Y-m-d',strtotime($this->session->userdata('enquiry_to_date'))));
        $this->db->group_end();
    }elseif(!empty($this->session->userdata('enquiry_from_date'))){
        $this->db->where('DATE_FORMAT(enquiry_history.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('enquiry_from_date'))));
        
    }
    if(!empty($this->session->userdata('monthname_enquiry'))){ 
        $this->db->where('DATE_FORMAT(enquiry_history.created_at,"%m")', $this->session->userdata('monthname_enquiry'));
        $this->db->where('DATE_FORMAT(enquiry_history.created_at,"%Y")', date('Y'));
    }

  if(!empty($this->session->userdata('assign_user_enquiry'))){
    $this->db->where('enquiry_history.userID', $this->session->userdata('assign_user_enquiry'));
   }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('customers.name', $_POST["search"]["value"]);
    $this->db->or_like('customers.email', $_POST["search"]["value"]);
    $this->db->or_like('customers.phone', $_POST["search"]["value"]);
    $this->db->or_like('enquiry_history.lead_type', $_POST["search"]["value"]);
    $this->db->or_like('enquiry_history.lead_sourse', $_POST["search"]["value"]);
    $this->db->or_like('lead_status.name', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('assigned.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('enquiry_history.id','desc');
	   return $this->db->count_all_results();
  }
  
  
  public function get_enquiry_history($condition){
    $this->db->select('enquiry_history.*');
    $this->db->from('enquiry_history');
    $this->db->where($condition);
	 return $this->db->get()->row();
  }
  
  public function delete_enquiry($condition){
	$this->db->where($condition);
	return $this->db->delete('enquiry');
  }
  
  public function delete_enquiry_history($condition){
	$this->db->where($condition);
	return $this->db->delete('enquiry_history');
  }



  function make_query_assigin_enquiry($condition)
  {
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('enquiry.*,customers.id as customerID,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus');
    $this->db->from('enquiry');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = enquiry.lead_status','left');
    $this->db->join('users','users.id = enquiry.userID','left');
    $this->db->join('users as assigned','assigned.id = enquiry.assigned_by','left');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('enquiry.userID',$usersID );
      }
    }

    if($this->session->userdata('customers_filter')){
      $this->db->where('customers.id',$this->session->userdata('customers_filter'));
    }

    if(!empty($this->session->userdata('followup_from_date')) AND !empty($this->session->userdata('followup_to_date')) ){
      $this->db->group_start();
        $this->db->where('DATE_FORMAT(enquiry.assigin_date,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('followup_from_date'))));
        $this->db->where('DATE_FORMAT(enquiry.assigin_date,"%Y-%m-%d") <=', date('Y-m-d',strtotime($this->session->userdata('followup_to_date'))));
        $this->db->group_end();
    }elseif(!empty($this->session->userdata('followup_from_date'))){
        $this->db->where('DATE_FORMAT(enquiry.assigin_date,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('followup_from_date'))));
        
    }
    if(!empty($this->session->userdata('monthname_followup'))){ 
        $this->db->where('DATE_FORMAT(enquiry.assigin_date,"%m")', $this->session->userdata('monthname_followup'));
        $this->db->where('DATE_FORMAT(enquiry.assigin_date,"%Y")', date('Y'));
    }

  if(!empty($this->session->userdata('assign_user_followup'))){
    $this->db->where('users.id', $this->session->userdata('assign_user_followup'));
   }

   if(!empty($this->session->userdata('lead_status_followup'))){
    $this->db->where('enquiry.lead_status', $this->session->userdata('lead_status_followup'));
   }

  //   if(!empty($this->session->userdata('enquiry_from_date')) AND !empty($this->session->userdata('enquiry_to_date')) ){
  //     $this->db->group_start();
  //       $this->db->where('DATE_FORMAT(enquiry.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('enquiry_from_date'))));
  //       $this->db->where('DATE_FORMAT(enquiry.created_at,"%Y-%m-%d") <=', date('Y-m-d',strtotime($this->session->userdata('enquiry_to_date'))));
  //       $this->db->group_end();
  //   }elseif(!empty($this->session->userdata('enquiry_from_date'))){
  //       $this->db->where('DATE_FORMAT(enquiry.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('enquiry_from_date'))));
        
  //   }
  //   if(!empty($this->session->userdata('monthname_enquiry'))){ 
  //       $this->db->where('DATE_FORMAT(enquiry.created_at,"%m")', $this->session->userdata('monthname_enquiry'));
  //       $this->db->where('DATE_FORMAT(enquiry.created_at,"%Y")', date('Y'));
  //   }

  // if(!empty($this->session->userdata('assign_user_enquiry'))){
  //   $this->db->where('enquiry.userID', $this->session->userdata('assign_user_enquiry'));
  //  }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('customers.name', $_POST["search"]["value"]);
    $this->db->or_like('customers.email', $_POST["search"]["value"]);
    $this->db->or_like('customers.phone', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_type', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_sourse', $_POST["search"]["value"]);
    $this->db->or_like('lead_status.name', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('assigned.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('enquiry.id','desc');
    
  }
    function make_datatables_assigin_enquiry($condition){
	  $this->make_query_assigin_enquiry($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data_assigin_enquiry($condition){
	  $this->make_query_assigin_enquiry($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_assigin_enquiry($condition)
  {
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('enquiry.*,customers.id as customerID,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus');
    $this->db->from('enquiry');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = enquiry.lead_status','left');
    $this->db->join('users','users.id = enquiry.userID','left');
    $this->db->join('users as assigned','assigned.id = enquiry.assigned_by','left');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('enquiry.userID',$usersID );
      }
    }

    if($this->session->userdata('customers_filter')){
      $this->db->where('customers.id',$this->session->userdata('customers_filter'));
    }

    if(!empty($this->session->userdata('followup_from_date')) AND !empty($this->session->userdata('followup_to_date')) ){
      $this->db->group_start();
        $this->db->where('DATE_FORMAT(enquiry.assigin_date,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('followup_from_date'))));
        $this->db->where('DATE_FORMAT(enquiry.assigin_date,"%Y-%m-%d") <=', date('Y-m-d',strtotime($this->session->userdata('followup_to_date'))));
        $this->db->group_end();
    }elseif(!empty($this->session->userdata('followup_from_date'))){
        $this->db->where('DATE_FORMAT(enquiry.assigin_date,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('followup_from_date'))));
        
    }
    if(!empty($this->session->userdata('monthname_followup'))){ 
        $this->db->where('DATE_FORMAT(enquiry.assigin_date,"%m")', $this->session->userdata('monthname_followup'));
        $this->db->where('DATE_FORMAT(enquiry.assigin_date,"%Y")', date('Y'));
    }

  if(!empty($this->session->userdata('assign_user_followup'))){
    $this->db->where('users.id', $this->session->userdata('assign_user_followup'));
   }

   if(!empty($this->session->userdata('lead_status_followup'))){
    $this->db->where('enquiry.lead_status', $this->session->userdata('lead_status_followup'));
   }

  //   if(!empty($this->session->userdata('enquiry_from_date')) AND !empty($this->session->userdata('enquiry_to_date')) ){
  //     $this->db->group_start();
  //       $this->db->where('DATE_FORMAT(enquiry.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('enquiry_from_date'))));
  //       $this->db->where('DATE_FORMAT(enquiry.created_at,"%Y-%m-%d") <=', date('Y-m-d',strtotime($this->session->userdata('enquiry_to_date'))));
  //       $this->db->group_end();
  //   }elseif(!empty($this->session->userdata('enquiry_from_date'))){
  //       $this->db->where('DATE_FORMAT(enquiry.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('enquiry_from_date'))));
        
  //   }
  //   if(!empty($this->session->userdata('monthname_enquiry'))){ 
  //       $this->db->where('DATE_FORMAT(enquiry.created_at,"%m")', $this->session->userdata('monthname_enquiry'));
  //       $this->db->where('DATE_FORMAT(enquiry.created_at,"%Y")', date('Y'));
  //   }

  // if(!empty($this->session->userdata('assign_user_enquiry'))){
  //   $this->db->where('enquiry.userID', $this->session->userdata('assign_user_enquiry'));
  //  }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('customers.name', $_POST["search"]["value"]);
    $this->db->or_like('customers.email', $_POST["search"]["value"]);
    $this->db->or_like('customers.phone', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_type', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_sourse', $_POST["search"]["value"]);
    $this->db->or_like('lead_status.name', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('assigned.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('enquiry.id','desc');
	   return $this->db->count_all_results();
  }


  
}