<?php 

class Sale_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}

  
	function make_query($condition)
  {
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('sales.*,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus,enquiry.lead_type,enquiry.lead_sourse,enquiry.requirement');
    $this->db->from('sales');
    $this->db->join('enquiry','enquiry.id = sales.enquiryID','left');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = enquiry.lead_status','left');
    $this->db->join('users','users.id = sales.sale_closed_by','left');
    $this->db->join('users as assigned','assigned.id = customers.userID','left');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('sales.sale_closed_by',$usersID );
      }
    }
    if(!empty($this->session->userdata('pending_balance'))){
      if($this->session->userdata('pending_balance')==1){
        $this->db->where('sales.pending_amount >',0);
      }

      if($this->session->userdata('pending_balance')==2){
        $this->db->where('sales.pending_amount',0);
      }
      
    }
    if(!empty($this->session->userdata('sale_from_date')) AND !empty($this->session->userdata('sale_to_date')) ){
      $this->db->group_start();
        $this->db->where('DATE_FORMAT(sales.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('sale_from_date'))));
        $this->db->where('DATE_FORMAT(sales.created_at,"%Y-%m-%d") <=', date('Y-m-d',strtotime($this->session->userdata('sale_to_date'))));
        $this->db->group_end();
    }elseif(!empty($this->session->userdata('sale_from_date'))){
        $this->db->where('DATE_FORMAT(sales.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('sale_from_date'))));
        
    }
    if(!empty($this->session->userdata('monthname'))){ 
        $this->db->where('DATE_FORMAT(sales.created_at,"%m")', $this->session->userdata('monthname'));
        $this->db->where('DATE_FORMAT(sales.created_at,"%Y")', date('Y'));
    }

  if(!empty($this->session->userdata('assign_user'))){
    $this->db->where('sales.sale_closed_by', $this->session->userdata('assign_user'));
   }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('customers.name', $_POST["search"]["value"]);
    $this->db->or_like('customers.email', $_POST["search"]["value"]);
    $this->db->or_like('customers.phone', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_type', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_sourse', $_POST["search"]["value"]);
    $this->db->or_like('lead_status.name', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('assigned.name', $_POST["search"]["value"]);
    $this->db->or_like('sales.product', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('sales.id','desc');
    
  }
    function make_datatables($condition){
	  $this->make_query($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
    echo $this->db->last_query(); die;
  }

  function get_filtered_data($condition){
	  $this->make_query($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data($condition)
  {
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('sales.*,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus,enquiry.lead_type,enquiry.lead_sourse,enquiry.requirement');
    $this->db->from('sales');
    $this->db->join('enquiry','enquiry.id = sales.enquiryID','left');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = enquiry.lead_status','left');
    $this->db->join('users','users.id = sales.sale_closed_by','left');
    $this->db->join('users as assigned','assigned.id = customers.userID','left');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('sales.sale_closed_by',$usersID );
      }
    }
    if(!empty($this->session->userdata('pending_balance'))){
      if($this->session->userdata('pending_balance')==1){
        $this->db->where('sales.pending_amount >',0);
      }

      if($this->session->userdata('pending_balance')==2){
        $this->db->where('sales.pending_amount',0);
      }
      
    }
    if(!empty($this->session->userdata('sale_from_date')) AND !empty($this->session->userdata('sale_to_date')) ){
      $this->db->group_start();
        $this->db->where('DATE_FORMAT(sales.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('sale_from_date'))));
        $this->db->where('DATE_FORMAT(sales.created_at,"%Y-%m-%d") <=', date('Y-m-d',strtotime($this->session->userdata('sale_to_date'))));
        $this->db->group_end();
    }elseif(!empty($this->session->userdata('sale_from_date'))){
        $this->db->where('DATE_FORMAT(sales.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('sale_from_date'))));
        
    }
    if(!empty($this->session->userdata('monthname'))){ 
        $this->db->where('DATE_FORMAT(sales.created_at,"%m")', $this->session->userdata('monthname'));
        $this->db->where('DATE_FORMAT(sales.created_at,"%Y")', date('Y'));
    }

  if(!empty($this->session->userdata('assign_user'))){
    $this->db->where('sales.sale_closed_by', $this->session->userdata('assign_user'));
   }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('customers.name', $_POST["search"]["value"]);
    $this->db->or_like('customers.email', $_POST["search"]["value"]);
    $this->db->or_like('customers.phone', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_type', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_sourse', $_POST["search"]["value"]);
    $this->db->or_like('lead_status.name', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('assigned.name', $_POST["search"]["value"]);
    $this->db->or_like('sales.product', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('sales.id','desc');
	   return $this->db->count_all_results();
  }


  function grand_total_sale($condition)
  {
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('sales.*,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus,enquiry.lead_type,enquiry.lead_sourse,enquiry.requirement');
    $this->db->from('sales');
    $this->db->join('enquiry','enquiry.id = sales.enquiryID','left');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = enquiry.lead_status','left');
    $this->db->join('users','users.id = sales.sale_closed_by','left');
    $this->db->join('users as assigned','assigned.id = customers.userID','left');
    $this->db->where($condition);
    if(!empty($this->session->userdata('pending_balance'))){
      if($this->session->userdata('pending_balance')==1){
        $this->db->where('sales.pending_amount',0);
      }

      if($this->session->userdata('pending_balance')==2){
        $this->db->where('sales.pending_amount >',0);
      }
      
    }
    if(!empty($this->session->userdata('sale_from_date')) AND !empty($this->session->userdata('sale_to_date')) ){
      $this->db->group_start();
        $this->db->where('DATE_FORMAT(sales.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('sale_from_date'))));
        $this->db->where('DATE_FORMAT(sales.created_at,"%Y-%m-%d") <=', date('Y-m-d',strtotime($this->session->userdata('sale_to_date'))));
        $this->db->group_end();
    }elseif(!empty($this->session->userdata('sale_from_date'))){
        $this->db->where('DATE_FORMAT(sales.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('sale_from_date'))));
        
    }
    if(!empty($this->session->userdata('monthname'))){ 
        $this->db->where('DATE_FORMAT(sales.created_at,"%m")', $this->session->userdata('monthname'));
        $this->db->where('DATE_FORMAT(sales.created_at,"%Y")', date('Y'));
    }

  if(!empty($this->session->userdata('assign_user'))){
    $this->db->where('sales.sale_closed_by', $this->session->userdata('assign_user'));
   }
   if($this->session->userdata('user_type')!=2){
    if(!empty($usersID)){
      $this->db->where_in('sales.sale_closed_by',$usersID );
    }
  }

   $this->db->order_by('sales.id','desc');
   $query =  $this->db->get()->result_array();

   foreach($query as $key=>$row){
    $this->db->select('sum(amount) as total_amount_paid');
    $this->db->from('payment_history');
    $this->db->where('saleID',$row['id']);
    $query[$key]['total_paid'] =  $this->db->get()->row_array();
   }
   return $query;
  }


  public function get_sale($condition){
    $this->db->select('sales.*,customers.userID as customer_assigin_id,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus,enquiry.lead_type,enquiry.lead_sourse,enquiry.requirement');
    $this->db->from('sales');
    $this->db->join('enquiry','enquiry.id = sales.enquiryID','left');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = enquiry.lead_status','left');
    $this->db->join('users','users.id = sales.sale_closed_by','left');
    $this->db->join('users as assigned','assigned.id = customers.userID','left');
    $this->db->where($condition);
    $this->db->order_by('sales.id','desc');
	 return $this->db->get()->row();
  }

  public function get_sales($condition){
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('sales.*,customers.userID as customer_assigin_id,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus,enquiry.lead_type,enquiry.lead_sourse,enquiry.requirement');
    $this->db->from('sales');
    $this->db->join('enquiry','enquiry.id = sales.enquiryID','left');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = enquiry.lead_status','left');
    $this->db->join('users','users.id = sales.sale_closed_by','left');
    $this->db->join('users as assigned','assigned.id = customers.userID','left');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('sales.sale_closed_by',$usersID );
      }
    }
   $this->db->order_by('sales.id','desc');
	  return $this->db->get()->result();
  }

  public function store_sale($data){
	 $this->db->insert('sales',$data);
   return $this->db->insert_id();
  }


  public function update_sale($data,$condition){
	$this->db->where($condition);
	return $this->db->update('sales',$data);
  }

  public function store_payment_history($data){
    return $this->db->insert('payment_history',$data);
      
   }

  public function store_sale_history($data){
   return $this->db->insert('sales_history',$data);
     
  }

   public function get_payment_histories($condition){
    $this->db->select('payment_history.*,users.name as user_name');
    $this->db->from('payment_history');
    $this->db->join('users','users.id=payment_history.userID','left');
    $this->db->where($condition);
    return $this->db->get()->result();
   }

   public function delete_payment($condition){
    $this->db->where($condition);
    return $this->db->delete('payment_history');
   }

   public function delete_sale($condition){
    $this->db->where($condition);
    return $this->db->delete('sales');
   }

   public function delete_sale_history($condition){
    $this->db->where($condition);
    return $this->db->delete('sales_history');
   }


   function make_query_payment($condition)
  {
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('payment_history.*,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,users.name as assigin_to_name');
    $this->db->from('payment_history');
    $this->db->join('sales','sales.id = payment_history.saleID','left');
    $this->db->join('enquiry','enquiry.id = sales.enquiryID','left');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('users','users.id = payment_history.userID','left');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('payment_history.userID',$usersID );
      }
    }

    if(!empty($this->session->userdata('sale_from_date')) AND !empty($this->session->userdata('sale_to_date')) ){
      $this->db->group_start();
        $this->db->where('DATE_FORMAT(payment_history.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('sale_from_date'))));
        $this->db->where('DATE_FORMAT(payment_history.created_at,"%Y-%m-%d") <=', date('Y-m-d',strtotime($this->session->userdata('sale_to_date'))));
        $this->db->group_end();
    }elseif(!empty($this->session->userdata('sale_from_date'))){
        $this->db->where('DATE_FORMAT(payment_history.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('sale_from_date'))));
        
    }
    if(!empty($this->session->userdata('monthname'))){ 
        $this->db->where('DATE_FORMAT(payment_history.created_at,"%m")', $this->session->userdata('monthname'));
        $this->db->where('DATE_FORMAT(payment_history.created_at,"%Y")', date('Y'));
    }

  if(!empty($this->session->userdata('assign_user'))){
    $this->db->where('payment_history.userID', $this->session->userdata('assign_user'));
   }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('customers.name', $_POST["search"]["value"]);
    $this->db->or_like('customers.email', $_POST["search"]["value"]);
    $this->db->or_like('customers.phone', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_type', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_sourse', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('payment_history.id','desc');
    
  }
    function make_datatables_payment($condition){
	  $this->make_query_payment($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	 return  $query->result_array(); 
   // echo $this->db->last_query(); die;
  }

  function get_filtered_data_payment($condition){
	  $this->make_query_payment($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_payment($condition)
  {
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('payment_history.*,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,users.name as assigin_to_name');
    $this->db->from('payment_history');
    $this->db->join('sales','sales.id = payment_history.saleID','left');
    $this->db->join('enquiry','enquiry.id = sales.enquiryID','left');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('users','users.id = payment_history.userID','left');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('payment_history.userID',$usersID );
      }
    }

    if(!empty($this->session->userdata('sale_from_date')) AND !empty($this->session->userdata('sale_to_date')) ){
      $this->db->group_start();
        $this->db->where('DATE_FORMAT(payment_history.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('sale_from_date'))));
        $this->db->where('DATE_FORMAT(payment_history.created_at,"%Y-%m-%d") <=', date('Y-m-d',strtotime($this->session->userdata('sale_to_date'))));
        $this->db->group_end();
    }elseif(!empty($this->session->userdata('sale_from_date'))){
        $this->db->where('DATE_FORMAT(payment_history.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('sale_from_date'))));
        
    }
    if(!empty($this->session->userdata('monthname'))){ 
        $this->db->where('DATE_FORMAT(payment_history.created_at,"%m")', $this->session->userdata('monthname'));
        $this->db->where('DATE_FORMAT(payment_history.created_at,"%Y")', date('Y'));
    }

  if(!empty($this->session->userdata('assign_user'))){
    $this->db->where('payment_history.userID', $this->session->userdata('assign_user'));
   }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('customers.name', $_POST["search"]["value"]);
    $this->db->or_like('customers.email', $_POST["search"]["value"]);
    $this->db->or_like('customers.phone', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_type', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_sourse', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('assigned.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('payment_history.id','desc');
    
	   return $this->db->count_all_results();
  }

  function grand_total_payment_history($condition)
  {
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('sum(amount) as total_amount_paid');
    $this->db->from('payment_history');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('payment_history.userID',$usersID );
      }
    }

    if(!empty($this->session->userdata('sale_from_date')) AND !empty($this->session->userdata('sale_to_date')) ){
      $this->db->group_start();
        $this->db->where('DATE_FORMAT(payment_history.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('sale_from_date'))));
        $this->db->where('DATE_FORMAT(payment_history.created_at,"%Y-%m-%d") <=', date('Y-m-d',strtotime($this->session->userdata('sale_to_date'))));
        $this->db->group_end();
    }elseif(!empty($this->session->userdata('sale_from_date'))){
        $this->db->where('DATE_FORMAT(payment_history.created_at,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('sale_from_date'))));
        
    }
    if(!empty($this->session->userdata('monthname'))){ 
        $this->db->where('DATE_FORMAT(payment_history.created_at,"%m")', $this->session->userdata('monthname'));
        $this->db->where('DATE_FORMAT(payment_history.created_at,"%Y")', date('Y'));
    }

  if(!empty($this->session->userdata('assign_user'))){
    $this->db->where('payment_history.userID', $this->session->userdata('assign_user'));
   }

  return $this->db->get()->row_array();


  }


  
}