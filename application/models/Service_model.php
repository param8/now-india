<?php 

class Service_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}

  
	function make_query($condition)
  {
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('services.*,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus,enquiry.lead_type,enquiry.lead_sourse,enquiry.requirement');
    $this->db->from('services');
    $this->db->join('sales','sales.id = services.saleID','left');
    $this->db->join('enquiry','enquiry.id = services.enquiryID','left');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = enquiry.lead_status','left');
    $this->db->join('users','users.id = services.userID','left');
    $this->db->join('users as assigned','assigned.id = enquiry.assigned_by','left');
    $this->db->where($condition);

    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('services.userID',$usersID );
      }
    }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('customers.name', $_POST["search"]["value"]);
    $this->db->or_like('customers.email', $_POST["search"]["value"]);
    $this->db->or_like('customers.phone', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_type', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_sourse', $_POST["search"]["value"]);
    $this->db->or_like('lead_status.name', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('sales.product', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('services.id','desc');
    
  }
    function make_datatables($condition){
	  $this->make_query($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data($condition){
	  $this->make_query($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data($condition)
  {
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('services.*,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus,enquiry.lead_type,enquiry.lead_sourse,enquiry.requirement');
    $this->db->from('services');
    $this->db->join('sales','sales.id = services.saleID','left');
    $this->db->join('enquiry','enquiry.id = services.enquiryID','left');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = enquiry.lead_status','left');
    $this->db->join('users','users.id = services.userID','left');
    $this->db->join('users as assigned','assigned.id = enquiry.assigned_by','left');
    $this->db->where($condition);

    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('services.userID',$usersID );
      }
    }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('customers.name', $_POST["search"]["value"]);
    $this->db->or_like('customers.email', $_POST["search"]["value"]);
    $this->db->or_like('customers.phone', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_type', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_sourse', $_POST["search"]["value"]);
    $this->db->or_like('lead_status.name', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('sales.product', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('services.id','desc');
	   return $this->db->count_all_results();
  }


  public function get_service($condition){
    $this->db->select('services.*,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus,enquiry.lead_type,enquiry.lead_sourse,enquiry.requirement');
    $this->db->from('services');
    $this->db->join('sales','sales.id = services.saleID','left');
    $this->db->join('enquiry','enquiry.id = services.enquiryID','left');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = enquiry.lead_status','left');
    $this->db->join('users','users.id = services.userID','left');
    $this->db->join('users as assigned','assigned.id = enquiry.assigned_by','left');
    $this->db->where($condition);
	 return $this->db->get()->row();
  }

  public function get_services($condition){
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('services.*,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus,enquiry.lead_type,enquiry.lead_sourse,enquiry.requirement');
    $this->db->from('services');
    $this->db->join('sales','sales.id = services.saleID','left');
    $this->db->join('enquiry','enquiry.id = services.enquiryID','left');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = enquiry.lead_status','left');
    $this->db->join('users','users.id = services.userID','left');
    $this->db->join('users as assigned','assigned.id = enquiry.assigned_by','left');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('services.userID',$usersID );
      }
    }
   $this->db->order_by('services.id','desc');
	  return $this->db->get()->result();
  }

  public function get_services_detail($condition){
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('services.*,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus,enquiry.lead_type,enquiry.lead_sourse,enquiry.requirement');
    $this->db->from('services');
    $this->db->join('sales','sales.id = services.saleID','left');
    $this->db->join('enquiry','enquiry.id = services.enquiryID','left');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = enquiry.lead_status','left');
    $this->db->join('users','users.id = services.userID','left');
    $this->db->join('users as assigned','assigned.id = enquiry.assigned_by','left');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('services.userID',$usersID );
      }
    }
   $this->db->order_by('services.id','desc');
	  return $this->db->get()->result_array();
  }

  public function store_service($data){
	 $this->db->insert('services',$data);
   return $this->db->insert_id();
  }


  public function update_service($data,$condition){

	$this->db->where($condition);
	return $this->db->update('services',$data);
  }

  public function get_service_types($condition){
    $this->db->where($condition);
    return $this->db->get('service_type')->result();
  }


  function make_datatables_renew($condition)
  {
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('services.*');
    $this->db->from('services');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('services.userID',$usersID );
      }
    }
    $query = $this->db->get()->result_array();

    $domain_previous_Month = array();
		$email_previous_Month = array();
		$hosting_previous_Month = array();
		$ssl_previous_Month = array();
		$seo_previous_Month = array();
		$smo_previous_Month = array();
		$smm_previous_Month = array();
		$ppc_previous_Month = array();
    $maintenence_previous_Month = array();
		foreach($query as $serve)
		{
			$service_detail = json_decode($serve['service_detail']);

			$domain_renew_date = !empty($service_detail->Domain->doamin_renew_date) ? date('Y-m-d',strtotime($service_detail->Domain->doamin_renew_date)) : '';
			$domain_month = strtotime($domain_renew_date.'-1 month');
			$domain_previous_Month[$serve['id'].'_Domain'] = !empty($service_detail->Domain->doamin_renew_date) ? date('Y-m-d',$domain_month) : '';


			$email_renew_date = date('Y-m-d',strtotime($service_detail->Email->email_rDate));
			$email_month = strtotime($email_renew_date.'-1 month');
			$email_previous_Month[$serve['id'].'_Email'] = !empty($service_detail->Email->email_rDate) ? date('Y-m-d',$email_month) : '';

			$hosting_renew_date = date('Y-m-d',strtotime($service_detail->Hosting->hosting_renew_date));
			$hosting_month = strtotime($hosting_renew_date.'-1 month');
			$hosting_previous_Month[$serve['id'].'_Hosting'] = !empty($hosting_renew_date) ? date('Y-m-d',$hosting_month) : '';


			$ssl_renew_date = date('Y-m-d',strtotime($service_detail->SSL->ssl_renew_date));
			$ssl_month = strtotime($ssl_renew_date.'-1 month');
			$ssl_previous_Month[$serve['id'].'_SSL'] = !empty($service_detail->SSL->ssl_renew_date) ? date('Y-m-d',$ssl_month) : '';
			$seo_renew_date = date('Y-m-d',strtotime($service_detail->SEO->seo_rDate));
			$seo_month = strtotime($seo_renew_date.'-1 month');
			$seo_previous_Month[$serve['id'].'_SEO'] = !empty($service_detail->SEO->seo_rDate) ? date('Y-m-d',$seo_month) : '';

			$smo_renew_date = date('Y-m-d',strtotime($service_detail->SMO->smo_rDate));
			$smo_month = strtotime($smo_renew_date.'-1 month');
			$smo_previous_Month[$serve['id'].'_SMO'] = !empty($service_detail->SMO->smo_rDate) ? date('Y-m-d',$smo_month) : '';

			$smm_renew_date = date('Y-m-d',strtotime($service_detail->SMM->smm_rDate));
			$smm_month = strtotime($smm_renew_date.'-1 month');
			$smm_previous_Month[$serve['id'].'_SMM'] = !empty($service_detail->SMM->smm_rDate) ? date('Y-m-d',$smm_month) : '';

			$ppc_renew_date = date('Y-m-d',strtotime($service_detail->PPC->ppc_rDate));
			$ppc_month = strtotime($ppc_renew_date.'-1 month');
			$ppc_previous_Month[$serve['id'].'_PPC'] = !empty($service_detail->PPC->ppc_rDate) ? date('Y-m-d',$ppc_month) : '';
      
      $maintenence_renew_date = date('Y-m-d',strtotime($service_detail->Maintenence->maintenence_rDate));
			$maintenence_month = strtotime($maintenence_renew_date.'-1 month');
			$maintenence_previous_Month[$serve['id'].'_Maintenence'] = !empty($service_detail->Maintenence->maintenence_rDate) ? date('Y-m-d',$maintenence_month) : '';
     
		}

		$domainData       = array_filter($domain_previous_Month);
		$emailData        = array_filter($email_previous_Month);
		$hostingData      = array_filter($hosting_previous_Month);
		$sslData          = array_filter($ssl_previous_Month);
		$seoData          = array_filter($seo_previous_Month);
		$smoData          = array_filter($smo_previous_Month);
		$smmData          = array_filter($smm_previous_Month);
		$ppcData          = array_filter($ppc_previous_Month);
    $maintenenceData  = array_filter($maintenence_previous_Month);
    // echo "<pre>";
    // print_r($hostingData);die;

    foreach($query as $key => $service){
      $service_details = json_decode($service['service_detail']);

			
			$sno = 1;
			foreach($service_details as $detail_key => $detail){
		

		$domainFilter  = 	!empty($domainData[$service['id'].'_'.$detail_key]) ? $domainData[$service['id'].'_'.$detail_key] <= date('Y-m-d') . ' OR ' : '';
		$emailFilter   = 	!empty($emailData[$service['id'].'_'.$detail_key]) ? $emailData[$service['id'].'_'.$detail_key] < date('Y-m-d'). ' OR ' : '';
		$hostingFilter = 	!empty($hostingData[$service['id'].'_'.$detail_key]) ? $hostingData[$service['id'].'_'.$detail_key] < date('Y-m-d'). ' OR ' : '';
		$sslFilter     = 	!empty($sslData[$service['id'].'_'.$detail_key]) ? $sslData[$service['id'].'_'.$detail_key] <= date('Y-m-d'). ' OR ' : '';
		$seoFilter     = 	!empty($seoData[$service['id'].'_'.$detail_key]) ? $seoData[$service['id'].'_'.$detail_key] < date('Y-m-d'). ' OR ' : '';
		$smoFilter     = 	!empty($smoData[$service['id'].'_'.$detail_key]) ? $smoData[$service['id'].'_'.$detail_key] < date('Y-m-d'). ' OR ' : '';
		$smmFilter     = 	!empty($smmData[$service['id'].'_'.$detail_key]) ? $smmData[$service['id'].'_'.$detail_key] < date('Y-m-d'). ' OR ' : '';
		$ppcFilter     = 	!empty($ppcData[$service['id'].'_'.$detail_key]) ? $ppcData[$service['id'].'_'.$detail_key] < date('Y-m-d'). ' OR ' : '';

    $maintenenceFilter  = 	!empty($maintenenceData[$service['id'].'_'.$detail_key]) ? $maintenenceData[$service['id'].'_'.$detail_key] < date('Y-m-d') : '';
        //echo "<pre>";
		   //print_r('SSL'.$sslData['13_SSL']. '<'. date('Y-m-d').' EMAIL '.$emailData[$service['id'].'_'.$detail_key]. '<'. date('Y-m-d'));

			
		if($domainFilter.$emailFilter.$hostingFilter.$sslFilter.$seoFilter.$smoFilter.$smmFilter.$ppcFilter.$maintenenceFilter)
		{
		
   
			$dmoain_name = $detail_key=='Domain' ? $detail->domain_name :($detail_key=='Email' ? $detail->email_domain_name :($detail_key=='Hosting' ? $detail->hosting_domain_name :($detail_key=='SSL' ? $detail->ssl_domain_name :($detail_key=='SEO' ? $detail->seo_url :($detail_key=='SMO' ? $detail->smo_url :($detail_key=='SMM' ? $detail->smm_url :($detail_key=='PPC' ? $detail->ppc_url :($detail_key=='Maintenence' ? $detail->websiteMaintenenceDetail :''))))))));

			$product_name = $detail_key=='Domain' ? 'Domain' :($detail_key=='Email' ? 'Email' :($detail_key=='Hosting' ? 'Hosting' :($detail_key=='SSL' ? 'SSL' :($detail_key=='SEO' ? 'SEO' :($detail_key=='SMO' ? 'SMO' :($detail_key=='SMM' ? 'SMM' :($detail_key=='PPC' ? 'PPC' :($detail_key=='Maintenence' ? 'Maintenence' :''))))))));

			$renew_date = $detail_key=='Domain' ? date('d-M-Y', strtotime($detail->doamin_renew_date))  :($detail_key=='Email' ? date('d-M-Y', strtotime($detail->email_rDate)) :($detail_key=='Hosting' ? date('d-M-Y', strtotime($detail->hosting_renew_date)) :($detail_key=='SSL' ? date('d-M-Y', strtotime($detail->ssl_renew_date)) :($detail_key=='SEO' ? date('d-M-Y', strtotime($detail->seo_rDate)) :($detail_key=='SMO' ? date('d-M-Y', strtotime($detail->smo_rDate)) :($detail_key=='SMM' ? date('d-M-Y', strtotime($detail->smm_rDate)) :($detail_key=='PPC' ? date('d-M-Y', strtotime($detail->ppc_rDate)) :($detail_key=='Maintenence' ? date('d-M-Y', strtotime($detail->maintenence_rDate)) :''))))))));

      
      if(($detail_key=='Domain' AND $detail->domainStatus=='New') OR ($detail_key=='Email' AND $detail->emailStatus=='New') OR ($detail_key=='Hosting' AND $detail->hostingStatus=='New')  OR ($detail_key=='SSL' AND $detail->sslStatus=='New') OR ($detail_key=='SEO' AND $detail->seoStatus=='New') OR ($detail_key=='SMO' AND $detail->smoStatus=='New') OR ($detail_key=='SMM' AND $detail->smmStatus=='New') OR ($detail_key=='PPC' AND $detail->ppcStatus=='New') OR ($detail_key=='Maintenence' AND $detail->maintenenceStatus=='New') ){
       // print_r($product_name);

			$button = '';
			$sub_array = array();
			// if($permission[0]=='View')
			// {
			 $button .= '<a href="'.base_url('renewal-services/'.base64_encode($service['id'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Renew Now!" class="btn btn-outline-primary btn-sm"> Renew Now!</a>';
			//}

			$sub_array[] = $sno++;

			$sub_array[] = $dmoain_name;
			$sub_array[] = $product_name;
			$sub_array[] = $renew_date;
			$sub_array[] = '<b class="text-danger">Expired</b>';
			$sub_array[] = $button;
		  $data[]      = $sub_array;

		 }
    }
    }
    
  }

  return $data;
}


public function delete_service($condition){
  $this->db->where($condition);
  return $this->db->delete('services');
}
    
}