<?php 
class Setting_model extends CI_Model 
{

  public function __construct()
  {
      parent::__construct();

  }

  public function update_siteInfo($data,$id){
    $this->db->where('adminID', $id);
   return  $this->db->update('site_info',$data);
     //echo $this->db->last_query();die;
  }

  

  function make_query_role($condition)
  {
    $this->db->select('role.*');
    $this->db->from('role');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('name', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
     
  }
    function make_datatables_role($condition){
	  $this->make_query_role($condition,);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array();
  }

  function get_filtered_data_role($condition){
	  $this->make_query_role($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_role($condition)
  {
    $this->db->select('role.*');
    $this->db->from('role');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('name', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
	   return $this->db->count_all_results();
  }

  public function get_roles($condition){
    $this->db->where($condition);
    return $this->db->get('role')->result();
  }

  public function get_role($condition){
    $this->db->where($condition);
    return  $this->db->get('role')->row();
     //echo $this->db->last_query();die;
  }

  
public function store_role($data){
  return $this->db->insert('role',$data);
}

public function update_role($data,$id){
  $this->db->where('id',$id);
  return $this->db->update('role',$data);
}

public function get_lead_status($condition){
  $this->db->where($condition);
  return $this->db->get('lead_status')->result();
}


function make_query_email_setting($condition)
  {
    $this->db->select('email_setting.*');
    $this->db->from('email_setting');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('type', $_POST["search"]["value"]);
    $this->db->like('subject', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
     
  }
    function make_datatables_email_setting($condition){
	  $this->make_query_email_setting($condition,);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array();
  }

  function get_filtered_data_email_setting($condition){
	  $this->make_query_email_setting($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_email_setting($condition)
  {
    $this->db->select('email_setting.*');
    $this->db->from('email_setting');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('type', $_POST["search"]["value"]);
    $this->db->like('subject', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
	   return $this->db->count_all_results();
  }

 public function get_email_setting($condition)
  {
    $this->db->select('email_setting.*');
    $this->db->from('email_setting');
    $this->db->where($condition);
    return $this->db->get()->row();
  }

  public function get_email_settings($condition)
  {
    $this->db->select('email_setting.*');
    $this->db->from('email_setting');
    $this->db->where($condition);
    return $this->db->get()->result();
  }

  public function store_email_setting($data){
  return $this->db->insert('email_setting',$data);
  }



}