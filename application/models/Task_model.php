<?php 

class Task_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}

  
	function make_query($condition)
  {
    $permission = permission('task');
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $sale_pemission = json_decode($role->other_permission)->task_developer;
    $this->db->select('task_maneger.*,sales.task_status as sales_task_status,users.name as assigin_to_name,assigned.name as assigin_by_name');
    $this->db->from('task_maneger');
    $this->db->join('sales','sales.id = task_maneger.saleID','left');
    $this->db->join('users','users.id = task_maneger.assign_to','left');
    $this->db->join('users as assigned','assigned.id = task_maneger.assign_by','left');
    $this->db->where($condition);
    if( $permission[7]!='Like Admin' AND $sale_pemission!=1){
      if(!empty($usersID)){
        $this->db->where_in('task_maneger.assign_by',$usersID );
      }
    }

    if($permission[7]!='Like Admin' AND $sale_pemission==1){
      $this->db->like('task_maneger.assign_to',$this->session->userdata('id') );
    }

    if(!empty($this->session->userdata('task_status'))){
        $this->db->where('task_maneger.work_status',$this->session->userdata('task_status'));
    }
    if(!empty($this->session->userdata('task_from_date')) AND !empty($this->session->userdata('task_to_date')) ){
      $this->db->group_start();
        $this->db->where('DATE_FORMAT(task_maneger.assign_date,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('task_from_date'))));
        $this->db->where('DATE_FORMAT(task_maneger.assign_date,"%Y-%m-%d") <=', date('Y-m-d',strtotime($this->session->userdata('task_to_date'))));
        $this->db->group_end();
    }elseif(!empty($this->session->userdata('task_from_date'))){
        $this->db->where('DATE_FORMAT(task_maneger.assign_date,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('task_from_date'))));
        
    }
    if(!empty($this->session->userdata('taskMonthName'))){ 
        $this->db->where('DATE_FORMAT(task_maneger.assign_date,"%m")', $this->session->userdata('taskMonthName'));
        $this->db->where('DATE_FORMAT(task_maneger.assign_date,"%Y")', date('Y'));
    }

  if(!empty($this->session->userdata('task_assign_user'))){
    $this->db->where('task_maneger.assign_by', $this->session->userdata('task_assign_user'));
   }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('task_maneger.task_name', $_POST["search"]["value"]);
    $this->db->or_like('task_maneger.priority', $_POST["search"]["value"]);
    $this->db->or_like('task_maneger.tools', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('assigned.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('task_maneger.id','desc');
    
  }
    function make_datatables($condition){
	  $this->make_query($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
     //echo $this->db->last_query();die;
  }

  function get_filtered_data($condition){
	  $this->make_query($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
  }
  function get_all_data($condition)
  {
    $permission = permission('task');
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $sale_pemission = json_decode($role->other_permission)->task_developer;
    $this->db->select('task_maneger.*,sales.task_status as sales_task_status,users.name as assigin_to_name,assigned.name as assigin_by_name');
    $this->db->from('task_maneger');
    $this->db->join('sales','sales.id = task_maneger.saleID','left');
    $this->db->join('users','users.id = task_maneger.assign_to','left');
    $this->db->join('users as assigned','assigned.id = task_maneger.assign_by','left');
    $this->db->where($condition);
    if($permission[7]!='Like Admin' AND $sale_pemission!=1){
      if(!empty($usersID)){
        $this->db->where_in('task_maneger.assign_by',$usersID );
      }
    }

    if($permission[7]!='Like Admin' AND $sale_pemission==1){
      $this->db->like('task_maneger.assign_to',$this->session->userdata('id') );
    }

    if(!empty($this->session->userdata('task_status'))){
      $this->db->where('task_maneger.work_status',$this->session->userdata('task_status'));
  }
  if(!empty($this->session->userdata('task_from_date')) AND !empty($this->session->userdata('task_to_date')) ){
    $this->db->group_start();
      $this->db->where('DATE_FORMAT(task_maneger.assign_date,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('task_from_date'))));
      $this->db->where('DATE_FORMAT(task_maneger.assign_date,"%Y-%m-%d") <=', date('Y-m-d',strtotime($this->session->userdata('task_to_date'))));
      $this->db->group_end();
  }elseif(!empty($this->session->userdata('task_from_date'))){
      $this->db->where('DATE_FORMAT(task_maneger.assign_date,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('task_from_date'))));
      
  }
  if(!empty($this->session->userdata('taskMonthName'))){ 
      $this->db->where('DATE_FORMAT(task_maneger.assign_date,"%m")', $this->session->userdata('taskMonthName'));
      $this->db->where('DATE_FORMAT(task_maneger.assign_date,"%Y")', date('Y'));
  }

if(!empty($this->session->userdata('task_assign_user'))){
  $this->db->where('task_maneger.assign_by', $this->session->userdata('task_assign_user'));
 }


   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('task_maneger.task_name', $_POST["search"]["value"]);
    $this->db->or_like('task_maneger.priority', $_POST["search"]["value"]);
    $this->db->or_like('task_maneger.tools', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('assigned.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
	   return $this->db->count_all_results();
  }


  public function get_task($condition){
    $this->db->select('task_maneger.*,sales.task_status as sales_task_status,users.name as assigin_to_name,assigned.name as assigin_by_name');
    $this->db->from('task_maneger');
    $this->db->join('sales','sales.id = task_maneger.saleID','left');
    $this->db->join('users','users.id = task_maneger.assign_to','left');
    $this->db->join('users as assigned','assigned.id = task_maneger.assign_by','left');
    $this->db->where($condition);
    $this->db->order_by('task_maneger.id','desc');
	 return $this->db->get()->row();
    //echo $this->db->last_query();
  }

  public function get_tasks($condition){
    $permission = permission('task');
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('task_maneger.*,sales.task_status as sales_task_status,users.name as assigin_to_name,assigned.name as assigin_by_name');
    $this->db->from('task_maneger');
    $this->db->join('sales','sales.id = task_maneger.saleID','left');
    $this->db->join('users','users.id = task_maneger.assign_to','left');
    $this->db->join('users as assigned','assigned.id = task_maneger.assign_by','left');
    $this->db->where($condition);
    if($permission[7]!='Like Admin'){
      if(!empty($usersID)){
        $this->db->where_in('task_maneger.assign_by',$usersID );
      }
    }
    $this->db->order_by('task_maneger.id','desc');
	  return $this->db->get()->result();
  }

  public function store_task($data){
	 $this->db->insert('task_maneger',$data);
   return $this->db->insert_id();
  }


  public function update_task($data,$condition){
	$this->db->where($condition);
	return $this->db->update('task_maneger',$data);
  }


  public function store_task_history($data){
   return $this->db->insert('task_maneger_history',$data);
     
  }


   public function delete_task($condition){
    $this->db->where($condition);
    return $this->db->delete('task_maneger');
   }

   public function delete_task_history($condition){
    $this->db->where($condition);
    return $this->db->delete('task_maneger_history');
   }

   public function store_task_report($data){
    return $this->db->insert('task_report',$data);
   }


   public function get_task_reports($condition){

    $this->db->where($condition);
    $query = $this->db->get('task_maneger')->row_array();
    $users = explode(',',$query['assign_to']);
  

    foreach($users as $row){
      $this->db->select('task_report.*');
      $this->db->from('task_report');
      $this->db->where(array('task_report.taskID'=>$query['id'],'task_report.userID'=>$row));
      $query[$row]['object'] = $this->db->get()->result_array();
    }
    return $query;

   }

   public function get_task_report($condition){
    $this->db->select('task_report.*');
    $this->db->from('task_report');
    $this->db->where($condition);
    return $this->db->get()->result_array();
   }

  
}