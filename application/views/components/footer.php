<script src="<?=base_url('public/admin/assets/vendor/libs/jquery/jquery.js')?>"></script>
    <script src="<?=base_url('public/admin/assets/vendor/libs/popper/popper.js')?>"></script>
    <script src="<?=base_url('public/admin/assets/vendor/js/bootstrap.js')?>"></script>
    <script src="<?=base_url('public/admin/assets/vendor/libs/node-waves/node-waves.js')?>"></script>
    <script src="<?=base_url('public/admin/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js')?>"></script>
    <script src="<?=base_url('public/admin/assets/vendor/libs/hammer/hammer.js')?>"></script>
    <script src="<?=base_url('public/admin/assets/vendor/libs/i18n/i18n.js')?>"></script>
    <script src="<?=base_url('public/admin/assets/vendor/libs/typeahead-js/typeahead.js')?>"></script>
    <script src="<?=base_url('public/admin/assets/vendor/js/menu.js')?>"></script>

    <!-- endbuild -->

    <!-- Vendors JS -->
    <script src="<?=base_url('public/admin/assets/vendor/libs/@form-validation/popular.js')?>"></script>
    <script src="<?=base_url('public/admin/assets/vendor/libs/@form-validation/bootstrap5.js')?>"></script>
    <script src="<?=base_url('public/admin/assets/vendor/libs/@form-validation/auto-focus.js')?>"></script>

    <!-- Main JS -->
    <script src="<?=base_url('public/admin/assets/js/main.js')?>"></script>

    <!-- Page JS -->
    <script src="<?=base_url('public/admin/assets/js/pages-auth.js')?>"></script>

    <script>
       $(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});

    </script>

<script>
$(document).ready(function() {
  $('.js-example-basic-single').select2();
});
</script>
<script>

function get_city(stateID,userID=NULL) {

  $.ajax({
    url: '<?=base_url('Ajax_controller/get_city')?>',
    type: 'POST',
    data: {
      stateID,
      userID
    },
    success: function(data) {
      $('.city').html(data);
    }
  });
}
</script>
  </body>
</html>
