<!doctype html>
<html
  lang="en"
  class="light-style layout-wide customizer-hide"
  dir="ltr"
  data-theme="theme-default"
  data-assets-path="<?=base_url('assets/')?>"
  data-template="vertical-menu-template">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

    <title><?=$page_title?></title>

    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="<?=base_url('public/admin/assets/img/favicon/favicon.ico')?>" />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&ampdisplay=swap" rel="stylesheet" />

    <!-- Icons -->
    <link rel="stylesheet" href="<?=base_url('public/admin/assets/vendor/fonts/fontawesome.css')?>" />
    <link rel="stylesheet" href="<?=base_url('public/admin/assets/vendor/fonts/tabler-icons.css')?>" />
    <link rel="stylesheet" href="<?=base_url('public/admin/assets/vendor/fonts/flag-icons.css')?>" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="<?=base_url('public/admin/assets/vendor/css/rtl/core.css')?>" class="template-customizer-core-css" />
    <link rel="stylesheet" href="<?=base_url('public/admin/assets/vendor/css/rtl/theme-default.css')?>" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="<?=base_url('public/admin/assets/css/demo.css')?>" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="<?=base_url('public/admin/assets/vendor/libs/node-waves/node-waves.css')?>" />
    <link rel="stylesheet" href="<?=base_url('public/admin/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css')?>" />
    <link rel="stylesheet" href="<?=base_url('public/admin/assets/vendor/libs/typeahead-js/typeahead.css')?>" />
    <!-- Vendor -->
    <link rel="stylesheet" href="<?=base_url('public/admin/assets/vendor/libs/@form-validation/form-validation.css')?>" />

    <!-- Page CSS -->
    <!-- Page -->
    <link rel="stylesheet" href="<?=base_url('public/admin/assets/vendor/css/pages/page-auth.css')?>" />

    <!-- Helpers -->
    <script src="<?=base_url('public/admin/assets/vendor/js/helpers.js')?>"></script>

    <script src="<?=base_url('public/admin/assets/vendor/js/template-customizer.js')?>"></script>

    <script src="<?=base_url('public/admin/assets/js/config.js')?>"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link   rel="stylesheet" href="<?=base_url('public/plugin/toast.css')?>">
    <script src="<?=base_url('public/plugin/toast.js')?>"></script>
    <script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.3/moment-with-locales.min.js"
    integrity="sha512-vFABRuf5oGUaztndx4KoAEUVQnOvAIFs59y4tO0DILGWhQiFnFHiR+ZJfxLDyJlXgeut9Z07Svuvm+1Jv89w5g=="
    crossorigin="anonymous"
    referrerpolicy="no-referrer"></script>
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <!-- include summernote css-->
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" />
 
  <!-- include summernote js-->
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
  </head>
