<body>
  <style>

  </style>
  <div class="main-wrapper">
    <div class="header">
      <div class="header-left">
        <a href="<?=base_url('dashboard')?>" class="logo">
          <img src="<?=base_url($siteinfo->site_logo)?>" alt="Logo">
        </a>
        <a href="<?=base_url('dashboard')?>" class="logo logo-small">
          <img src="<?=base_url($siteinfo->site_logo)?>" alt="Logo" width="30" height="30">
        </a>
      </div>
      <a href="javascript:void(0);" id="toggle_btn">
        <i class="fe fe-text-align-left"></i>
      </a>
      <!-- <div class="top-nav-search">
          <form>
            <input type="text" class="form-control" placeholder="Search here">
            <button class="btn" type="submit"><i class="fa fa-search"></i></button>
          </form>
        </div> -->
      <a class="mobile_btn" id="mobile_btn">
        <i class="fa fa-bars"></i>
      </a>
      <ul class="nav user-menu">
        <?php 
          $role_other_permission = json_decode($role->other_permission);
        if($role_other_permission->sale_heading_countion){
        ?>
       
        <li class="nav-item dropdown noti-dropdown">
          <a href="<?=base_url('followup-status/').base64_encode('Re Enquiry')?>" class="dropdown-toggle nav-link ">
            <span class="btn btn-outline-info font-weight-bold"> Re Enquiry<span class="badge badge-pill"><?=count($total_re_enquiry)?></span>
          </a>
        </li>
        <li class="nav-item dropdown noti-dropdown">
          <a href="<?=base_url('followup-status/').base64_encode('New Enquiry')?>" class="dropdown-toggle nav-link ">
            <span class="btn btn-outline-primary"> New Enquiry<span class="badge badge-pill"><?=count($total_new_enquiry)?></span>
          </a>
        </li>
        <li class="nav-item dropdown noti-dropdown">
          <a href="<?=base_url('followup-status/').base64_encode('Today FollowUp')?>" class="dropdown-toggle nav-link ">
            <span class="btn btn-outline-success"> Today's FollowUp <span class="badge badge-pill"><?=count($total_todays_followup)?></span>
          </a>
        </li>
        <li class="nav-item dropdown noti-dropdown">
          <a href="<?=base_url('followup-status/').base64_encode('Hot Lead')?>" class="dropdown-toggle nav-link ">
            <span class="btn btn-outline-danger"> Hot Lead<span class="badge badge-pill blink"><?=count($total_hot_lead)?></span>
          </a>
        </li>
        <li class="nav-item dropdown noti-dropdown">
          <a href="<?=base_url('followup-status/').base64_encode('Warm Lead')?>" class="dropdown-toggle nav-link " >
            <span class="btn btn-outline-warning  "> Warm Lead<span class="badge badge-pill"><?=count($total_warm_lead)?></span>
          </a>    
        </li>
       <?php } ?>
        <li class="nav-item dropdown has-arrow">
          <a href="#" class="dropdown-toggle nav-link" data-bs-toggle="dropdown">
            <span class="user-img lo_go"><img class="rounded-circle"
                src="<?=base_url($this->session->userdata('profile_pic'))?>" width=""
                alt="<?=$this->session->userdata('name')?>"></span>
          </a>
          <div class="dropdown-menu">
            <div class="user-header">
              <div class="avatar avatar-sm">
                <img src="<?=base_url($this->session->userdata('profile_pic'))?>" alt="User Image"
                  class="avatar-img rounded-circle">
              </div>
              <div class="user-text">
                <h6><?=$this->session->userdata('name')?></h6>
                <p class="text-muted mb-0"><?=$this->session->userdata('role')?></p>
              </div>
            </div>
            <a class="dropdown-item" href="javascript:void(0)"><?=$this->session->userdata('email')?></a>
            <a class="dropdown-item" href="<?=base_url('profile')?>">My Profile</a>
            <a class="dropdown-item" href="<?=base_url('Authantication/logout')?>">Logout</a>
          </div>
        </li>
      </ul>
    </div>