<div class="page-wrapper">
  <div class="content container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-sm-6">
          <h3 class="page-title">Settings</h3>
          <ul class="breadcrumb">
            <li class=""><a href="#">Dashboard</a></li>/
            <li class=""><a href="#">Settings</a></li>/
            <li class="breadcrumb-item active"><?=$page_title?></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="settings-menu-links">
      <ul class="nav nav-tabs menu-tabs">
        <li class="nav-item <?=($page_title=='Profile')?'active':'';?>">
          <a class="nav-link" href="<?=base_url('profile');?>">Profile</a>
        </li>
        <li class="nav-item <?=($page_title=='Change Password')?'active':'';?>">
          <a class="nav-link" href="<?=base_url('change-password');?>">Change Password</a>
        </li>
      </ul>
    </div>