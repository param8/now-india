<div class="page-wrapper">
  <div class="content container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-sm-12">
          <h3 class="page-title text-success">Welcome <?=$this->session->userdata('name')?>!</h3>
          <ul class="breadcrumb">
            <li class="breadcrumb-item active">Dashboard</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-4 col-lg-4 col-sm-6 col-12">
        <a href="<?=base_url('sales')?>">
          <div class="card">
            <div class="card-body">
              <div class="dash-widget-header">
                <span class="dash-widget-icon text-primary border-primary">
                  <i class="fa fa-shopping-cart"></i>
                </span>
                <div class="dash-count">
                  <h5><?=!empty($total_sale) ? $total_sale : 0?></h5>
                </div>
              </div>
              <div class="dash-widget-info">
                <h6 class="text-muted">Sale <span class="text-danger">(<?=date('M-Y')?>)</span></h6>
                <div class="progress progress-md">
                  <div class="progress-bar progress-bar-striped" role="progressbar"
                    style="width: <?=$traget_percentage?>%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">
                  </div>
                </div>
                <!-- <div class="progress progress-sm">
                <div class="progress-bar bg-primary w-<?//=$traget_percentage?>"></div>
              </div> -->
              </div>
            </div>
          </div>
        </a>
      </div>
      <div class="col-xl-4 col-lg-4 col-sm-6 col-12">
        <a href="<?=base_url('sales')?>">
          <div class="card">
            <div class="card-body">
              <div class="dash-widget-header">
                <span class="dash-widget-icon text-success">
                  <i class="fe fe-credit-card"></i>
                </span>
                <div class="dash-count">
                  <h5><i class="fa fa-inr" aria-hidden="true"></i>
                    <?=!empty($sale_amount) ? $sale_amount : 0?></h5>
                </div>
              </div>
              <div class="dash-widget-info">
                <h6 class="text-muted">Sale Amount <span class="text-danger">(<?=date('M-Y')?>)</span></h6>

                <div class="progress progress-md">
                  <div class="progress-bar progress-bar-striped bg-success" role="progressbar"
                    style="width: <?=$traget_percentage?>%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">
                  </div>
                </div>
                <!-- <div class="progress progress-sm">
                <div class="progress-bar bg-success w-<?//=$traget_percentage?>"></div>
              </div> -->
              </div>
            </div>
          </div>
        </a>
      </div>
      <div class="col-xl-4 col-lg-4 col-sm-6 col-12">
        <a href="<?=base_url('payment-history')?>">
          <div class="card">
            <div class="card-body">
              <div class="dash-widget-header">
                <span class="dash-widget-icon text-success border-success">
                  <i class="fa fa-money"></i>
                </span>
                <div class="dash-count">
                  <h5><i class="fa fa-inr" aria-hidden="true"></i>
                    <?=!empty($receivedAmount) ? $receivedAmount : 0?>
                  </h5>
                </div>
              </div>
              <div class="dash-widget-info">
                <h6 class="text-muted">Total Amount Recieved <span class="text-danger">(<?=date('M-Y')?>)</span></h6>
                <div class="progress progress-md">
                  <div class="progress-bar progress-bar-striped bg-success" role="progressbar"
                    style="width: <?=$pending_amount_percentage?>%" aria-valuenow="10" aria-valuemin="0"
                    aria-valuemax="100">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
      <div class="col-xl-6 col-lg-6 col-sm-6 col-12">
        <a href="<?=base_url('sales')?>">
          <div class="card">
            <div class="card-body">
              <div class="dash-widget-header">
                <span class="dash-widget-icon text-danger border-danger">
                  <i class="fa fa-sort-amount-asc"></i>
                </span>
                <div class="dash-count">
                  <h5><i class="fa fa-inr" aria-hidden="true"></i>
              <?php 
                 $cmonth = date('m');
                    if($cmonth >= 4)
                    {
                     $month = date('Y-m');
                    //$year = date('Y', strtotime('+1 year'));
                     $year = date('Y');
                    }
                    else
                    {
                      $month = date('Y-m');
                   // $year = date('Y', strtotime('-1 year'));
                      $year = date('Y')-1;
                     }
                     $max_year = $year+1;
                 ?>
                    <?=!empty($total_pending_amount->totalPending) ? round($total_pending_amount->totalPending,2) : 0?>
                  </h5>
                </div>
              </div>
              <div class="dash-widget-info">
                <h6 class="text-muted">Total Pending Amount <span
                    class="text-danger">(<?= $year.'-'.$max_year;?>)</span></h6>
                <div class="progress progress-md">
                  <div class="progress-bar progress-bar-striped bg-danger" role="progressbar"
                    style="width: <?=$pending_amount_percentage?>%" aria-valuenow="10" aria-valuemin="0"
                    aria-valuemax="100">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>

      <div class="col-xl-6 col-lg-6 col-sm-6 col-12">
        <a href="<?=base_url('sales')?>">
          <div class="card">
            <div class="card-body">
              <div class="dash-widget-header">
                <span class="dash-widget-icon text-warning border-warning">
                  <i class="fa fa-sort-amount-asc"></i>
                </span>
                <div class="dash-count">
                  <h5><i class="fa fa-inr" aria-hidden="true"></i>
                    <?=!empty($pending_amount->totalPending) ? $pending_amount->totalPending : 0?>
                  </h5>
                </div>
              </div>
              <div class="dash-widget-info">
                <h6 class="text-muted">Monthly Pending Amount <span class="text-danger">(<?=date('M-Y')?>)</span></h6>
                <div class="progress progress-md">
                  <div class="progress-bar progress-bar-striped bg-warning" role="progressbar"
                    style="width: <?=$pending_amount_percentage?>%" aria-valuenow="10" aria-valuemin="0"
                    aria-valuemax="100">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
      <!-- <div class="col-xl-4 col-lg-4 col-sm-6 col-12">
        <a href="<?=base_url('payment-history')?>">
          <div class="card">
            <div class="card-body">
              <div class="dash-widget-header">
                <span class="dash-widget-icon text-danger border-danger">
                  <i class="fe fe-credit-card"></i>
                </span>
                <div class="dash-count">
                  <h5><i class="fa fa-inr" aria-hidden="true"></i>
                    <?//=!empty($receivedAmount) ? $receivedAmount : 0?>
                  </h5>
                </div>
              </div>
              <div class="dash-widget-info">
                <h6 class="text-muted">Total Pending Amount</h6>
                <div class="progress progress-md">
                  <div class="progress-bar progress-bar-striped bg-danger" role="progressbar"
                    style="width: <?//=$pending_amount_percentage?>%" aria-valuenow="10" aria-valuemin="0"
                    aria-valuemax="100">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div> -->
      <!-- <div class="col-xl-3 col-sm-6 col-12">
        <div class="card">
          <div class="card-body">
            <div class="dash-widget-header">
              <span class="dash-widget-icon text-warning border-warning">
                <i class="fe fe-folder"></i>
              </span>
              <div class="dash-count">
                <h3>$62523</h3>
              </div>
            </div>
            <div class="dash-widget-info">
              <h6 class="text-muted">Revenue</h6>
              <div class="progress progress-sm">
                <div class="progress-bar bg-warning w-50"></div>
              </div>
            </div>
          </div>
        </div>
      </div> -->
    </div>



    <div class="row">
      <div class="col-md-12 col-lg-6">

        <div class="card card-chart">
          <div class="card-header">
            <h5 class="card-title">This Month Sale Amount Activity</h5>
          </div>
          <div class="card-body">
            <div id="monthly_sale_amount"></div>
          </div>
        </div>

      </div>
      <div class="col-md-12 col-lg-6">

        <div class="card card-chart">
          <div class="card-header">
            <h5 class="card-title">This Month Recieved Amount Activity</h5>
          </div>
          <div class="card-body">
            <div id="monthly_recieve_amount"></div>
          </div>
        </div>

      </div>
    </div>
    <?php if($this->session->userdata('user_type')!=2){?>
    <div class="row">
      <div class="col-md-12 col-lg-6">

        <div class="card card-chart">
          <div class="card-header">
            <h5 class="card-title">Your Monthly Performance</h5>
          </div>
          <div class="card-body">
            <div id="user_monthly_performance"></div>
          </div>
        </div>

      </div>
      <div class="col-md-12 col-lg-6">

        <div class="card card-chart">
          <div class="card-header">
            <h5 class="card-title">Your Curent Financial Year Performance</h5>
          </div>
          <div class="card-body">
            <div id="year_sale_performance"></div>
          </div>
        </div>

      </div>
    </div>
    <?php } 
if(count($user_permission) > 1 OR $this->session->userdata('user_type')==2){ ?>

    <div class="row">
      <div class="col-md-12 col-lg-12">

        <div class="card card-chart">
          <div class="card-header">
            <h5 class="card-title">Monthly Flow Chart</h5>
          </div>
          <div class="card-body">
            <div id="monthly_flow_chart" style="height:400px;"></div>
          </div>
        </div>

      </div>

    </div>

  </div>


  <div class="row">
    <div class="col-md-12 col-lg-6">

      <div class="card card-chart">
        <div class="card-header">
          <h5 class="card-title">User wise company performance</h5>
        </div>
        <div class="card-body">
          <div id="user_wise_monthly_performance"></div>
        </div>
      </div>

    </div>
    <div class="col-md-12 col-lg-6">

      <div class="card card-chart">
        <div class="card-header">
          <h5 class="card-title">Curent Financial Year Performance</h5>
        </div>
        <div class="card-body">
          <div id="user_wise_yearly_performance"></div>
        </div>
      </div>

    </div>
  </div>
  <?php } ?>

</div>



<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script>
google.charts.load('current', {
  'packages': ['corechart']
});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

  var data = google.visualization.arrayToDataTable([
    ['Task', 'Working'],
    <?= $this_month_sale_amount;?>
  ]);

  var options = {
    colors: ['#C9190B', '#7CC674'],
    title: 'This Month Sale Amount Activity',
    pieHole: 0.5,
    legend: {
      position: 'right',
      alignment: 'center',
    },
    height: '300',
  };

  var chart = new google.visualization.PieChart(document.getElementById('monthly_sale_amount'));

  chart.draw(data, options);
}
</script>

<script>
google.charts.load('current', {
  'packages': ['corechart']
});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

  var data = google.visualization.arrayToDataTable([
    ['Task', 'Working'],
    //['Total Target [₹-100000]',100000],['Recieved Amount [₹-10000]', 10000],
    <?=$this_month_recieve_amount;?>
  ]);

  var options = {
    colors: ['#8481DD', '#EC7A08'],
    title: 'This Month Recieved Amount Activity',
    pieHole: 0.5,
    legend: {
      position: 'right',
      alignment: 'center',
    },
    height: '300',
  };

  var chart = new google.visualization.PieChart(document.getElementById('monthly_recieve_amount'));

  chart.draw(data, options);
}
</script>

<script>
google.charts.load('current', {
  'packages': ['bar']
});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
  var data = google.visualization.arrayToDataTable([
    ['Month', 'Target Amount', 'Total Sale Amount', 'Payment Recieved'],

    <?php foreach($user_monthly_performance as $monthly_performance){
                  echo $monthly_performance;
               }
             ?>
  ]);

  var options = {
    chart: {
      title: 'Your Monthly Performance',
      subtitle: 'Target , Target Achieved',
    },
    colors: ['#B2B0EA', '#F4B678', '#BDE2B9'],
    height: 400,
  };

  var chart = new google.charts.Bar(document.getElementById('user_monthly_performance'));

  chart.draw(data, google.charts.Bar.convertOptions(options));
}
</script>

<script>
google.charts.load('current', {
  'packages': ['bar']
});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
  var data = google.visualization.arrayToDataTable([
    ['Year', 'Target Amount', 'Total Sale Amount', 'Payment Recieved'],

    <?php foreach($year_sale_performance as $sale_yearly){
                  echo $sale_yearly;
               }
             ?>
  ]);

  var options = {
    chart: {
      title: 'Your Year Performance',
      subtitle: 'Target , Target Achieved',
    },
    colors: ['#003737', '#009596', '#73C5C5'],
    height: 400,
  };

  var chart = new google.charts.Bar(document.getElementById('year_sale_performance'));

  chart.draw(data, google.charts.Bar.convertOptions(options));
}
</script>

<script>
// chart for full financial year 
google.charts.load('current', {
  'packages': ['bar']
});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
  var data = google.visualization.arrayToDataTable([
    ['Month', 'Target Amount', 'Total Order value', 'Payment Recieved'],

    <?php foreach($user_yearly_sale_data as $user_yearly_data){
                  echo $user_yearly_data;
               }
             ?>
  ]);

  var options = {
    chart: {
      title: 'Your Curent Financial Year Performance',
      subtitle: 'Target , Target Achieved',
    },
    colors: ['#DB4437', '#0F9D58', '#4285F4'],
    height: 400
  };

  var chart = new google.charts.Bar(document.getElementById('user_salechart_yearly'));

  chart.draw(data, google.charts.Bar.convertOptions(options));
}
</script>



<script type="text/javascript">
google.charts.load('current', {
  'packages': ['bar']
});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
  var data = google.visualization.arrayToDataTable([
    ['User', 'Target Amount', 'Total Order value', 'Payment Recieved', {
      role: 'link'
    }],
    //['2014', 10000, 4000, 200],
    <?php foreach($user_wise_monthly_performance as $sales_data){
                  echo $sales_data;
                }
             ?>
  ]);

  var options = {
    chart: {
      title: 'User Wise Company Performance',
      subtitle: 'Target, Achived, and Payment Recieved',
    },
    colors: ['#8BC1F7', '#73C5C5', '#4CB140'],
    height: 400,
  };

  var chart = new google.charts.Bar(document.getElementById('user_wise_monthly_performance'));
  google.visualization.events.addListener(chart, 'select', function(e) {
    var selection = chart.getSelection();
    if (selection.length) {
      var row = selection[0].row;
      let link = data.getValue(row, 4);
      window.open(link, '_blank');
    }
  });

  chart.draw(data, google.charts.Bar.convertOptions(options));
}
</script>

<script type="text/javascript">
google.charts.load('current', {
  'packages': ['bar']
});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
  var data = google.visualization.arrayToDataTable([
    ['User', 'Target Amount', 'Total Order value', 'Payment Recieved', {
      role: 'link'
    }],
    //['2014', 10000, 4000, 200],
    <?php foreach($user_wise_yearly_performance as $sales_year){
                  echo $sales_year;
                }
             ?>
  ]);

  var options = {
    chart: {
      title: 'User Wise Company Performance',
      subtitle: 'Target, Achived, and Payment Recieved',
    },
    colors: ['#06C', '#23511E', '#B8BBBE'],
    height: 400,
  };

  var chart = new google.charts.Bar(document.getElementById('user_wise_yearly_performance'));
  google.visualization.events.addListener(chart, 'select', function(e) {
    var selection = chart.getSelection();
    if (selection.length) {
      var row = selection[0].row;
      let link = data.getValue(row, 4);
      window.open(link, '_blank');
    }
  });

  chart.draw(data, google.charts.Bar.convertOptions(options));
}
</script>

<script src="<?php echo base_url('public/admin/assets/js/anychart.js')?>"></script>
<script>
anychart.onDocumentReady(function() {

  // add data
  var data = [

    <?php foreach($monthly_flow_chart as $report){
      echo $report;
    }
      ?>

  ];

  // create a data set
  var dataSet = anychart.data.set(data);



  // map the data for all series
  var firstSeriesData = dataSet.mapAs({
    x: 0,
    value: 1
  });
  var secondSeriesData = dataSet.mapAs({
    x: 0,
    value: 2
  });
  var thirdSeriesData = dataSet.mapAs({
    x: 0,
    value: 3
  });

  // create a line chart
  var chart = anychart.line();
  // create the series and name them
  var firstSeries = chart.splineArea(firstSeriesData);
  firstSeries.name("Target");
  var secondSeries = chart.splineArea(secondSeriesData);
  secondSeries.name("Total Sales");
  var thirdSeries = chart.splineArea(thirdSeriesData);
  thirdSeries.name("Payment Recieved");



  thirdSeries.normal().fill();
  thirdSeries.normal().stroke();

  // add a legend
  chart.legend().enabled(true);

  // add a title
  chart.title("Monthly Flow Chart");

  // specify where to display the chart
  chart.container("monthly_flow_chart");

  // draw the resulting chart
  chart.draw();

});
</script>