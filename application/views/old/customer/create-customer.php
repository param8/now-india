<style>
.search_phone {
  display: block;
  position: absolute;
  background-color: #f7f7f7;
  padding: 12px 6px;
  border: 1px solid #d2d1d1;
  box-shadow: 0px 7px 0px 0px rgb(0 0 0 / 13%);
  max-height: 200px;
  border-radius: 7px;
  min-height: 180px;
  overflow-y: auto;
  border-bottom-left-radius: 5px;
  left: 24px;
  border-bottom-right-radius: 5px;
  z-index: 3;
  margin-top: 5px;
}

.search_phone p {
  background: white;
  padding: 5px;
  border-radius: 3px;
  margin-bottom: 5px;
}

.search_phone p a {
  color: dimgrey;
}
</style>
<div class="page-wrapper">
  <div class="content container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col">
          <h3 class="page-title">Create <?=$page_title?></h3>
          <ul class="breadcrumb">
            <li class=""><a href="<?=base_url('dashboard')?>">Dashboard</a></li>/
            <li class="breadcrumb-item active">Create <?=$page_title?></li>
          </ul>
        </div>
      </div>
    </div>
    <form action="<?=base_url('Customer/store')?>" id="customerAddForm" method="post">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-xl-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">Enter Customer Detail</h4>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Phone No. <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="phone" id="phone" minlength="10" maxlength="10"
                      oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
                      onkeyup="get_customer(this.value)">
                    <div class="row"><span class="search_phone col-lg-3 col-md-4 col-sm-6 col-12" id="customer_phone"
                        style="display:none"></span></div>
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Full Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="name" id="name">
                  </div>
                </div>

                <div class="col-md-4 col-lg-4 col-xl-4">
                  <div class="form-group">
                    <label>Company Name </label>
                    <input type="text" class="form-control" name="company_name" id="company_name">
                  </div>
                </div>

                <div class="col-md-4 col-lg-4 col-xl-4">
                  <div class="form-group">
                    <label>Email </label>
                    <input type="email" class="form-control" name="email" id="email">
                  </div>
                </div>


                <div class="col-md-4 col-lg-4 col-xl-4">
                  <div class="form-group">
                    <label>Alternate Contact No </label>
                    <input type="text" class="form-control" name="alternate_no" id="alternate_no" minlength="10"
                      maxlength="10"
                      oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                  </div>
                </div>

                <div class="col-md-12 col-lg-12 col-xl-12">
                  <div class="form-group">
                    <label>Address </label>
                    <textarea class="form-control" name="address" id="address"></textarea>
                  </div>
                </div>

                <!-- <div class="col-md-6 col-lg-6 col-xl-6" id="refresh_state">
                  <div class="form-group">
                    <label>State </label>
                    <select class="form-control js-example-basic-single" name="state" id="state"
                      onchange="get_city(this.value,null)">
                      <option value="">Select State</option>
                
                    </select>
                  </div>
                </div> -->
                <!-- <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>City </label>
                    <select class="form-control city js-example-basic-single" name="city" id="city">
                      <option value="">Select City</option>
                    </select>
                  </div>
                </div> -->
                <!-- <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Pincode </label>
                    <input type="text" class="form-control" name="pincode" id="pincode" minlength="6" maxlength="6"
                      oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                  </div>
                </div> -->


                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Lead Type </label>
                    <input type="text" class="form-control" name="lead_type" id="lead_type">
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Lead source </label>
                    <input type="text" class="form-control" name="lead_sourse" id="lead_sourse">
                  </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Lead Status <span class="text-danger">*</span></label>
                    <select class="form-control " name="lead_status" id="lead_status" readonly>

                      <?php foreach($lead_status as $lead_statu){?>
                      <option value="<?=$lead_statu->id?>" <?=$lead_statu->id==1 ? 'selected' : ''?>>
                        <?=$lead_statu->name?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>User <span class="text-danger">*</span></label>
                    <select class="form-control js-example-basic-single" name="userID" id="userID">
                      <option value="">Select User</option>
                      <?php foreach($users as $user){?>
                      <option value="<?=$user->id?>" <?=$user->id==$this->session->userdata('id') ? 'selected' : ''?>>
                        <?=$user->name .'('.$user->roleName.')'?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-12 col-lg-12 col-xl-12">
                  <div class="form-group">
                    <label>Requirement </label>
                    <textarea class="form-control" name="requirement" id="requirement"></textarea>
                  </div>
                </div>

                <div class="text-center">
                  <span class="btn  btn-outline-success mr-5" onclick="checkCheckBox()"
                    title="Sale please check checkbox"><input type="checkbox" name="is_sale" id="is_sale" class=""
                      value="1"> <b class=" ml-5 mr-5">Sale</b></span>
                  <button type="submit" class="btn btn-outline-primary">Save</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
<script>
$("form#customerAddForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 100000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        if (data.sale_page == 0) {
          ajaxSendMail(data.id);
          setTimeout(function() {
            location.href = "<?=base_url('customers')?>";
          }, 2000)
        } else {
          setTimeout(function() {

            location.href = "<?=base_url('create-sale/')?>" + data.sale_page;

          }, 1000)

        }

      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

function ajaxSendMail(id) {
  $.ajax({
    url: "<?=base_url('Send_mail/customerSendMail')?>",
    type: 'POST',
    data: {
      id
    },
    success: function(data) {
      toastNotif({
        text: 'Mail sent successfully',
        color: '#5bc83f',
        timeout: 10000,
        icon: 'valid'
      });
      setTimeout(function() {
        location.href = "<?=base_url('customers')?>";
      }, 1000)

    },

  });
}

function get_customer(phone) {
  $.ajax({
    url: '<?=base_url('Ajax_controller/customer_detail')?>',
    type: 'POST',
    data: {
      phone
    },
    success: function(data) {
      console.log(data);
      if (data != "") {
        $('#customer_phone').show();
        $('#customer_phone').html(data);
      } else {
        $('#customer_phone').hide();
        $('#name').val('');
        $('#name').attr('readonly', false);
        $('#company_name').val('');
        $('#company_name').attr('readonly', false);
        $('#alternate_no').val('');
        $('#alternate_no').attr('readonly', false);
        $('#email').val('');
        $('#email').attr('readonly', false);
        $('#address').val('');
        $('#address').attr('readonly', false);
        $('#pincode').val('');
        $('#pincode').attr('readonly', false);
        $('#refresh_state').load(' #refresh_state');
        //$('#city').val('');
      }
    }
  });
}

function get_customer_data(contact) {
  //Assigning value to "search" div in "search.php" file.
  $('#phone').val(contact);
  //$('#coursesReset').show();
  //Hiding "display" div in "search.php" file.
  $('#customer_phone').hide();
  $.ajax({
    url: '<?=base_url('Ajax_controller/get_customer_field')?>',
    type: 'POST',
    data: {
      contact
    },
    dataType: 'json',
    success: function(data) {
      if (data != "") {
        $('#name').val(data.name);
        $('#name').attr('readonly', true);
        $('#company_name').val(data.company_name);
        $('#company_name').attr('readonly', true);
        $('#alternate_no').val(data.alternate_no);
        $('#alternate_no').attr('readonly', true);
        $('#email').val(data.email);
        $('#email').attr('readonly', true);
        $('#address').val(data.address);
        $('#address').attr('readonly', true);
        $('#pincode').val(data.pincode);
        $('#pincode').attr('readonly', true);
        $('#state').html('<option value="' + data.state + '">' + data.stateName + '</option>');
        $('#city').html('<option value="' + data.city + '">' + data.cityName + '</option>');
      } else {
        $('#name').val('');
        $('#name').attr('readonly', false);
        $('#company_name').val('');
        $('#company_name').attr('readonly', false);
        $('#alternate_no').val('');
        $('#alternate_no').attr('readonly', false);
        $('#email').val('');
        $('#email').attr('readonly', false);
        $('#address').val('');
        $('#address').attr('readonly', false);
        $('#pincode').val('');
        $('#pincode').attr('readonly', false);

      }

    }
  });
}


function checkCheckBox() {
  if ($("#is_sale").is(":checked")) {
    $('#is_sale').prop('checked', false);
  } else {
    $('#is_sale').prop('checked', true);
  }

}
</script>