<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">

      <div class="page-header">
        <div class="row">
          <div class="col-sm-6">
            <h3 class="page-title"><?=$page_title?></h3>
            <ul class="breadcrumb">
              <li><a href="<?=base_url('dashboard')?>">Dashboard/</a></li>
              <li class="breadcrumb-item"><a href="javascript:(0);"><?=$page_title?></a></li>
            </ul>
          </div>
          <?php if($this->session->userdata('user_type')!=1 && $permission[4]=='Upload'){?>
          <div class="col-sm-6">
            <div class="float-right">
              <a type="button" class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="assiginEnquiry()"
                style="float: right">Assigin <?=$page_title?></a>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-body">
              <div class="text-center text-danger">
                <?//=$message?>
              </div>
              <?php if($this->session->userdata('user_type')!=1 && $permission[8]=='Filter'){?>
              <div class="row">
                <div class="col-sm-12">
                  <div class="card">
                    <div class="card-body">
                      <form method="post" id="filter-data" action="<?=base_url('Enquiry/setSessionEnquiry');?>">
                        <div class="row">



                          <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                            <div class="reset_btn">
                              <label>Months</label>
                              <?php $months = array('0'=>'All Months','01'=>'January','02'=>'February','03'=>'March','04'=>'April','05'=>'May','06'=>'June','07'=>'July','08'=>'August','09'=>'September','10'=>'October','11'=>'November','12'=>'December');?>
                              <select id="monthname_enquiry" class="form-control" name="monthname_enquiry"
                                onchange="this.form.submit();">
                                <?php foreach($months as $month_key=>$month){?>
                                <option value="<?=$month_key?>"
                                  <?=(!empty($this->session->userdata('monthname_enquiry')) AND $this->session->userdata('monthname_enquiry')==$month_key) ? 'selected' :  '' ?>>
                                  <?=$month?></option>
                                <?php } ?>
                              </select>
                              <span class="close_button" onclick="resetMonthName()" id="resetMonthName"
                                <?=empty($this->session->userdata('monthname_enquiry')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                            </div>
                          </div>

                          <?php //if($this->session->userdata('user_type')==2){?>
                          <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                            <div class="reset_btn">
                              <label>Sale Users</label>
                              <select id="assign_user_enquiry" class="form-control" name="assign_user_enquiry"
                                onchange="this.form.submit();">
                                <option value="">All Users</option>
                                <?php foreach($users as $user){?>
                                <option value="<?=$user->id?>"
                                  <?=$this->session->userdata('assign_user_enquiry')==$user->id ? 'selected':'' ;?>>
                                  <?=$user->name.'('.$user->roleName.')'?></option>
                                <?php } ?>
                              </select>
                              <span class="close_button" onclick="resetAssignUser()" id="resetAssignUser"
                                <?=empty($this->session->userdata('assign_user_enquiry')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                            </div>
                          </div>
                          <?php //} ?>

                          <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                            <div class="reset_btn">
                              <label>From Date</label>
                              <div class="input-group">
                                <input type="text" class="form-control" onfocus="(this.type='date')"
                                  placeholder="From Date" name="enquiry_from_date" id="enquiry_from_date"
                                  value="<?=!empty($this->session->userdata('enquiry_from_date')) ? $this->session->userdata('enquiry_from_date'):'';?>"
                                  onchange="this.form.submit();">
                              </div>
                              <span class="close_button" onclick="resetFromDate()" id="resetFromDate"
                                <?=empty($this->session->userdata('enquiry_from_date')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                            </div>
                          </div>
                          <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                            <div class="reset_btn">
                              <label>To Date</label>
                              <div class="input-group">
                                <input type="text" class="form-control" onfocus="(this.type='date')"
                                  placeholder="To Date" name="enquiry_to_date" id="enquiry_to_date"
                                  value="<?=!empty($this->session->userdata('enquiry_to_date')) ? $this->session->userdata('enquiry_to_date') :'';?>"
                                  onchange="this.form.submit();">
                              </div>
                              <span class="close_button" onclick="resetToDate()" id="resetToDate"
                                <?=empty($this->session->userdata('enquiry_to_date')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                            </div>
                          </div>


                        </div>
                        <input type="hidden" name="url" id= "url" value="<?=$url?>">
                      </form>
                      <hr>
                      <?php } if($this->session->userdata('user_type')==1){?>
                      <div class="row">
                        <div class="col-sm-6">
                          <select class="form-control" name="enquiryAdminID" id="enquiryAdminID"
                            onchange="getEnquiryAdminSession(this.value)">
                            <option value="">Select Admin</option>
                            <?php foreach($admins as $admin){?>
                            <option value="<?=$admin->id?>"
                              <?=$admin->id == $this->session->userdata('enquiryAdminID') ? 'selected' : ''?>>
                              <?=$admin->name?>
                            </option>
                            <?php } ?>
                          </select>

                        </div>

                      </div>
                      <hr>
                      <?php } ?>

                      <div class="table-responsive">
                        <table class=" table table-hover table-center mb-0" id="enquiryDataTable">
                          <thead>
                            <tr>
                              <th>S.no.</th>
                              <?php if($this->session->userdata('user_type')!=1){?>
                              <th>Action</th>
                              <?php } ?>
                              <th>Client Name</th>
                              <th>Client Phone No.</th>
                              <th>Assign Date</th>
                              <th>Lead Type</th>
                              <th>Lead Source</th>
                              <th>Lead Status</th>
                              <th>Assign To</th>
                              <th>Assign By</th>
                              <th>Enquery Date</th>
                            </tr>
                          </thead>
                          <tbody>

                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="assiginEnquiry" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
          aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-tasks text-warning"></i> Assigin
                  <?=$page_title?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="close_modal()">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form action="<?=base_url('Enquiry/assigin_enquiry')?>" method="POST" id="assiginEnquiryForm">
                <div class="modal-body">
                  <div class="form-group">
                    <label>Users <span class="text-danger">*</span></label>
                    <select class="form-control " name="userID" id="userID">
                      <option value="">Select User</option>
                      <?php foreach($users as $user){?>
                      <option value="<?=$user->id?>"><?=$user->name .'('.$user->roleName.')'?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="table-responsive" style="max-height:400px;">
                    <table class=" table table-hover table-center mb-0" id="assiginDataTable">
                      <thead>
                        <tr>
                          <th>S.no.</th>
                          <th><input type="checkbox" id="select-all"> Select All</th>
                          <th>Assign To</th>
                          <th>Client Name</th>
                          <th>Client Phone No.</th>
                          <th>Requirement</th>
                          <th>Date</th>
                        </tr>
                      </thead>
                      <tbody>


                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn  btn-outline-danger " onclick="close_modal()" data-dismiss="modal"
                    aria-label="Close">Close <i class="fa fa-close"></i></button>
                  <button type="submit" class="btn  btn-outline-success ">Submit </button>
                </div>
              </form>
            </div>
          </div>
        </div>


        <script>
        $(document).ready(function() {
          var dataTable = $('#enquiryDataTable').DataTable({
            "processing": true,
            "serverSide": true,
            buttons: [{
              extend: 'excelHtml5',
              text: 'Download Excel'
            }],
            "order": [],
            "ajax": {
              url: "<?=base_url('Enquiry/ajaxEnquiry/'.$uri)?>",
              type: "POST"
            },
            "columnDefs": [{
              "targets": [0],
              "orderable": false,
            }, ],
          });
        });

        $(document).ready(function() {
          var dataTable = $('#assiginDataTable').DataTable({
            "processing": true,
            "serverSide": true,
            buttons: [{
              extend: 'excelHtml5',
              text: 'Download Excel'
            }],
            "order": [],
            "ajax": {
              url: "<?=base_url('Enquiry/ajaxAssiginEnquiry/'.$uri)?>",
              type: "POST"
            },
            "aoColumnDefs": [{
                "bSortable": false,
                "aTargets": [0, 1, 2, 3, 4, 5]
              },
              {
                "bSearchable": false,
                "aTargets": [0, 1, 2, 3, 4, 5]
              }
            ],
            aLengthMenu: [
              [10, 25, 50, 100, 200, -1],
              [10, 25, 50, 100, 200, "All"]
            ],

          });
        });

        // $(document).ready(function() {
        //   //alert('dfgfgf');
        //   // $('#example').DataTable();
        //   // } );
        //   var dataTable = $('#assiginDataTable').DataTable({

        //   });
        // });



        function delete_user(id) {

          Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.value) {
              $.ajax({
                url: '<?=base_url('User/delete')?>',
                type: 'POST',
                data: {
                  id
                },
                dataType: 'json',
                success: function(data) {
                  if (data.status == 200) {
                    toastNotif({
                      text: data.message,
                      color: '#5bc83f',
                      timeout: 5000,
                      icon: 'valid'
                    });
                    setTimeout(function() {

                      location.href = "<?=base_url('users')?>"

                    }, 1000)


                  } else if (data.status == 302) {
                    toastNotif({
                      text: data.message,
                      color: '#da4848',
                      timeout: 5000,
                      icon: 'error'
                    });

                  }
                }
              })
            }
          })
        }

        function getEnquiryAdminSession(id) {
          $.ajax({
            url: "<?=base_url('Ajax_controller/getEnquiryAdminSession')?>",
            type: 'POST',
            data: {
              id
            },
            success: function(data) {
              location.reload();
            },

          });
        }

        function view_customer(id) {
          $.ajax({
            url: "<?=base_url('Customer/customerViewForm')?>",
            type: 'POST',
            data: {
              id
            },
            success: function(data) {
              $('#viewCustomer').modal('show');
              $('#view_data').html(data);
            },

          });
        }

        $('#select-all').click(function(event) {
          if (this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function() {
              //this.checked = true;  
              var cells = $('#assiginDataTable').dataTable().api().cells().nodes();
              $(cells).find('.chk_cls').prop('checked', this.checked);
            });
          } else {
            $(':checkbox').each(function() {
              this.checked = false;
            });
          }
        });

        function close_modal() {
          $('#assiginEnquiry').modal('hide');
        }

        function assiginEnquiry() {
          $('#assiginEnquiry').modal('show');
        }

        $("form#assiginEnquiryForm").submit(function(e) {
          $(':input[type="submit"]').prop('disabled', true);
          e.preventDefault();
          var formData = new FormData(this);
          $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {
              if (data.status == 200) {
                toastNotif({
                  text: data.message,
                  color: '#5bc83f',
                  timeout: 5000,
                  icon: 'valid'
                });
                $(':input[type="submit"]').prop('disabled', false);
                setTimeout(function() {

                  location.href = "<?=base_url('enquires')?>";

                }, 1000)

              } else if (data.status == 403) {
                toastNotif({
                  text: data.message,
                  color: '#da4848',
                  timeout: 5000,
                  icon: 'error'
                });

                $(':input[type="submit"]').prop('disabled', false);
              } else {
                toastNotif({
                  text: data.message,
                  color: '#da4848',
                  timeout: 5000,
                  icon: 'error'
                });
                $(':input[type="submit"]').prop('disabled', false);
              }
            },
            error: function() {}
          });
        });




        function resetFromDate() {
          $.ajax({
            url: '<?=base_url("Enquiry/resetFromDateEnquiry")?>',
            type: 'POST',
            data: {
              ResetSesession: 'ResetSesession'
            },
            success: function(data) {
              location.reload();
            },
          });
        }

        function resetToDate() {
          $.ajax({
            url: '<?=base_url("Enquiry/resetToDateEnquiry")?>',
            type: 'POST',
            data: {
              ResetSesession: 'ResetSesession'
            },
            success: function(data) {
              location.reload();
            },
          });
        }

        function resetMonthName() {
          $.ajax({
            url: '<?=base_url("Enquiry/resetMonthNameEnquiry")?>',
            type: 'POST',
            data: {
              ResetSesession: 'ResetSesession'
            },
            success: function(data) {
              location.reload();
            },
          });
        }

        function resetAssignUser() {
          $.ajax({
            url: '<?=base_url("Enquiry/resetAssignUserEnquiry")?>',
            type: 'POST',
            data: {
              ResetSesession: 'ResetSesession'
            },
            success: function(data) {
              location.reload();
            },
          });
        }
        </script>