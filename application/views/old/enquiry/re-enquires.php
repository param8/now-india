<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">

      <div class="page-header">
        <div class="row">
          <div class="col-sm-6">
            <h3 class="page-title"><?=$page_title?></h3>
            <ul class="breadcrumb">
              <li><a href="<?=base_url('dashboard')?>">Dashboard/</a></li>
              <li class="breadcrumb-item"><a href="javascript:(0);"><?=$page_title?></a></li>
            </ul>
          </div>
          <?php if($this->session->userdata('user_type')!=1 && $permission[4]=='Upload') { ?>
          <!-- <div class="col-sm-6">
            <div class="float-right">
              <a type="button" class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="assiginEnquiry()"
                style="float: right">Assigin <?//=$page_title?></a>
            </div>
          </div> -->
          <?php } ?>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-body">
              <div class="text-center text-danger">
                <?//=$message?>
              </div>
              <?php if($this->session->userdata('user_type')==1){ ?>
              <div class="row">
                <div class="col-sm-6">
                  <select class="form-control" name="enquiryAdminID" id="enquiryAdminID"
                    onchange="getEnquiryAdminSession(this.value)">
                    <option value="">Select Admin</option>
                    <?php foreach($admins as $admin){ ?>
                    <option value="<?=$admin->id?>"
                      <?=$admin->id == $this->session->userdata('enquiryAdminID') ? 'selected' : ''?>><?=$admin->name?>
                    </option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <hr>
              <?php } ?>
              <div class="table-responsive">
                <table class=" table table-hover table-center mb-0" id="enquiryDataTable">
                  <thead>
                    <tr>
                      <th>S.no.</th>
                      <?php if($this->session->userdata('user_type')!=1){?>
                      <th>Action</th>
                      <?php } ?>
                      <th>Client Name</th>
                      <th>Client Phone No.</th>
                      <th>Lead Type</th>
                      <th>Lead Source</th>
                      <th>Lead Status</th>
                      <th>Assign To</th>
                      <th>Assign By</th>
                      <th>Assign Date</th>
                      <th>Enquery Date</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- <div class="modal fade" id="assiginEnquiry" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-tasks text-warning"></i> Assigin <?//=$page_title?>
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="close_modal()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?//=base_url('Enquiry/assigin_enquiry')?>" method="POST" id="assiginEnquiryForm">
        <div class="modal-body">
          <div class="form-group">
            <label>Users <span class="text-danger">*</span></label>
            <select class="form-control " name="userID" id="userID">
              <option value="">Select User</option>
              <?php //foreach($users as $user){?>
              <option value="<?//=$user->id?>"><?//=$user->name .'('.$user->roleName.')'?></option>
              <?php //} ?>
            </select>
          </div>
          <div class="table-responsive" style="max-height:400px;">
            <table class=" table table-hover table-center mb-0" id="assiginDataTable">
              <thead>
                <tr>
                  <th>S.no.</th>
                  <th><input type="checkbox" id="select-all"> Select All</th>
                  <th>Assign To</th>
                  <th>Client Name</th>
                  <th>Client Phone No.</th>
                  <th>Requirement</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody>


              </tbody>
            </table>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn  btn-outline-danger " onclick="close_modal()" data-dismiss="modal"
            aria-label="Close">Close <i class="fa fa-close"></i></button>
          <button type="submit" class="btn  btn-outline-success ">Submit </button>
        </div>
      </form>
    </div>
  </div>
</div> -->


<script>
$(document).ready(function() {
  var dataTable = $('#enquiryDataTable').DataTable({
    "processing": true,
    "serverSide": true,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Download Excel'
    }],
    "order": [],
    "ajax": {
      url: "<?=base_url('Enquiry/ajaxReEnquiry/'.$uri)?>",
      type: "POST"
    },
    "columnDefs": [{
      "targets": [0],
      "orderable": false,
    }, ],
  });
});

// $(document).ready(function() {
//   var dataTable = $('#assiginDataTable').DataTable({
//     "processing": true,
//     "serverSide": true,
//     buttons: [{
//       extend: 'excelHtml5',
//       text: 'Download Excel'
//     }],
//     "order": [],
//     "ajax": {
//       url: "<?//=base_url('Enquiry/ajaxAssiginEnquiry/'.$uri)?>",
//       type: "POST"
//     },
//     "aoColumnDefs": [{
//         "bSortable": false,
//         "aTargets": [0, 1, 2, 3, 4, 5]
//       },
//       {
//         "bSearchable": false,
//         "aTargets": [0, 1, 2, 3, 4, 5]
//       }
//     ],
//     aLengthMenu: [
//       [10, 25, 50, 100, 200, -1],
//       [10, 25, 50, 100, 200, "All"]
//     ],

//   });
// });



function delete_user(id) {

  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        url: '<?=base_url('User/delete')?>',
        type: 'POST',
        data: {
          id
        },
        dataType: 'json',
        success: function(data) {
          if (data.status == 200) {
            toastNotif({
              text: data.message,
              color: '#5bc83f',
              timeout: 5000,
              icon: 'valid'
            });
            setTimeout(function() {

              location.href = "<?=base_url('users')?>"

            }, 1000)


          } else if (data.status == 302) {
            toastNotif({
              text: data.message,
              color: '#da4848',
              timeout: 5000,
              icon: 'error'
            });

          }
        }
      })
    }
  })
}

function getEnquiryAdminSession(id) {
  $.ajax({
    url: "<?=base_url('Ajax_controller/getEnquiryAdminSession')?>",
    type: 'POST',
    data: {
      id
    },
    success: function(data) {
      location.reload();
    },

  });
}

function view_customer(id) {
  $.ajax({
    url: "<?=base_url('Customer/customerViewForm')?>",
    type: 'POST',
    data: {
      id
    },
    success: function(data) {
      $('#viewCustomer').modal('show');
      $('#view_data').html(data);
    },

  });
}

$('#select-all').click(function(event) {
  if (this.checked) {
    // Iterate each checkbox
    $(':checkbox').each(function() {
      //this.checked = true;  
      var cells = $('#assiginDataTable').dataTable().api().cells().nodes();
      $(cells).find('.chk_cls').prop('checked', this.checked);
    });
  } else {
    $(':checkbox').each(function() {
      this.checked = false;
    });
  }
});

function close_modal() {
  $('#assiginEnquiry').modal('hide');
}

function assiginEnquiry() {
  $('#assiginEnquiry').modal('show');
}

$("form#assiginEnquiryForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('enquires')?>";

        }, 1000)

      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});
</script>