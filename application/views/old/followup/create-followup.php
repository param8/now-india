<div class="page-wrapper">
  <div class="content container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col">
          <h3 class="page-title">Create <?=$page_title?></h3>
          <ul class="breadcrumb">
            <li class=""><a href="<?=base_url('dashboard')?>">Dashboard</a></li>/
            <li class="breadcrumb-item active">Create <?=$page_title?></li>
          </ul>
        </div>
      </div>
    </div>
    <?php if($this->session->userdata('user_type')!=1){?>
    <form action="<?=base_url('Followup/store')?>" id="followupAddForm" method="post">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-xl-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">Enter Followup Detail</h4>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-4 col-lg-4 col-xl-4">
                  <div class="form-group">
                    <label>Client Name </label>
                    <input type="text" class="form-control" name="name" id="name" value="<?=$enquiry->customerName?>"
                      readonly>
                  </div>
                </div>
                <div class="col-md-4 col-lg-4 col-xl-4">
                  <div class="form-group">
                    <label>Phone No. </label>
                    <input type="text" class="form-control" name="phone" id="phone" value="<?=$enquiry->customerPhone?>"
                      readonly>
                  </div>
                </div>

                <div class="col-md-4 col-lg-4 col-xl-4">
                  <div class="form-group">
                    <label>Alt Phone No. </label>
                    <input type="text" class="form-control" maxlength="10"
                      oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" name="alternative_no" id="alternative_no" value="<?=$enquiry->customerAlternateNo?>"
                      >
                  </div>
                </div>

                <div class="col-md-4 col-lg-4 col-xl-4">
                  <div class="form-group">
                    <label>Company Name </label>
                    <input type="text" class="form-control" name="company_name" id="company_name"
                      value="<?=$enquiry->company_name?>" >
                  </div>
                </div>

                <div class="col-md-4 col-lg-4 col-xl-4">
                  <div class="form-group">
                    <label>Email </label>
                    <input type="email" class="form-control" name="email" id="email"
                      value="<?=$enquiry->customerEmail?>" >
                  </div>
                </div>
                
                <div class="col-md-4 col-lg-4 col-xl-4" id="notSelectedLead" >
                  <div class="form-group">
                    <label>Lead Status <span class="text-danger">*</span></label>
                    <select class="form-control js-example-basic-single" name="lead_status" id="lead_status">
                      <option value="">Select Lead Status</option>
                      <?php foreach($lead_status as $lead){?>
                      <option value="<?=$lead->id?>" <?=$lead->id == $enquiry->lead_status ? 'selected' : ''?>><?=$lead->name?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="col-md-4 col-lg-4 col-xl-4" id="selectedLead" style="display:none">
                  <div class="form-group">
                    <label>Lead Status <span class="text-danger">*</span></label>
                    <select class="form-control " name="lead_status" id="lead_status1" disabled>
                      <option value="">Select Lead Status</option>
                      <?php foreach($lead_status as $lead){?>
                      <option value="<?=$lead->id?>" <?=$lead->id == 5 ? 'selected' : ''?> ><?=$lead->name?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-3 col-lg-3 col-xl-3">
                  <div class="form-group">
                    <label>Lead Type </label>
                    <input type="text" class="form-control" name="lead_type" id="lead_type"
                      value="<?=$enquiry->lead_type?>" readonly>
                  </div>
                </div>
                <div class="col-md-3 col-lg-3 col-xl-3">
                  <div class="row">
                  <div class="col-md-6 col-lg-6 col-xl-6">
                    <div class="form-group">
                      <label>Date <span class="text-danger">*</span></label>
                      <input type="text" class="form-control" onfocus="(this.type='date')" name="followup_date" id="followup_date" value="<?=date('d-m-Y')?>">
                    </div>
                  </div>
                  <div class="col-md-6 col-lg-6 col-xl-6">
                    <div class="form-group">
                      <label>Time <span class="text-danger">*</span></label>
                      <input type="time" class="form-control" name="followup_time" id="followup_time" value="<?=date('h:i')?>">
                    </div>
                  </div>
                      </div>
                </div>
                <div class="col-md-3 col-lg-3 col-xl-3">
                  <div class="form-group">
                    <label>Requirement </label>
                    <textarea class="form-control" name="requirement" id="requirement"><?=$enquiry->requirement?></textarea>
                  </div>
                </div>
                <div class="col-md-3 col-lg-3 col-xl-3">
                  <div class="form-group">
                    <label>Remark <span class="text-danger">*</span></label>
                    <textarea class="form-control" name="remark" id="remark"></textarea>
                  </div>
                </div>

                <div class="text-center">
                  <span class="btn btn-rounded btn-outline-success mr-5" onclick="checkCheckBox()" title="Sale please check checkbox"><input type="checkbox" name="sale" id="sale" class="" value="1"> <b class=" ml-5 mr-5">Sale</b></span>
                  <!-- <span><a href="<?//=base_url('create-sale/'.base64_encode($enquiryID))?>" class="btn btn-success">Sale</a></span> -->
                  <button type="submit" class="btn btn-rounded btn-outline-primary">Save</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
 <?php } ?>
    <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-body">
              <div class="text-center text-danger"><?//=$message?></div>

              <div class="table-responsive">
                <table class=" table table-hover table-center mb-0" id="followupHistoryDataTable">
                  <thead>
                    <tr>
                      <th>S.no.</th>
                      <th>Customer Name</th>
                      <th>Lead Status</th>
                      <th>Requirement</th>
                      <th>Remark</th>
                      <th>Followup Date</th>
                      <th>Followup By</th>
                      <!-- <th>Followup Created Date</th> -->
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>
</div>
<script>

$(document).ready(function() {
  //alert('dfgfgf');
  // $('#example').DataTable();
  // } );
  var dataTable = $('#followupHistoryDataTable').DataTable({
    "processing": true,
    "serverSide": true,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Download Excel'
    }],
    "order": [],
    "ajax": {
      url: "<?=base_url('Followup/ajaxFollowupHistory/'.$enquiryID)?>",
      type: "POST"
    },
    "columnDefs": [{
      "targets": [0],
      "orderable": false,
    }, ],
  });
});
$("form#followupAddForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("enquiryID", '<?=$enquiryID?>');
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        if(data.sale_page==0){
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {
          
          location.href = "<?=base_url('create-followup/'.base64_encode($enquiryID))?>";

        }, 1000)
      }else{
        location.href = "<?=base_url('create-sale/'.base64_encode($enquiryID))?>";
      }

      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});


function checkCheckBox(){
  if ($("#sale").is(":checked")) {
    
    $('#sale').prop('checked', false);
    $('#notSelectedLead').show();
    $('#lead_status').prop('disabled', false);
    $('#selectedLead').hide();
    $('#lead_status1').prop('disabled', true);
    $('#remark').val('');
  }else{
   
    $('#sale').prop('checked', true);
    $('#notSelectedLead').hide();
    $('#lead_status').prop('disabled', true);
    $('#selectedLead').show();
    $('#lead_status1').prop('disabled', false);
    $('#remark').val('Sale Done');
  }
  // if(!$("#sale").is(":checked")) {
    
  // }
}


</script>