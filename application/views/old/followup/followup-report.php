<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">

      <div class="page-header">
        <div class="row">
          <div class="col-sm-6">
            <h3 class="page-title"><?=$page_title?></h3>
            <ul class="breadcrumb">
              <li><a href="<?=base_url('dashboard')?>">Dashboard/</a></li>
              <li class="breadcrumb-item"><a href="javascript:(0);"><?=$page_title?></a></li>
            </ul>
          </div>
          <?php if($this->session->userdata('user_type')!=1 && $permission[4]=='Upload'){?>
          <!-- <div class="col-sm-6">
            <div class="float-right">
              <a type="button" class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="assiginEnquiry()"
                style="float: right">Assigin Enquiry</a>
            </div>
          </div> -->
          <?php } ?>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-body">
            <?php if($this->session->userdata('user_type')!=1 && $permission[8]=='Filter'){?>
              <form method="post" id="filter-data" action="<?=base_url('Followup/setSessionFollowup');?>">
                <div class="row">

                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <div class="reset_btn">
                      <label>Customers</label><br>
                      <select id="customers_filter" class="form-control js-example-basic-multiple"
                        name="customers_filter" onchange="this.form.submit();">
                        <option value="">Select Customer</option>
                        <?php foreach($customers as $customer){?>
                        <option value="<?=$customer->id?>"
                          <?=($this->session->userdata('customers_filter')==$customer->id)?'selected':'';?>>
                          <?=$customer->name.'('.$customer->phone.')'?></option>
                        <?php } ?>
                      </select>
                      <span class="close_button" onclick="resetCustomerFilter()" id="resetCustomerFilter"
                        <?=empty($this->session->userdata('customers_filter')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>

                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <div class="reset_btn">
                      <label>Months</label>
                      <?php $months = array('0'=>'All Months','01'=>'January','02'=>'February','03'=>'March','04'=>'April','05'=>'May','06'=>'June','07'=>'July','08'=>'August','09'=>'September','10'=>'October','11'=>'November','12'=>'December');?>
                      <select id="monthname_followup" class="form-control" name="monthname_followup"
                        onchange="this.form.submit();">
                        <?php foreach($months as $month_key=>$month){?>
                        <option value="<?=$month_key?>"
                          <?=(!empty($this->session->userdata('monthname_followup')) AND $this->session->userdata('monthname_followup')==$month_key) ? 'selected' :  '' ?>>
                          <?=$month?></option>
                        <?php } ?>
                      </select>
                      <span class="close_button" onclick="resetMonthName()" id="resetMonthName"
                        <?=empty($this->session->userdata('monthname_followup')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>

                  <?php //if($this->session->userdata('user_type')==2){?>
                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <div class="reset_btn">
                      <label>Sale Users</label>
                      <select id="assign_user_followup" class="form-control" name="assign_user_followup"
                        onchange="this.form.submit();">
                        <option value="">All Users</option>
                        <?php foreach($users as $user){?>
                        <option value="<?=$user->id?>"
                          <?=$this->session->userdata('assign_user_followup')==$user->id ? 'selected':'' ;?>>
                          <?=$user->name.'('.$user->roleName.')'?></option>
                        <?php } ?>
                      </select>
                      <span class="close_button" onclick="resetAssignUser()" id="resetAssignUser"
                        <?=empty($this->session->userdata('assign_user_followup')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>
                  <?php //} ?>

                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <div class="reset_btn">
                      <label>From Date</label>
                      <div class="input-group">
                        <input type="text" class="form-control" onfocus="(this.type='date')" placeholder="From Date"
                          name="followup_from_date" id="followup_from_date"
                          value="<?=!empty($this->session->userdata('followup_from_date')) ? $this->session->userdata('followup_from_date'):'';?>"
                          onchange="this.form.submit();">
                      </div>
                      <span class="close_button" onclick="resetFromDate()" id="resetFromDate"
                        <?=empty($this->session->userdata('followup_from_date')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>
                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <div class="reset_btn">
                      <label>To Date</label>
                      <div class="input-group">
                        <input type="text" class="form-control" onfocus="(this.type='date')" placeholder="To Date"
                          name="followup_to_date" id="followup_to_date"
                          value="<?=!empty($this->session->userdata('followup_to_date')) ? $this->session->userdata('followup_to_date') :'';?>"
                          onchange="this.form.submit();">
                      </div>
                      <span class="close_button" onclick="resetToDate()" id="resetToDate"
                        <?=empty($this->session->userdata('followup_to_date')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>

                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <label>Lead Status</label>
                    <div class="input-group">

                      <select id="lead_status_followup" class="form-control" name="lead_status_followup"
                        onchange="this.form.submit();">
                        <option value="">Select Lead Status</option>
                        <?php foreach($leads as $lead){?>
                        <option value="<?=$lead->id?>"
                          <?=$this->session->userdata('lead_status_followup')==$lead->id ? 'selected':'' ;?>>
                          <?=$lead->name?></option>
                        <?php } ?>
                      </select>
                      <span class="close_button" onclick="resetLeadStatusFollowup()" id="resetleadStatus"
                        <?=empty($this->session->userdata('lead_status_followup')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>
                </div>
              </form>
              <hr>
              <?php } ?>
              <?php if($this->session->userdata('user_type')==1){?>
              <div class="row">
                <div class="col-sm-6">
                  <select class="form-control" name="followupAdminID" id="followupAdminID"
                    onchange="getFollowupAdminSession(this.value)">
                    <option value="">Select Admin</option>
                    <?php foreach($admins as $admin){?>
                    <option value="<?=$admin->id?>"
                      <?=$admin->id == $this->session->userdata('followupAdminID') ? 'selected' : ''?>><?=$admin->name?>
                    </option>
                    <?php } ?>
                  </select>

                </div>

              </div>
              <hr>
              <?php } ?>
              <div class="table-responsive">
                <table class=" table table-hover table-center mb-0" id="followupDataTable">
                  <thead>
                    <tr>
                      <th>S.no.</th>
                      <th>Action</th>
                      <th>Client Name</th>
                      <th>Client Phone No.</th>
                      <th>Assign To</th>
                      <th>Last Followup</th>
                      <th>Followup Remark</th>
                      <th>Lead Status</th>
                      <th>Assign By</th>
                      <th>Assign Date</th>
                      <th>Enquery Date</th>
                      <th>Lead Type</th>
                      <th>Lead Source</th>
                      <th>Followup Created</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="followupReport" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-file text-warning"></i> <?=$page_title?>
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="close_modal()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
        <div class="modal-body">
         
          <div class="table-responsive" style="max-height:550px;">
            <table class=" table table-hover table-center mb-0" id="followupHistoryDataTable">
              <thead>
                <tr>
                <th>S.no.</th>
                <th>Customer Name</th>
                <th>Lead Status</th>
                <th>Requirement</th>
                <th>Remark</th>
                <th>Followup Date </th>
                <th>Followup By</th>
                <!-- <th>Followup Created Date</th> -->
                </tr>
              </thead>
              <tbody>
                

              </tbody>
            </table>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn  btn-outline-danger " onclick="close_modal()" data-dismiss="modal"
            aria-label="Close">Close <i class="fa fa-close"></i></button>
        </div>

    </div>
  </div>
</div>


<script>
$(document).ready(function() {
  //alert('dfgfgf');
  // $('#example').DataTable();
  // } );
  var dataTable = $('#followupDataTable').DataTable({
    "processing": true,
    "serverSide": true,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Download Excel'
    }],
    "order": [],
    "ajax": {
      url: "<?=base_url('Followup/ajaxFollowupReport/'.$uri)?>",
      type: "POST"
    },
    "columnDefs": [{
      "targets": [0],
      "orderable": false,
    }, ],
  });
});


$(document).ready(function() {
  var dataTable = $('#assiginDataTable').DataTable({
    "processing": true,
    "serverSide": true,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Download Excel'
    }],
    "order": [],
    "ajax": {
      url: "<?=base_url('Enquiry/ajaxAssiginEnquiry/'.$uri)?>",
      type: "POST"
    },
    "aoColumnDefs": [
        { "bSortable": false, "aTargets": [ 0, 1, 2, 3,4,5 ] }, 
        { "bSearchable": false, "aTargets": [ 0, 1, 2,3,4,5] }
      ],
      aLengthMenu: [
        [10,25, 50, 100, 200, -1],
        [10,25, 50, 100, 200,"All"]
      ],

  });
});

function getFollowupAdminSession(id){
  $.ajax({
    url: "<?=base_url('Ajax_controller/getFollowupAdminSession')?>",
    type: 'POST',
    data: {id},
    success: function(data) {
      location.reload();
    },
   
  });
}



function close_modal() {
  location.reload();
}

function view_report(enquiryID) {
  
  var dataTable = $('#followupHistoryDataTable').DataTable({
    "processing": true,
    "serverSide": true,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Download Excel'
    }],
    "order": [],
    "ajax": {
      url: "<?=base_url('Followup/ajaxFollowupHistory/')?>"+enquiryID,
      type: "POST"
    },
    "columnDefs": [{
      "targets": [0],
      "orderable": false,
    }, ],
  });

  $('#followupReport').modal('show');
}


function resetCustomerFilter() {
          $.ajax({
            url: '<?=base_url("Followup/resetCustomerFilter")?>',
            type: 'POST',
            data: {
              ResetSesession: 'ResetSesession'
            },
            success: function(data) {
              location.reload();
            },
          });
        }

        function resetFromDate() {
          $.ajax({
            url: '<?=base_url("Followup/resetFromDateFollowup")?>',
            type: 'POST',
            data: {
              ResetSesession: 'ResetSesession'
            },
            success: function(data) {
              location.reload();
            },
          });
        }

        function resetToDate() {
          $.ajax({
            url: '<?=base_url("Followup/resetToDateFollowup")?>',
            type: 'POST',
            data: {
              ResetSesession: 'ResetSesession'
            },
            success: function(data) {
              location.reload();
            },
          });
        }

        function resetMonthName() {
          $.ajax({
            url: '<?=base_url("Followup/resetMonthNameFollowup")?>',
            type: 'POST',
            data: {
              ResetSesession: 'ResetSesession'
            },
            success: function(data) {
              location.reload();
            },
          });
        }

        function resetAssignUser() {
          $.ajax({
            url: '<?=base_url("Followup/resetAssignUserFollowup")?>',
            type: 'POST',
            data: {
              ResetSesession: 'ResetSesession'
            },
            success: function(data) {
              location.reload();
            },
          });
        }

        function resetLeadStatusFollowup() {
          $.ajax({
            url: '<?=base_url("Followup/resetLeadStatusFollowup")?>',
            type: 'POST',
            data: {
              ResetSesession: 'ResetSesession'
            },
            success: function(data) {
              location.reload();
            },
          });
        }

        function getFollowupAdminSession(id) {
          $.ajax({
            url: "<?=base_url('Ajax_controller/getFollowupAdminSession')?>",
            type: 'POST',
            data: {
              id
            },
            success: function(data) {
              location.reload();
            },

          });
        }

</script>