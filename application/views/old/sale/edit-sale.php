<div class="page-wrapper">
  <div class="content container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col">
          <h3 class="page-title">Create <?=$page_title?></h3>
          <ul class="breadcrumb">
            <li class=""><a href="<?=base_url('dashboard')?>">Dashboard</a></li>/
            <li class="breadcrumb-item active">Create <?=$page_title?></li>
          </ul>
        </div>
      </div>
    </div>
    <form action="<?=base_url('Sale/update')?>" id="saleEditForm" method="post">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-xl-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">Enter <?=$page_title?> Detail</h4>
            </div>
            <div class="card-body">
              <div class="row">

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Company Name </label>
                    <input type="text" class="form-control" name="company_name" id="company_name"
                      value="<?=$sale->company_name?>">
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Full Name </label>
                    <input type="text" class="form-control" value="<?=$sale->customerName?>" readonly>
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Phone No. </label>
                    <input type="text" class="form-control" minlength="10" maxlength="10"
                      oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
                      onkeyup="get_customer(this.value)" value="<?=$sale->customerPhone?>" readonly>
                  </div>
                </div>


                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Email </label>
                    <input type="email" class="form-control" value="<?=$sale->customerEmail?>" readonly>
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Website </label>
                    <input type="text" class="form-control" name="website" id="website" placeholder="Website"
                      value="<?=$sale->website?>">
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Sale Date <span class="text-danger">*</span></label>
                    <input type="text" onfocus="(this.type='date')" class="form-control" name="sale_date" id="sale_date"
                      placeholder="Sale Date" value="<?=$sale->sale_date?>">
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Sale Amount <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="sale_amount" id="sale_amount"
                      oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                      placeholder="Sale Amount" onkeyup="amountCalculation()" value="<?=$sale->sale_amount?>" readonly>
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="row">
                    <div class="col-md-4 col-lg-4 col-xl-4">
                      <div class="form-group">
                        <label>GST Apply </label><br>
                        <div class="btn btn-rounded btn-outline-success mr-5" title="Apply
                            GST">
                          <input type="checkbox" name="gst" id="gst" class="" value="1" onclick="checkGst()"
                            <?=$sale->gst==1 ? 'checked' : ''?>> <span class=" ml-5 mr-5">Apply
                            GST</span>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-xl-4" id="gst_amount_div" style="display:none">
                      <div class="form-group">
                        <label>GST Amount </label>
                        <input type="text" class="form-control" name="gst_amount" id="gst_amount"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                          placeholder="GST Amount" onkeyup="amountCalculation()" readonly
                          value="<?=$sale->gst_amount?>">
                      </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-xl-4">
                      <div class="form-group">
                        <label>Total Amount </label>
                        <input type="text" class="form-control" name="total_amount" id="total_amount"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                          placeholder="Total Amount" value="<?=$sale->total_amount?>" readonly>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Advance Payment Amount <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="paid_amount" id="paid_amount"
                      oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                      placeholder="Advance Payment Amount" value="<?=$sale->advance_amount?>"
                      onkeyup="amountCalculation()" readonly>
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Pending Amount <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="pending_amount" id="pending_amount"
                      oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                      placeholder="Pending Amount" value="<?=$sale->pending_amount?>" readonly>
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Payment Mode <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="payment_mode" id="payment_mode"
                      placeholder="Payment Mode" value="<?=$sale->payment_mode?>">
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Lead source </label>
                    <input type="text" class="form-control" value="<?=$sale->lead_sourse?>" readonly>
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Delivery Date </label>
                    <input type="text" onfocus="(this.type='date')" class="form-control" name="delivery_date"
                      id="delivery_date" placeholder="Delivery Date" value="<?=$sale->delivery_date?>">
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Content Date </label>
                    <input type="text" onfocus="(this.type='date')" class="form-control" name="content_date"
                      id="content_date" placeholder="Content Date" value="<?=$sale->content_date?>">
                  </div>
                </div>



                <!-- <div class="col-md-12 col-lg-12 col-xl-12">
                  <div class="form-group">
                    <label>Product/Requirement <span class="text-danger">*</span></label>
                    <textarea class="form-control" name="product" id="product"
                      placeholder="Product/Requirement"><?//=$sale->product?></textarea>
                  </div>
                </div> -->

                <div class="text-center">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
<script>
$("form#saleEditForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("id", '<?=$sale->id?>');
  formData.append("enquiryID", '<?=$sale->enquiryID?>');
  formData.append("sale_create_by", '<?=$sale->customer_assigin_id?>');
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('sales')?>";

        }, 1000)

      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

function amountCalculation() {
  var sale_amount = $('#sale_amount').val().length === 0 ? 0 : parseFloat($('#sale_amount').val());

  var paid_amount = $('#paid_amount').val().length === 0 ? 0 : parseFloat($('#paid_amount').val());



  if (sale_amount === 0) {
    toastNotif({
      text: 'Please enter sale amount ',
      color: '#da4848',
      timeout: 5000,
      icon: 'error'
    });

  } else {
    var gst_amount = $('#gst').is(':checked') ? Math.round((sale_amount * 18) / 100) : 0;
    $('#gst_amount').val(gst_amount);
    var gst_sale_amount = sale_amount + gst_amount;
    if (paid_amount > gst_sale_amount) {
      toastNotif({
        text: 'Sale Amount should be greater than advance amount',
        color: '#da4848',
        timeout: 5000,
        icon: 'error'
      });
      $('#pending_amount').val('');
    } else {
      var pending_amount = gst_sale_amount - paid_amount;
      $('#pending_amount').val(pending_amount);
      $('#total_amount').val(gst_sale_amount);
    }

  }

}

function checkGst() {

  if ($('#gst').is(':checked')) {

    $('#gst_amount_div').show();
  } else {
    $('#gst_amount').val('');
    $('#gst_amount_div').hide();
  }
  amountCalculation();
}

$(document).ready(function() {
  checkGst();
});
</script>