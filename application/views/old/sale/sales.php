<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">

      <div class="page-header">
        <div class="row">
          <div class="col-sm-6">
            <h3 class="page-title"><?=$page_title?></h3>
            <ul class="breadcrumb">
              <li><a href="<?=base_url('dashboard')?>">Dashboard/</a></li>
              <li class="breadcrumb-item"><a href="javascript:(0);"><?=$page_title?></a></li>
            </ul>
          </div>
          <?php if($this->session->userdata('user_type')!=1 && $permission[1]=='Add'){?>
          <!-- <div class="col-sm-6">
            <div class="float-right">
              <a type="button" class="btn btn-primary btn-sm" href="<?//=base_url('create-customer')?>"
                style="float: right">Create <?//=$page_title?></a>
            </div>
          </div> -->
          <?php } ?>
        </div>
      </div>
      <?php if($this->session->userdata('user_type')!=1 && $permission[8]=='Filter'){?>
      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-body">
              <form method="post" id="filter-data" action="<?=base_url('Sale/setSession');?>">
                <div class="row">

                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <div class="reset_btn">
                      <label>Sale Status</label>
                      <?php $amount_status = array('1'=>'Pending Amount','2'=>'Complete Amount');?>
                      <select id="pending_balance" class="form-control" name="pending_balance"
                        onchange="this.form.submit();">
                        <option value="">Select Sale Status</option>
                        <?php foreach($amount_status as $amount_status_key=>$status_amount){?>
                        <option value="<?=$amount_status_key?>"
                          <?=($this->session->userdata('pending_balance')==$amount_status_key)?'selected':'';?>>
                          <?=$status_amount?></option>
                        <?php } ?>
                      </select>
                      <span class="close_button" onclick="resetPending_balance()" id="resetPending_balance"
                        <?=empty($this->session->userdata('pending_balance')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>

                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <div class="reset_btn">
                      <label>Months</label>
                      <?php $months = array('0'=>'All Months','01'=>'January','02'=>'February','03'=>'March','04'=>'April','05'=>'May','06'=>'June','07'=>'July','08'=>'August','09'=>'September','10'=>'October','11'=>'November','12'=>'December');?>
                      <select id="monthname" class="form-control" name="monthname" onchange="this.form.submit();">
                        <?php foreach($months as $month_key=>$month){?>
                        <option value="<?=$month_key?>"
                          <?=(!empty($this->session->userdata('monthname')) AND $this->session->userdata('monthname')==$month_key) ? 'selected' :  '' ?>>
                          <?=$month?></option>
                        <?php } ?>
                      </select>
                      <span class="close_button" onclick="resetMonthName()" id="resetMonthName"
                        <?=empty($this->session->userdata('monthname')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>

                  <?php //if($permission[7] == 'Like Admin' || $this->session->userdata('user_type')==2){?>
                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <div class="reset_btn">
                      <label>Sale Users</label>
                      <select id="assign_user" class="form-control" name="assign_user" onchange="this.form.submit();">
                        <option value="">All Users</option>
                        <?php foreach($users as $user){?>
                        <option value="<?=$user->id?>"
                          <?=$this->session->userdata('assign_user')==$user->id ? 'selected':'' ;?>>
                          <?=$user->name.'('.$user->roleName.')'?></option>
                        <?php } ?>
                      </select>
                      <span class="close_button" onclick="resetAssignUser()" id="resetAssignUser"
                        <?=empty($this->session->userdata('assign_user')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>
                  <?php //} ?>
                  <!-- <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <div class="input-group">
                      <label>Customer Name / Phone</label>
                      <input type="text" class="form-control" placeholder="Customer Name/Phone"
                        aria-label="Customer Name/Phone" aria-describedby="basic-addon2" name="customer_detail"
                        id="customer_detail">
                      <span class="input-group-text text-success" id="basic-addon2" style="cursor:pointer"
                        onclick="getCustomerSale()"><i class="fa fa-search" aria-hidden="true"></i></span>
                    </div>
                  </div> -->


                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <div class="reset_btn">
                      <label>From Date</label>
                      <div class="input-group">
                        <input type="text" class="form-control" onfocus="(this.type='date')" placeholder="From Date"
                          name="sale_from_date" id="sale_from_date"
                          value="<?=!empty($this->session->userdata('sale_from_date')) ? $this->session->userdata('sale_from_date'):'';?>"
                          onchange="this.form.submit();">
                      </div>
                      <span class="close_button" onclick="resetFromDate()" id="resetFromDate"
                        <?=empty($this->session->userdata('sale_from_date')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>
                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <div class="reset_btn">
                      <label>To Date</label>
                      <div class="input-group">
                        <input type="text" class="form-control" onfocus="(this.type='date')" placeholder="To Date"
                          name="sale_to_date" id="sale_to_date"
                          value="<?=!empty($this->session->userdata('sale_to_date'))? $this->session->userdata('sale_to_date') :'';?>"
                          onchange="this.form.submit();">
                      </div>
                      <span class="close_button" onclick="resetToDate()" id="resetToDate"
                        <?=empty($this->session->userdata('sale_to_date')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>
                </div>
              </form>
              <hr>
              <?php } ?>
              <?php if($this->session->userdata('user_type')==1){?>
              <div class="row">
                <div class="col-sm-6">
                  <select class="form-control" name="saleAdminID" id="saleAdminID"
                    onchange="getSaleAdminSession(this.value)">
                    <option value="">Select Admin</option>
                    <?php foreach($admins as $admin){?>
                    <option value="<?=$admin->id?>"
                      <?=$admin->id == $this->session->userdata('saleAdminID') ? 'selected' : ''?>><?=$admin->name?>
                    </option>
                    <?php } ?>
                  </select>

                </div>

              </div>
              <hr>



              <?php } ?>


              <div class="table-responsive">
                <table class=" table table-hover table-center mb-0" id="saleDataTable">
                  <thead>
                    <tr>
                      <th>S.no.</th>
                      <th>Action</th>
                      <th>Company Name</th>
                      <th>Sale Date</th>
                      <th>Sale Amount</th>
                      <th>GST</th>
                      <th>Total Amount</th>
                      <th>Paid Amount</th>
                      <th>Pending Amount</th>
                      <th>Client Name</th>
                      <th>Client Phone No.</th>
                      <th>Delivery Date</th>
                      <th>Closed BY</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="viewSale" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-eye text-warning"></i> View <?=$page_title?>
        </h5>
        <button type="button" class="close " onclick="close_modal()" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body" id="view_data">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn  btn-outline-danger " onclick="close_modal()" data-dismiss="modal"
          aria-label="Close">Close</button>
      </div>

    </div>
  </div>
</div>

<div class="modal fade" id="payment_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-credit-card text-secondary"></i> Payment History
        </h5>
        <button type="button" class="close " onclick="close_modal()" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('Sale/store_payment_history')?>" method="POST" id="paymentAddForm">
        <div class="modal-body" id="view_payment_data">

        </div>
      </form>
      <div class="modal-footer">
        <button type="button" class="btn  btn-outline-danger " onclick="close_modal()" data-dismiss="modal"
          aria-label="Close">Close</button>
      </div>

    </div>
  </div>
</div>

<div class="modal fade" id="servicesModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-wrench text-warning"></i> Services
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="close_modal()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="viewServices_detail" style="background-color:#f8f9fa">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn  btn-outline-danger " onclick="close_modal()" data-dismiss="modal"
          aria-label="Close">Close <i class="fa fa-close"></i></button>
      </div>

    </div>
  </div>
</div>


<script>
$(document).ready(function() {
  //alert('dfgfgf');
  // $('#example').DataTable();
  // } );
  var dataTable = $('#saleDataTable').DataTable({
    "processing": true,
    "serverSide": true,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Download Excel'
    }],
    "order": [],
    "ajax": {
      url: "<?=base_url('Sale/ajaxSale/'.$uri)?>",
      type: "POST"
    },
    "columnDefs": [{
      "targets": [0],
      "orderable": true,
    }, ],
  });
});



function resetPending_balance() {
  $.ajax({
    url: '<?=base_url("Sale/resetPending_balance")?>',
    type: 'POST',
    data: {
      ResetSesession: 'ResetSesession'
    },
    success: function(data) {
      location.reload();
    },
  });
}

function resetFromDate() {
  $.ajax({
    url: '<?=base_url("Sale/resetFromDate")?>',
    type: 'POST',
    data: {
      ResetSesession: 'ResetSesession'
    },
    success: function(data) {
      location.reload();
    },
  });
}

function resetToDate() {
  $.ajax({
    url: '<?=base_url("Sale/resetToDate")?>',
    type: 'POST',
    data: {
      ResetSesession: 'ResetSesession'
    },
    success: function(data) {
      location.reload();
    },
  });
}

function resetMonthName() {
  $.ajax({
    url: '<?=base_url("Sale/resetMonthName")?>',
    type: 'POST',
    data: {
      ResetSesession: 'ResetSesession'
    },
    success: function(data) {
      location.reload();
    },
  });
}

function resetAssignUser() {
  $.ajax({
    url: '<?=base_url("Sale/resetAssignUser")?>',
    type: 'POST',
    data: {
      ResetSesession: 'ResetSesession'
    },
    success: function(data) {
      location.reload();
    },
  });
}

function getSaleAdminSession(id) {
  $.ajax({
    url: "<?=base_url('Ajax_controller/getSaleAdminSession')?>",
    type: 'POST',
    data: {
      id
    },
    success: function(data) {
      location.reload();
    },

  });
}

function view_sales(id) {
  $.ajax({
    url: "<?=base_url('Sale/viewSaleForm')?>",
    type: 'POST',
    data: {
      id
    },
    success: function(data) {
      $('#viewSale').modal('show');
      $('#view_data').html(data);
    },

  });
}

function amountCalculation() {
  var sale_amount = $('#sale_amount').val().length === 0 ? 0 : parseFloat($('#sale_amount').val());

  var paid_amount = $('#paid_amount').val().length === 0 ? 0 : parseFloat($('#paid_amount').val());



  if (sale_amount === 0) {
    toastNotif({
      text: 'Please enter sale amount ',
      color: '#da4848',
      timeout: 5000,
      icon: 'error'
    });

  } else {
    var gst_amount = $('#gst').is(':checked') ? Math.round((sale_amount * 18) / 100) : 0;
    $('#gst_amount').val(gst_amount);
    var gst_sale_amount = sale_amount + gst_amount;
    if (paid_amount > gst_sale_amount) {
      toastNotif({
        text: 'Sale Amount should be greater than advance amount',
        color: '#da4848',
        timeout: 5000,
        icon: 'error'
      });
      $('#pending_amount').val('');
    } else {
      var pending_amount = gst_sale_amount - paid_amount;
      $('#pending_amount').val(pending_amount);
    }

  }

}


function checkGst() {

  if ($('#gst').is(':checked')) {

    $('#gst_amount_div').show();
  } else {
    $('#gst_amount').val('');
    $('#gst_amount_div').hide();
  }
  amountCalculation();
}

function close_modal() {
  $('#viewSale').modal('hide');
  $('#payment_modal').modal('hide');
  $('#servicesModel').modal('hide');
}

function add_payment(id) {
  $.ajax({
    url: "<?=base_url('Sale/viewPaymentForm')?>",
    type: 'POST',
    data: {
      id
    },
    success: function(data) {
      $('#payment_modal').modal('show');
      $('#view_payment_data').html(data);
    },

  });
}

function add_payment_history() {
  if ($('#add_payment_div').is(':hidden')) {
    $('#add_payment_div').show();
  } else {
    $('#add_payment_div').hide();
  }
}


$("form#paymentAddForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 10000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        ajaxSendMail(data.saleID, data.id)
        // setTimeout(function() {

        //   location.href = "<?//=base_url('sales')?>";

        // }, 1000)

      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

function ajaxSendMail(saleID, id) {
  $.ajax({
    url: "<?=base_url('Send_mail/paymentSendMail')?>",
    type: 'POST',
    data: {
      saleID,
      id
    },
    success: function(data) {
      toastNotif({
        text: 'Mail sent successfully',
        color: '#5bc83f',
        timeout: 10000,
        icon: 'valid'
      });
      setTimeout(function() {
        location.href = "<?= base_url('sales')?>";
      }, 1000)
    },
  });
}

function view_services(id) {
  $.ajax({
    url: '<?=base_url('Services/get_service_detail')?>',
    type: 'post',
    data: {
      id
    },
    dataType: 'html',
    success: function(response) {
      $('#servicesModel').modal('show');
      $('#viewServices_detail').html(response);
      togglePay();
    }
  });
}
</script>

<script>
function delete_sale(id) {

  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        url: '<?=base_url('Sale/delete')?>',
        type: 'POST',
        data: {
          id
        },
        dataType: 'json',
        success: function(data) {
          if (data.status == 200) {
            toastNotif({
              text: data.message,
              color: '#5bc83f',
              timeout: 5000,
              icon: 'valid'
            });
            setTimeout(function() {

              location.href = "<?=base_url('sales')?>"

            }, 1000)


          } else if (data.status == 302) {
            toastNotif({
              text: data.message,
              color: '#da4848',
              timeout: 5000,
              icon: 'error'
            });

          }
        }
      })
    }
  })
}
</script>

<script>
$('#requirements').summernote({
  height: 300,
});
</script>