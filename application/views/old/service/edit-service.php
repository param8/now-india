<link rel="stylesheet"
  href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
<script type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js"></script>
<style>
.border-bottom {
  border-bottom: 1px solid gainsboro;
}

.service-list label {
  cursor: pointer;
  padding: 0 103px 0px 5px;
}


#domainform,
#emailform,
#hostingform,
#sslform,
#softwareform,
#websiteform,
#whatsappform,
#smsform,
#seoform,
#smoform,
#smmform,
#ppcform,
#massEmailform,
#otherDetailform,
#maintenenceWebsiteForm,
#otherform {
  display: none;
}

.service-list>li:hover {
  cursor: pointer;
  background: #284b895c;
}

.service-list {
  display: none;
  width: 100%;
  position: inherit;
  font-size: 13px;
  border-radius: 2px;
  -webkit-box-shadow: 0 2px 6px rgb(0 0 0 / 10%);
  box-shadow: 0 2px 6px rgb(0 0 0 / 10%);
  border: 1px solid rgba(0, 0, 0, .1);
}

.service-list>li {
  margin: 0;
  height: 100%;
  cursor: pointer;
  font-weight: 400;
  padding: 3px 20px 3px 30px;
  border-bottom: 1px solid lightgray;
}

#select_all {}

body {
  color: #6a6c6f;
  background-color: #f1f3f6;
  margin-top: 30px;
}

.container {
  max-width: 960px;
}

.table>tbody>tr.active>td,
.table>tbody>tr.active>th,
.table>tbody>tr>td.active,
.table>tbody>tr>th.active,
.table>tfoot>tr.active>td,
.table>tfoot>tr.active>th,
.table>tfoot>tr>td.active,
.table>tfoot>tr>th.active,
.table>thead>tr.active>td,
.table>thead>tr.active>th,
.table>thead>tr>td.active,
.table>thead>tr>th.active {
  background-color: #fff;
}

.table-bordered>tbody>tr>td,
.table-bordered>tbody>tr>th,
.table-bordered>tfoot>tr>td,
.table-bordered>tfoot>tr>th,
.table-bordered>thead>tr>td,
.table-bordered>thead>tr>th {
  border-color: #e4e5e7;
}

.table tr.header {
  font-weight: bold;
  background-color: #fff;
  cursor: pointer;
  -webkit-user-select: none;
  /* Chrome all / Safari all */
  -moz-user-select: none;
  /* Firefox all */
  -ms-user-select: none;
  /* IE 10+ */
  user-select: none;
  /* Likely future */
}

.table tr:not(.header) {
  display: none;
}

.table .header td:after {
  opacity: 0;
  content: "\002b";
  position: relative;
  top: 1px;
  display: inline-block;
  font-family: 'Glyphicons Halflings';
  font-style: normal;
  font-weight: 400;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  float: right;
  color: #999;
  text-align: center;
  padding: 3px;
  transition: transform .25s linear;
  -webkit-transition: -webkit-transform .25s linear;
}

.table .header.active td:after {
  content: "\2212";
}

.btn-group {
  width: 100%;
}

.btn-group button {
  width: 100%;
}

/*ul{*/
/*  text-decoration: underline;*/
/*}*/
.thead-dark tr {
  background: #ff4300cc;
  color: #fff;
}

.modal-dialog {
  width: 850px;
}

.modal-content {
  border-radius: 0px;
}

ul label {
  font-weight: bold;
}

h2.h4.bg-primary {
  padding: 5px;
}
</style>
<?php 
$serviceID = $service->id;
$service_type_array = array();
$service_detail = array();
 foreach(json_decode($service->service_detail) as $service_type_key=>$detail){

  $service_type_array[] = $service_type_key;
  $service_detail[$service_type_key] = $detail;
 }
//  echo "<pre>";
//  print_r($service_detail);die;
?>
<div class="content">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col">
            <h3 class="page-title">Create <?=$page_title?></h3>
            <ul class="breadcrumb">
              <li class=""><a href="<?=base_url('dashboard')?>">Dashboard</a></li>/
              <li class="breadcrumb-item active">Create <?=$page_title?></li>
            </ul>
          </div>
        </div>
      </div>
      <form action="<?=base_url('Services/update')?>" id="serviceEditForm" method="post">
        <div class="row">
          <div class="col-md-12 col-lg-12 col-xl-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Enter <?=$page_title?> Detail</h4>
              </div>
              <div class="card-body">
                <div class="row">
                  <input type="hidden" name="sale_id" id="sale_id" value="<?=$saleID?>">
                  <div class="col-md-6 col-lg-6 col-xl-6">
                    <div class="form-group">
                      <label>Services </label>
                      <button type="button" class="form-control btn btn-default" id="show-services"><strong>Select
                          Services</strong> <b class="caret"></b></button>
                      <ul class="service-list" id="service-list" style="list-style-type:none;padding-left: 0;">
                        <!-- € -->
                        <?php foreach($service_types as $service_type){?>
                        <li>
                          <input type="checkbox" class="mt-2" id="<?=$service_type->slug?>" name="services[]"
                            value="<?=$service_type->name?>"
                            <?=in_array($service_type->name,$service_type_array) ? 'checked' : ''?>><label
                            for="<?=$service_type->slug?>"><?=$service_type->name?></label>
                        </li>
                        <?php } ?>
                      </ul>
                    </div>
                  </div>


                  <div class="text-right" style="margin: 0 10px 5px 0;">

                    <?php if(!empty($row)){
                      //print_r($row);die;?>
                    <a class="btn btn-primary  btn-rounded" data-toggle="modal"
                      onclick="viewservices(<?php echo $row->enq_id?>)" data-target="#detailModal"><i
                        class="fa fa-eye"></i>View Last Purchase Detail</a>

                    <div class="modal fade" tabindex="-1" role="dialog" id="detailModal">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center"><strong>Previous Purchase Data</strong></h4>
                          </div>

                          <div class="modal-body">
                            <div class="row" id="viewServices_detail"
                              style="display: flex; width: 100%; flex-wrap: wrap;text-align: left;">

                            </div>
                          </div>
                        </div><!-- /.modal-content -->
                      </div><!-- /.modal-dialog -->
                    </div>
                    <?php } ?>
                  </div>
                  <section id="domainform">
                    <div id="domainTtype" class="row">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">Domain</h2>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="brand_name"><strong>Domain Name</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="domain" name="domain"
                            placeholder="Enter Domain Name" value="<?=$service_detail['Domain']->domain_name?>">
                        </div>
                      </div>

                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="booking_date"><strong>Booking Date</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="date" class="form-control" id="booking_date" name="booking_date"
                            value="<?=$service_detail['Domain']->domain_booking_date?>">
                        </div>
                      </div>

                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="brand_name"><strong>Renewal Years</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" onkeypress='validate(event)' autocomplete="off"
                            id="year" name="year" placeholder="Enter Year"
                            value="<?=$service_detail['Domain']->domain_year?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="rDate"><strong>Renewal Date</strong></label>
                          <?php $StaringDate = strtotime(date("Y-m-d"));$newEndingDate =strtotime( "+ 1 year",$StaringDate);?>
                          <input type="date" class="form-control" id="rDate" onkeyup="domainprice()" name="rDate"
                            value="<?=$service_detail['Domain']->doamin_renew_date?>" readonly>
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="price"><strong>Price/Year</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" autocomplete="off" id="price" onkeyup="domainprice()"
                            name="price" placeholder="Enter Price"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Domain']->doamin_price?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="price"><strong>Total Price</strong></label>
                          <input type="text" class="form-control" autocomplete="off" id="dtotalprice" name="dtotalprice"
                            placeholder="Total Price" value="<?=$service_detail['Domain']->domain_total_price?>"
                            readonly>
                        </div>
                      </div>
                      <input type="hidden" name="domainStatus" id="domainStatus"
                        value="<?=$service_detail['Domain']->domainStatus?>">
                    </div>
                  </section>
                  <section id="emailform">
                    <div class="row col-md-12 col-lg-12 col-xl-12">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">Email</h2>
                      <div class="col-md-6 col-lg-6 col-xl-6" id="email-domain">
                        <div class="form-group">
                          <label for="emailDomain"><strong>Domain Name</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" name="emailDomain" id="emailDomain"
                            placeholder="Enter Domain Name" value="<?=$service_detail['Email']->email_domain_name?>">
                        </div>
                      </div>

                      <div class="col-md-6 col-lg-6 col-xl-6">
                        <div class="form-group">
                          <label for="adminmail"><strong>Admin Mail Id</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" name="adminmail" id="adminmail" class="form-control"
                            placeholder="Enter Admin Mail Id"
                            value="<?=$service_detail['Email']->email_admin_mail_id?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="booking_date"><strong>Booking Date</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="date" class="form-control" id="email_booking_date" name="email_booking_date"
                            value="<?=$service_detail['Email']->email_booking_date?>">
                        </div>
                      </div>

                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="brand_name"><strong>Renewal Years</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" onkeyup="emailprice()" autocomplete="off"
                            id="email_year" name="email_year" onkeypress='validate(event)'
                            value="<?=$service_detail['Email']->email_year?>" placeholder="Enter Year">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="rDate"><strong>Renewal Date</strong></label>
                          <?php $StaringDate = strtotime(date("Y-m-d"));$newEndingDate =strtotime( "+ 1 year",$StaringDate);?>
                          <input type="date" class="form-control" id="email_rDate" onkeyup="domainprice()"
                            name="email_rDate"
                            value="<?=date('Y-m-d',strtotime($service_detail['Email']->email_rDate))?>" readonly>
                        </div>
                      </div>
                      <?php 
                        $email_datas  = json_decode($service_detail['Email']->email_data);
                        $emailCounter = 0;
                        foreach($email_datas as $datas){
                          $emailCounter = count($datas);
                          foreach($datas as $data_key=>$data){
                            $data_key = $data_key+1;
                            //print_r($data_key);
                      ?>
                      <div id="rows" class="row col-md-12" style="margin-bottom:10px">

                        <div class="col-md-3 col-lg-3 col-xl-3 row">
                          <div class="form-group row">
                            <input type="hidden" name="emailcounter" id="emailcounter" value="<?=count($datas)?>" />
                            <div class="col-md-12">
                              <label for="box_size"><strong> Size of Box</strong>
                                <font color="#FF0000">*</font>
                              </label>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xl-4">
                              <input type="text" class="form-control" autocomplete="off" id="box_size<?=$data_key;?>"
                                name="box_size[]" placeholder="Box size"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                value="<?=$data[0]?>">
                            </div>

                            <div class="col-md-6 col-lg-6 col-xl-6">
                              <?php $email_type = array('mb'=>'MB','gb'=>'GB'); ?>
                              <select class="form-control" name="emailsizetype[]" id="emb<?=$data_key?>">
                                <?php foreach($email_type as $email_key=>$type){?>
                                <option value="<?=$email_key?>" <?=$email_key==$data[1] ? 'selected': ''?>><?=$type?>
                                </option>
                                <?php } ?>
                              </select>
                              <!-- <input type="radio" name="emailsizetype<?//=$data_key?>" id="emb" value="mb"><label
                                for="emb" <?//=$data[1] =='mb' ? 'checked' : ''?>>
                                &nbsp;MB</label>
                              <input type="radio" name="emailsizetype<?//=$data_key?>" id="egb" value="gb" checked><label
                                for="egb" <?//=$data[1] =='gb' ? 'checked' : ''?>>
                                &nbsp;GB</label>-->
                            </div>
                          </div>
                        </div>

                        <div class="col-md-3 col-lg-3 col-xl-3">
                          <div class="form-group">
                            <label for="sp"><strong>Service Provider</strong>
                              <font color="#FF0000">*</font>
                            </label>
                            <select name="emailservice[]" id="emailservice<?=$data_key;?>" class="form-control">
                              <option disabled selected>Select</option>
                              <?php
                              $serviceproviders = array('Rediffmail PRO','Rediff Enterprises Solution','Google Workspace','Netcore Solution','Microsoft');
                             //print_r($serviceprovider);die;
                             
                              foreach($serviceproviders as $service)
                              { 
                             ?>
                              <option value="<?=$service?>" <?=$data[2] ==$service ? 'selected' : ''?>><?=$service?>
                              </option>
                              <?php  }
                               ?>
                            </select>
                          </div>
                        </div>

                        <div class="col-md-2 col-lg-2 col-xl-2">
                          <div class="form-group">
                            <label for="box_size"><strong> Number of Emails</strong>
                              <font color="#FF0000">*</font>
                            </label>
                            <input type="text" class="form-control" id="numEmail<?=$data_key?>" name="numEmail[]"
                              onkeyup="emailprice()" placeholder="Number of Email"
                              oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                              value="<?=$data[3]?>">
                          </div>
                        </div>
                        <div class="col-md-2 col-lg-2 col-xl-2">
                          <div class="form-group">
                            <label for="cost"><strong>Cost/Email</strong>
                              <font color="#FF0000">*</font>
                            </label>
                            <input type="text" name="costpemail[]" id="costpemail<?=$data_key?>" onkeyup="emailprice()"
                              class="form-control price_class" placeholder="Enter Cost Per Email"
                              oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                              value="<?=$data[4]?>">
                          </div>
                        </div>
                        <!-- <div class="col-md-1 col-lg-1 col-xl-1 ">
                          <label for="cost"><strong>Action</strong>
                            <button type="button" name="add" id="add" class="btn btn-success btn_success mt-2"><i
                                class="fa fa-plus"></i></button>
                        </div> -->
                      </div>
                      <?php } } ?>
                      <div class="col-md-6 col-lg-6 col-xl-6">
                        <div class="form-group">
                          <label for="cost"><strong>Total Cost</strong></label>
                          <input type="text" name="tcostemail" id="tcostemail" class="form-control"
                            placeholder="Total Price" value="<?=$service_detail['Email']->email_total_price?>" readonly>
                        </div>
                      </div>
                      <input type="hidden" name="emailStatus" id="emailStatus"
                        value="<?=$service_detail['Email']->emailStatus?>">
                    </div>
                  </section>
                  <section id="hostingform">
                    <div class="row col-md-12 col-lg-12 col-xl-12">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">Hosting</h2>
                      <div class="col-md-12 col-lg-12 col-xl-12">
                        <div class="form-group row">
                          <div class="col-md-4 col-lg-4 col-xl-4" id="host-Domain">
                            <div class="form-group">
                              <label for="hostDomain"><strong>Domain Name</strong>
                                <font color="#FF0000">*</font>
                              </label>
                              <input type="text" class="form-control" name="hostDomain" id="hostDomain"
                                placeholder="Enter Domain Name"
                                value="<?=$service_detail['Hosting']->hosting_domain_name?>">
                            </div>
                          </div>
                          <div class="col-md-8 col-lg-8 col-xl-8 row form-group">
                            <div class="col-md-12 col-lg-12 col-xl-12">

                              <label for="hostingspace"><strong>Hosting Space</strong>
                                <font color="#FF0000">*</font>
                              </label>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xl-6">
                              <input type="text" class="form-control" id="hostingspace" name="hostingspace"
                                placeholder="Enter Hosting Space" autocomplete="off"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                value="<?=$service_detail['Hosting']->hosting_space?>">
                            </div>
                            <div class="col-md-6 col-lg-6 col-xl-6">
                              <?php $hosting_types = array('mb'=>'MB','gb'=>'GB');
                                foreach($hosting_types as $hosting_types_key=>$hosting_type){
                              ?>
                              <input type="radio" name="hspacetype" id="<?=$hosting_types_key?>"
                                <?=$hosting_types_key==$service_detail['Hosting']->hosting_space_type ? 'checked' : ''?>
                                value="<?=$hosting_types_key?>"><label for="<?=$hosting_types_key?>">
                                &nbsp;<?=$hosting_type?></label>
                              <?php } ?>
                            </div>
                            <!-- <div class="col-md-6 col-lg-6 col-xl-6">
                              <input type="radio" name="hspacetype" id="mb" value="mb"><label for="mb"> &nbsp;MB</label>
                              <input type="radio" name="hspacetype" id="gb" value="gb" checked><label for="gb">
                                &nbsp;GB</label>
                            </div> -->
                          </div>
                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="hpanel"><strong>Panel</strong>
                                <font color="#FF0000">*</font>
                              </label>
                              <!-- <input type="text" class="form-control" id="hpanel" name="hpanel" placeholder="Enter Panel" autocomplete="off" > -->
                              <select name="hpanel" id="hpanel" class="form-control" onchange="otherform()">
                                <option disabled selected>Select</option>
                                <?php
                                    $panels = array('Rediff','GoDaddy','Hostgator','Dedicated');
                                    $sno=1;
                                    foreach($panels as $panel)
                                    { 
                                      ?>
                                <option value="<?=$panel?>"
                                  <?=$service_detail['Hosting']->hosting_panel==$panel ? 'selected' : ''?>><?=$panel?>
                                </option>
                                <?php  }
                                            ?>
                                <option value="other" id="other"
                                  <?=$service_detail['Hosting']->hosting_panel=='other' ? 'selected' : ''?>>Other
                                </option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-4 col-lg-4 col-xl-4" id="otherform">
                            <div class="form-group">
                              <label for="other"><strong>Other Panel</strong>
                                <font color="#FF0000">*</font>
                              </label>
                              <input type="text" class="form-control" name="otherpanel"
                                value="<?=$service_detail['Hosting']->hosting_panel=='other' ? $service_detail['Hosting']->hosting_panel_name : ''?>"
                                placeholder="Enter panel name">
                            </div>
                          </div>
                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="host_booking_date"><strong>Booking Date</strong>
                                <font color="#FF0000">*</font>
                              </label>
                              <input type="date" class="form-control" id="host_booking_date" name="host_booking_date"
                                value="<?=date('Y-m-d',strtotime($service_detail['Hosting']->hosting_booking_date))?>">
                            </div>
                          </div>

                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="hostingyear"><strong>Renewal Year</strong></label>
                              <input type="text" class="form-control" id="hostingyear" name="hostingyear"
                                autocomplete="off" value="<?=$service_detail['Hosting']->hosting_renew_year?>"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                            </div>
                          </div>
                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="host_ren_date"><strong>Renewal Date </strong>
                                <font color="#FF0000">*</font>
                              </label>
                              <input type="date" class="form-control" id="host_ren_date" name="host_ren_date"
                                value="<?=date('Y-m-d'.strtotime($service_detail['Hosting']->hosting_renew_date))?>"
                                readonly>
                            </div>
                          </div>
                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="hostingcost"><strong>Cost/Year</strong></label>
                              <input type="text" class="form-control" id="hostingcost" name="hostingcost"
                                placeholder="Enter Hosting Cost" autocomplete="off"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                value="<?=$service_detail['Hosting']->hosting_price?>">
                            </div>
                          </div>
                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="thostingcost"><strong>Total Cost</strong></label>
                              <input type="text" class="form-control" id="thostingcost" name="thostingcost"
                                placeholder="Total Hosting Cost" readonly
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                value="<?=$service_detail['Hosting']->hosting_total_price?>">
                            </div>
                          </div>
                        </div>
                      </div>
                      <input type="hidden" name="hostingStatus" id="hostingStatus"
                        value="<?=$service_detail['Hosting']->hostingStatus?>">
                    </div>
                  </section>
                  <section id="sslform">
                    <div class="row col-md-12 col-lg-12 col-xl-12">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">SSL</h2>
                      <div class="col-md-12 col-lg-12 col-xl-12">
                        <div class="form-group row">
                          <div class="col-md-4 col-lg-4 col-xl-4" id="ssl-Domain">
                            <div class="form-group">
                              <label for="sslDomain"><strong>Domain Name</strong>
                                <font color="#FF0000">*</font>
                              </label>
                              <input type="text" class="form-control" name="sslDomain" id="sslDomain"
                                placeholder="Enter Domain Name" value="<?=$service_detail['SSL']->ssl_domain_name?>">
                            </div>
                          </div>
                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="ssl_booking_date"><strong>Booking Date</strong>
                                <font color="#FF0000">*</font>
                              </label>
                              <input type="date" class="form-control" id="ssl_booking_date" name="ssl_booking_date"
                                value="<?=$service_detail['SSL']->ssl_booking_date?>">
                            </div>
                          </div>

                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="sslyear"><strong>Renewal Year</strong></label>
                              <input type="text" class="form-control" id="sslyear" name="sslyear" value="1"
                                autocomplete="off"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                value="<?=$service_detail['SSL']->ssl_renew_year?>">
                            </div>
                          </div>
                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="ssl_ren_date"><strong>Renewal Date</strong>
                                <font color="#FF0000">*</font>
                              </label>
                              <input type="date" class="form-control" id="ssl_ren_date" name="ssl_ren_date"
                                value="<?=$service_detail['SSL']->ssl_renew_date?>" readonly>
                            </div>
                          </div>
                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="sslcost"><strong>Cost/Year</strong></label>
                              <input type="number" class="form-control" id="sslcost" name="sslcost"
                                placeholder="Enter SSL Cost" autocomplete="off"
                                value="<?=$service_detail['SSL']->ssl_price?>">
                            </div>
                          </div>
                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="tsslcost"><strong>Total Cost</strong></label>
                              <input type="text" class="form-control" id="tsslcost" name="tsslcost"
                                placeholder="Total SSL Cost" readonly
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                value="<?=$service_detail['SSL']->ssl_total_price?>">
                            </div>
                          </div>
                        </div>
                      </div>
                      <input type="hidden" name="sslStatus" id="sslStatus"
                        value="<?=$service_detail['SSL']->sslStatus?>">
                    </div>
                  </section>
                  <section id="websiteform">
                    <div class="row">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">Website Design</h2>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="urlwebdesign"><strong>URL For Website Design</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="urlwebdesign" name="urlwebdesign"
                            placeholder="Enter Website URL" autocomplete="off"
                            value="<?=$service_detail['Web Designing']->web_desigin_url?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="webcost"><strong>Price</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" name="webcost" id="webcost" class="form-control"
                            placeholder="Enter Price For Webdesign"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Web Designing']->web_desigin_price?>">
                        </div>
                      </div>
                      <input type="hidden" name="desiginStatus" id="desiginStatus"
                        value="<?=$service_detail['Web Designing']->desiginStatus?>">
                    </div>
                  </section>
                  <section id="softwareform">
                    <div class="row">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">Software Development</h2>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="softdetail"><strong>Detail</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="softdetail" name="softdetail"
                            placeholder="Enter Software Detail" autocomplete="off"
                            value="<?=$service_detail['Software Development']->Software_development_detail?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="softcost"><strong>Price</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" name="softcost" id="softcost" class="form-control"
                            onkeyup="Softwareprice()" placeholder="Enter Price Of Software"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Software Development']->Software_development_price?>">
                        </div>
                      </div>
                      <div class="col-md-2 col-lg-2 col-xl-2" style="padding-top: 20px; height: 75px;">
                        <div class="form-group">
                          <input type="checkbox" id="maint" name="maintenence"
                            <?=!empty($service_detail['Software Development']->Software_development_maintenence) ? 'checked' : ''?>
                            value="maintenence"><label for="maint">&nbsp;Maintenence</label>
                        </div>
                      </div>
                      <div id="maintenenceform" class="col-md-8 col-lg-8 col-xl-8 row"
                        style="display:none;margin: 0;padding: 0;">
                        <div class="col-md-6 col-lg-6 col-xl-6">
                          <div class="form-group">
                            <label for="maintcost"><strong>Maintenence Price/Year</strong>
                              <font color="#FF0000">*</font>
                            </label>
                            <input type="text" name="maintcost" id="maintcost" class="form-control"
                              onkeyup="Softwareprice()" placeholder="Enter Maintenence Price"
                              oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                              value="<?=$service_detail['Software Development']->Software_development_maintenence_price?>">
                          </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xl-6">
                          <div class="form-group">
                            <label for="maintyear"><strong>Maintenence Time (Year)</strong>
                              <font color="#FF0000">*</font>
                            </label>
                            <input type="text" name="maintyear" id="maintyear" class="form-control"
                              onkeyup="Softwareprice()" placeholder="Enter Maintenence Time In Year"
                              value="<?=$service_detail['Software Development']->Software_development_maintenence_year?>">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="tpricesoft"><strong>Total Price </strong></label>
                          <input type="text" name="tpricesoft" id="tpricesoft" class="form-control"
                            placeholder="Total Price" readonly
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Software Development']->Software_development_total_price?>">
                        </div>
                      </div>
                      <input type="hidden" name="developStatus" id="developStatus"
                        value="<?=$service_detail['Software Development']->developStatus?>">
                    </div>
                  </section>
                  <section id="maintenenceWebsiteForm">
                    <div class="row">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">Maintenence</h2>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="softdetail"><strong>Detail</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="websiteMaintenenceDetail"
                            name="websiteMaintenenceDetail" placeholder="Enter Software Detail" autocomplete="off"
                            value="<?=$service_detail['Maintenence']->websiteMaintenenceDetail?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="maintenencecost"><strong>Maintenence Price/Month</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" name="maintenencecost" id="maintenencecost" class="form-control"
                            onkeyup="maintenencePrice()" placeholder="Enter Price Of Software"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Maintenence']->maintenencecost?>">
                        </div>
                      </div>

                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="maintenenceyear"><strong>Maintenence Time (Month)</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" name="maintenenceyear" id="maintenenceyear" class="form-control"
                            onkeyup="maintenencePrice()" placeholder="Enter Maintenence Time In Month"
                            value="<?=$service_detail['Maintenence']->maintenenceyear?>">
                        </div>
                      </div>

                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="maintenencedate"><strong>Start Date</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="date" class="form-control" id="maintenencedate" name="maintenencedate"
                            onchange="maintenencePrice()" value="<?=$service_detail['Maintenence']->maintenencedate?>">
                        </div>
                      </div>

                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="maintenence_rDate"><strong>Renewal Date</strong></label>

                          <input type="date" class="form-control" id="maintenence_rDate" onkeyup="domainprice()"
                            name="maintenence_rDate"
                            value="<?=date("Y-m-d",strtotime($service_detail['Maintenence']->maintenence_rDate))?>"
                            readonly>
                        </div>
                      </div>

                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="tpricemaintenence"><strong>Total Price </strong></label>
                          <input type="text" name="tpricemaintenence" id="tpricemaintenence" class="form-control"
                            placeholder="Total Price" readonly
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Maintenence']->tpricemaintenence?>">
                        </div>
                      </div>
                      <input type="hidden" name="maintenenceStatus" id="maintenenceStatus"
                        value="<?=$service_detail['Maintenence']->tpricemaintenence?>">
                    </div>
                  </section>
                  <section id="whatsappform">
                    <div class="row col-md-12 col-lg-12 col-xl-12">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">Whatsapp</h2>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="nWhatsapp"><strong>Quantity Of Whatsapp</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="nWhatsapp" name="nWhatsapp"
                            onkeyup="whatsappprice()" placeholder="Enter Whatsapp" autocomplete="off"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Whatsapp']->whatsapp_qty?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="pWhatsapp"><strong>Price/Whatsapp</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="pWhatsapp" name="pWhatsapp"
                            onkeyup="whatsappprice()" placeholder="Enter Whatsapp Price" autocomplete="off"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Whatsapp']->whatsapp_price?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="twhatsprice"><strong>Total Price</strong></label>
                          <input type="text" class="form-control" id="twhatsprice" name="twhatsprice"
                            placeholder="Total Price" readonly
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Whatsapp']->whatsapp_qty*$service_detail['Whatsapp']->whatsapp_price?>">
                        </div>
                      </div>
                      <input type="hidden" name="whatsappStatus" id="whatsappStatus"
                        value="<?=$service_detail['Whatsapp']->whatsappStatus?>">
                    </div>
                  </section>
                  <section id="smsform">
                    <div class="row col-md-12 col-lg-12 col-xl-12">
                      <h2 class="col-md-12 text-center h4 border-bottom">SMS</h2>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="smsqty"><strong>SMS Quantinty</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="smsqty" name="smsqty" onkeyup="smsprice()"
                            placeholder="Enter SMS Quantinty" autocomplete="off"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['SMS']->sms_qty?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="pricesms"><strong>Price/SMS (Rupees)</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="pricesms" name="pricesms" onkeyup="smsprice()"
                            placeholder="Enter SMS Price (Rupees)" autocomplete="off"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['SMS']->sms_price?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="smsTotalprice"><strong>Total Price</strong></label>
                          <input type="text" class="form-control" id="smsTotalprice" name="smsTotalprice"
                            placeholder="Total Price (Rupees)" readonly
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['SMS']->sms_qty*$service_detail['SMS']->sms_price?>">
                        </div>
                      </div>
                      <input type="hidden" name="smsStatus" id="smsStatus"
                        value="<?=$service_detail['SMS']->smsStatus?>">
                    </div>
                  </section>
                  <section id="seoform">
                    <div class="row col-md-12 col-lg-12 col-xl-12">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">SEO</h2>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="seourl"><strong>URL</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="seourl" name="seourl"
                            placeholder="Enter URL For SEO" autocomplete="off"
                            value="<?=$service_detail['SEO']->seo_url?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="seocost"><strong>Monthly Cost</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="seocost" name="seocost"
                            placeholder="Enter Monthly Cost" autocomplete="off"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['SEO']->seo_price?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="seodate"><strong>Start Date</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="date" class="form-control" id="seodate" name="seodate" onchange="seoprice()"
                            value="<?=$service_detail['SEO']->seo_date?>">
                        </div>
                      </div>

                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="brand_name"><strong>Renewal Month</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" onkeyup="seoprice()" autocomplete="off" id="seo_month"
                            name="seo_month" onkeypress='validate(event)' value="<?=$service_detail['SEO']->seo_month?>"
                            placeholder="Enter Months">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="rDate"><strong>Renewal Date</strong></label>
                          <?php $StaringDate = strtotime(date("Y-m-d"));$newEndingDate =strtotime( "+ 1 month",$StaringDate);?>
                          <input type="date" class="form-control" id="seo_rDate" onkeyup="domainprice()"
                            name="seo_rDate" value="<?=date("Y-m-d",strtotime($service_detail['SEO']->seo_rDate))?>"
                            readonly>
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="seocost"><strong>Total Cost</strong></label>
                          <input type="text" class="form-control" id="tseocost" name="tseocost"
                            value="<?=$service_detail['SEO']->tseocost?>" readonly>
                        </div>
                      </div>
                      <input type="hidden" name="seoStatus" id="seoStatus"
                        value="<?=$service_detail['SEO']->seoStatus?>">
                    </div>
                  </section>
                  <section id="smoform">
                    <div class="row col-md-12 col-lg-12 col-xl-12">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">SMO</h2>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="smourl"><strong>URL</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="smourl" name="smourl"
                            placeholder="Enter URL For SMO" autocomplete="off"
                            value="<?=$service_detail['SMO']->smo_url?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="smocost"><strong>Monthly Cost</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="smocost" name="smocost"
                            placeholder="Enter Monthly Cost" autocomplete="off"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['SMO']->smo_price?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="smodate"><strong>Start Date</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="date" class="form-control" id="smodate" name="smodate" onchange="smoprice()"
                            value="<?=$service_detail['SMO']->smo_date?>">
                        </div>
                      </div>


                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="brand_name"><strong>Renewal Month</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" onkeyup="smoprice()" autocomplete="off" id="smo_month"
                            name="smo_month" onkeypress='validate(event)' value="<?=$service_detail['SMO']->smo_month?>"
                            placeholder="Enter Months">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="rDate"><strong>Renewal Date</strong></label>
                          <?php $StaringDate = strtotime(date("Y-m-d"));$newEndingDate =strtotime( "+ 1 month",$StaringDate);?>
                          <input type="date" class="form-control" id="smo_rDate" name="smo_rDate"
                            value="<?=date("Y-m-d",strtotime($service_detail['SMO']->smo_rDate))?>" readonly>
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="tsmocost"><strong>Total Cost</strong></label>
                          <input type="text" class="form-control" id="tsmocost" name="tsmocost"
                            value="<?=$service_detail['SMO']->tsmocost?>" readonly>
                        </div>
                      </div>
                      <input type="hidden" name="smoStatus" id="smoStatus"
                        value="<?=$service_detail['SMO']->smoStatus?>">
                    </div>
                  </section>
                  <section id="smmform">
                    <div class="row col-md-12 col-lg-12 col-xl-12">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">SMM</h2>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="smmurl"><strong>URL</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="smmurl" name="smmurl"
                            placeholder="Enter URL For SMM" autocomplete="off"
                            value="<?=$service_detail['SMM']->smm_url?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="smmcost"><strong>Monthly Cost</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="smmcost" name="smmcost"
                            placeholder="Enter Monthly Cost" autocomplete="off"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['SMM']->smm_price?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="smmdate"><strong>Start Date</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="date" class="form-control" id="smmdate" name="smmdate" onchange="smmprice()"
                            value="<?=date('Y-m-d',strtotime($service_detail['SMM']->smm_date))?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="brand_name"><strong>Renewal Month</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" onkeyup="smmprice()" autocomplete="off" id="smm_month"
                            name="smm_month" onkeypress='validate(event)' value="<?=$service_detail['SMM']->smm_month?>"
                            placeholder="Enter Months">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="rDate"><strong>Renewal Date</strong></label>
                          <?php $StaringDate = strtotime(date("Y-m-d"));$newEndingDate =strtotime( "+ 1 month",$StaringDate);?>
                          <input type="date" class="form-control" id="smm_rDate" name="smm_rDate"
                            value="<?=date('Y-m-d',strtotime($service_detail['SMM']->smm_rDate))?>" readonly>
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="tsmmcost"><strong>Total Cost</strong></label>
                          <input type="text" class="form-control" id="tsmmcost" name="tsmmcost"
                            value="<?=$service_detail['SMM']->tsmmcost?>" readonly>
                        </div>
                      </div>
                      <input type="hidden" name="smmStatus" id="smmStatus"
                        value="<?=$service_detail['SMM']->smmStatus?>">
                    </div>
                  </section>
                  <section id="ppcform">
                    <div class="row col-md-12 col-lg-12 col-xl-12">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">PPC</h2>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="ppcurl"><strong>URL</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="ppcurl" name="ppcurl"
                            placeholder="Enter URL For PPC" autocomplete="off"
                            value="<?=$service_detail['PPC']->ppc_url?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="ppccost"><strong>Monthly Cost</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="ppccost" name="ppccost"
                            placeholder="Enter Monthly Cost" autocomplete="off"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['PPC']->ppc_price?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="ppcdate"><strong>Start Date</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="date" class="form-control" id="ppcdate"
                            value="<?=date('Y-m-d',strtotime($service_detail['PPC']->ppc_date))?>" name="ppcdate"
                            onchange="ppcprice()">
                        </div>

                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="brand_name"><strong>Renewal Month</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" onkeyup="ppcprice()" autocomplete="off" id="ppc_month"
                            name="ppc_month" onkeypress='validate(event)' value="<?=$service_detail['PPC']->ppc_month?>"
                            placeholder="Enter Months">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="ppc_rDate"><strong>Renewal Date</strong></label>
                          <?php $StaringDate = strtotime(date("Y-m-d"));$newEndingDate =strtotime( "+ 1 month",$StaringDate);?>
                          <input type="date" class="form-control" id="ppc_rDate" name="ppc_rDate"
                            value="<?=date('Y-m-d',strtotime($service_detail['PPC']->ppc_rDate))?>" readonly>
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="tppccost"><strong>Total Cost</strong></label>
                          <input type="text" class="form-control" id="tppccost" name="tppccost"
                            value="<?=$service_detail['PPC']->tppccost?>" readonly>
                        </div>
                        <input type="hidden" name="ppcStatus" id="ppcStatus"
                          value="<?=$service_detail['PPC']->ppcStatus?>">
                      </div>
                  </section>

                  <section id="massEmailform">
                    <div class="row col-md-12 col-lg-12 col-xl-12">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">Mass Email</h2>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="massEmail_domain"><strong>Domain Name</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="massEmail_domain" name="massEmail_domain"
                            placeholder="Enter Domain for Mass Email"
                            value="<?=$service_detail['Mass Email']->mass_email_domain?>" autocomplete="off">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="no_of_email"><strong>Quantity Of Email</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="no_of_email" name="no_of_email"
                            onkeyup="massemailprice()" placeholder="Enter Quantity Of Email" autocomplete="off"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Mass Email']->no_of_email?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="massEmailcost"><strong>Price</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="massEmailcost" name="massEmailcost"
                            onkeyup="massemailprice()" placeholder="Enter MassEmail Price" autocomplete="off"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Mass Email']->mass_email_price?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="t_mass_email_ccost"><strong>Total Price</strong></label>
                          <input type="text" class="form-control" id="t_mass_email_ccost" name="t_mass_email_ccost"
                            placeholder="Total Price" readonly
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Mass Email']->t_mass_email_ccost?>">
                        </div>
                      </div>

                      <input type="hidden" name="mass_email_Status" id="mass_email_Status"
                        value="<?=$service_detail['Mass Email']->mass_email_Status?>">
                  </section>
                  <section id="otherDetailform">
                    <div class="row col-md-12 col-lg-12 col-xl-12">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">Other</h2>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="other_detail"><strong>Other Detail</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="other_detail" name="other_detail"
                            placeholder="Enter Other Detail" autocomplete="off"
                            value="<?=$service_detail['Other']->other_detail?>">
                        </div>
                      </div>

                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="no_of_other"><strong>Quantity</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="no_of_other" name="no_of_other"
                            onkeyup="otherprice()" placeholder="Enter Quantity" autocomplete="off"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Other']->no_of_other?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="othercost"><strong>Price</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="othercost" name="othercost" onkeyup="otherprice()"
                            placeholder="Enter Other Price" autocomplete="off"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Other']->othercost?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="t_other_ccost"><strong>Total Price</strong></label>
                          <input type="text" class="form-control" id="t_other_ccost" name="t_other_ccost"
                            placeholder="Total Price" readonly
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Other']->t_other_ccost?>">
                        </div>
                      </div>


                      <input type="hidden" name="other_Status" id="other_Status"
                        value="<?=$service_detail['Other']->other_Status?>">
                  </section>
                  <div class="text-center">
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<script>
function validate(evt) {
  var theEvent = evt || window.event;

  // Handle paste
  if (theEvent.type === 'paste') {
    key = event.clipboardData.getData('text/plain');
  } else {
    // Handle key press
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
  }
  var regex = /[0-9]|\./;
  if (!regex.test(key)) {
    theEvent.returnValue = false;
    if (theEvent.preventDefault) theEvent.preventDefault();
  }
}

var checked = false;

function checkedAll() {
  var aa = document.getElementsByName("services[]");
  checked = document.getElementById('select_all').checked;

  for (var i = 0; i < aa.length; i++) {
    aa[i].checked = checked;
  }
}
</script>
<script type="application/javascript">
function domainprice() {
  var price = document.getElementById('price').value;
  var year = document.getElementById('year').value;
  var totalprice = price + +year;
  document.getElementById('dtotalprice').value = totalprice;
}

function Softwareprice() {
  var price = document.getElementById('softcost').value;
  var maintyear = document.getElementById('maintyear').value;
  var maintcost = document.getElementById('maintcost').value;
  var maint_price = maintyear * maintcost;
  var total_price = maint_price + +price;
  document.getElementById('tpricesoft').value = total_price;
}

function maintenencePrice() {

  let month = document.getElementById('maintenenceyear').value;
  let maintcost = document.getElementById('maintenencecost').value;
  let date = document.getElementById('maintenencedate').value;
  let ren_date = moment(date).add(month, "Months").format("YYYY-MM-DD")
  document.getElementById('maintenence_rDate').value = ren_date;
  let total_price = month * maintcost;
  document.getElementById('tpricemaintenence').value = total_price;
}

function whatsappprice() {
  var wqnty = document.getElementById('nWhatsapp').value;
  var price = document.getElementById('pWhatsapp').value;

  var tprice = wqnty * price;
  //alert(tprice);
  document.getElementById('twhatsprice').value = tprice;
}

function smsprice() {
  var smsqty = document.getElementById('smsqty').value;
  var pricesms = document.getElementById('pricesms').value;

  var tprice = smsqty * pricesms;
  //alert(tprice);
  document.getElementById('smsTotalprice').value = tprice;
}

$('#seocost').on('keyup', function() {
  seoprice();
  //alert(rendate);
});

function seoprice() {
  var price = document.getElementById('seocost').value;
  var month = document.getElementById('seo_month').value;
  var date = document.getElementById('seodate').value;
  var totalprice = price * month;
  var ren_date = moment(date).add(month, "Months").format("YYYY-MM-DD")
  document.getElementById('seo_rDate').value = ren_date;
  document.getElementById('tseocost').value = totalprice;
}

$('#smocost').on('keyup', function() {
  smoprice();
  //alert(rendate);
});

function smoprice() {
  var price = document.getElementById('smocost').value;
  var month = document.getElementById('smo_month').value;
  var date = document.getElementById('smodate').value;
  var totalprice = price * month;
  var ren_date = moment(date).add(month, "Months").format("YYYY-MM-DD")
  document.getElementById('smo_rDate').value = ren_date;
  document.getElementById('tsmocost').value = totalprice;
}

$('#smmcost').on('keyup', function() {
  smmprice();
  //alert(rendate);
});

function smmprice() {
  var price = document.getElementById('smmcost').value;
  var month = document.getElementById('smm_month').value;
  var date = document.getElementById('smmdate').value;
  var totalprice = price * month;
  var ren_date = moment(date).add(month, "Months").format("YYYY-MM-DD")
  document.getElementById('smm_rDate').value = ren_date;
  document.getElementById('tsmmcost').value = totalprice;
}

$('#ppccost').on('keyup', function() {
  ppcprice();
  //alert(rendate);
});

function ppcprice() {
  var price = document.getElementById('ppccost').value;
  var month = document.getElementById('ppc_month').value;
  var date = document.getElementById('ppcdate').value;
  var totalprice = price * month;
  var ren_date = moment(date).add(month, "Months").format("YYYY-MM-DD")
  document.getElementById('ppc_rDate').value = ren_date;
  document.getElementById('tppccost').value = totalprice;
}

$('#massEmailcost').on('keyup', function() {
  massemailprice();
  //alert(rendate);
});

function massemailprice() {
  // var price = document.getElementById('massEmailcost').value;
  // var month = document.getElementById('mass_email_month').value;
  // var date = document.getElementById('massEmailDate').value;
  // var totalprice = price * month;
  // var ren_date = moment(date).add(month, "Months").format("YYYY-MM-DD")
  // document.getElementById('mass_email_rDate').value = ren_date;
  // document.getElementById('t_mass_email_ccost').value = totalprice;

  var no_of_email = document.getElementById('no_of_email').value;
  var massEmailcost = document.getElementById('massEmailcost').value;

  var t_mass_email_ccost = no_of_email * massEmailcost;
  //alert(tprice);
  document.getElementById('t_mass_email_ccost').value = t_mass_email_ccost;
}

function otherprice() {

  var no_of_other = document.getElementById('no_of_other').value;
  var othercost = document.getElementById('othercost').value;

  var t_other_ccost = no_of_other * othercost;

  document.getElementById('t_other_ccost').value = t_other_ccost;
}

// $(function() {
//   $('input[name="Q_date"]').daterangepicker({
//     singleDatePicker: true,
//     showDropdowns: true,
//     minYear: 1901,
//     maxYear: parseInt(moment().format('YYYY'), 10)
//   });
// });
</script>

<script>
$(document).ready(function() {
  var i = 1;
  $('#add').click(function() {
    i++;
    document.getElementById("emailcounter").value = Number(i);

    $('#rows').append('<div id="row' + i +
      '" class="dynamic-added row col-md-12" style="margin-bottom:10px"><div class="col-md-3 col-lg-3 col-xl-3 row"><div class="form-group row"> <div class="col-md-12"> <label for="box_size"><strong> Size of Box</strong><font color="#FF0000">*</font></label> </div> <div class="col-md-4 col-lg-4 col-xl-4"> <input type="number" class="form-control" autocomplete="off" id="box_size' +
      i +
      '" name="box_size[]" placeholder="Box size" > </div> <div class="col-md-6 col-lg-6 col-xl-6"> <input type="radio" name="emailsizetype' +
      i +
      '[]" id="emb' +
      i +
      '" value="mb"> <label for="emb"> &nbsp;MB</label> <input type="radio" name="emailsizetype' +
      i +
      '[]" id="egb' +
      i +
      '" value="gb" checked> <label for="egb"> &nbsp;GB </label> </div> </div> </div> <div class="col-md-3 col-lg-3 col-xl-3"> <div class="form-group"> <label for="sp"> <strong> Service Provider </strong> <font color="#FF0000">*</font> </label> <select name="emailservice[]" id="emailservice' +
      i +
      '" class="form-control"><option value="">Select</option><option value="Rediffmail PRO">Rediffmail PRO</option><option value="Rediff Enterprises Solution">Rediff Enterprises Solution</option><option value="Google Workspace">Google Workspace</option><option value="Netcore Solution">Netcore Solution</option><option value="Microsoft">Microsoft</option></select></div></div><div class="col-md-2 col-lg-2 col-xl-2"><div class="form-group"><label for="box_size"><strong> Number of Emails</strong><font color="#FF0000">*</font></label><input type="number" class="form-control" id="numEmail' +
      i +
      '" name="numEmail[]" onkeyup="emailprice()" placeholder="Number of Email" > </div> </div> <div class="col-md-2 col-lg-2 col-xl-2"><div class="form-group"><label for="cost"><strong>Cost/Email</strong><font color="#FF0000">*</font></label><input type="number" name="costpemail[]" id="costpemail' +
      i +
      '" onkeyup="emailprice()" class="form-control price_class" placeholder="Enter Cost Per Email" ></div></div><div class="col-md-1 col-lg-1 col-xl-1 "><label for="cost"><strong>Action</strong><button type="button" name="remove" id="' +
      i + '" class="btn btn-danger btn_remove"><i class="fa fa-times"></button></div></div>');
  });
});
$(document).on('click', '.btn_remove', function() {
  var button_id = $(this).attr("id");
  var j = new $('#emailcounter').val();
  j--;
  document.getElementById("emailcounter").value = Number(j);
  $('#row' + button_id + '').remove();

  emailprice();
});
</script>
<script type="text/javascript">
$("#sale_total_order_value").keyup(function() {
  // alert('qqqqqq');
  var total_value = Number(this.value);
  var advance_payment = Number($('#sale_advance_payment').val());
  var gst_amount = Number($('#gst_amount').val());
  var pending_amount = (total_value + gst_amount) - advance_payment;
  $('#sale_pending_payment').val(pending_amount);
});

(function() {
  var url = window.location.pathname;;
  var id = url.substring(url.lastIndexOf('/') + 1);
  document.getElementById('sale_id').value = id;
})();
(function() {
  $('#booking_date').on('input', function() {
    var date = new Date($('#booking_date').val());
    var yearval = new $('#year').val();
    if (yearval == '') {
      year = date.getFullYear() + 1 + +yearval;
    } else {
      year = date.getFullYear() + +yearval;
    }
    let days = date.getDate();
    let count_day = String(days).length;
    let day = count_day == 1 ? '0' + days : days;
    month = String(date.getMonth() + 1).padStart(2, '0');
    year = date.getFullYear() + +yearval;
    //String(date.getMonth() + 1).padStart(2, '0');
    var rendate = [year, month, day].join('-');
    document.getElementById('rDate').value = rendate;
    //alert(rendate);
  });
  $('#booking_date').on('input', function() {
    var date = new Date($('#booking_date').val());
    var yearval = new $('#year').val();
    if (yearval == '') {
      year = date.getFullYear() + 1 + +yearval;
    } else {
      year = date.getFullYear() + +yearval;
    }
    let days = date.getDate();
    let count_day = String(days).length;
    let day = count_day == 1 ? '0' + days : days;
    month = String(date.getMonth() + 1).padStart(2, '0');
    year = date.getFullYear() + +yearval;
    //String(date.getMonth() + 1).padStart(2, '0');
    var rendate = [year, month, day].join('-');
    console.log(rendate);
    document.getElementById('rDate').value = rendate;
    //alert(rendate);
  });
  $('#year').on('keyup', function() {
    var date = new Date($('#booking_date').val());
    var yearval = new $('#year').val();
    if (yearval == '') {
      year = date.getFullYear() + 1 + +yearval;
    } else {
      year = date.getFullYear() + +yearval;
    }
    let days = date.getDate();
    let count_day = String(days).length;
    let day = count_day == 1 ? '0' + days : days;
    month = String(date.getMonth() + 1).padStart(2, '0');

    //String(date.getMonth() + 1).padStart(2, '0');
    var rendate = [year, month, day].join('-');
    document.getElementById('rDate').value = rendate;
    //alert(rendate);
  });

  $('#year').on('keyup', function() {
    var price = new Number($('#price').val());
    var yearval = new $('#year').val();
    var totalprice = price * yearval;
    document.getElementById('dtotalprice').value = totalprice;
  });
  $('#price').on('keyup', function() {
    var price = new Number($('#price').val());
    var yearval = new $('#year').val();
    var totalprice = price * yearval;
    document.getElementById('dtotalprice').value = totalprice;
  });
})();

(function() {
  $('#host_booking_date').on('input', function() {
    var date = new Date($('#host_booking_date').val());
    var yearval = new $('#hostingyear').val();
    if (yearval == '') {
      year = date.getFullYear() + 1 + +yearval;
    } else {
      year = date.getFullYear() + +yearval;
    }
    let days = date.getDate();
    let count_day = String(days).length;
    let day = count_day == 1 ? '0' + days : days;
    month = String(date.getMonth() + 1).padStart(2, '0');
    year = date.getFullYear() + +yearval;
    //String(date.getMonth() + 1).padStart(2, '0');
    var rendate = [year, month, day].join('-');
    document.getElementById('host_ren_date').value = rendate;
    //alert(rendate);
  });
  $('#host_booking_date').on('input', function() {
    var date = new Date($('#host_booking_date').val());
    var yearval = new $('#hostingyear').val();
    if (yearval == '') {
      year = date.getFullYear() + 1 + +yearval;
    } else {
      year = date.getFullYear() + +yearval;
    }
    let days = date.getDate();
    let count_day = String(days).length;
    let day = count_day == 1 ? '0' + days : days;
    month = String(date.getMonth() + 1).padStart(2, '0');
    year = date.getFullYear() + +yearval;
    //String(date.getMonth() + 1).padStart(2, '0');
    var rendate = [year, month, day].join('-');
    document.getElementById('host_ren_date').value = rendate;
    //alert(rendate);
  });
  $('#hostingyear').on('keyup', function() {
    var date = new Date($('#host_booking_date').val());
    var hyearval = new $('#hostingyear').val();
    if (hyearval == '') {
      hyear = date.getFullYear() + 1 + +hyearval;
    } else {
      hyear = date.getFullYear() + +hyearval;
    }
    day = date.getDate();
    month = String(date.getMonth() + 1).padStart(2, '0');

    //String(date.getMonth() + 1).padStart(2, '0');
    var rendate = [hyear, month, day].join('-');
    document.getElementById('host_ren_date').value = rendate;
    //alert(rendate);
  });

  $('#hostingyear').on('keyup', function() {
    var hostprice = new Number($('#hostingcost').val());
    var hyearval = new $('#hostingyear').val();
    var htotalprice = hostprice * hyearval;
    document.getElementById('thostingcost').value = htotalprice;
  });
  $('#hostingcost').on('keyup', function() {
    var hostprice = new Number($('#hostingcost').val());
    var hyearval = new $('#hostingyear').val();
    var htotalprice = hostprice * hyearval;
    document.getElementById('thostingcost').value = htotalprice;
  });
})();

(function() {
  $('#ssl_booking_date').on('input', function() {
    var date = new Date($('#ssl_booking_date').val());
    var yearval = new $('#sslyear').val();
    if (yearval == '') {
      year = date.getFullYear() + 1 + +yearval;
    } else {
      year = date.getFullYear() + +yearval;
    }
    let days = date.getDate();
    let count_day = String(days).length;
    let day = count_day == 1 ? '0' + days : days;
    month = String(date.getMonth() + 1).padStart(2, '0');
    year = date.getFullYear() + +yearval;
    //String(date.getMonth() + 1).padStart(2, '0');
    var rendate = [year, month, day].join('-');
    document.getElementById('ssl_ren_date').value = rendate;
    //alert(rendate);
  });
  $('#ssl_booking_date').on('input', function() {
    var date = new Date($('#ssl_booking_date').val());
    var yearval = new $('#sslyear').val();
    if (yearval == '') {
      year = date.getFullYear() + 1 + +yearval;
    } else {
      year = date.getFullYear() + +yearval;
    }
    let days = date.getDate();
    let count_day = String(days).length;
    let day = count_day == 1 ? '0' + days : days;
    month = String(date.getMonth() + 1).padStart(2, '0');
    year = date.getFullYear() + +yearval;
    //String(date.getMonth() + 1).padStart(2, '0');
    var rendate = [year, month, day].join('-');
    document.getElementById('ssl_ren_date').value = rendate;
    //alert(rendate);
  });
  $('#sslyear').on('keyup', function() {
    var date = new Date($('#ssl_booking_date').val());
    var syearval = new $('#sslyear').val();
    if (syearval == '') {
      syear = date.getFullYear() + 1 + +syearval;
    } else {
      syear = date.getFullYear() + +syearval;
    }
    let days = date.getDate();
    let count_day = String(days).length;
    let day = count_day == 1 ? '0' + days : days;
    month = String(date.getMonth() + 1).padStart(2, '0');

    //String(date.getMonth() + 1).padStart(2, '0');
    var rendate = [syear, month, day].join('-');
    document.getElementById('ssl_ren_date').value = rendate;
    //alert(rendate);
  });

  $('#sslyear').on('keyup', function() {
    var sslprice = new Number($('#sslcost').val());
    var syearval = new $('#sslyear').val();
    var stotalprice = sslprice * syearval;
    document.getElementById('tsslcost').value = stotalprice;
  });
  $('#sslcost').on('keyup', function() {
    var sslprice = new Number($('#sslcost').val());
    var syearval = new $('#sslyear').val();
    var stotalprice = sslprice * syearval;
    document.getElementById('tsslcost').value = stotalprice;
  });
})();
$("form#serviceEditForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("serviceID", <?=$serviceID?>);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {
          location.href = "<?= base_url('services')?>";
        }, 1000)

      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});
</script>

<script>
$(document).ready(function() {
  $('#show-services').click(function(event) {
    event.stopPropagation();
    $("#service-list").slideToggle("fast");
  });
  $("#service-list").on("click", function(event) {
    event.stopPropagation();
  });
});

$(document).on("click", function() {
  $("#service-list").hide();
});
// $(document).on('click', '#show-services', function(){ 
//  $("#service-list").toggle(this.show);
// });
</script>
<script>
$("#show-services").on("click", function() {
  if ($('input#domain').is(':checked')) {
    $("#domainform").show();
  } else {
    $("#domainform").hide();
  }

  if ($('input#email').is(':checked')) {
    $("#emailform").show();
  } else {
    $("#emailform").hide();
  }

  if ($('input#hosting').is(':checked')) {
    $("#hostingform").show();
  } else {
    $("#hostingform").hide();
  }

  if ($('input#ssl').is(':checked')) {
    $("#sslform").show();
  } else {
    $("#sslform").hide();
  }

  if ($('input#webDesigning').is(':checked')) {
    $("#websiteform").show();
  } else {
    $("#websiteform").hide();
  }

  if ($('input#softwareDevelopment').is(':checked')) {
    $("#softwareform").show();
  } else {
    $("#softwareform").hide();
  }
  if ($('input#whatsapp').is(':checked')) {
    $("#whatsappform").show();
  } else {
    $("#whatsappform").hide();
  }
  if ($('input#sms').is(':checked')) {
    $("#smsform").show();
  } else {
    $("#smsform").hide();
  }
  if ($('input#seo').is(':checked')) {
    $("#seoform").show();
  } else {
    $("#seoform").hide();
  }

  if ($('input#smo').is(':checked')) {
    $("#smoform").show();
  } else {
    $("#smoform").hide();
  }
  if ($('input#sms').is(':checked')) {
    $("#smmform").show();
  } else {
    $("#smmform").hide();
  }
  if ($('input#ppc').is(':checked')) {
    $("#ppcform").show();
  } else {
    $("#ppcform").hide();
  }

  if ($('input#massEmail').is(':checked')) {
    $("#massEmailform").show();
  } else {
    $("#massEmailform").hide();
  }

  if ($('input#other').is(':checked')) {
    $("#otherDetailform").show();
  } else {
    $("#otherDetailform").hide();
  }

  if ($('input#maintenence').is(':checked')) {
    $("#maintenenceWebsiteForm").show();
  } else {
    $("#maintenenceWebsiteForm").hide();
  }
});
// Form hide/show js
$(document).ready(function() {
  $('#domain').click(function() {
    $("#domainform").toggle(this.checked);
  });
});
$(document).ready(function() {
  $('#domain').click(function() {
    if ($('input#domain').is(':checked')) {
      $("#email-domain").hide();
    } else {
      $("#email-domain").show();
    }
  });
  $('#email').click(function() {
    $("#emailform").toggle(this.checked);
    if ($('input#domain').is(':checked')) {
      $("#email-domain").hide();
    } else {
      $("#email-domain").show();
    }
  });

});

$(document).ready(function() {
  $('#domain').click(function() {
    if ($('input#domain').is(':checked')) {
      $("#host-Domain").hide();
    } else {
      $("#host-Domain").show();
    }
  });
  $('#hosting').click(function() {
    $("#hostingform").toggle(this.checked);
    if ($('input#domain').is(':checked')) {
      $("#host-Domain").hide();
    } else {
      $("#host-Domain").show();
    }
  });

});

$(document).ready(function() {
  $('#domain').click(function() {
    if ($('input#domain').is(':checked')) {
      $("#ssl-Domain").hide();
    } else {
      $("#ssl-Domain").show();
    }
  });
  $('#ssl').click(function() {
    $("#sslform").toggle(this.checked);
    if ($('input#domain').is(':checked')) {
      $("#ssl-Domain").hide();
    } else {
      $("#ssl-Domain").show();
    }
  });

});


/////////////get price per email////////////////////
function emailprice() {
  let counter = new Number($('#emailcounter').val());

  let text = <?php echo $sno;?>;
  //alert(text);die;
  let alltotal = 0;

  var email_year = document.getElementById('email_year').value;

  for (let j = 1; j <= counter; j++) {
    var totalcost = new Array();
    for (let i = text; i <= counter; i++) {
      var numid = 'numEmail' + i;
      var costid = 'costpemail' + i;
      var emailnum = new Number($("#" + numid).val());
      var emailcost = new Number($("#" + costid).val());
      totalcost[i] = emailnum * emailcost;
    }
    alltotal += totalcost[j];
  }
  document.getElementById('tcostemail').value = alltotal * email_year;
  // alert(alltotal);
}
</script>
<script>
$('#email_booking_date').on('input', function() {
  var date = new Date($('#email_booking_date').val());
  var yearval = new $('#email_year').val();
  if (yearval == '') {
    year = date.getFullYear() + 1 + +yearval;
  } else {
    year = date.getFullYear() + +yearval;
  }
  let days = date.getDate();
  let count_day = String(days).length;
  let day = count_day == 1 ? '0' + days : days;
  month = String(date.getMonth() + 1).padStart(2, '0');
  year = date.getFullYear() + +yearval;
  //String(date.getMonth() + 1).padStart(2, '0');
  var rendate = [year, month, day].join('-');
  document.getElementById('email_rDate').value = rendate;
  //alert(rendate);
});

$('#email_year').on('keyup', function() {
  var date = new Date($('#email_booking_date').val());
  var yearval = new $('#email_year').val();
  if (yearval == '') {
    year = date.getFullYear() + 1 + +yearval;
  } else {
    year = date.getFullYear() + +yearval;
  }
  let days = date.getDate();
  let count_day = String(days).length;
  let day = count_day == 1 ? '0' + days : days;
  month = String(date.getMonth() + 1).padStart(2, '0');

  //String(date.getMonth() + 1).padStart(2, '0');
  var rendate = [year, month, day].join('-');
  document.getElementById('email_rDate').value = rendate;
  //alert(rendate);
});
</script>


<script>
/////////////get price per email////////////////////
$(document).ready(function() {
  $('#hosting').click(function() {
    $("#hostingform").toggle(this.checked);
  });
});

$(document).ready(function() {
  $('#ssl').click(function() {
    $("#sslform").toggle(this.checked);
  });
});
$(document).ready(function() {
  $('#softwareDevelopment').click(function() {
    $("#softwareform").toggle(this.checked);
  });
});
$(document).ready(function() {
  $('#webDesigning').click(function() {
    $("#websiteform  ").toggle(this.checked);
  });
});
$(document).ready(function() {
  $('#whatsapp').click(function() {
    $("#whatsappform").toggle(this.checked);
  });
});
$(document).ready(function() {
  $('#sms').click(function() {
    $("#smsform").toggle(this.checked);
  });
});
$(document).ready(function() {
  $('#seo').click(function() {
    $("#seoform").toggle(this.checked);
  });
});
$(document).ready(function() {
  $('#smo').click(function() {
    $("#smoform").toggle(this.checked);
  });
});
$(document).ready(function() {
  $('#smm').click(function() {
    $("#smmform").toggle(this.checked);
  });
});
$(document).ready(function() {
  $('#ppc').click(function() {
    $("#ppcform").toggle(this.checked);
  });
});
$(document).ready(function() {
  $('#massEmail').click(function() {
    $("#massEmailform").toggle(this.checked);
  });
});

$(document).ready(function() {
  $('#other').click(function() {
    $("#otherDetailform").toggle(this.checked);
  });
});

$(document).ready(function() {
  $('#maintenence').click(function() {
    $("#maintenenceWebsiteForm").toggle(this.checked);
  });
});
</script>
<script>
$(function() {
  $("#maint").on("click", function() {
    $("#maintenenceform").toggle(this.checked);
  });

});

function otherform() {
  if ($('#hpanel').val() == 'other') {
    $("#otherform").show();
  } else {
    $("#otherform").hide();
  }
}
</script>
<script>
function view_services(id) {
  // alert(id);
  $.ajax({
    url: '<?=base_url('Product_Form_Controller/get_product_detail/')?>' + id,
    type: 'post',
    dataType: 'html',
    success: function(response) {
      // alert(response);
      //console.log(response);
      $('#viewServices_detail').html(response);
      togglePay();
    }
  });
}
</script>