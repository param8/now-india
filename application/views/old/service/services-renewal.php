<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">

      <div class="page-header">
        <div class="row">
          <div class="col-sm-6">
            <h3 class="page-title"><?=$page_title?></h3>
            <ul class="breadcrumb">
              <li><a href="<?=base_url('dashboard')?>">Dashboard/</a></li>
              <li class="breadcrumb-item"><a href="javascript:(0);"><?=$page_title?></a></li>
            </ul>
          </div>
          <?php if($this->session->userdata('user_type')!=1 && $permission=='Add'){?>
          <!-- <div class="col-sm-6">
            <div class="float-right">
              <a type="button" class="btn btn-primary btn-sm" href="<?//=base_url('create-customer')?>"
                style="float: right">Create <?//=$page_title?></a>
            </div>
          </div> -->
          <?php } ?>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-body">
              <div class="text-center text-danger">
                <?//=$message?>
              </div>
              <?php if($this->session->userdata('user_type')==1){?>
              <div class="row">
                <div class="col-sm-6">
                  <select class="form-control" name="serviceAdminID" id="serviceAdminID"
                    onchange="getServiceAdminSession(this.value)">
                    <option value="">Select Admin</option>
                    <?php foreach($admins as $admin){?>
                    <option value="<?=$admin->id?>"
                      <?=$admin->id == $this->session->userdata('serviceAdminID') ? 'selected' : ''?>><?=$admin->name?>
                    </option>
                    <?php } ?>
                  </select>

                </div>

              </div>
              <hr>
              <?php } ?>
              <div class="table-responsive">
                <table class=" table table-hover table-center mb-0" id="serviceRenewalDataTable">
                  <thead>
                    <tr>
                      <th>S.no.</th>
                      <th>Domain / Url</th>
                      <th>Product</th>
                      <th>Expiry Date</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                     $sno = 1;
                    foreach($services as $key=>$renew){?>
                  <tr>
                    <td><?=$sno++?></td>
                    <td><?=$renew[1]?></td>
                    <td><?=$renew[2]?></td>
                    <td><?=$renew[3]?></td>
                    <td><?=$renew[4]?></td>
                    <td><?=$renew[5]?></td>
                    </tr>
                    <?php } ?>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="servicesModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-wrench text-warning"></i> <?=$page_title?>
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="close_modal()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="viewServices_detail" style="background-color:#f8f9fa">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn  btn-outline-danger " onclick="close_modal()" data-dismiss="modal"
          aria-label="Close">Close <i class="fa fa-close"></i></button>
      </div>

    </div>
  </div>
</div>


<script>
$(document).ready(function() {
  var dataTable = $('#serviceRenewalDataTable').DataTable({
    "processing": true,
    "serverSide": false,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Download Excel'
    }],
    "order": [],
    
    "columnDefs": [{
      "targets": [0],
      "orderable": true,
    }, ],
  });
});




function getServiceAdminSession(id) {
  $.ajax({
    url: "<?=base_url('Ajax_controller/getServiceAdminSession')?>",
    type: 'POST',
    data: {
      id
    },
    success: function(data) {
      location.reload();
    },

  });
}

function view_services(id) {
  $.ajax({
    url: '<?=base_url('Services/get_renew_service_detail')?>',
    type: 'post',
    data: {
      id
    },
    dataType: 'html',
    success: function(response) {
      $('#servicesModel').modal('show');
      $('#viewServices_detail').html(response);
      togglePay();
    }
  });
}

function close_modal() {
  $('#servicesModel').modal('hide');
}
</script>