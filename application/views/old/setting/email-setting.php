<div class="card">


  <div class="content container-fluid">

    <div class="page-header mt-2">
      <div class="row">
        <div class="col-sm-6">
          <h3 class="page-title"><?=$page_title?></h3>
          <ul class="breadcrumb">
            <li><a href="<?=base_url('dashboard')?>">Dashboard/</a></li>
            <li class="breadcrumb-item"><a href="javascript:(0);"><?=$page_title?></a></li>
          </ul>
        </div>
        <?php if($this->session->userdata('user_type')!=1 && $permission=='Add'){?>
        <div class="col-sm-6">
          <div class="float-right">
            <a type="button" class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="addEmailSettingModal()"
              style="float: right">Create <?=$page_title?></a>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-body">
            <?php if($this->session->userdata('user_type')==1){?>
            <div class="row">
              <div class="col-sm-6">
                <select class="form-control" name="roleAdminID" id="roleAdminID"
                  onchange="getroleAdminSession(this.value)">
                  <option value="">Select Admin</option>
                  <?php foreach($admins as $admin){?>
                  <option value="<?=$admin->id?>"
                    <?=$admin->id == $this->session->userdata('roleAdminID') ? 'selected' : ''?>><?=$admin->name?>
                  </option>
                  <?php } ?>
                </select>

              </div>

            </div>
            <hr>
            <?php } ?>

            <div class="table-responsive">
              <table class=" table table-hover table-center mb-0" id="emailSettingDataTable">
                <thead>
                  <tr>
                    <th>S.no.</th>
                    <th>Action</th>
                    <th>Type</th>
                    <th>Assigin Email</th>
                    <th>Subject</th>
                    <th>Created_at</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade bd-example-modal-lg" id="addEmailSetting" tabindex="-1" role="dialog"
  aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-plus text-success"></i> Add <?=$page_title?>
        </h5>
        <button type="button" class="close" data-dismiss="modal" onclick="closeModal()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="addEmailSettingForm" action="<?=base_url('Setting/store_email_setting')?>" method="POST">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6 col-lg-6 col-xl-6">
              <div class="form-group">
                <label>Email Type <span class="star-red">*</span></label>
                <input type="text" class="form-control" name="type" value="">
              </div>
            </div>

            <div class="col-md-6 col-lg-6 col-xl-6">
              <div class="form-group">
                <label>Assigin Email <span class="star-red">*</span></label>
                <select class="form-control " name="asigin_email[]" >
                  <option value="">Select User</option>
                  <?php
                  
                  foreach($users as $users){?>
                  <option value="<?=$users->id?>"><?=$users->name.'('.$users->email.')'?></option>
                  <?php } ?>

                </select>
              </div>
            </div>

            <div class="col-md-6 col-lg-6 col-xl-6">
              <div class="form-group">
                <label>Subject <span class="star-red">*</span></label>
                <input type="text" class="form-control" name="subject">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="editEmailSetting" tabindex="-1" role="dialog"
  aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-edit text-success"></i> Edit <?=$page_title?>
        </h5>
        <button type="button" class="close" data-dismiss="modal" onclick="closeModal()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="editEmailSettingForm" action="<?=base_url('Setting/update_email_setting')?>" method="POST">
        <div class="modal-body" id="email_setting_form_data">

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
  //alert('dfgfgf');
  // $('#example').DataTable();
  // } );
  var dataTable = $('#emailSettingDataTable').DataTable({
    "processing": true,
    "serverSide": true,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Download Excel'
    }],
    "order": [],
    "ajax": {
      url: "<?=base_url('Setting/ajaxEmailSetting/'.$uri)?>",
      type: "POST"
    },
    "columnDefs": [{
      "targets": [0],
      "orderable": false,
    }, ],
  });
});



$("form#addEmailSettingForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {
          location.reload();
        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});



function delete_role(id) {

  Swal.fire({
    title: 'Are you sure?',
    text: "If you delete a role users will be delete",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        url: '<?=base_url('Setting/delete_role')?>',
        type: 'POST',
        data: {
          id
        },
        dataType: 'json',
        success: function(data) {
          if (data.status == 200) {
            toastNotif({
              text: data.message,
              color: '#5bc83f',
              timeout: 5000,
              icon: 'valid'
            });
            setTimeout(function() {

              location.href = "<?=base_url('role')?>"

            }, 1000)


          } else if (data.status == 302) {
            toastNotif({
              text: data.message,
              color: '#da4848',
              timeout: 5000,
              icon: 'error'
            });

          }
        }
      })
    }
  })
}

function getroleAdminSession(id) {
  $.ajax({
    url: "<?=base_url('Ajax_controller/getroleAdminSession')?>",
    type: 'POST',
    data: {
      id
    },
    success: function(data) {
      location.reload();
    },

  });
}

function addEmailSettingModal() {
  $('#addEmailSetting').modal('show');
}

function closeModal() {
  location.reload()
}

function editEmailSettingModal(id) {
  $.ajax({
    url: "<?=base_url('Setting/editEmailSettingForm')?>",
    type: 'POST',
    data: {
      id
    },
    success: function(data) {
      $('#editEmailSetting').modal('show');
      $('#email_setting_form_data').html(data);
    },

  });
}

$("form#editEmailSettingForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {
          location.reload();
        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});
</script>