<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">

      <div class="page-header">
        <div class="row">
          <div class="col-sm-6">
            <h3 class="page-title"><?=$page_title?></h3>
            <ul class="breadcrumb">
              <li><a href="<?=base_url('dashboard')?>">Dashboard/</a></li>
              <li class="breadcrumb-item"><a href="javascript:(0);"><?=$page_title?></a></li>
            </ul>
          </div>
          <?php if($this->session->userdata('user_type')!=1 && $permission=='Add'){?>
          <div class="col-sm-6">
            <div class="float-right">
              <a type="button" class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="addRoleModal()"
                style="float: right">Create <?=$page_title?></a>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-body">
              <?php if($this->session->userdata('user_type')==1){?>
              <div class="row">
                <div class="col-sm-6">
                  <select class="form-control" name="roleAdminID" id="roleAdminID"
                    onchange="getroleAdminSession(this.value)">
                    <option value="">Select Admin</option>
                    <?php foreach($admins as $admin){?>
                    <option value="<?=$admin->id?>"
                      <?=$admin->id == $this->session->userdata('roleAdminID') ? 'selected' : ''?>><?=$admin->name?>
                    </option>
                    <?php } ?>
                  </select>

                </div>

              </div>
              <hr>
              <?php } ?>

              <div class="table-responsive">
                <table class=" table table-hover table-center mb-0" id="roleDataTable">
                  <thead>
                    <tr>
                      <th>S.no.</th>
                      <th>Action</th>
                      <th>Name</th>
                      <th>Created_at</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade bd-example-modal-lg" id="addRole" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-plus text-success"></i> Add <?=$page_title?>
        </h5>
        <button type="button" class="close" data-dismiss="modal" onclick="closeModal()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="addRoleForm" action="<?=base_url('Setting/store_role')?>" method="POST">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6 col-lg-6 col-xl-6">
              <div class="form-group">
                <label>Role Name <span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Role Name">
              </div>
            </div>

            <div class="col-md-6 col-lg-6 col-xl-6">
              <div class="form-group">
                <label>Parent Role <span class="text-danger">*</span></label>
                <select class="form-control" name="parent_role" id="parent_role">
                  <option value="0">Select Parent</option>
                  <?php foreach($parent_roles as $parent_role){?>
                  <option value="<?=$parent_role->id?>"><?=$parent_role->name?>
                  </option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="col-md-12 col-lg-12 col-xl-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Provide Permission</h4>
                </div>

                <div class="card-body">
                  <div class="table-responsive">
                    <table class=" table table-hover table-center mb-0">
                      <thead>
                        <tr>
                          <th>Menu</th>
                          <th>View</th>
                          <th>Add</th>
                          <th>Edit</th>
                          <th>Delete</th>
                          <th>Upload</th>
                          <th>Export</th>
                          <th>Download</th>
                          <th>Like Admin</th>
                          <th>Filter</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                    // print_r($menus);
                    foreach($menues as $menu){
                      if($this->session->userdata('user_type')==1){
                      
                      ?>
                        <tr>
                          <td><?=$menu['parent_menu']?></td>
                          <td><input type="checkbox" name="view[<?=$menu['id']?>]" value="View"></td>
                          <td><input type="checkbox" name="add[<?=$menu['id']?>]" value="Add"></td>
                          <td><input type="checkbox" name="edit[<?=$menu['id']?>]" value="Edit"></td>
                          <td><input type="checkbox" name="delete[<?=$menu['id']?>]" value="Delete"></td>
                          <td><input type="checkbox" name="upload[<?=$menu['id']?>]" value="Upload"></td>
                          <td><input type="checkbox" name="export[<?=$menu['id']?>]" value="Export"></td>
                          <td><input type="checkbox" name="download[<?=$menu['id']?>]" value="Download"></td>
                          <td><input type="checkbox" name="like_admin[<?=$menu['id']?>]" value="Like Admin"></td>
                          <td><input type="checkbox" name="filter[<?=$menu['id']?>]" value="Filter"></td>
                        </tr>
                        <?php  } 
                    if($menu['parent_menu']!='Setting'){
                  ?>
                        <tr>
                          <td><?=$menu['parent_menu']?></td>
                          <td><input type="checkbox" name="view[<?=$menu['id']?>]" value="View"></td>
                          <td><input type="checkbox" name="add[<?=$menu['id']?>]" value="Add"></td>
                          <td><input type="checkbox" name="edit[<?=$menu['id']?>]" value="Edit"></td>
                          <td><input type="checkbox" name="delete[<?=$menu['id']?>]" value="Delete"></td>
                          <td><input type="checkbox" name="upload[<?=$menu['id']?>]" value="Upload"></td>
                          <td><input type="checkbox" name="export[<?=$menu['id']?>]" value="Export"></td>
                          <td><input type="checkbox" name="download[<?=$menu['id']?>]" value="Download"></td>
                          <td><input type="checkbox" name="like_admin[<?=$menu['id']?>]" value="Like Admin"></td>
                          <td><input type="checkbox" name="filter[<?=$menu['id']?>]" value="Filter"></td>
                        </tr>
                        <?php 
                 } } ?>
                      </tbody>
                    </table>
                  </div>


                </div>
              </div>
            </div>

            <div class="col-md-12 col-lg-12 col-xl-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Provide Other Permission</h4>
                </div>

                <div class="card-body">
                  <div class="col-md-6 col-lg-6 col-xl-6">
                    <div class="form-group">
                      
                      <input type="checkbox" class="form-check-input" id="sale_heading_countion" name="sale_heading_countion" placeholder="Sale Heading Counting" value="1">
                      <label class="form-check-label" for="sale_heading_countion ml-5">Sale Heading Counting </label>
                    </div>
                  </div>

                  <div class="col-md-6 col-lg-6 col-xl-6">
                    <div class="form-group">
                      
                      <input type="checkbox" class="form-check-input" id="task_heading_countion" name="task_heading_countion" placeholder="Sale Heading Counting" value="1">
                      <label class="form-check-label" for="task_heading_countion ml-5">Task Heading Counting </label>
                    </div>
                  </div>

                  <div class="col-md-6 col-lg-6 col-xl-6">
                    <div class="form-group">
                      
                      <input type="checkbox" class="form-check-input" id="task_sale" name="task_sale" placeholder="Task Sale" value="1">
                      <label class="form-check-label" for="task_sale ml-5">Task Sale</label>
                    </div>
                  </div>

                  <div class="col-md-6 col-lg-6 col-xl-6">
                    <div class="form-group">
                      
                      <input type="checkbox" class="form-check-input" id="task_developer" name="task_developer" placeholder="Task For Developer" value="1">
                      <label class="form-check-label" for="task_developer ml-5">Task For Developer</label>
                    </div>
                  </div>


                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="editRole" tabindex="-1" role="dialog"
  aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-edit text-success"></i> Edit <?=$page_title?>
        </h5>
        <button type="button" class="close" data-dismiss="modal" onclick="closeModal()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="editRoleForm" action="<?=base_url('Setting/update_role')?>" method="POST">
        <div class="modal-body" id="role_form_data">

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
  //alert('dfgfgf');
  // $('#example').DataTable();
  // } );
  var dataTable = $('#roleDataTable').DataTable({
    "processing": true,
    "serverSide": true,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Download Excel'
    }],
    "order": [],
    "ajax": {
      url: "<?=base_url('Setting/ajaxRole/'.$uri)?>",
      type: "POST"
    },
    "columnDefs": [{
      "targets": [0],
      "orderable": false,
    }, ],
  });
});



$("form#addRoleForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {
          location.reload();
        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});



function delete_role(id) {

  Swal.fire({
    title: 'Are you sure?',
    text: "If you delete a role users will be delete",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        url: '<?=base_url('Setting/delete_role')?>',
        type: 'POST',
        data: {
          id
        },
        dataType: 'json',
        success: function(data) {
          if (data.status == 200) {
            toastNotif({
              text: data.message,
              color: '#5bc83f',
              timeout: 5000,
              icon: 'valid'
            });
            setTimeout(function() {

              location.href = "<?=base_url('role')?>"

            }, 1000)


          } else if (data.status == 302) {
            toastNotif({
              text: data.message,
              color: '#da4848',
              timeout: 5000,
              icon: 'error'
            });

          }
        }
      })
    }
  })
}

function getroleAdminSession(id) {
  $.ajax({
    url: "<?=base_url('Ajax_controller/getroleAdminSession')?>",
    type: 'POST',
    data: {
      id
    },
    success: function(data) {
      location.reload();
    },

  });
}

function addRoleModal() {
  $('#addRole').modal('show');
}

function closeModal() {
  location.reload()
}

function editRoleModal(id) {
  $.ajax({
    url: "<?=base_url('Setting/editRoleForm')?>",
    type: 'POST',
    data: {
      id
    },
    success: function(data) {
      $('#editRole').modal('show');
      $('#role_form_data').html(data);
    },

  });
}

$("form#editRoleForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {
          location.reload();
        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});


</script>