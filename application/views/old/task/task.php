<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">

      <div class="page-header">
        <div class="row">
          <div class="col-sm-6">
            <h3 class="page-title"><?=$page_title?></h3>
            <ul class="breadcrumb">
              <li><a href="<?=base_url('dashboard')?>">Dashboard/</a></li>
              <li class="breadcrumb-item"><a href="javascript:(0);"><?=$page_title?></a></li>
            </ul>
          </div>
          <?php if($this->session->userdata('user_type')!=1 && $permission[1]=='Add'){?>
          <!-- <div class="col-sm-6">
            <div class="float-right">
              <a type="button" class="btn btn-primary btn-sm" href="<?//=base_url('create-task')?>"
                style="float: right">Create <?//=$page_title?></a>
            </div>
          </div> -->
          <?php } ?>
        </div>
      </div>
      <?php if($this->session->userdata('user_type')!=1 && $permission[8]=='Filter'){?>
      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-body">
              <form method="post" id="filter-data" action="<?=base_url('Task/setSession');?>">
                <div class="row">

                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <div class="reset_btn">
                      <label>Task Status</label>
                      <?php $task_status = array('Not Started','Work In Progress','Completed');?>
                      <select id="task_status" class="form-control" name="task_status" onchange="this.form.submit();">
                        <option value="">Select Task Status</option>
                        <?php foreach($task_status as $task_status){?>
                        <option value="<?=$task_status?>"
                          <?=($this->session->userdata('task_status')==$task_status)?'selected':'';?>>
                          <?=$task_status?></option>
                        <?php } ?>
                      </select>
                      <span class="close_button" onclick="resetTaskStatus()" id="resetTaskStatus"
                        <?=empty($this->session->userdata('task_status')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>

                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <div class="reset_btn">
                      <label>Months</label>
                      <?php $months = array('0'=>'All Months','01'=>'January','02'=>'February','03'=>'March','04'=>'April','05'=>'May','06'=>'June','07'=>'July','08'=>'August','09'=>'September','10'=>'October','11'=>'November','12'=>'December');?>
                      <select id="taskMonthName" class="form-control" name="taskMonthName"
                        onchange="this.form.submit();">
                        <?php foreach($months as $month_key=>$month){?>
                        <option value="<?=$month_key?>"
                          <?=(!empty($this->session->userdata('taskMonthName')) AND $this->session->userdata('taskMonthName')==$month_key) ? 'selected' :  '' ?>>
                          <?=$month?></option>
                        <?php } ?>
                      </select>
                      <span class="close_button" onclick="resetMonthName()" id="resetMonthName"
                        <?=empty($this->session->userdata('taskMonthName')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>

                  <?php //if($permission[7] == 'Like Admin' || $this->session->userdata('user_type')==2){?>
                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <div class="reset_btn">
                      <label>Task Assigin By</label>
                      <select id="task_assign_user" class="form-control" name="task_assign_user"
                        onchange="this.form.submit();">
                        <option value="">All Users</option>
                        <?php foreach($users as $user){?>
                        <option value="<?=$user->id?>"
                          <?=$this->session->userdata('task_assign_user')==$user->id ? 'selected':'' ;?>>
                          <?=$user->name.'('.$user->roleName.')'?></option>
                        <?php } ?>
                      </select>
                      <span class="close_button" onclick="resetTaskAssignUser()" id="resetTaskAssignUser"
                        <?=empty($this->session->userdata('task_assign_user')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>
                  <?php //} ?>


                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <div class="reset_btn">
                      <label>From Date</label>
                      <div class="input-group">
                        <input type="text" class="form-control" onfocus="(this.type='date')" placeholder="From Date"
                          name="task_from_date" id="task_from_date"
                          value="<?=!empty($this->session->userdata('task_from_date')) ? $this->session->userdata('task_from_date'):'';?>"
                          onchange="this.form.submit();">
                      </div>
                      <span class="close_button" onclick="resetTaskFromDate()" id="resetTaskFromDate"
                        <?=empty($this->session->userdata('task_from_date')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>
                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <div class="reset_btn">
                      <label>To Date</label>
                      <div class="input-group">
                        <input type="text" class="form-control" onfocus="(this.type='date')" placeholder="To Date"
                          name="task_to_date" id="task_to_date"
                          value="<?=!empty($this->session->userdata('task_to_date'))? $this->session->userdata('task_to_date') :'';?>"
                          onchange="this.form.submit();">
                      </div>
                      <span class="close_button" onclick="resetTaskToDate()" id="resetTaskToDate"
                        <?=empty($this->session->userdata('task_to_date')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>
                </div>
              </form>
              <hr>
              <?php } ?>
              <?php if($this->session->userdata('user_type')==1){?>
              <div class="row">
                <div class="col-sm-6">
                  <select class="form-control" name="saleAdminID" id="saleAdminID"
                    onchange="getSaleAdminSession(this.value)">
                    <option value="">Select Admin</option>
                    <?php foreach($admins as $admin){?>
                    <option value="<?=$admin->id?>"
                      <?=$admin->id == $this->session->userdata('saleAdminID') ? 'selected' : ''?>><?=$admin->name?>
                    </option>
                    <?php } ?>
                  </select>

                </div>

              </div>
              <hr>
              <?php } ?>


              <div class="table-responsive">
                <table class=" table table-hover table-center mb-0" id="taskDataTable">
                  <thead>
                    <tr>
                      <th>S.no.</th>
                      <th>Action</th>
                      <th>Assign To Name</th>
                      <th>Assign By Name</th>
                      <th>Product Name</th>
                      <th>Priority</th>
                      <th>Work Status</th>
                      <th>Assign Date</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="viewTaskReportForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-file text-success"></i> Daily Task Report
        </h5>
        <button type="button" class="close " onclick="close_modal()" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('Task/store_task_report')?>" id="taskReportForm">
        <div class="modal-body" id="view_report_form">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn  btn-outline-danger " onclick="close_modal()" data-dismiss="modal"
            aria-label="Close">Close</button>
          <button type="submit" class="btn  btn-outline-success">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>


<div class="modal fade" id="viewTaskForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-file text-success"></i> View Task
        </h5>
        <button type="button" class="close " onclick="close_modal()" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('Task/approve_disapprove_task')?>" id="taskApproveDisapproveForm">
        <div class="modal-body" id="view_task_form">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn  btn-outline-danger " onclick="close_modal()" data-dismiss="modal"
            aria-label="Close">Close</button>
          <button type="submit" class="btn  btn-outline-success">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>


<div class="modal fade" id="approve_disapprove_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-file text-success"></i> Approve or Disapprove
          Task
        </h5>
        <button type="button" class="close " onclick="close_modal()" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('Task/approve_disapprove_task')?>" id="taskApproveDisapproveForm">
        <div class="modal-body" id="approve_disapprove_task_form">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn  btn-outline-danger " onclick="close_modal()" data-dismiss="modal"
            aria-label="Close">Close</button>
          <button type="submit" class="btn  btn-outline-success">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>


<script>
$(document).ready(function() {
  //alert('dfgfgf');
  // $('#example').DataTable();
  // } );
  var dataTable = $('#taskDataTable').DataTable({
    "processing": true,
    "serverSide": true,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Download Excel'
    }],
    "order": [],
    "ajax": {
      url: "<?=base_url('Task/ajaxTask/'.$uri)?>",
      type: "POST"
    },
    "columnDefs": [{
      "targets": [0],
      "orderable": true,
    }, ],
  });
});


$("form#taskReportForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 10000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        ajaxSendMail(data.id);
        // setTimeout(function() {
        //   url = "<?//=base_url('task')?>";
        //   location.href = url;
        // }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});


function ajaxSendMail(id) {
  $.ajax({
    url: "<?=base_url('Send_mail/workReportSendEmail')?>",
    type: 'POST',
    data: {
      id
    },
    success: function(data) {
      toastNotif({
        text: 'Mail sent successfully',
        color: '#5bc83f',
        timeout: 10000,
        icon: 'valid'
      });
      setTimeout(function() {
        url = "<?=base_url('task')?>";
        location.href = url;
      }, 1000)
    },
  });
}


$("form#taskApproveDisapproveForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 100000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        ajaxSendMail(data.taskID, data.assign_to, data.task_status)
        // setTimeout(function() {
        //   url = "<?//=base_url('task')?>";
        //   location.href = url;
        // }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

function ajaxSendMail(taskID, assign_to, task_status) {
  $.ajax({
    url: "<?=base_url('Send_mail/taskUpdateSendMail')?>",
    type: 'POST',
    data: {
      taskID,
      assign_to,
      task_status
    },
    success: function(data) {
      toastNotif({
        text: 'Mail sent successfully',
        color: '#5bc83f',
        timeout: 10000,
        icon: 'valid'
      });
      setTimeout(function() {
        url = "<?=base_url('task')?>";
        location.href = url;
      }, 1000)
    },
  });
}


function close_modal() {
  $('#viewTaskReportForm').modal('hide');
  $('#viewTaskForm').modal('hide');
}


function daily_task_report(id) {
  $.ajax({
    url: '<?=base_url('Task/daily_task_report')?>',
    type: 'post',
    data: {
      id
    },
    dataType: 'html',
    success: function(response) {
      $('#viewTaskReportForm').modal('show');
      $('#view_report_form').html(response);
      togglePay();
    }
  });
}

function approve_or_disapprove_task(id) {
  $.ajax({
    url: '<?=base_url('Task/approve_or_disapprove_task_form')?>',
    type: 'post',
    data: {
      id
    },
    dataType: 'html',
    success: function(response) {
      $('#approve_disapprove_modal').modal('show');
      $('#approve_disapprove_task_form').html(response);
      togglePay();
    }
  });
}


function check_task_status(val) {

  if (val == 2) {
    $('#reason_div').show();

  } else {
    $('#reason_div').hide();
  }

}


function resetTaskStatus() {
  $.ajax({
    url: '<?=base_url("Task/resetTaskStatus")?>',
    type: 'POST',
    data: {
      ResetSesession: 'ResetSesession'
    },
    success: function(data) {
      location.reload();
    },
  });
}

function resetTaskFromDate() {
  $.ajax({
    url: '<?=base_url("Task/resetTaskFromDate")?>',
    type: 'POST',
    data: {
      ResetSesession: 'ResetSesession'
    },
    success: function(data) {
      location.reload();
    },
  });
}

function resetTaskToDate() {
  $.ajax({
    url: '<?=base_url("Task/resetTaskToDate")?>',
    type: 'POST',
    data: {
      ResetSesession: 'ResetSesession'
    },
    success: function(data) {
      location.reload();
    },
  });
}

function resetMonthName() {
  $.ajax({
    url: '<?=base_url("Task/resetMonthName")?>',
    type: 'POST',
    data: {
      ResetSesession: 'ResetSesession'
    },
    success: function(data) {
      location.reload();
    },
  });
}

function resetTaskAssignUser() {
  $.ajax({
    url: '<?=base_url("Task/resetTaskAssignUser")?>',
    type: 'POST',
    data: {
      ResetSesession: 'ResetSesession'
    },
    success: function(data) {
      location.reload();
    },
  });
}

function get_user(id, assign_to) {
  $.ajax({
    url: '<?=base_url('Ajax_controller/get_users')?>',
    type: 'post',
    data: {
      id,
      assign_to
    },
    dataType: 'html',
    success: function(response) {
      $('#user').html(response);

    }
  });
}




function get_work_percentage(val) {
  var work = $('#work').val();

  if (val != '') {
    var percentage = (val * 100) / work;
    $('#complete_work').val(work - percentage);
  } else {

  }
}
</script>

<script>
function delete_sale(id) {

  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        url: '<?=base_url('Sale/delete')?>',
        type: 'POST',
        data: {
          id
        },
        dataType: 'json',
        success: function(data) {
          if (data.status == 200) {
            toastNotif({
              text: data.message,
              color: '#5bc83f',
              timeout: 5000,
              icon: 'valid'
            });
            setTimeout(function() {

              location.href = "<?=base_url('sales')?>"

            }, 1000)


          } else if (data.status == 302) {
            toastNotif({
              text: data.message,
              color: '#da4848',
              timeout: 5000,
              icon: 'error'
            });

          }
        }
      })
    }
  })
}
</script>