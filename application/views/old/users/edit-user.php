<div class="page-wrapper">
  <div class="content container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col">
          <h3 class="page-title">Create <?=$page_title?></h3>
          <ul class="breadcrumb">
            <li class=""><a href="<?=base_url('dashboard')?>">Dashboard</a></li>/
            <li class="breadcrumb-item active">Create <?=$page_title?></li>
          </ul>
        </div>
      </div>
    </div>
    <form action="<?=base_url('User/update')?>" id="userEditForm" method="post">
      <div class="row">
        <div class="col-md-6 col-lg-6 col-xl-6">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">Enter Basic Detail</h4>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label>Full Name <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="name" id="name" value="<?=$user->name?>">
              </div>



              <div class="form-group">
                <label>Email Address <span class="text-danger">*</span></label>
                <input type="email" class="form-control" name="email" id="email" value="<?=$user->email?>">
              </div>


              <div class="form-group">
                <label>Contact No. <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="phone" id="phone" minlength="10" maxlength="10"
                  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
                  value="<?=$user->phone?>">
              </div>

              <div class="form-group">
                <label>Alternative No. </label>
                <input type="text" class="form-control" name="alternative_no" id="alternative_no" minlength="10"
                  maxlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
                  value="<?=$user->user_alternative_no?>">
              </div>

              <div class="form-group">
                <label>User Role <span class="text-danger">*</span></label>
                <select class="form-control" name="user_type" id="user_type">

                  <?php foreach($roles as $role){?>
                  <option value="<?=$role->id?>" <?=$user->user_type== $role->id ? 'selected' : ''?>><?=$role->name?>
                  </option>
                  <?php } ?>
                </select>
              </div>



              <div class="form-group">
                <label>Gender <span class="text-danger">*</span></label><br>
                <?php $genders = array('Male','Female');
                  foreach($genders as $gender){
                  ?>
                <span><input type="radio" class="" name="gender" value="<?=$gender?>"
                    <?=$gender==$user->gender ? 'checked' : ''?>> <?=$gender?></span>
                <?php
                 }
                ?>
                <!-- <span><input type="radio" class="" name="gender" value="Male" checked> Male</span>
                <span class="ml-5"><input type="radio" class="" name="gender" value="Female"> Female</span> -->
              </div>


            </div>
          </div>
        </div>

        <div class="col-md-6 col-lg-6 col-xl-6">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">Enter Location</h4>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label>Address <span class="text-danger">*</span></label>
                <textarea class="form-control" name="address" id="address"><?=$user->address?></textarea>
              </div>
              <div class="form-group">
                <label>State <span class="text-danger">*</span></label>
                <select class="form-control js-example-basic-single" name="state" id="state"
                  onchange="get_city(this.value)">
                  <option value="">Select State</option>
                  <?php foreach($states as $state){?>
                  <option value="<?=$state->id?>" <?=$state->id==$user->state ? 'selected' : ''?>><?=$state->name?>
                  </option>
                  <?php } ?>
                </select>
              </div>

              <div class="form-group">
                <label>City <span class="text-danger">*</span></label>
                <select class="form-control city js-example-basic-single" name="city" id="city">

                  <option value="">Select City</option>

                </select>
              </div>
              <div class="form-group">
                <label>Pincode <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="pincode" id="pincode" minlength="6" maxlength="6"
                  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
                  value="<?=$user->pincode?>">
              </div>

              <!-- <div class="table-responsive">
                <table class=" table table-hover table-center mb-0">
                  <thead>
                    <tr>
                      <th>Menu</th>
                      <th>View</th>
                      <th>Add</th>
                      <th>Edit</th>
                      <th>Delete</th>
                      <th>Upload</th>
                      <th>Export</th>
                      <th>Download</th>
                      <th>Like Admin</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    // print_r($menus);
                    foreach($menues as $menu){
                      $menuID = $menu['id'];
                      $permission = $user->permission;
                      $permission = json_decode($permission)->$menuID;
                      if($menu['parent_menu']!='Setting'){
                      ?>
                    <tr>
                      <td><?=$menu['parent_menu']?></td>
                      <td><input type="checkbox" name="view[<?=$menu['id']?>]" <?=in_array('View',$permission) ? 'checked' : ''?> value="View"></td>
                      <td><input type="checkbox" name="add[<?=$menu['id']?>]" <?=in_array('Add',$permission) ? 'checked' : ''?> value="Add"></td>
                      <td><input type="checkbox" name="edit[<?=$menu['id']?>]" <?=in_array('Edit',$permission) ? 'checked' : ''?> value="Edit"></td>
                      <td><input type="checkbox" name="delete[<?=$menu['id']?>]" <?=in_array('Delete',$permission) ? 'checked' : ''?> value="Delete"></td>
                      <td><input type="checkbox" name="upload[<?=$menu['id']?>]" <?=in_array('Upload',$permission) ? 'checked' : ''?> value="Upload"></td>
                      <td><input type="checkbox" name="export[<?=$menu['id']?>]" <?=in_array('Export',$permission) ? 'checked' : ''?> value="Export"></td>
                      <td><input type="checkbox" name="download[<?=$menu['id']?>]" <?=in_array('Download',$permission) ? 'checked' : ''?> value="Download"></td>
                      <td><input type="checkbox" name="like_admin[<?=$menu['id']?>]" <?=in_array('Like-admin',$permission) ? 'checked' : ''?> value="Like-admin"></td>
                      <td><input type="checkbox" name="like_manager[<?=$menu['id']?>]" <?=in_array('Like-Manager',$permission) ? 'checked' : ''?> value="Like-Manager"></td>
                      
                    </tr>
                    <?php } } ?>
                  </tbody>
                </table>
              </div> -->
              <div class="text-end">
                <button type="submit" class="btn btn-primary">Edit</button>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
  </form>
</div>
</div>
</div>
<script>
$("form#userEditForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("id", '<?=$user->id?>');
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('edit-user/'.base64_encode($user->id))?>";

        }, 1000)

      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

$(document).ready(function() {
  get_city(<?=$user->state?>, <?=$user->id?>);
});
</script>