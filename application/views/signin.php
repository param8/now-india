<body>
  <!-- Content -->

  <div class="container-xxl">
    <div class="authentication-wrapper authentication-basic container-p-y">
      <div class="authentication-inner py-4">
        <!-- Login -->
        <div class="card">
          <div class="card-body">
            <!-- Logo -->
            <div class="app-brand justify-content-center mb-4 mt-2">
              <a href="index.html" class="app-brand-link gap-2">
              <img class="img-fluid" src="<?=base_url($siteinfo->site_logo)?>" alt="Logo">
              </a>
            </div>
            <!-- /Logo -->
            <h4 class="mb-1 pt-2">Access to our dashboard 👋</h4>
            <!-- <p class="mb-4">Please sign-in to your account and start the adventure</p> -->

            <form action="<?=base_url('Authantication/login')?>" id="adminLoginForm" method="post"  class="mb-3">
              <div class="mb-3">
                <label for="email" class="form-label">Email or Username</label>
                <input type="text" class="form-control" id="email" name="email"
                  placeholder="Enter your email or username" autofocus />
              </div>
              <div class="mb-3 form-password-toggle">
                <div class="d-flex justify-content-between">
                  <label class="form-label" for="password">Password</label>
                  <a href="#">
                    <small>Forgot Password?</small>
                  </a>
                </div>
                <div class="input-group input-group-merge">
                  <input type="password" id="password" class="form-control" name="password"
                    placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                    aria-describedby="password" />
                  <span class="input-group-text cursor-pointer"><i class="ti ti-eye-off"></i></span>
                </div>
              </div>
              <!-- <div class="mb-3">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" id="remember-me" />
                  <label class="form-check-label" for="remember-me"> Remember Me </label>
                </div>
              </div> -->
              <div class="mb-3">
                <button class="btn btn-primary d-grid w-100" type="submit">Sign in</button>
              </div>
            </form>

            <!-- <p class="text-center">
              <span>New on our platform?</span>
              <a href="auth-register-basic.html">
                <span>Create an account</span>
              </a>
            </p>

            <div class="divider my-4">
              <div class="divider-text">or</div>
            </div> -->

            <!-- <div class="d-flex justify-content-center">
              <a href="javascript:;" class="btn btn-icon btn-label-facebook me-3">
                <i class="tf-icons fa-brands fa-facebook-f fs-5"></i>
              </a>

              <a href="javascript:;" class="btn btn-icon btn-label-google-plus me-3">
                <i class="tf-icons fa-brands fa-google fs-5"></i>
              </a>

              <a href="javascript:;" class="btn btn-icon btn-label-twitter">
                <i class="tf-icons fa-brands fa-twitter fs-5"></i>
              </a>
            </div> -->
          </div>
        </div>
        <!-- /Register -->
      </div>
    </div>
  </div>

    <script>
    $("form#adminLoginForm").submit(function(e) {
      $(':input[type="submit"]').prop('disabled', true);
      e.preventDefault();
      var formData = new FormData(this);
      $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(data) {
          if (data.status == 200) {
            toastNotif({
              text: data.message,
              color: '#5bc83f',
              timeout: 5000,
              icon: 'valid'
            });
            $(':input[type="submit"]').prop('disabled', false);
            setTimeout(function() {
              //  console.log(data.user_type);
              if (data.user_type == 'Student') {
                if (data.verifivation_status == 1) {
                  location.href = "<?=base_url('student-dashboard')?>";
                } else {
                  location.href = "<?=base_url('verification')?>";
                }
              } else if (data.user_type == 'Faculties') {
                location.href = "<?=base_url('faculty-dashboard')?>";
              } else {
                location.href = "<?=base_url('dashboard')?>";
              }

            }, 1000)

          } else if (data.status == 403) {
            toastNotif({
              text: data.message,
              color: '#da4848',
              timeout: 5000,
              icon: 'error'
            });

            $(':input[type="submit"]').prop('disabled', false);
          } else {
            toastNotif({
              text: data.message,
              color: '#da4848',
              timeout: 5000,
              icon: 'error'
            });
            $(':input[type="submit"]').prop('disabled', false);
          }
        },
        error: function() {}
      });
    });
    </script>